# README #

### What is this repository for? ###

* Farm Africa Data Management
* Version 1.1

[![Build Status](https://semaphoreci.com/api/v1/buluma/farm_africa/branches/master/shields_badge.svg)](https://semaphoreci.com/buluma/farm_africa)

#### Project Upload Status ####
[![buddy pipeline](https://app.buddy.works/buluma-1/farm-africa/pipelines/pipeline/62850/badge.svg?token=287807e6acbd363877fd8d9769eb71e57cb3fa02d4a93985fbf4730386050f36 "buddy pipeline")](https://app.buddy.works/buluma-1/farm-africa/pipelines/pipeline/62850)

### How do I get set up? ###

* Clone Repo
* Work on local branch
* export full database with drop table option selected
* upload db in db_dump as filename.sql (you may want to delete the previous filename.sql file)
* sync repo with bitbucket
* confirm site is working correctly: http://gbc.me.ke/farmafrica_wip/

-------------------------------------------------------------------------
					DONT'S
-------------------------------------------------------------------------

1. Do not commit your configuration.php

### Who do I talk to? ###

* Michael Buluma
* Stevie