<?php
require_once 'library/config.php';
require_once 'library/functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '';

switch ($view) {
	case 'adduser' :
		$content 	= 'user/add.php';		
		$pageTitle 	= 'Farm Africa - Add Users';
		break;
    
    case 'addfarm' :
		$content 	= 'farm/add.php';		
		$pageTitle 	= 'Farm Africa - Add Farm';
		break;

	case 'addpond' :
		$content 	= 'farm/pond.php';		
		$pageTitle 	= 'Farm Africa - Add Pond';
		break;

	case 'addpond1' :
		$content 	= 'farm/pond2.php';		
		$pageTitle 	= 'Farm Africa - Add Pond';
		break;

    case 'extfarminfo' :
		$content 	= 'farm/ext_farm_info.php';		
		$pageTitle 	= 'Farm Africa - Extended Farm info';
		break;

	case 'contactperson' :
		$content 	= 'farm/contact_person.php';		
		$pageTitle 	= 'Farm Africa - Contact Person info';
		break;

	case 'farmstaff' :
		$content 	= 'farm/farm_staff.php';		
		$pageTitle 	= 'Farm Africa -Staff info';
		break;
		
	case 'prodcycle' :
		$content 	= 'farm/production_cycle.php';		
		$pageTitle 	= 'Farm Africa - Production cycle';
		break;

	case 'assesments' :
		$content 	= 'farm/farm_assesments.php';		
		$pageTitle 	= 'Farm Africa - Production cycle';
		break;

	case 'addfarmer' :
		$content 	= 'farmer/add.php';		
		$pageTitle 	= 'Farm Africa - Enrol Farmer';
		break;
   
    case 'addfarminfo' :
		$content 	= 'farm/farm_info.php';		
		$pageTitle 	= 'Farm Africa - Farm Cost/Sales/Harvest info';
		break;
		
	case 'operatingcost' :
		$content 	= 'farm/addoperatingcost.php';		
		$pageTitle 	= 'Farm Africa - Operating Cost';
		break;

	case 'recurrentcost' :
		$content 	= 'farm/addrecurrentcost.php';		
		$pageTitle 	= 'Farm Africa - Recurrent Cost';
		break;

	case 'addsales' :
		$content 	= 'farm/addsales.php';		
		$pageTitle 	= 'Farm Africa - Add Sales';
		break;

	case 'addstock' :
		$content 	= 'farm/addstock.php';		
		$pageTitle 	= 'Farm Africa - Add Stock';
		break;

	case 'addsample' :
		$content 	= 'farm/addsample.php';		
		$pageTitle 	= 'Farm Africa - Add sample';
		break;

	case 'addharvest' :
		$content 	= 'farm/addharvest.php';		
		$pageTitle 	= 'Farm Africa - Add Harvest';
		break;
	

	default :
		$content 	= 'list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
}

$script    = array('user.js','farm.js','farmer.js','training.js');

require_once 'template.php';

?>