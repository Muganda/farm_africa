<?php
$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];

$years=getYears();
$counties=getCountiest();
$fishinfo=getfishstocked();
//$graphdata=farmerbargraph();
$colspan="2";
$rowspan="2";
function clean($string) {
    $string = trim($string);
    $string = str_replace(' ', '', $string);
    $string = str_replace('-', '', $string);
    $str1 = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    $tolower = strtolower($str1);
    return $tolower;
}
?>

 <div class="wrapper wrapper-content">
        
        <div class="row">
          <div class="col-lg-9">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div>
                  <span class="pull-right text-right"><h3 class="font-bold no-margins">Training attendance per module</h3>
                    <br/></span>
                  <h3 class="font-bold no-margins">Farmer per county</h3>
                  
                </div>

                <div class="m-t-sm">

                  <div class="row">
                    <div class="col-md-6">
                      <div id="charty">
                        <script type="text/javascript">
                          $(document).ready(function() {
                              var options = {
                                  chart: {
                                      renderTo: 'container',
                                      type: 'bar'
                                  },
                                  title: {
                                      text: '',
                                      x: -20 //center
                                  },
                                  subtitle: {
                                      text: '',
                                      x: -20
                                  },
                                  xAxis: {
                                      categories: []
                                  },
                                  yAxis: {
                                      title: {
                                          text: 'Farmers'
                                      },
                                      plotLines: [{
                                              value: 0,
                                              width: 1,
                                              color: '#808080'
                                          }]
                                  },
                                  tooltip: {
                                      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                      pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                                  },
                                  plotOptions: {
                                      series: {
                                          borderWidth: 0,
                                          dataLabels: {
                                              enabled: true,
                                              format: '{point.y}'
                                          }
                                      }
                                  },
                                  legend: {
                                      layout: 'vertical',
                                      align: 'right',
                                      verticalAlign: 'top',
                                      x: -40,
                                      y: 100,
                                      floating: true,
                                      borderWidth: 1,
                                      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                                      shadow: true
                                  },
                                  series: []
                              };
                              $.getJSON("library/common.php?f=bargraph", function(json) {
                                  options.xAxis.categories = json[0]['data']; //xAxis: {categories: []}
                                  options.series[0] = json[1];
                                  chart = new Highcharts.Chart(options);
                              });
                          });
                      </script>
                      <div id="container">
            
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <script type="text/javascript">
                          $(document).ready(function() {
                              var options = {
                                  chart: {
                                      renderTo: 'farmer',
                                      type: 'bar'
                                  },
                                  title: {
                                      text: '',
                                      x: -20 //center
                                  },
                                  
                                  xAxis: {
                                      categories: []
                                  },
                                  yAxis: {
                                      title: {
                                          text: 'Farmers'
                                      },
                                      plotLines: [{
                                              value: 0,
                                              width: 1,
                                              color: '#808080'
                                          }]
                                  },
                                  tooltip: {
                                      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                      pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                                  },
                                  plotOptions: {
                                      series: {
                                          borderWidth: 0,
                                          dataLabels: {
                                              enabled: true,
                                              format: '{point.y}'
                                          }
                                      }
                                  },
                                  legend: {
                                      layout: 'vertical',
                                      align: 'right',
                                      verticalAlign: 'top',
                                      x: -40,
                                      y: 100,
                                      floating: true,
                                      borderWidth: 1,
                                      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                                      shadow: true
                                  },
                                  series: []
                              };
                              $.getJSON("library/common.php?f=training", function(json) {
                                  options.xAxis.categories = json[0]['data']; //xAxis: {categories: []}
                                  options.series[0] = json[1];
                                  chart = new Highcharts.Chart(options);
                              });
                          });
                      </script>
                      <div id="farmer">
            
                        </div> 
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  
                    <div class="col-md-12">
                     <div class="table-responsive " style="height: 50%">
                    <table id="" class="table table-striped table-bordered table-sm"  style="width: 90%">
                      <thead >
                      <tr>
                          <th>Counties</th>
                          <th> Ponds</th>
                      </tr>
                    </thead>
                     <tbody >
                    <?php foreach ($counties as $key=> $county):?>
                    <tr >
                     <th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>
                    <td>
                      <?php
                          $pondsinfo =getpondsincounty();
                            foreach ($pondsinfo as $key => $pond) {
                              if ($pond['farm_county'] == $county['id']) {
                                  echo $pond['no_total'];
                              }
                            }     
                        ?>
                    </td>
                      </tr>
                     <?php endforeach; ?>
                    </table>
                  </div>
                  </div>
                  </div>
                
              </div>
              
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <div class="ibox-tools">
                  <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">Config option 1</a>
                    </li>
                    <li><a href="#">Config option 2</a>
                    </li>
                  </ul>
                  <a class="close-link hidden">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </div>
              <div class="ibox-content">
                
                <div class="table-responsive">
                  <table id="good" class="table table-striped table-bordered table-sm"  style="width: 60%; ">
                    <thead >
                      <tr>
                          <th></th>
                              <?php foreach ($years as $key => $year): ?>
                          <th colspan="2"> <?php echo $year ?></th><?php endforeach; ?>
                      </tr>
                      <tr>
                          <td >County</td>
                            <?php foreach ($years as $key => $year): ?>
                              <td >Tilapia</td>
                              <td >Catfish</td>
                            <?php endforeach; ?>
                      </tr>
     
                    </thead>
                  <tbody >
                    <?php foreach ($counties as $key=> $county):?>
                    <tr >
                     <th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>
         
                   <?php foreach ($years as $key => $year): ?>
                    <td>
                      <?php
                     $fishstock =getfishstocked(); 
                     foreach ($fishstock as $key => $stock) {
                              if ($stock['farm_county'] == $county['id']&& $stock['t_stck_year'] == $year) {
                                  echo $stock['tilapia_fingerling_stocked'];
                              }
                            }      
                     ?>
                  </td>
                  <td > 
                  <?php
                     
                     foreach ($fishstock as $key => $stock) {
                              if ($stock['farm_county'] == $county['id']&& $stock['c_stck_year'] == $year) {
                                  echo $stock['catfish_fingerling_stocked'];
                              }
                            }      
                     ?>
                 </td> 
                          
                   <?php endforeach; ?>
              

                      </tr>
                     <?php endforeach; ?>
                     </tbody>
                    
                  </table>
                </div>

              </div>
            </div>
          </div>

        </div>


      </div>

