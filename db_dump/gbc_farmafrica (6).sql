-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 21, 2017 at 05:20 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gbc_farmafrica`
--

-- --------------------------------------------------------

--
-- Table structure for table `farm_costs`
--

DROP TABLE IF EXISTS `farm_costs`;
CREATE TABLE `farm_costs` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) DEFAULT NULL COMMENT 'costs incurred during production cycle ',
  `cost_type` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `date_created` varchar(45) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farm_costs`
--

INSERT INTO `farm_costs` (`id`, `farm_id`, `cost_type`, `amount`, `date_created`, `year`, `month`, `modified_by`, `modified_date`) VALUES
(1, 1, 1, 1000, '2017', 2016, 6, 0, '2017'),
(2, 1, 1, 1002, '2016', 2017, 5, 0, '2016');

-- --------------------------------------------------------

--
-- Table structure for table `farm_sales`
--

DROP TABLE IF EXISTS `farm_sales`;
CREATE TABLE `farm_sales` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) DEFAULT NULL,
  `whole_fish_sold` int(11) DEFAULT NULL,
  `whole_fish_avg_price_kg` int(11) DEFAULT NULL,
  `value_added_fish_sold` int(11) DEFAULT NULL,
  `value_added_avg_price_kg` int(11) DEFAULT NULL,
  `fish_type` int(11) DEFAULT NULL,
  `date_created` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farm_sales`
--

INSERT INTO `farm_sales` (`id`, `farm_id`, `whole_fish_sold`, `whole_fish_avg_price_kg`, `value_added_fish_sold`, `value_added_avg_price_kg`, `fish_type`, `date_created`, `modified_by`, `date_modified`) VALUES
(1, 1, 12, 122, 23, 23, 1, '0000-00-00', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `harvest_information`
--

DROP TABLE IF EXISTS `harvest_information`;
CREATE TABLE `harvest_information` (
  `id` int(11) NOT NULL,
  `havest_since_last_visit` int(11) DEFAULT NULL,
  `farm_id` int(11) DEFAULT NULL,
  `pond_number` int(11) DEFAULT NULL,
  `harvest_date` date DEFAULT NULL,
  `harvest_type` int(11) DEFAULT NULL,
  `pieces_harvested` int(11) DEFAULT NULL,
  `avg_weight_piece` varchar(45) DEFAULT NULL COMMENT 'Kgs of feed fed (from stocking to last sample date)',
  `total_weight_kg` int(11) DEFAULT NULL COMMENT 'FCR (weight/ feed)',
  `feed_type` int(11) DEFAULT NULL,
  `production_cycle` varchar(45) DEFAULT NULL,
  `fcr` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `harvest_information`
--

INSERT INTO `harvest_information` (`id`, `havest_since_last_visit`, `farm_id`, `pond_number`, `harvest_date`, `harvest_type`, `pieces_harvested`, `avg_weight_piece`, `total_weight_kg`, `feed_type`, `production_cycle`, `fcr`, `date_created`, `modified_by`, `modified_date`) VALUES
(1, 123, 1, 1, '2017-09-03', 1, 2, '23', 33, 2, 'none', 'FCR', '2017-09-27 00:00:00', 2, '2017-09-20');

-- --------------------------------------------------------

--
-- Table structure for table `sampling_feeding`
--

DROP TABLE IF EXISTS `sampling_feeding`;
CREATE TABLE `sampling_feeding` (
  `id` int(11) NOT NULL,
  `pond_number` varchar(45) DEFAULT NULL,
  `remaining_pieces` varchar(45) DEFAULT NULL,
  `last_sample_date` date DEFAULT NULL,
  `sample_weight` int(11) DEFAULT NULL,
  `main_feed_type` int(11) DEFAULT NULL,
  `kgs_feed` varchar(45) DEFAULT NULL COMMENT 'Kgs of feed fed (from stocking to last sample date)',
  `fcr` int(11) DEFAULT NULL COMMENT 'FCR (weight/ feed)',
  `date_created` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sampling_feeding`
--

INSERT INTO `sampling_feeding` (`id`, `pond_number`, `remaining_pieces`, `last_sample_date`, `sample_weight`, `main_feed_type`, `kgs_feed`, `fcr`, `date_created`, `modified_by`, `modified_date`) VALUES
(1, '1', '12', '2017-09-19', 12, 12, '2', 22, '2017-09-03', 2, '2017-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `training_modules`
--

DROP TABLE IF EXISTS `training_modules`;
CREATE TABLE `training_modules` (
  `id` int(11) NOT NULL,
  `training_module` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training_modules`
--

INSERT INTO `training_modules` (`id`, `training_module`) VALUES
(1, 'Module 1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `farm_costs`
--
ALTER TABLE `farm_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farm_sales`
--
ALTER TABLE `farm_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harvest_information`
--
ALTER TABLE `harvest_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sampling_feeding`
--
ALTER TABLE `sampling_feeding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_modules`
--
ALTER TABLE `training_modules`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
