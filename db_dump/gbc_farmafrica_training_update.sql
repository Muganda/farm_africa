-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 02, 2017 at 12:09 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gbc_farmafrica`
--

-- --------------------------------------------------------

--
-- Table structure for table `farmer_training`
--

DROP TABLE IF EXISTS `farmer_training`;
CREATE TABLE `farmer_training` (
  `id` int(11) NOT NULL,
  `farmer_id` varchar(50) DEFAULT NULL,
  `training_date` date DEFAULT NULL,
  `training_module` int(11) DEFAULT NULL,
  `venue` varchar(45) DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `trainer` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `submitted_by` int(11) DEFAULT NULL,
  `modified_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farmer_training`
--

INSERT INTO `farmer_training` (`id`, `farmer_id`, `training_date`, `training_module`, `venue`, `comments`, `trainer`, `date_created`, `submitted_by`, `modified_date`, `modified_by`) VALUES
(6, '1', NULL, 7, NULL, NULL, NULL, '2017-09-28 19:37:19', 1, '2017-10-02 11:20:58.128244', 1),
(7, '3', NULL, 7, NULL, NULL, NULL, '2017-09-28 19:38:31', 1, '2017-10-02 10:43:44.353684', 0),
(8, '9', NULL, 8, NULL, NULL, NULL, '2017-09-28 19:38:31', 1, '2017-10-04 10:43:44.353684', 0),
(9, 'K001', NULL, 9, NULL, NULL, NULL, '2017-09-28 19:38:31', 1, '2017-10-11 10:43:44.353684', 0),
(10, 'T004', NULL, 9, NULL, NULL, NULL, '2017-09-28 19:38:31', 1, '2017-10-02 10:43:44.353684', 0),
(11, 'T007', NULL, 10, NULL, NULL, NULL, '2017-09-28 19:38:32', 1, '2017-10-02 10:43:44.353684', 0),
(12, 'N001', NULL, 15, NULL, NULL, NULL, '2017-09-28 20:29:41', 1, '2017-10-02 10:43:44.353684', 0);

-- --------------------------------------------------------

--
-- Table structure for table `training_modules`
--

DROP TABLE IF EXISTS `training_modules`;
CREATE TABLE `training_modules` (
  `id` int(11) NOT NULL,
  `training_module` varchar(45) DEFAULT NULL,
  `no_of_ppl_invited` int(11) DEFAULT NULL,
  `training_date` varchar(50) DEFAULT NULL,
  `training_venue` varchar(50) DEFAULT NULL,
  `region` varchar(266) NOT NULL,
  `trainer_name` varchar(50) DEFAULT NULL,
  `submitted_by` varchar(50) DEFAULT NULL,
  `date_submitted` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training_modules`
--

INSERT INTO `training_modules` (`id`, `training_module`, `no_of_ppl_invited`, `training_date`, `training_venue`, `region`, `trainer_name`, `submitted_by`, `date_submitted`) VALUES
(7, 'Fish Keeping', 0, '09-12-2017', 'Kakamega', 'Western', 'Millicent', '1', '2017-09-28 13:27:48'),
(8, 'Maize', 20, '10-12-2017', 'Kisumu', 'Nyanza', 'Mark', '1', '2017-09-28 13:27:49'),
(9, 'Water', 0, '09-05-2017', 'Nairobi', 'Nairobi', 'Kipchirchir', '1', '2017-09-28 18:17:03'),
(10, 'Harvest', 0, '03-30-2017', 'Mombasa', 'Coast', 'Brown', '1', '2017-09-28 18:17:03'),
(11, 'Bee keeping', 0, '09-08-2017', 'Kitui', 'Central', 'Njogu', '1', '2017-09-28 19:40:36'),
(12, 'Tilapia fish', 0, '09-07-2017', 'Machakos', 'North Eastern', 'Kimani, Ndungu, Luke', '1', '2017-09-28 20:20:01'),
(13, 'Home Farming', 0, '09-04-2017', 'Busia', 'Western', 'Job,Zebedayo,Otoyo', '1', '2017-09-28 20:22:07'),
(14, 'Pond farming', 0, '07-11-2017', 'Kitale', 'Western', 'Njoroge,Otis,Okello', '1', '2017-09-28 20:22:07'),
(15, 'Game fishing', 0, '09-05-2017', 'Kisumu', 'Nyanza', 'Jacob,Peter', '1', '2017-09-28 20:28:41'),
(16, 'Harvesting fish', 0, '10-05-2017', 'Nakuru', 'Central', 'James', '1', '2017-09-28 20:28:41'),
(17, 'Module 56', 34, '10-18-2017', 'Nairobi', 'Nairobi', 'None', '1', '2017-10-02 13:21:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `farmer_training`
--
ALTER TABLE `farmer_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_modules`
--
ALTER TABLE `training_modules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `farmer_training`
--
ALTER TABLE `farmer_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `training_modules`
--
ALTER TABLE `training_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
