<?php
require_once 'library/config.php';
require_once 'library/functions.php';


$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();


$view = (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '';

switch ($view) {
	case 'adduser' :
		$content 	= 'user/add.php';		
		$pageTitle 	= 'Farm Africa - Add Users';
		break;

	 case 'editfarmer' :
		$content 	= 'farmer/edit_farm_info.php';		
		$pageTitle 	= 'Farm Africa - Edit Farmer profile';
		break;

	case 'editextfarminfo' :
		$content 	= 'farm/edit_farmextinfo.php';		
		$pageTitle 	= 'Farm Africa - Edit Farm information';
		break;

	case 'editbasicfarminfo' :
		$content 	= 'farm/edit_basicfarm.php';		
		$pageTitle 	= 'Farm Africa - Edit Basic Farm information';
		break;

	case 'edicontactperson' :
		$content 	= 'farm/editcontactperson.php';		
		$pageTitle 	= 'Farm Africa - Edit Contact Person information';
		break;

	case 'editpond' :
		$content 	= 'farm/editpond.php';		
		$pageTitle 	= 'Farm Africa - Edit Pond information';
		break;

	case 'editcost' :
		$content 	= 'farm/editcost.php';		
		$pageTitle 	= 'Farm Africa - Edit Cost information';
		break;

	case 'editprodcycle' :
		$content 	= 'farm/prodcycleedit.php';		
		$pageTitle 	= 'Farm Africa - Edit Production cycle ';
		break;

	case 'editstaff' :
		$content 	= 'farm/editfarmstaff.php';		
		$pageTitle 	= 'Farm Africa - Edit Staff Information';
		break;

	case 'editassesmnt' :
		$content 	= 'farm/editassessment.php';		
		$pageTitle 	= 'Farm Africa - Edit Assessment';
		break;

	case 'edit' :
		$content 	= 'user/edit.php';		
		$pageTitle 	= 'Farm Africa - Edit User';
		break;

	case 'addeditstock' :
		$content 	= 'farm/edit_stock.php';		
		$pageTitle 	= 'Farm Africa - Stock';
		break;

	case 'operation' :
		$content 	= 'farm/operations.php';		
		$pageTitle 	= 'Farm Africa - Edit Operations';
		break;

	case 'editsample' :
		$content 	= 'farm/editsample.php';		
		$pageTitle 	= 'Farm Africa - Sample';
		break;

	case 'edit_harvest' :
		$content 	= 'farm/edit_harvest.php';		
		$pageTitle 	= 'Farm Africa - Harvest';
		break;
	case 'edit_sales' :
		$content 	= 'farm/edit_sales.php';		
		$pageTitle 	= 'Farm Africa - Sales';
		break;
		

	default :
		$content 	= 'farmer/edit_farm_info.php';		
		$pageTitle 	= 'Farm Africa - View Users';
}

$script    = array('user.js','farmer.js','farm.js');

require_once 'template.php';

?>