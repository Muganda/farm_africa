<?php
require_once 'farmer/addtab.php';
if (!defined('WEB_ROOT')) {
	exit;
}
$errorMessage = '&nbsp;';
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

// $sql = "SELECT farmer_id, firstname, lastname FROM farms_owners order by f_id desc
// limit 1";
// $result = dbQuery($dbConn,$sql);
// while($row = dbFetchAssoc($result)) {
//     extract($row);
//    }


?> 

<div class="prepend-1 span-12">
<h4 class="errorMessage"><?php echo $errorMessage; ?></h4>
<div class="col-sm-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
 
   <div class="form-group row">
   <label for="farmer_id" class="col-sm-3 col-form-label">Farm ID:<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="farmer_id" type="text" id="farmer_id" value="" required=""><?php getfarmerid($dbConn);?>
      </select></label>
   <label class="col-sm-3 col-form-label">Upload Farm Photo:<input type="file" name="file" style="float: left;" class="btn btn-default"/></label>
   </div>
   
  
    <!-- <div class="form-group row">
        <p onclick="geoFindMe()" type="button" class="btn btn-default btn-lg">
      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></p>
    <div id="out"></div>
  </div> -->
   <div class="form-group row">
   <label for="farm_name" class="col-sm-3 col-form-label">Name of Farm:<input class="form-control" name="farm_name" type="text" id="farm_name" required="" ></label>
   <label for="farm_county" class="col-sm-3 col-form-label">County :<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="farm_county" type="text" id="farm_county"  required=""><?php getCounties($dbConn);?></select></label>
   <label for="farm_subcounty" class="col-sm-3 col-form-label">SubCounty:<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="farm_subcounty" type="text" id="farm_subcounty"  required=""><?php getSubcounties($dbConn);?></select></label>
   </div>
   <div class="form-group row" >
    <input class="form-control" name="farm_longitude" type="hidden" id="out" value="" required="" >
   <input class="form-control" name="farm_latitude" type="hidden" id="farm_latitude" value="" required="" >
   <label for="farm_village" class="col-sm-3 col-form-label">Village:<input class="form-control" name="farm_village" type="text" id="farm_village"  ></label>
   <label for="tot_no_of_tilapia" class="col-sm-3 col-form-label">Total Number of Tilapia Stocked:<input class="form-control" name="tot_no_of_tilapia" type="number" id="tot_no_of_tilapia" required="" ></label>
   <label for="tot_no_of_catfish" class="col-sm-3 col-form-label">Total Number of Catfish Stocked:<input class="form-control" name="tot_no_of_catfish" type="number" id="tot_no_of_catfish" required="" ></label>
   
  </div>

   <div class="form-group row" >
   <!-- <label for="pond_sample_temperature" class="col-sm-3 col-form-label">Temperature of sampled pond:<input class="form-control" name="pond_sample_temperature" type="number" id="pond_sample_temperature" value="<?php echo $pond_sample_temperature; ?>" required="" ></label>
   <label for="pond_sample_time" class="col-sm-3 col-form-label">Time of day:<select class="form-control" name="pond_sample_time" type="text" id="pond_sample_time"  required="" >
    <option value="" >--select--</option>
    <option value="Morning">Morning</option>
    <option value="Noon">Noon</option>
    <option value="Evening">Evening</option>
    </select></label> -->
    <label for="farm_landmark" class="col-sm-3 col-form-label">Landmark:<input class="form-control" name="farm_landmark" type="text" id="farm_landmark" ></label>
    <label for="ponds_number" class="col-sm-3 col-form-label">Total Number of Ponds:<input class="form-control" name="ponds_number" type="number" id="ponds_number" required="" ></label>
   <label for="ponds_stocked" class="col-sm-3 col-form-label">Ponds Stocked:<input class="form-control" name="ponds_stocked" type="number" id="ponds_stocked"  required="" ></label>
   </div>
  <div class="form-group row" >
  <label for="records_kept" class="col-sm-3 col-form-label">Which records do you keep:<select class="form-control" id ="records_kept" name="records_kept" required="">
    <option value="<?php echo $records_kept; ?>" ><?php echo $records_kept; ?></option>
    <option value="Manual">Manual</option>
    <option value="Automatic">Automatic</option>
    <option value="None">None</option>
    </select></label>
    <label for="record_keeping" class="col-sm-4 col-form-label">Have you recieved a record keeping book from KMAP:<select class="form-control" name="record_keeping" required="">
    <option value="Yes">Yes</option>
    <option value="No">No</option>
    </select></label>
  </div>
  

  </div>
  <div class="form-group row" >
  <label for="has_business_plan" class="col-sm-3 col-form-label">Does he/she have a business plan?</label>
    Yes <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="Yes" id="yes"> No <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="No" id="no">
    <div id="iftrue" style="visibility:hidden">
    <label for="business_plan_last_update" class="col-sm-3 col-form-label">When was the Last time of business plan update:<input class="date form-control" name="business_plan_last_update" type="text"  ></label>
        
  </div>
</div>

 <p align="left"> 
  <input name="submit" id="submit" type="submit" value="SUBMIT" class="btn btn-primary" />
 </p>
</form>
 </tbody>
</table>
</div>


</div>