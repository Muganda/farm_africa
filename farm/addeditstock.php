<?php
if (!defined('WEB_ROOT')) {
exit;
}
if(isset($_GET['id']) && (int)$_GET['id']>0){
$s_id = (int)$_GET['id'];
}else{
header('Location: ../index.php');
}
$errorMessage = "";
$farmerid="select farmer_id from farms where id='$s_id'";
$resultf = dbQuery($dbConn,$farmerid);
while($row = dbFetchAssoc($resultf)) {
extract($row);
}

$fid = "SELECT id,pond_number  FROM farms_ponds where farmer_id='$farmer_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
extract($row);
$tabs[]=array("pond_number"=>$pond_number,"id"=>$id);
}
?>
<div class="container">

<div class="row">
<div class="col-sm-12">
<div class="panel-heading" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
<div id="tabs">
<ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
<?php

$tabTitle = "";
$tabTitleClass = "";

foreach($tabs as $tab) {

if($i == 0) {
$tabTitleClass = "active";
} else {
$tabTitleClass = "";
}

/* Build our tab pane title */
$tabTitle = '<li class="'.$tabTitleClass.'"><a href="#'.$tab['id'].'" data-toggle="tab">'.$tab['pond_number'].'</a></li>';

echo $tabTitle;

$i++;

} ?>
</ul>

</div>

</div>
<div class="panel-body">

<div class="tab-content">
<?php
foreach($tabs as $tab) {


if($j == 0) {

?>

<div id="<?php echo $tab['id']; ?>" class="tab-pane fade in active">
<?php
$sql = "SELECT id,pond_number,pond_area,farmer_id,catfish_fingerling_stocked,tilapia_fingerling_stocked,stocking_date,expected_harvest_date,fingerlings_source,price_per_fingerling,size_of_fingerlings_gms from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."' ";

$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
extract($row);
?>

<div class="prepend-1 span-17">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<tbody>
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editstock" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
<div class="form-group row">
<label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
</div>

<div class="form-group row">
<input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >

<label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" required="" ></label>
<label for="fingerlings_source" class="col-md-3 col-form-label">Source Of Fingerlings:<input class="form-control" name="fingerlings_source" type="text" id="fingerlings_source" value="<?php echo $fingerlings_source; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" required="" ></label>
<label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
</div>
<div class="form-group row" >
<label for="tilapia_price_per_fingerling" class="col-md-3 col-form-label">Price per Tilapia Fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="text" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>" required="" ></label>
<label for="catfish_price_per_fingerling" class="col-md-3 col-form-label">Price per Catfish Fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="text" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>" required="" ></label>
</div>
<div class="form-group row" >

<label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Tilapia Fingerlings:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" required="" ></label>
<label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Catfish Fingerlings:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" required="" ></label>

</div>
<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Farmoperationstable&id=<?php echo $s_id; ?>';" />
</p>
</form>
</tbody>
</table>
</div>
</div>
<?php
} // end while

?>

</div>
<?php


} else {
// $tabContentClass = "tab-pane fade";
?>
<div id="<?php echo $tab['id']; ?>" class="tab-pane fade">
<?php
$sql = "SELECT id,pond_number,pond_area,farmer_id,catfish_fingerling_stocked,tilapia_fingerling_stocked,stocking_date,expected_harvest_date,fingerlings_source,price_per_fingerling,size_of_fingerlings_gms from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."'";

$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
extract($row);
?>


<div class="prepend-1 span-17">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<tbody>
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editstock" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
<div class="form-group row">
<label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
</div>

<div class="form-group row">
<input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >

<label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" required="" ></label>
<label for="fingerlings_source" class="col-md-3 col-form-label">Source Of Fingerlings:<input class="form-control" name="fingerlings_source" type="text" id="fingerlings_source" value="<?php echo $fingerlings_source; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" required="" ></label>
<label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
</div>
<div class="form-group row" >
<label for="tilapia_price_per_fingerling" class="col-md-3 col-form-label">Price per Tilapia Fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="text" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>" required="" ></label>
<label for="catfish_price_per_fingerling" class="col-md-3 col-form-label">Price per Catfish Fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="text" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>" required="" ></label>
</div>
<div class="form-group row" >

<label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Tilapia Fingerlings:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" required="" ></label>
<label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Catfish Fingerlings:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" required="" ></label>

</div>
<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Farmoperationstable&id=<?php echo $s_id; ?>';" />
</p>
</form>
</tbody>
</table>
</div>
</div>
<?php
} // end while

?>
</div>
<?php
}
$j++;

}
?>
</div>
</div>

</div>
</div>
</div>

<?php

echo '</ul>';



?>

