<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $s_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}

$farmerid="select farmer_id from farms where id='$s_id'";
$resultf = dbQuery($dbConn,$farmerid);
while($row = dbFetchAssoc($resultf)) {
  extract($row);
}

$fid = "SELECT id,pond_number  FROM farms_ponds where farmer_id='$farmer_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
  extract($row);
 $tabs[]=array("pond_number"=>$pond_number,"id"=>$id);
}

  ?>
<?php require_once 'addoperationstab.php';?>
<div class="container">

     <div class="row">
      <div class="col-sm-12">
        <h5>Record Harvest</h5>
        <div class="panel-heading" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
        <div id="tabs">
        <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
                <?php

                $tabTitle = "";
                $tabTitleClass = "";

                foreach($tabs as $tab) {

                   if($i == 0) {
                       $tabTitleClass = "active";
                   } else {
                       $tabTitleClass = "";
                   }

                   /* Build our tab pane title */
                   $tabTitle = '<li class="'.$tabTitleClass.'"><a href="#'.$tab['id'].'" data-toggle="tab">'.$tab['pond_number'].'</a></li>';

                   echo $tabTitle;

                   $i++;

                } ?>
         </ul>

       </div>
              
    </div>
    <div class="panel-body">
        
      <div class="tab-content">
      <?php
      foreach($tabs as $tab) {
          

              if($j == 0) {
                  
                  ?>

                  <div id="<?php echo $tab['id']; ?>" class="tab-pane fade in active">
              
                 
               <div class="prepend-1 span-17">
                
                <div class="table-responsive">
                
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editharvest" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

               <?php
               $sqltab = "SELECT pond_number,farmer_id,stocking_date,harvest_type,expected_harvest_date,tilapia_fingerling_stocked,tilapia_harvested,avg_weight_tilapia_harvested,(tilapia_harvested * avg_weight_tilapia_harvested) as total_tilapia_weight, (tilapia_fingerling_stocked-tilapia_harvested) as rem_tilapia_pieces, catfish_fingerling_stocked,catfish_harvested,avg_weight_catfish_harvested,(catfish_harvested * avg_weight_catfish_harvested) as total_catfish_weight,(catfish_fingerling_stocked-catfish_harvested) as rem_catfish_pieces, (((tilapia_harvested + catfish_harvested)/(tilapia_fingerling_stocked + catfish_fingerling_stocked))*100) as percentage_harvested  from pond_stocking  where farmer_id='$farmer_id' and  id='".$tab['id']."'";

                   $resulttab = dbQuery($dbConn,$sqltab);
                   while($row = dbFetchAssoc($resulttab)) {
                extract($row);
               ?>
                  
                <div class="form-group row">
                <input class="form-control input-sm" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
                <label for="pond_number" class="col-md-2 col-form-label">Pond Number:<input class="form-control input-sm" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                </div>
                <div class="form-group row">
                <label for="stocking_date" class="col-md-2 col-form-label">Date Stocked:<input class="date form-control input-sm" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly></label>
                <label for="expected_harvest_date" class="col-md-2 col-form-label">Expected Date of Harvest:<input class="date form-control input-sm" name="expected_harvest_date" type="text" id="expected_harvest_date" value="<?php echo $expected_harvest_date; ?>" readonly ></label>
                <label for="harvest_date" class="col-md-2 col-form-label">Date Harvested:<input class="date form-control input-sm" name="harvest_date" type="text" id="harvest_date" value=""  ></label>
                <label for="harvest_type" class="col-md-2 col-form-label">Harvest type:<select class="form-control input-sm" name="harvest_type" value="<?php echo $harvest_type; ?>">
                <option value="<?php echo $harvest_type; ?>"><?php echo $harvest_type; ?></option>
                <option value="Partial">Partial</option>
                <option value="Complete">Complete</option>
                </select></label>
                <!-- <input type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                <input type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                 -->
                </div>
                <div class="form-group row">
                <label for="tilapia_fingerling_stocked" class="col-md-2 col-form-label">Tilapia Stocked:<input class="form-control input-sm" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
                <input class="form-control input-sm" name="tilapia_harvested" type="hidden" id="tilapia_harvested" value="<?php echo $tilapia_harvested; ?>" >
                <label for="tilapia_harvest" class="col-md-2 col-form-label">Tilapia Harvested:<input class="form-control input-sm" name="tilapia_harvest" type='number' id='tilapiaharvest' onkeyup="validateTilapia()" onclick="validateTilapia()" value=""  >
                  <p id="msg"></p>
                </label>
                <label for="rem_tilapia_pieces" class="col-md-2 col-form-label">Tilapia pieces remaining:<input class="form-control input-sm" name="rem_tilapia_pieces" type='number' id='remtilapiapieces' onkeyup="validateTilapia()" onclick="validateTilapia()" value="<?php echo $rem_tilapia_pieces; ?>" readonly ></label>
                <label for="avg_weight_tilapia_harvested" class="col-md-2 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_tilapia_harvested" type="text" id="avg_weight_tilapia_harvested" value="<?php echo $avg_weight_tilapia_harvested; ?>" required="" ></label>
                
                </div>
               <div class="form-group row">
                <label for="catfish_fingerling_stocked" class="col-md-2 col-form-label">Catfish Stocked:<input class="form-control input-sm" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
                <input class="form-control input-sm" name="catfish_harvested" type="hidden" id="catfish_harvested" value="<?php echo $catfish_harvested; ?>" >
                <label for="catfish_harvest" class="col-md-2 col-form-label">Catfish Harvested:<input class="form-control input-sm" name="catfish_harvest" type='number' id='catfishharvest' onkeyup="validateCatfish()" onclick="validateCatfish()" value=""  >
                  <p id="message"></p>
                </label>
                <label for="rem_catfish_pieces" class="col-md-2 col-form-label">Catfish pieces remaining:<input class="form-control input-sm" name="rem_catfish_pieces" type='number' id='remcatfishpieces' onkeyup="validateCatfish()" onclick="validateCatfish()" value="<?php echo $rem_catfish_pieces; ?>" readonly ></label>
                <label for="avg_weight_catfish_harvested" class="col-md-2 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_catfish_harvested" type="text" id="avg_weight_catfish_harvested" value="<?php echo $avg_weight_catfish_harvested; ?>" required="" ></label>
                
                </div>
               
              
              <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Farmoperationstable&id=<?php echo $s_id; ?>';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
                
               </div>
                  <?php


              } else {
                  // $tabContentClass = "tab-pane fade";
                  ?>
                  <div id="<?php echo $tab['id']; ?>" class="tab-pane fade">
               <div class="prepend-1 span-17">
                <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editharvest" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                 $sqltab = "SELECT pond_number,farmer_id,stocking_date,harvest_type,expected_harvest_date,tilapia_fingerling_stocked,tilapia_harvested,avg_weight_tilapia_harvested,(tilapia_harvested * avg_weight_tilapia_harvested) as total_tilapia_weight, (tilapia_fingerling_stocked-tilapia_harvested) as rem_tilapia_pieces, catfish_fingerling_stocked,catfish_harvested,avg_weight_catfish_harvested,(catfish_harvested * avg_weight_catfish_harvested) as total_catfish_weight,(catfish_fingerling_stocked-catfish_harvested) as rem_catfish_pieces,(((tilapia_harvested + catfish_harvested)/(tilapia_fingerling_stocked + catfish_fingerling_stocked))*100) as percentage_harvested  from pond_stocking  where farmer_id='$farmer_id' and  id='".$tab['id']."'";

                   $resulttab = dbQuery($dbConn,$sqltab);
                   while($row = dbFetchAssoc($resulttab)) {
                extract($row);
               ?>
               
                  
                <div class="form-group row">
                <input class="form-control input-sm" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
                <label for="pond_number" class="col-md-2 col-form-label">Pond Number:<input class="form-control input-sm" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                </div>
                <div class="form-group row">
                <label for="stocking_date" class="col-md-2 col-form-label">Date Stocked:<input class="date form-control input-sm" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly></label>
                <label for="expected_harvest_date" class="col-md-2 col-form-label">Expected Date of Harvest:<input class="date form-control input-sm" name="expected_harvest_date" type="text" id="expected_harvest_date" value="<?php echo $expected_harvest_date; ?>" readonly ></label>
                <label for="harvest_date" class="col-md-2 col-form-label">Date Harvested:<input class="date form-control input-sm" name="harvest_date" type="text" id="harvest_date" value=""  ></label>
                <label for="harvest_type" class="col-md-2 col-form-label">Harvest type:<select class="form-control input-sm" name="harvest_type" value="<?php echo $harvest_type; ?>">
                <option value="<?php echo $harvest_type; ?>"><?php echo $harvest_type; ?></option>
                <option value="Partial">Partial</option>
                <option value="Complete">Complete</option>
                </select></label>
                <!-- <input type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                <input type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                 -->
                </div>
                <div class="form-group row">
                <label for="tilapia_fingerling_stocked" class="col-md-2 col-form-label">Tilapia Stocked:<input class="form-control input-sm" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
                <input class="form-control input-sm" name="tilapia_harvested" type="hidden" id="tilapia_harvested" value="<?php echo $tilapia_harvested; ?>" >
                <label for="tilapia_harvest" class="col-md-2 col-form-label">Tilapia Harvested:<input class="form-control input-sm" name="tilapia_harvest" type='number' id='tilapiaharvest2' onkeyup="validateTilapia2()" onclick="validateTilapia2()" value=""  >
                  <p id="msg1"></p>
                </label>
                <label for="rem_tilapia_pieces" class="col-md-2 col-form-label">Tilapia pieces remaining:<input class="form-control input-sm" name="rem_tilapia_pieces" type='number' id='remtilapiapieces2' onkeyup="validateTilapia2()" onclick="validateTilapia2()" value="<?php echo $rem_tilapia_pieces; ?>" readonly ></label>
                <label for="avg_weight_tilapia_harvested" class="col-md-2 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_tilapia_harvested" type="text" id="avg_weight_tilapia_harvested" value="<?php echo $avg_weight_tilapia_harvested; ?>" required="" ></label>
                
                </div>
               <div class="form-group row">
                <label for="catfish_fingerling_stocked" class="col-md-2 col-form-label">Catfish Stocked:<input class="form-control input-sm" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
                <input class="form-control input-sm" name="catfish_harvested" type="hidden" id="catfish_harvested" value="<?php echo $catfish_harvested; ?>" >
                <label for="catfish_harvest" class="col-md-2 col-form-label">Catfish Harvested:<input class="form-control input-sm" name="catfish_harvest" type='number' id='catfishharvest2' onkeyup="validateCatfish2()" onclick="validateCatfish2()" value=""  >
                  <p id="message1"></p>
                </label>
                <label for="rem_catfish_pieces" class="col-md-2 col-form-label">Catfish pieces remaining:<input class="form-control input-sm" name="rem_catfish_pieces" type='number' id='remcatfishpieces2' onkeyup="validateCatfish2()" onclick="validateCatfish2()" value="<?php echo $rem_catfish_pieces; ?>" readonly ></label>
                <label for="avg_weight_catfish_harvested" class="col-md-2 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_catfish_harvested" type="text" id="avg_weight_catfish_harvested" value="<?php echo $avg_weight_catfish_harvested; ?>" required="" ></label>
                
                </div>
                <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Farmoperationstable&id=<?php echo $s_id; ?>';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
               </div>
                  <?php
              }

                    /* Build our tab content */
                    
                    // $tabContent = 
                    // '';

                    // echo $tabContent;

                    $j++;
                
            }
        ?>
        </div>
        </div>

       </div>
      </div>
     </div>
    
<?php
  
  echo '</ul>';
  


?>
  
