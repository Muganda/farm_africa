<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

?> 
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div>
<div class="col-sm-3" style="margin-top: -15px;"><h5><font color="">Add Operating  Cost 
  </font></h5><h4></h4></div>
  <form action= "" method="post" >
  
  <div class="col-sm-2">
  <select class="form-control input-sm" name="farmer" style="margin-top: -15px;" >
  <option>Select Farmer</option>
    <?php getfarmerid($dbConn);?>
    </select>
  </div>
 <div class="col-sm-2"><input name="submit" id="submit" type="submit" value="Select" class="btn btn-primary" style="margin-top: -15px;" /></div>
</form>
</div>  
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
<div class="form-group row">
    <label class="col-sm-6 col-form-label"><h4><?php echo $errorMessage; ?></h4></label>
    </div>
</div>
<div class="ibox-content">
<div class="row">
  <?php
   if(isset($_POST['farmer']) && $_POST['farmer'] != '') {
    $farmer = $_POST['farmer'];
    $sql = "SELECT farmer_id from farms_owners where  farmer_id='$farmer' ";
   };
$result = dbQuery($dbConn,$sql);
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);
?>
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=addcost" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group row">
    <label for="pondname" class="col-sm-3 col-form-label">
    <?php
    $sqlp = "SELECT id,pond_number from farms_ponds  where farmer_id='$farmer'";
    $resultp = dbQuery($dbConn,$sqlp);
    ?>
    <select class="form-control input-sm" name="pond"  >
      <option>Filter by Pond</option>
    <?php while($row = dbFetchAssoc($resultp)) {
      extract($row);
      ?>
      <option value="<?php echo $id; ?>"><?php echo $pond_number; ?></option>
      <?php
    }
    ?></select>
  </label>
</div>

<div class="form-group row" >
  <label for="cost_date" class="col-sm-3 col-form-label">Date:<input class="form-control" name="cost_date" type="date" id="date" value="" required="" ></label>
  
  <input class="form-control" name="farmer" type="hidden" id="farmer" value="<?php echo $farmer; ?>" readonly >
</div>

<div class="form-group row">
  <label for="feeds_costs" class="col-sm-3 col-form-label">Feeds Cost:<input class="form-control" name="feeds_costs" type="number" id="feeds_costs" value="<?php echo $feeds_costs; ?>" required="" ></label>
  <label for="fertilizer_cost" class="col-sm-3 col-form-label">Fertilizer Cost:<input class="form-control" name="fertilizer_cost" type="number" id="fertilizer_cost" value="<?php echo $fertilizer_cost; ?>" required="" ></label>
  <label for="manila_twine_cost" class="col-sm-3 col-form-label">Manilla Twine cost:<input class="form-control" name="manila_twine_cost" type="number" id="manila_twine_cost" value="<?php echo $manila_twine_cost; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="catfish_price_per_fingerling" class="col-sm-3 col-form-label">Catfish Price per fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="number" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>" required="" ></label>
  <label for="tilapia_price_per_fingerling" class="col-sm-3 col-form-label">Tilapia Price per fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="number" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>" required="" ></label>
  <label for="lime_cost" class="col-sm-3 col-form-label">Lime Cost:<input class="form-control" name="lime_cost" type="number" id="lime_cost" value="<?php echo $lime_cost; ?>" required="" ></label>
</div>
<div class="form-group row">
  <label for="fishing_net_cost" class="col-sm-3 col-form-label">Fishing net cost:<input class="form-control" name="fishing_net_cost" type="number" id="fishing_net_cost" value="<?php echo $fishing_net_cost; ?>" required="" ></label>
  <label for="extension_services_cost" class="col-sm-3 col-form-label">Extension services cost:<input class="form-control" name="extension_services_cost" type="number" id="extension_services_cost" value="<?php echo $extension_services_cost; ?>" required="" ></label>
  <label for="sampling_cost" class="col-sm-3 col-form-label">Sampling Cost:<input class="form-control" name="sampling_cost" type="number" id="sampling_cost" value="<?php echo $sampling_cost; ?>" required="" ></label>
</div>
<!-- <div class="form-group row">
  <label for="equipment_cost" class="col-sm-3 col-form-label">Equipment cost:<input class="form-control" name="equipment_cost" type="number" id="equipment_cost" value="<?php  $equipment_cost; ?>" required="" ></label>
  <label for="water_costs" class="col-sm-3 col-form-label">Water costs:<input class="form-control" name="water_costs" type="number" id="water_costs" value="<?php  $water_costs; ?>" required="" ></label>
  <label for="transportation_cost" class="col-sm-3 col-form-label">Transportation cost:<input class="form-control" name="transportation_cost" type="number" id="transportation_cost" value="<?php  $transportation_cost; ?>" required="" ></label>
  
</div> -->


  <div class="form-group row">
    <label class="col-sm-6 col-form-label">
      <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Costs';" />
    </label>
    </div>

 </div>
</form>
 </tbody>
</table>
<?php 

}//while
}else {
  
?>
<h4 class="text-danger">No Matching Records Found</h4>

<?php 
}
?>
</div>
</div>          
          

 </div>

</div>

</div>

</div>
</div>