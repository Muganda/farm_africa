<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

?> 
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div>
<div class="col-sm-3" style="margin-top: -15px;"><h5><font color="">Add Recurrent Costs 
  </font></h5><h4></h4></div>
  <form action= "" method="post" >
  
  <div class="col-sm-2">
  <select class="form-control input-sm" name="farmer" style="margin-top: -15px;" >
  <option>Select Farmer</option>
    <?php getfarmerid($dbConn);?>
    </select>
  </div>
 <div class="col-sm-2"><input name="submit" id="submit" type="submit" value="Select" class="btn btn-primary" style="margin-top: -15px;" /></div>
</form>
</div>  
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
<div class="form-group row">
    <label class="col-sm-6 col-form-label"><h4><?php echo $errorMessage; ?></h4></label>
    </div>
</div>
<div class="ibox-content">
<div class="row">
  <?php
   if(isset($_POST['farmer']) && $_POST['farmer'] != '') {
    $farmer = $_POST['farmer'];
    $sql = "SELECT farmer_id from farms_owners where  farmer_id='$farmer' ";
   };
$result = dbQuery($dbConn,$sql);
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);
?>
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=addrecurrentcost" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group row">
    <label for="pondname" class="col-sm-3 col-form-label">
    <?php
    $sqlp = "SELECT id,pond_number from farms_ponds  where farmer_id='$farmer'";
    $resultp = dbQuery($dbConn,$sqlp);
    ?>
    <select class="form-control input-sm" name="pond"  >
      <option>Filter by Pond</option>
    <?php while($row = dbFetchAssoc($resultp)) {
      extract($row);
      ?>
      <option value="<?php echo $id; ?>"><?php echo $pond_number; ?></option>
      <?php
    }
    ?></select>
  </label>
</div>

<div class="form-group row" >
  <label for="date_cost_incurred" class="col-sm-3 col-form-label">Date:<input class="form-control" name="date_cost_incurred" type="date" id="date_cost_incurred" value="" required="" ></label>
  
  <input class="form-control" name="farmer" type="hidden" id="farmer" value="<?php echo $farmer; ?>" readonly >
</div>

<div class="form-group row">
  <label for="annual_salary" class="col-sm-3 col-form-label">Annual Salary(Own labour):<input class="form-control" name="annual_salary" type="number" id="annual_salary" value="<?php echo $annual_salary; ?>"  ></label>
  <label for="other_labour" class="col-sm-3 col-form-label">Other Labour:<input class="form-control" name="other_labour" type="number" id="other_labour" value="<?php echo $other_labour; ?>"  ></label>
  <label for="electricity" class="col-sm-3 col-form-label">Electricity:<input class="form-control" name="electricity" type="number" id="electricity" value="<?php echo $electricity; ?>"  ></label>
</div>
<div class="form-group row">
<label for="fuel" class="col-sm-3 col-form-label">Fuel:<input class="form-control" name="fuel" type="number" id="fuel" value="<?php echo $fuel; ?>"  ></label>
  <label for="rent_oxygen_tank" class="col-sm-3 col-form-label">Oxygen Tank Rent:<input class="form-control" name="rent_oxygen_tank" type="number" id="rent_oxygen_tank" value="<?php echo $rent_oxygen_tank; ?>"  ></label>
  <label for="price_per_oxygen_tank" class="col-sm-3 col-form-label">Price per Oxygen Tank:<input class="form-control" name="price_per_oxygen_tank" type="number" id="price_per_oxygen_tank" value="<?php echo $price_per_oxygen_tank; ?>"  ></label>
</div>
<div class="form-group row">
  <label for="transport" class="col-sm-3 col-form-label">Transport:<input class="form-control" name="transport" type="number" id="transport" value="<?php echo $transport; ?>"  ></label>
  <label for="administration_cost" class="col-sm-3 col-form-label">Administration Costs:<input class="form-control" name="administration_cost" type="number" id="administration_cost" value="<?php echo $administration_cost; ?>"  ></label>
</div>
  <div class="form-group row">
    <label class="col-sm-6 col-form-label">
      <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Recurentcosts';" />
    </label>
    </div>

 </div>
</form>
 </tbody>
</table>
<?php 

}//while
}else {
  
?>
<h4 class="text-danger">No Matching Records Found</h4>

<?php 
}
?>
</div>
</div>          
          

 </div>

</div>

</div>

</div>
</div>