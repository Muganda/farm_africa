<?php
if (!defined('WEB_ROOT')) {
exit;
}
$errorMessage = "";
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
$s_id = (int)$_GET['id'];
} else {
header('Location: ../index.php');
}

$sql="SELECT f.id as fid,s.id as sid,s.farmer_id,s.remaining_harvest from farm_sales s join farms f on s.farmer_id=f.farmer_id  where s.id='$s_id'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
extract($row);
}
?>
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5><font color="">Record Sales<?php echo $errorMessage; ?></font></h5>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">

<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
<tbody>
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=sales" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

<div class="form-group row">
<label for="remaining_harvest" class="col-md-2 col-form-label">Available Fish in KGs:<input class="form-control" name="remaining_harvest" type="text" id="remaining_harvest" value="<?php echo $remaining_harvest; ?>" readonly></label>
<label for="sales_date" class="col-md-2 col-form-label">Date of Sale:<input class="date form-control input-sm" name="sales_date" type="text" id="sales_date" value=""  ></label>
</div>

<div class="form-group row">
<input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
<input class="form-control" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >

<label for="whole_tilapia_fish_sold" class="col-md-2 col-form-label">Quantity in Kgs of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_sold" type="text" id="whole_tilapia_fish_sold" value="" required="" ></label>
<label for="whole_tilapia_fish_avg_price_kg" class="col-md-2 col-form-label">Avg Price per kg of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_avg_price_kg" type="text" id="whole_tilapia_fish_avg_price_kg" value="" required="" ></label>
<label for="whole_catfish_fish_sold" class="col-md-2 col-form-label">Quantity in Kgs of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_sold" type="text" id="whole_catfish_fish_sold" value="" required="" ></label>
<label for="whole_catfish_fish_avg_price_kg" class="col-md-2 col-form-label">Avg Price per kg of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_avg_price_kg" type="text" id="whole_catfish_fish_avg_price_kg" value="" required="" ></label>
</div>
<div class="form-group row">
<label for="value_added_tilapia_kgs_sold" class="col-md-2 col-form-label">Quantity in Kgs of VA Tilapia Sold:<input class="form-control" name="value_added_tilapia_kgs_sold" type="text" id="value_added_tilapia_kgs_sold" value="" required="" ></label>
<label for="tilapia_value_added_avg_price_kg" class="col-md-2 col-form-label">Price per kg of VA Tilapia per Kg:<input class="form-control" name="tilapia_value_added_avg_price_kg" type="text" id="tilapia_value_added_avg_price_kg" value="" required="" ></label>
<label for="value_added_catfish_kgs_sold" class="col-md-2 col-form-label">Quantity in Kgs of VA Catfish Sold:<input class="form-control" name="value_added_catfish_kgs_sold" type="text" id="value_added_catfish_kgs_sold" value="" required="" ></label>
<label for="catfish_value_added_avg_price_kg" class="col-md-2 col-form-label">Price per kg of VA Catfish per Kg:<input class="form-control" name="catfish_value_added_avg_price_kg" type="text" id="catfish_value_added_avg_price_kg" value="" required="" ></label>
</div>
<div class="form-group row" >
<label for="tilapia_value_addition_form" class="col-md-2 col-form-label">Form of Tilapia Value addition:<input class="form-control" name="tilapia_value_addition_form" type="text" id="tilapia_value_addition_form" value="" required="" ></label>
<label for="catfish_value_addition_form" class="col-md-2 col-form-label">Form of Catfish Value addition:<input class="form-control" name="catfish_value_addition_form" type="text" id="catfish_value_addition_form" value="" required="" ></label>
<label for="tilapia_kgs_consumed" class="col-md-2 col-form-label">KGs of Tilapia Consumed:<input class="form-control" name="tilapia_kgs_consumed" type="text" id="tilapia_kgs_consumed" value="" required="" ></label>
<label for="catfish_kgs_consumed" class="col-md-2 col-form-label">KGs of Catfish Consumed:<input class="form-control" name="catfish_kgs_consumed" type="text" id="catfish_kgs_consumed" value="" required="" ></label>
</div>
<div class="form-group row" >

<label for="tilapia_kgs_spoilt" class="col-md-2 col-form-label">Kgs of Tilapia spoilt:<input class="form-control" name="tilapia_kgs_spoilt" type="text" id="tilapia_kgs_spoilt" value="" required="" ></label>
<label for="catfish_kgs_spoilt" class="col-md-2 col-form-label">Kgs of Catfish spoilt:<input class="form-control" name="catfish_kgs_spoilt" type="text" id="catfish_kgs_spoilt" value="" required="" ></label>


</div>

<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<input name="btnCancel" id="btnCancel" type="button" value="Back" class="btn btn-danger" onClick="window.location.href='view.php?v=Sales&id=<?php echo $fid; ?>';" />

</p>

</form>
</tbody>
</table>
</div>

</div>

</div>
</div>

</div>