<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
$agent= $_SESSION['user_id'];
$sqlgetcounty="SELECT county_of_operation from users where id='$agent'";
$rslt= dbQuery($dbConn,$sqlgetcounty);
while($row = dbFetchAssoc($rslt)) {
 extract($row);
}
$sql = "SELECT c.id as cid,c.farmer_id,c.year,c.tilapia_price_per_fingerling,c.catfish_price_per_fingerling,c.feeds_costs,c.fertilizer_cost,c.lime_cost,c.manila_twine_cost,c.fishing_net_cost,c.extension_services_cost,c.water_costs,c.transportation_cost,c.sampling_cost,c.equipment_cost,c.date_created,c.modified_by,c.modified_date,f.id,f.farm_county,f.tot_no_of_tilapia,f.tot_no_of_catfish,(f.tot_no_of_tilapia * c.tilapia_price_per_fingerling) as tilapia_cost,(f.tot_no_of_catfish * c.catfish_price_per_fingerling) as catfish_cost, ((f.tot_no_of_tilapia * c.tilapia_price_per_fingerling) + (f.tot_no_of_catfish * c.catfish_price_per_fingerling)) as total_cost,u.id from farm_costs c join farms f on c.farmer_id=f.farmer_id join users u on u.id=c.modified_by where f.farmer_id!='' and f.farm_county='$county_of_operation' group by c.farmer_id,c.year";

 $result = dbQuery($dbConn,$sql);

?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><font color="">Farm Costs &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; <a href="<?php echo WEB_ROOT; ?>csv/cost.csv" >Download</a>&ensp;CSV&ensp; &ensp; &ensp; &ensp; &ensp;&ensp; &ensp; &ensp; &ensp; &ensp;</font></h5><h4><?php echo $errorMessage; ?></h4>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">

            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
              <thead>
                  <tr>
                   <td><b>Farm ID</td>
                   <td><b>Year</td>
                   <td><b>County</td>
                   <td><b>Tilapia cost</td>
                   <td><b>Catfish Cost</td>
                   <td><b>Total Fingerling Cost</td>
                   <td><b>Feeds Cost</td>
                   <td><b>Fertilizer Cost</td>
                   <td><b>Lime Cost</td>
                   <td><b>Manila Twine Cost</td>
                   <td><b>Fishing Net Cost</td>
                   <td><b>Extension services cost</td>
                   <td><b>Water cost</td>
                   <td><b>Transportation cost</td>
                   <td><b>Sampling Cost</td>
                   <td><b>Equipment Cost</td>
                  </tr>
                  
                </thead>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>">
                   <td><a href="javascript:editfarmcosts(<?php echo $cid; ?>);"><?php echo $farmer_id; ?></a></td>
                   <td><?php echo $year; ?></td>
                   <td><?php echo $farm_county; ?></td>
                   <td><?php echo $tilapia_cost; ?></td>
                   <td><?php echo $catfish_cost; ?></td>
                   <td><?php echo $total_cost; ?></td>
                   <td><?php echo $feeds_costs; ?></td>
                   <td><?php echo $fertilizer_cost; ?></td>
                   <td><?php echo $lime_cost; ?></td>
                   <td><?php echo $manila_twine_cost; ?></td>
                   <td><?php echo $fishing_net_cost; ?></td>
                   <td><?php echo $extension_services_cost; ?></td>
                   <td><?php echo $water_costs; ?></td>
                   <td><?php echo $transportation_cost; ?></td>
                   <td><?php echo $sampling_cost; ?></td> 
                   <td><?php echo $equipment_cost; ?></td>
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
                  </table>
                  <form action="<?php echo WEB_ROOT; ?>farm/importcost.php" method="post" enctype="multipart/form-data" id="import_form">
                  <div class="col-md-3">
                  <input type="file" name="file" style="float: left;"/>
                  </div>
                  <div class="col-md-5">
                  <input type="submit" class="btn btn-primary" name="import_data" value="Import">
                  </form>
                  
                </div>

              </div>

            </div>
          </div>

        </div>
        <body id="body" style="overflow:hidden;">
          <div id="abc">
          <!-- Popup Div Starts Here -->
          <div class="col-sm-11">
          <div id="popupContact">
          <div class="col-sm-12">
          <table class="table table-striped" style="margin-top:40px">
                 <thead>
                        <th width="15%"><b>Name of Farmer</th>
                        <th width="15%"><b>Name of Farm</th>
                        <th width="15%"><b>Type of Cost</th>
                        <th width="15%"><b>Year</th>
                        <th width="15%"><b>Month</th>
                        <th width="15%"><b>Amount</th>
                        <th width="10%"><button class="btn btn-default btn-circle" id="close" type="button" onclick ="div_hide()"><i class="glyphicon glyphicon-remove"></i></button></th>

                  </thead>

                </table>
              </div>
              <div class="col-sm-12">
                <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=cost" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
            <div class="rowdata row1">
              <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
                <tbody>
                <tr>
                 <div class="form-group row">
                  
                  <td width="16%"><input type="text" class="farmer_id" name="farmer_id[]"  id="farmer_id" value="<?php getFarmer();?>" placeholder="Farmer name or ID" required="" /></td>
                  <td width="16%"><input type="text" class="form-control" name="farm_id[]"  id="farm_id" value="" required="" /></td>
                  <td width="16%"><input type="text" class="form-control" name="cost_type[]" id="cost_type" value="" required="" /></td>
                  <td width="16%"><input type="text" class="form-control" name="year[]"  id="year" value="" required="" /></td>
                  <td width="16%"><input type="text" class="form-control" name="month[]"  id="month" value="" required="" /></td>
                  <td width="16%"><input type="text" class="form-control" name="amount[]"  id="amount" value="" required="" /></td>
                  
                  <td width="2%"><button class="btn btn-success" id="AddRow" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
                  <td width="2%"><button class="btn btn-danger remove" id="deleteRow" type="button"><i class="glyphicon glyphicon-remove"></i></button></td>
                </div>
              </tr>
              </tbody>
              </table>

            </div>
            <span >
              <input name="submit" id="submit" type="submit" value="SUBMIT" class="btn btn-primary" />
            </span>
          </form>

          </div>
            
            
           
            
          </div>
          </div>
          <!-- Popup Div Ends Here -->
          </div>
          <!-- Display Popup Button -->

          </body>