<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$agent= $_SESSION['user_id'];
$sqlgetcounty="SELECT county_of_operation from users where id='$agent'";
$rslt= dbQuery($dbConn,$sqlgetcounty);
while($row = dbFetchAssoc($rslt)) {
 extract($row);
}
$sql = "SELECT f.id as fid,f.farmer_id as farmer,f.farm_county,f.farm_name,count(p.pond_number) as ponds_number,f.modified_by,f.date_modified,o.farmer_id,o.firstname,o.lastname,CASE WHEN p.pond_area IS NULL THEN '0' ELSE sum(p.pond_area) END AS tot_pond_area  from  farms f join farms_owners o on f.farmer_id=o.farmer_id left join farms_ponds p on f.farmer_id=p.farmer_id where o.farmer_id!='' and f.farm_county='$county_of_operation' group by o.farmer_id";
$result = dbQuery($dbConn,$sql);

?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><font color="">Farm Operations</font></h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">

            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
              <thead>
                  <tr>
                   <td><b>Farm ID</td>
                   <td><b>First Name</td>
                   <td><b>Last Name</td>
                   <td><b>Total Number of Ponds</td>
                   <td><b>Total Pond Size(M2)</td>
                    <td><b>County</td>
                  </tr>
                  
                </thead>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>">
                   <td><a href="javascript:farmoperationstable(<?php echo $fid; ?>);"><?php echo $farmer_id; ?></a></td>
                   <td><?php echo $firstname; ?></td>
                   <td><?php echo $lastname; ?></td>
                   <td><?php echo $ponds_number; ?></td>
                   <td><?php echo $tot_pond_area; ?></td>
                   <td><?php echo $farm_county; ?></td>
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
                  </table>
                </div>

              </div>

            </div>
          </div>

        </div>