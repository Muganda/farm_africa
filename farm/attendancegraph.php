
 <div class="wrapper wrapper-content">
        <div class="row">
          <div class="col-lg-5">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div>
                  <span class="pull-right text-right"><h3 class="font-bold no-margins"></h3>
                    <br/></span>
                  <h4 class="font-bold no-margins">Farmer per county</h4>
                </div>
                <div class="m-t-sm">
                  <div class="row">
                    <div class="">
                      <div id="">
                        <script type="text/javascript">
                      $(document).ready(function() {
                      var options = {
                      chart: {
                      renderTo: 'farmer',
                      type: 'column'
                      },
                      title: {
                      text: '',
                      x: -20 //center
                      },
                      subtitle: {
                      text: '',
                      x: -20
                      },
                      xAxis: {
                      categories: []
                      },
                      yAxis: {
                      title: {
                      text: 'Farmers'
                      },
                      plotLines: [{
                      value: 0,
                      width: 1,
                      color: '#808080'
                      }]
                      },
                      tooltip: {
                      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                      pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                      },
                      plotOptions: {
                      series: {
                      borderWidth: 0,
                      dataLabels: {
                      enabled: true,
                      format: '{point.y}'
                      }
                      }
                      },
                      legend: {
                      layout: 'vertical',
                      align: 'right',
                      verticalAlign: 'top',
                      x: -40,
                      y: 100,
                      floating: true,
                      borderWidth: 1,
                      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                      shadow: true
                      },
                      series: []
                      };

                      $.getJSON("library/farmerdata.php",  function(json) {
                      options.xAxis.categories = json[0]['data']; //xAxis: {categories: []}
                      options.series[0] = json[1];
                      chart = new Highcharts.Chart(options);
                      });

                      });
                      </script>
                      <div id="farmer">

                        </div>
                        
                      </div>
                    </div>
                    
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div>
                  <span class="pull-right text-right"><h3 class="font-bold no-margins"></h3>
                    <br/></span>
                  <h4 class="font-bold no-margins">Training attendance per module</h4>
                </div>
                <div class="row">
                    <div class="m-t-sm">
                     <div class="">
                      
                      <script type="text/javascript">
                      $(document).ready(function() {
                      
                      var options = {
                      chart: {
                      renderTo: 'container',
                      type: 'column'
                      },
                      title: {
                      text: '',
                      x: -20 //center
                      },
                      subtitle: {
                      text: '',
                      x: -20
                      },
                      xAxis: {
                      categories: []
                      },
                      yAxis: {
                      title: {
                      text: 'Farmers'
                      },
                      plotLines: [{
                      value: 0,
                      width: 1,
                      color: '#808080'
                      }]
                      },
                      tooltip: {
                      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                      pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                      },
                      plotOptions: {
                      series: {
                      borderWidth: 0,
                      dataLabels: {
                      enabled: true,
                      format: '{point.y}'
                      }
                      }
                      },
                      legend: {
                      layout: 'vertical',
                      align: 'right',
                      verticalAlign: 'top',
                      x: -40,
                      y: 100,
                      floating: true,
                      borderWidth: 1,
                      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                      shadow: true
                      },
                      series: []
                      };

                      $.getJSON("library/trainingdata.php",  function(json) {
                      options.xAxis.categories = json[0]['data']; //xAxis: {categories: []}
                      options.series[0] = json[1];
                      chart = new Highcharts.Chart(options);
                      });

                      });
                      </script>
                      <div id="container">
            
                      </div> 
                    </div>
                  </div>
                  </div>
                
              </div>
              
            </div>
          </div>
          <div class="col-lg-3">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div class="row">
                  <h4 class="font-bold no-margins">Number of ponds per county</h4>
                    <div class="col-md-12">

                     <div class="table-responsive " style="height: 57%; margin-top: 2px">
                    <table id="" class="table table-striped table-bordered table-sm"  style="width: 90%">
                      <thead >
                      <tr>
                          <th>Counties</th>
                          <th> Ponds</th>
                      </tr>
                    </thead>
                     <tbody >
                    <?php foreach ($counties as $key=> $county):?>
                    <tr >
                     <th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>
                    <td>
                      <?php
                          $pondsinfo =getpondsincounty($dbConn);
                            foreach ($pondsinfo as $key => $pond) {
                              if ($pond['farm_county'] == $county['id']) {
                                  echo $pond['no_total'];
                              }
                            }     
                        ?>
                    </td>
                      </tr>
                     <?php endforeach; ?>
                    </table>
                  </div>
                  </div>
                  </div>
                
              </div>
              
            </div>
          </div>

        </div>

        


      </div>

