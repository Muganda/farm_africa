<?php
require_once 'farmer/addtab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

// $sql = "SELECT id, farm_name FROM farms order by id desc
// limit 1";
// $result = dbQuery($dbConn,$sql);
// while($row = dbFetchAssoc($result)) {
//     extract($row);
//    }


?> 
<div class="prepend-1 span-12">
<h4>Add Contact Persons </h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-sm-12">
<table class="table table-striped ">
         <thead>
                <th></th>
                <th width="16%">Farmer ID</th>
                <th width="15%">First Name</th>
                <th width="15%">Last Name</th>
                <th width="14%">Telephone</th>
                <th width="20%">Email</th>
                <th width="12%">Gender</th>
                <th width="15%">Position</th>
                <th><button class="btn btn-success" onClick="addRow('dataTable')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
          <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button"><i class="glyphicon glyphicon-remove"></i></button></th>
                </thead>
        </table>
      </div>
      <div class="col-sm-12">
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=contact" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
               <td width="15%"><select  class="form-control" data-show-subtext="true" data-live-search="true" name="farmer_id[]" type="text" id="farmer_id" value="" required=""><?php getfarmerid($dbConn);?>
                  </select>
               </td>
               <td width="14%"><input type="text" class="form-control"  required="required" name="contact_first_name[]"/></td>
               <td width="14%"><input type="text" class="form-control" required="required" name="contact_last_name[]"/></td>
               <td width="13%"><input type="text" class="form-control"  class="small"  name="contact_telephone[]"/></td>
               <td><input type="text" class="form-control"  class="small"  name="contact_email[]"/></td>
               <td ><select id="contact_gender" name="contact_gender[]" class="form-control" required="required">
                        <option>....</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
               </td>
               <td><select id="contact_position" name="contact_position[]" class="form-control" required="required">
                        <option>....</option>
                        <option value="Owner">Owner</option>
                        <option value="Farm Manager">Farm Manager</option>
                        <option value="Farm Hand">Farm Hand</option>
                        <option value="Relative Owner">Relative Owner</option>
                        <option value="Other">Other</option>
                    </select>
               </td>
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <span>
      <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
      
      
      
    </span>
  </form>

</div>
</div>
