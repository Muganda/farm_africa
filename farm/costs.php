<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
$sql = "SELECT c.id as cid,c.farmer_id,c.pond_number as pond_id,p.pond_number as pond,c.year,c.tilapia_price_per_fingerling,c.catfish_price_per_fingerling,c.feeds_costs,c.fertilizer_cost,c.lime_cost,c.manila_twine_cost,c.fishing_net_cost,c.extension_services_cost,c.water_costs,c.transportation_cost,c.sampling_cost,c.equipment_cost,(p.tilapia_fingerling_stocked * c.tilapia_price_per_fingerling) as tilapia_cost,(p.catfish_fingerling_stocked * c.catfish_price_per_fingerling) as catfish_cost, ((p.tilapia_fingerling_stocked * c.tilapia_price_per_fingerling) + (p.catfish_fingerling_stocked * c.catfish_price_per_fingerling)) as total_cost from farm_costs c join farms_ponds p on c.pond_number=p.id where c.farmer_id!='' group by c.id,c.year";

 $result = dbQuery($dbConn,$sql);

?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><font color="">Operating Costs &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; <a href="<?php echo WEB_ROOT; ?>csv/cost.csv" >Download</a>&ensp;CSV&ensp; &ensp; &ensp; &ensp; &ensp;&ensp; &ensp; &ensp; &ensp; &ensp;</font></h5><h4><?php echo $errorMessage; ?></h4>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">

            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
              <thead>
                  <tr>
                   <td><b>Farmer ID</td>
                   <td><b>Pond </td>
                   <td><b>Year Cost incurred</td>
                   <td><b>Tilapia cost</td>
                   <td><b>Catfish Cost</td>
                   <td><b>Total Fingerling Cost</td>
                   <td><b>Feeds Cost</td>
                   <td><b>Fertilizer Cost</td>
                   <td><b>Lime Cost</td>
                   <td><b>Manila Twine Cost</td>
                   <td><b>Fishing Net Cost</td>
                   <td><b>Extension services cost</td>
                   <td><b>Water cost</td>
                   <td><b>Transportation cost</td>
                   <td><b>Sampling Cost</td>
                   <td><b>Equipment Cost</td>
                  </tr>
                  
                </thead>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>">
                   <td><a href="javascript:editfarmcosts(<?php echo $pond_id; ?>);"><?php echo $farmer_id; ?></a></td>
                   <td><?php echo $pond; ?></td>
                   <td><?php echo $year; ?></td>
                   <td><?php echo $tilapia_cost; ?></td>
                   <td><?php echo $catfish_cost; ?></td>
                   <td><?php echo $total_cost; ?></td>
                   <td><?php echo $feeds_costs; ?></td>
                   <td><?php echo $fertilizer_cost; ?></td>
                   <td><?php echo $lime_cost; ?></td>
                   <td><?php echo $manila_twine_cost; ?></td>
                   <td><?php echo $fishing_net_cost; ?></td>
                   <td><?php echo $extension_services_cost; ?></td>
                   <td><?php echo $water_costs; ?></td>
                   <td><?php echo $transportation_cost; ?></td>
                   <td><?php echo $sampling_cost; ?></td> 
                   <td><?php echo $equipment_cost; ?></td>
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
          </table>
  <form action="<?php echo WEB_ROOT; ?>farm/importcost.php" method="post" enctype="multipart/form-data" id="import_form">
  <div class="col-md-3">
  <input type="file" name="file" style="float: left;"/>
  </div>
  <div class="col-md-5">
  <input type="submit" class="btn btn-primary" name="import_data" value="Import">
  </form>
   <input name="btnAddUser" type="button" id="btnAddUser" value="Record Costs (+)" class="btn btn-default" onClick="addOpcost()">
  </div>

</div>

</div>
</div>

</div>
