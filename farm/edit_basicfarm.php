<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";

  $sql="SELECT f.farm_name,f.id as farm_id,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_landmark,f.keeps_records,f.farm_longitude,f.farm_latitude,f.record_keeping,f.records_kept,f.has_business_plan,f.business_plan_last_update,f.ponds_number,f.ponds_stocked,f.pond_sample_temperature,f.pond_sample_time,f.farmer_id, f.id,o.farmer_id,o.firstname,o.lastname,f.tot_no_of_tilapia,f.tot_no_of_catfish, o.f_id,c.id,c.county_name, s.id,s.subcounty_name FROM farms f inner join farms_owners o on f.farmer_id=o.farmer_id left join counties c on f.farm_county=c.id left join sub_counties s on f.farm_subcounty=s.id where f_id='$f_id' ";

$result = dbQuery($dbConn,$sql);
?> 

<div class="prepend-1 span-12">
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);

?>
<h4>&nbsp;&nbsp;&nbsp;Edit Basic Information for: <font color="blue"><?php echo $firstname; ?>,<?php echo $lastname; ?></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of Farm Identification Number: <font color="blue"><?php echo $farmer_id; ?></font></h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=basicinfo" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
 
   <div class="form-group row">
   <input class="form-control input-sm" name="f_id" type="hidden" id="f_id" value="<?php echo $f_id; ?>" readonly></label>
   <input class="form-control input-sm" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" readonly></label>
   </div>
    <!-- <div class="form-group row">
        <p onclick="geoFindMe()" type="button" class="btn btn-default btn-lg">
      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></p>
    <div id="out"></div>
  </div> -->
   <div class="form-group row">
   <label for="farm_name" class="col-sm-3 col-form-label">Name of Farm:<input class="form-control input-sm" name="farm_name" type="text" id="farm_name" value="<?php echo $farm_name; ?>" required="" ></label>
   <label class="col-sm-3 col-form-label">Upload Farm Photo:<input type="file" name="file" style="float: left;" class="btn btn-default"/></label>
   </div>
   <div class="form-group row" >
    <input class="form-control input-sm" name="farm_longitude" type="hidden" id="farm_longitude" value="<?php echo $farm_longitude; ?>" required="" >
   <input class="form-control input-sm" name="farm_latitude" type="hidden" id="farm_latitude" value="<?php echo $farm_latitude; ?>" required="" >
   <label for="farm_county" class="col-sm-3 col-form-label">County :<select class="form-control input-sm" name="farm_county" type="text" id="farm_county"  required="" ><option  value="<?php echo $farm_county; ?>"><?php echo $county_name; ?></option><?php getCounties($dbConn)?></select></label>
   <label for="farm_subcounty" class="col-sm-3 col-form-label">SubCounty:<select class="form-control input-sm" name="farm_subcounty" type="text" id="sel_depart"  required="" ><option  value="<?php echo $farm_subcounty; ?>"><?php echo $subcounty_name; ?></option><?php getSubcounties($dbConn)?></select></label>
   <label for="farm_village" class="col-sm-3 col-form-label">Village:<input class="form-control input-sm" name="farm_village" type="text" id="farm_village" value="<?php echo $farm_village; ?>" ></label> 
  </div>
  

   <div class="form-group row" >
    <label for="farm_landmark" class="col-sm-3 col-form-label">Landmark:<input class="form-control input-sm" name="farm_landmark" type="text" id="farm_landmark" value="<?php echo $farm_landmark; ?>"  ></label>
    <label for="ponds_number" class="col-sm-3 col-form-label">Total No. of Ponds:<input class="form-control input-sm" name="ponds_number" type="number" id="ponds_number" value="<?php echo $ponds_number; ?>" required="" ></label>
   <label for="ponds_stocked" class="col-sm-3 col-form-label">Ponds Stocked:<input class="form-control input-sm" name="ponds_stocked" type="number" id="ponds_stocked" value="<?php echo $ponds_stocked; ?>" required="" ></label>
   </div>
  <div class="form-group row" >
  <label for="records_kept" class="col-sm-3 col-form-label">Which records do you keep:<select class="form-control input-sm" id ="records_kept" name="records_kept">
    <option value="<?php echo $records_kept; ?>" ><?php echo $records_kept; ?></option>
    <option value="Manual">Manual</option>
    <option value="Automatic">Automatic</option>
    <option value="None">None</option>
    </select></label>
     <label for="record_keeping" class="col-sm-4 col-form-label">Have you recieved a record keeping book from KMAP:<select class="form-control input-sm" name="record_keeping" required="">
    <option value="<?php echo $record_keeping; ?>" ><?php echo $record_keeping; ?></option>
    <option value="Yes">Yes</option>
    <option value="No">No</option>
    </select></label>
  </div>

  </div>
  <div class="form-group row" >
  <label for="has_business_plan" class="col-sm-3 col-form-label">Does he/she have a business plan?</label>
    Yes <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="Yes" id="yes"> No <input type="radio" onclick="javascript:businessplan();" name="has_business_plan" value="No" id="no">
    <div id="iftrue" style="visibility:hidden">
    <label for="business_plan_last_update" class="col-sm-3 col-form-label">When was the Last time of business plan update:<input class="date form-control input-sm" name="business_plan_last_update" type="text" id="business_plan_last_update" value="<?php echo $business_plan_last_update; ?>" ></label>
        
  </div>
  
  </div>
  <div class="form-group row">
 <p align="left"> 
  <input name="submit" id="submit" type="submit" value="SUBMIT" class="btn btn-primary" />
  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=profile&id=<?php echo $f_id; ?>';" />
 </p>
 </div>

</form>
 </tbody>

</table>

</div>
<?php 

}//while
}else {
?>
<p><h4>Farm information for selected farmer does not exist.</h4></p>
<div class="form-group " >
 <p align="center"> 

      <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Farmer';" />
   
 </p>
 </div>
<?php 
} 
?>
</div>

