<?php
if (!defined('WEB_ROOT')) {
exit;
}
$errorMessage = "";
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
$s_id = (int)$_GET['id'];
} else {
header('Location: ../index.php');
}

$sql="SELECT  * from pond_stocking where id='$s_id'";
$sql = "SELECT s.id as sid,f.id as fid,s.pond_number as pondnumber,p.id,p.pond_number as name,s.expected_harvest_date,s.farmer_id,s.stocking_date,s.harvest_type,s.harvest_date,s.tilapia_fingerling_stocked,s.tilapia_harvested,s.avg_weight_tilapia_harvested,(s.tilapia_harvested * s.avg_weight_tilapia_harvested) as total_tilapia_weight, (s.tilapia_fingerling_stocked-s.tilapia_harvested) as rem_tilapia_pieces, s.catfish_fingerling_stocked,s.catfish_harvested,s.avg_weight_catfish_harvested,(s.catfish_harvested * s.avg_weight_catfish_harvested) as total_catfish_weight,(s.catfish_fingerling_stocked-s.catfish_harvested) as rem_catfish_pieces, (((s.tilapia_harvested + s.catfish_harvested)/(s.tilapia_fingerling_stocked + s.catfish_fingerling_stocked))*100) as percentage_harvested  from pond_stocking s join farms_ponds p on p.id=s.pond_number join farms f on s.farmer_id=f.farmer_id where s.id='$s_id'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
extract($row);
}
?>
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5><font color="">Record Harvest<?php echo $errorMessage; ?></font></h5>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">

<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
<tbody>
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editharvest" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

<div class="form-group row">
<input class="form-control" name="pondnumber" type="hidden" id="pondnumber" value="<?php echo $pondnumber; ?>" >
<input class="form-control" name="sid" type="hidden" id="sid" value="<?php echo $sid; ?>" >
<input class="form-control input-sm" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
<label for="name" class="col-md-2 col-form-label">Pond Number:<input class="form-control" name="name" type="text" id="name" value="<?php echo $name; ?>" readonly></label>
</div>
<div class="form-group row">
<label for="stocking_date" class="col-md-2 col-form-label">Date Stocked:<input class="date form-control input-sm" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly></label>
<label for="expected_harvest_date" class="col-md-2 col-form-label">Expected Date of Harvest:<input class="date form-control input-sm" name="expected_harvest_date" type="text" id="expected_harvest_date" value="<?php echo $expected_harvest_date; ?>" readonly ></label>
<label for="harvest_date" class="col-md-2 col-form-label">Date Harvested:<input class="date form-control input-sm" name="harvest_date" type="text" id="harvest_date" value=""  ></label>
<label for="harvest_type" class="col-md-2 col-form-label">Harvest type:<select class="form-control input-sm" name="harvest_type" value="<?php echo $harvest_type; ?>">
<option value="<?php echo $harvest_type; ?>"><?php echo $harvest_type; ?></option>
<option value="Partial">Partial</option>
<option value="Complete">Complete</option>
</select></label>
<!-- <input type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
<input type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
-->
</div>
<div class="form-group row">
<label for="tilapia_fingerling_stocked" class="col-md-2 col-form-label">Tilapia Pieces Stocked:<input class="form-control input-sm" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
<input class="form-control input-sm" name="tilapia_harvested" type="hidden" id="tilapia_harvested" value="<?php echo $tilapia_harvested; ?>" >
<label for="tilapia_harvest" class="col-md-2 col-form-label">Tilapia Pieces Harvested:<input class="form-control input-sm" name="tilapia_harvest" type='number' id='tilapiaharvest' onkeyup="validateTilapia()" onclick="validateTilapia()" value=""  >
<p class="text-danger" id="msg"></p>
</label>
<label for="rem_tilapia_pieces" class="col-md-2 col-form-label">Tilapia pieces remaining:<input class="form-control input-sm" name="rem_tilapia_pieces" type='number' id='remtilapiapieces' onkeyup="validateTilapia()" onclick="validateTilapia()" value="<?php echo $rem_tilapia_pieces; ?>" readonly ></label>
<label for="avg_weight_tilapia_harvested" class="col-md-2 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_tilapia_harvested" type="text" id="avg_weight_tilapia_harvested" value="<?php echo $avg_weight_tilapia_harvested; ?>" required="" ></label>

</div>
<div class="form-group row">
<label for="catfish_fingerling_stocked" class="col-md-2 col-form-label">Catfish Pieces Stocked:<input class="form-control input-sm" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
<input class="form-control input-sm" name="catfish_harvested" type="hidden" id="catfish_harvested" value="<?php echo $catfish_harvested; ?>" >
<label for="catfish_harvest" class="col-md-2 col-form-label">Catfish Pieces Harvested:<input class="form-control input-sm" name="catfish_harvest" type='number' id='catfishharvest' onkeyup="validateCatfish()" onclick="validateCatfish()" value=""  >
<p class="text-danger" id="message"></p>
</label>
<label for="rem_catfish_pieces" class="col-md-2 col-form-label">Catfish pieces remaining:<input class="form-control input-sm" name="rem_catfish_pieces" type='number' id='remcatfishpieces' onkeyup="validateCatfish()" onclick="validateCatfish()" value="<?php echo $rem_catfish_pieces; ?>" readonly ></label>
<label for="avg_weight_catfish_harvested" class="col-md-2 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_catfish_harvested" type="text" id="avg_weight_catfish_harvested" value="<?php echo $avg_weight_catfish_harvested; ?>" required="" ></label>

</div>
<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<input name="btnCancel" id="btnCancel" type="button" value="Back" class="btn btn-danger" onClick="window.location.href='view.php?v=Harvest&id=<?php echo $fid; ?>';" />

</p>

</form>
</tbody>
</table>
</div>

</div>

</div>
</div>

</div>