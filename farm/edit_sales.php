<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $s_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}

$farmerid="select farmer_id from farms where id='$s_id'";
$resultf = dbQuery($dbConn,$farmerid);
while($row = dbFetchAssoc($resultf)) {
  extract($row);
}

$fid = "SELECT id,pond_number  FROM farms_ponds where farmer_id='$farmer_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
  extract($row);
 $tabs[]=array("pond_number"=>$pond_number,"id"=>$id);
}

  ?>
<?php require_once 'operationstab.php';?>
<div class="container">

     <div class="row">
      <div class="col-sm-12">
        <div class="panel-body">
  <div class="tab-content">
  <div class="table-responsive">
    <div class="form-group row">
    <form action= "" method="post">
  <div class="col-sm-3">
  <select class="form-control input-sm" name="sales_year" style="margin-top: 15px;" ><?php getYear($dbConn);?></select>
  </div>
  <div class="col-sm-3">
  <select class="form-control input-sm" name="sales_month" style="margin-top: 15px;" ><?php getMonths($dbConn);?></select>
  </div>
  <div class="col-sm-3"><input name="submit" id="submit" type="submit" value="Filter" class="btn btn-primary" style="margin-top: 15px;" /></div>
</form>
</div>
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editsales" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                  if(isset($_POST['sales_year']) && $_POST['sales_year'] != '') {
                  $sales_year = $_POST['sales_year'];
                  $sql = "SELECT * from farm_sales where farmer_id='$farmer_id' and sales_year='$sales_year'";
                 };
                 if(isset($_POST['sales_month']) && $_POST['sales_month'] != '') {
                  $sales_month = $_POST['sales_month'];
                  $sql = "SELECT * from farm_sales where farmer_id='$farmer_id' and sales_month='$sales_month'";
                 };
                 $sql = "SELECT farmer_id from farms where  id='$s_id'";
                   $result = dbQuery($dbConn,$sql);
                   if(dbAffectedRows() == 1){
                   while($row = dbFetchAssoc($result)) {
                   extract($row);
                 ?>
               
                  
                <div class="form-group row">
                      <label for="total_harvest" class="col-md-3 col-form-label">Total Harvest(KGs):<input class="form-control" name="total_harvest" type="text" id="total_harvest" value="<?php echo $total_harvest; ?>" readonly></label>
                      </div>
                  
                      <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      <input class="form-control" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
                      
                      <label for="whole_tilapia_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_sold" type="text" id="whole_tilapia_fish_sold" value="<?php echo $whole_tilapia_fish_sold; ?>" required="" ></label>
                      <label for="whole_tilapia_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_avg_price_kg" type="text" id="whole_tilapia_fish_avg_price_kg" value="<?php echo $whole_tilapia_fish_avg_price_kg; ?>" required="" ></label>
                      <label for="whole_catfish_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_sold" type="text" id="whole_catfish_fish_sold" value="<?php echo $whole_catfish_fish_sold; ?>" required="" ></label>
                      <label for="whole_catfish_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_avg_price_kg" type="text" id="whole_catfish_fish_avg_price_kg" value="<?php echo $whole_catfish_fish_avg_price_kg; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="value_added_tilapia_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Tilapia Sold:<input class="form-control" name="value_added_tilapia_kgs_sold" type="text" id="value_added_tilapia_kgs_sold" value="<?php echo $value_added_tilapia_kgs_sold; ?>" required="" ></label>
                      <label for="tilapia_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Tilapia per Kg:<input class="form-control" name="tilapia_value_added_avg_price_kg" type="text" id="tilapia_value_added_avg_price_kg" value="<?php echo $tilapia_value_added_avg_price_kg; ?>" required="" ></label>
                      <label for="value_added_catfish_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Catfish Sold:<input class="form-control" name="value_added_catfish_kgs_sold" type="text" id="value_added_catfish_kgs_sold" value="<?php echo $value_added_catfish_kgs_sold; ?>" required="" ></label>
                      <label for="catfish_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Catfish per Kg:<input class="form-control" name="catfish_value_added_avg_price_kg" type="text" id="catfish_value_added_avg_price_kg" value="<?php echo $catfish_value_added_avg_price_kg; ?>" required="" ></label>
                      </div>
                     <div class="form-group row" >
                      <label for="tilapia_value_addition_form" class="col-md-3 col-form-label">Form of Tilapia Value addition:<input class="form-control" name="tilapia_value_addition_form" type="text" id="tilapia_value_addition_form" value="<?php echo $tilapia_value_addition_form; ?>" required="" ></label>
                      <label for="tilapia_kgs_consumed" class="col-md-3 col-form-label">KGs of Tilapia Consumed:<input class="form-control" name="tilapia_kgs_consumed" type="text" id="tilapia_kgs_consumed" value="<?php echo $tilapia_kgs_consumed; ?>" required="" ></label>
                      <label for="catfish_value_addition_form" class="col-md-3 col-form-label">Form of Catfish Value addition:<input class="form-control" name="catfish_value_addition_form" type="text" id="catfish_value_addition_form" value="<?php echo $catfish_value_addition_form; ?>" required="" ></label>
                      <label for="catfish_kgs_consumed" class="col-md-3 col-form-label">KGs of Catfish Consumed:<input class="form-control" name="catfish_kgs_consumed" type="text" id="catfish_kgs_consumed" value="<?php echo $catfish_kgs_consumed; ?>" required="" ></label>
                      </div>
                      <div class="form-group row" >
                      
                      <label for="tilapia_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Tilapia spoilt:<input class="form-control" name="tilapia_kgs_spoilt" type="text" id="tilapia_kgs_spoilt" value="<?php echo $tilapia_kgs_spoilt; ?>" required="" ></label>
                      <label for="catfish_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Catfish spoilt:<input class="form-control" name="catfish_kgs_spoilt" type="text" id="catfish_kgs_spoilt" value="<?php echo $catfish_kgs_spoilt; ?>" required="" ></label>
                      
                     
                    </div>
                <?php
                }
                }else{
                $sql = "SELECT farmer_id from farms   where id='$s_id' ";
                $result = dbQuery($dbConn,$sql);
                if(dbAffectedRows() == 1){
                while($row = dbFetchAssoc($result)){
                extract($row);
                ?>
                <div class="form-group row">
                      <label for="total_harvest" class="col-md-3 col-form-label">Total Harvest(KGs):<input class="form-control" name="total_harvest" type="text" id="total_harvest" value="<?php echo $total_harvest; ?>" required=""></label>
                      </div>
                  
                      <div class="form-group row">
                      <input class="form-control" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
                      <label for="whole_tilapia_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_sold" type="text" id="whole_tilapia_fish_sold" value="<?php echo $whole_tilapia_fish_sold; ?>" required="" ></label>
                      <label for="whole_tilapia_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_avg_price_kg" type="text" id="whole_tilapia_fish_avg_price_kg" value="<?php echo $whole_tilapia_fish_avg_price_kg; ?>" required="" ></label>
                      <label for="whole_catfish_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_sold" type="text" id="whole_catfish_fish_sold" value="<?php echo $whole_catfish_fish_sold; ?>" required="" ></label>
                      <label for="whole_catfish_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_avg_price_kg" type="text" id="whole_catfish_fish_avg_price_kg" value="<?php echo $whole_catfish_fish_avg_price_kg; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="value_added_tilapia_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Tilapia Sold:<input class="form-control" name="value_added_tilapia_kgs_sold" type="text" id="value_added_tilapia_kgs_sold" value="<?php echo $value_added_tilapia_kgs_sold; ?>" required="" ></label>
                      <label for="tilapia_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Tilapia per Kg:<input class="form-control" name="tilapia_value_added_avg_price_kg" type="text" id="tilapia_value_added_avg_price_kg" value="<?php echo $tilapia_value_added_avg_price_kg; ?>" required="" ></label>
                      <label for="value_added_catfish_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Catfish Sold:<input class="form-control" name="value_added_catfish_kgs_sold" type="text" id="value_added_catfish_kgs_sold" value="<?php echo $value_added_catfish_kgs_sold; ?>" required="" ></label>
                      <label for="catfish_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Catfish per Kg:<input class="form-control" name="catfish_value_added_avg_price_kg" type="text" id="catfish_value_added_avg_price_kg" value="<?php echo $catfish_value_added_avg_price_kg; ?>" required="" ></label>
                      </div>
                     <div class="form-group row" >
                      <label for="tilapia_value_addition_form" class="col-md-3 col-form-label">Form of Tilapia Value addition:<input class="form-control" name="tilapia_value_addition_form" type="text" id="tilapia_value_addition_form" value="<?php echo $tilapia_value_addition_form; ?>" required="" ></label>
                      <label for="tilapia_kgs_consumed" class="col-md-3 col-form-label">KGs of Tilapia Consumed:<input class="form-control" name="tilapia_kgs_consumed" type="text" id="tilapia_kgs_consumed" value="<?php echo $tilapia_kgs_consumed; ?>" required="" ></label>
                      <label for="catfish_value_addition_form" class="col-md-3 col-form-label">Form of Catfish Value addition:<input class="form-control" name="catfish_value_addition_form" type="text" id="catfish_value_addition_form" value="<?php echo $catfish_value_addition_form; ?>" required="" ></label>
                      <label for="catfish_kgs_consumed" class="col-md-3 col-form-label">KGs of Catfish Consumed:<input class="form-control" name="catfish_kgs_consumed" type="text" id="catfish_kgs_consumed" value="<?php echo $catfish_kgs_consumed; ?>" required="" ></label>
                      </div>
                      <div class="form-group row" >
                      
                      <label for="tilapia_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Tilapia spoilt:<input class="form-control" name="tilapia_kgs_spoilt" type="text" id="tilapia_kgs_spoilt" value="<?php echo $tilapia_kgs_spoilt; ?>" required="" ></label>
                      <label for="catfish_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Catfish spoilt:<input class="form-control" name="catfish_kgs_spoilt" type="text" id="catfish_kgs_spoilt" value="<?php echo $catfish_kgs_spoilt; ?>" required="" ></label>
                      
                     
                    </div>
                <?php
               
                } // end while
                }
              }
                ?>
                
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>  
  </div>
  </div> 
      </div>
     </div>
    

