<?php
if (!defined('WEB_ROOT')) {
exit;
}
$errorMessage = "";
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
$s_id = (int)$_GET['id'];
} else {
header('Location: ../index.php');
}

$sql="SELECT  * from pond_stocking where id='$s_id'";
$sql = "SELECT s.id as sid,f.id as fid,p.id,p.pond_number as name,s.pond_number as pondnumber,s.farmer_id,s.catfish_fingerling_stocked,s.tilapia_fingerling_stocked,s.stocking_date,c.tilapia_price_per_fingerling,c.catfish_price_per_fingerling,s.expected_harvest_date,s.fingerlings_source,s.tilapia_fingerling_stocking_weight,s.catfish_fingerling_stocking_weight from pond_stocking s join farms_ponds p on p.id=s.pond_number join farms f on s.farmer_id=f.farmer_id join farm_costs c on s.pond_number=c.pond_number  where s.id='$s_id'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
extract($row);
}
?>
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5><font color="">Edit Stock<?php echo $errorMessage; ?></font></h5>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">

<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
<tbody>
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editstock" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

<div class="form-group row">
<label for="name" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="name" type="text" id="name" value="<?php echo $name; ?>" readonly></label>
</div>

<div class="form-group row">

<input class="form-control" name="pondnumber" type="hidden" id="pondnumber" value="<?php echo $pondnumber; ?>" >
<input class="form-control" name="sid" type="hidden" id="sid" value="<?php echo $sid; ?>" >


<label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" required="" ></label>
<label for="fingerlings_source" class="col-md-3 col-form-label">Source Of Fingerlings:<input class="form-control" name="fingerlings_source" type="text" id="fingerlings_source" value="<?php echo $fingerlings_source; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" required="" ></label>
<label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
</div>
<!-- <div class="form-group row" >
<label for="tilapia_price_per_fingerling" class="col-md-3 col-form-label">Price per Tilapia Fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="text" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>" required="" ></label>
<label for="catfish_price_per_fingerling" class="col-md-3 col-form-label">Price per Catfish Fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="text" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>" required="" ></label>
</div> -->
<div class="form-group row" >

<label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Average weight in gms(Tilapia):<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" required="" ></label>
<label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Average weight in gms(Catfish):<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" required="" ></label>

</div>
<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<input name="btnCancel" id="btnCancel" type="button" value="Back" class="btn btn-danger" onClick="window.location.href='view.php?v=stocking&id=<?php echo $fid; ?>';" />
</p>

</form>
</tbody>
</table>
</div>

</div>

</div>
</div>

</div>