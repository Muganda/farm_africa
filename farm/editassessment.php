<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id,firstname,lastname  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT a.id as aid,a.farmer_id,a.assessor_name,a.date_assesed,a.remarks,a.recommendation,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join assessments a on f.farmer_id=a.farmer_id where f_id='$f_id' ";

$result = dbQuery($dbConn,$sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h4>&nbsp;&nbsp;&nbsp;Edit Extension Information for: <font color="blue"><?php echo $firstname; ?>,<?php echo $lastname; ?></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of Farm Identification Number: <font color="blue"><?php echo $farmer_id; ?></font></h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editassesments" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
<div class="table-responsive">
<table class="table table-striped table-bordered table-sm">
<thead>
                <th>#</th>
                <th>Date assessed</th>
                <th>Assesor Name</th>
                <th>Remarks/Observation</th>
                <th>Recommendation</th>
                <!-- <th></th> -->
                </thead>
   <tbody>
   
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  static $assesmntno=0;
  $assesmntno++;
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
    <input class="form-control" name="aid[]" type="hidden" id="aid" value="<?php echo $aid; ?>" readonly >
    <input class="form-control" name="fid" type="hidden" id="fid" value="<?php echo $f_id; ?>" readonly >
    <td><?php echo $assesmntno; ?></td>
     <td><input class="form-control" name="date_assesed[]" type="text" id="" value="<?php echo $date_assesed; ?>"></td>
     <td><input class="form-control" name="assessor_name[]" type="text" id="" value="<?php echo $assessor_name; ?>"></td>
     <td><input class="form-control" name="remarks[]" type="text" id="" value="<?php echo $remarks; ?>"></td>
     <td><input class="form-control" name="recommendation[]" type="text" id="" value="<?php echo $recommendation; ?>"></td>
   
  </tr>
<?php
} // end while

?>
    
</tbody></table>
</div>
<tr><td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Assesment infomation(+)" class="btn btn-default" onClick="div_showx()"></td></tr>
<tr><td colspan="24" align="center"><input name="submit" id="submit_popup" type="submit" value="SUBMIT" class="btn btn-primary" /></td></tr>
</form>
</div>

</div>
<div class="prepend-1 span-12">
<div class="col-md-12">
<tbody>

<body id="body" style="overflow:hidden;">
<div id="abcd" style="margin-top: -250px !important;">
<!-- Popup Div Starts Here -->
<div id="" class="" role="dialog" style="margin-top: 100px;" >
  <div class="modal-dialog " style="width: 75%; margin-left: 20%;">
    <!-- Start: Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <!-- <button type="button" class="close" onClick="div_hidex()">&times;</button> -->
        <h4 class="modal-title">Add assessment Information <div style="float: right;"><button class="btn btn-success" onClick="addRow('dataTable')" type="button" s><i class="glyphicon glyphicon-plus" ></i></button>
      <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button" ><i class="glyphicon glyphicon-remove"></i></button></div></h4>
      </div>
      
      <div class="modal-body" >
     <table class="table table-striped table-sm">
       <thead>
                <th width="3%"></th>
                <th width="25%">Date assessed</th>
                <th width="25%">Assesor Name</th>
                <th width="25%">Remarks</th>
                <th width="25%">Recommendation</th>
                </thead>
        </table>
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=doassesments" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <input type="hidden"  class="form-control" name="fid" value="<?php echo $f_id; ?>">
               <input type="hidden" class="form-control" name="farmerid" value="<?php echo $farmer_id; ?>"/>
               <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
               <td><input type="text" class="date form-control" required="required" name="date_assesed[]"/></td>
               <td><input type="text" class="form-control" required="required" name="assessor_name[]"/></td>
               <td><input type="text" class="form-control" required="required" name="remarks[]"/></td>
               <td><input type="text" class="form-control"   name="recommendation[]"/></td>
               
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <div class="modal-footer">
   
    <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
      <button type="button" class="btn btn-default" onClick="div_hidex()" >Close</button>
    </div>
  </form>
    
    </div>
    
      </div>
      
    </div>
  </div>
</div>
</body>    
</tbody>
</div>
</div>
    




