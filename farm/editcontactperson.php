<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id,firstname,lastname  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT c.id as cid,c.farmer_id as farmerid,c.contact_first_name,c.contact_last_name,c.contact_telephone,c.contact_email,c.contact_gender,c.contact_email,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_contact_persons c on f.farmer_id=c.farmer_id where f_id='$f_id' ";

  $result = dbQuery($dbConn,$sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h4>&nbsp;&nbsp;&nbsp;Edit Contact Person Information for: <font color="blue"><?php echo $firstname; ?>,<?php echo $lastname; ?></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of Farm Identification Number: <font color="blue"><?php echo $farmer_id; ?></font></h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editcontact" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser" role="form" class="form-inline" role="form" >
<table class="table table-striped table-bordered table-sm">
<thead>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Telephone</th>
                <th>Email</th>
                <th>Gender</th>
                
                <!-- <th></th> -->
                </thead>
    <tbody>
   
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  static $contactno=0;
  $contactno++;
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
    <input class="form-control" name="cid[]" type="hidden" id="cid" value="<?php echo $cid; ?>" readonly >
    <input class="form-control" name="fid" type="hidden" id="fid" value="<?php echo $f_id; ?>" readonly >
    <td><?php echo $contactno; ?></td>
    <td><input class="form-control" name="contact_first_name[]" type="text" id="" value="<?php echo $contact_first_name; ?>"></td>
     <td><input class="form-control" name="contact_last_name[]" type="text" id="" value="<?php echo $contact_last_name; ?>"></td>
     <td><input class="form-control" name="contact_telephone[]" type="text" id="" value="<?php echo $contact_telephone; ?>"></td>
    <td><input class="form-control" name="contact_email[]" type="text" id="" value="<?php echo $contact_email; ?>"></td>
     <td><select class="form-control" name="contact_gender[]">
          <option  value="<?php echo $contact_gender; ?>"><?php echo $contact_gender; ?></option>
          <?php getGender($dbConn);?>
          </select>
     </td>
   
  </tr>
<?php
} // end while

?>
    
</tbody></table>
<tr><td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Contact person(+)"  class="btn btn-default" onClick="div_showx()"></td></tr>
<tr><td colspan="24" align="center"><input name="submit" id="submit_popup" type="submit" value="SUBMIT" class="btn btn-primary" /></td></tr>
</form>
</div>

<div class="prepend-1 span-12">
<div class="col-md-12">
<tbody>

<body id="body" style="overflow:hidden;">
<div id="abcd" style="margin-top: -350px !important;">
<!-- Popup Div Starts Here -->
<div id="" class="" role="dialog" style="margin-top: 100px;" >
  <div class="modal-dialog " style="width: 75%; margin-left: 20%;">
    <!-- Start: Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <!-- <button type="button" class="close" onClick="div_hidex()">&times;</button> -->
        <h4 class="modal-title">Add Contact Person <div style="float: right;"><button class="btn btn-success" onClick="addRow('dataTable')" type="button" s><i class="glyphicon glyphicon-plus" ></i></button>
      <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button" ><i class="glyphicon glyphicon-remove"></i></button></div></h4>
      </div>
      
      <div class="modal-body" >
     <table class="table table-striped table-sm">
       <thead>
                <th></th>
                <th width="20%">First Name</th>
                <th width="15%">Last Name</th>
                <th width="20%">Email</th>
                <th width="15%">Telephone</th>
                <th width="15%">Gender</th>
                <th width="20%">Position</th>
                </thead>
        </table>
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=addcontact" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <input type="hidden"  class="form-control" name="fid" value="<?php echo $f_id; ?>">
               <input type="hidden" class="form-control" name="farmerid" value="<?php echo $farmer_id; ?>"/>
               <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
               <td><input type="text" class="form-control" required="required" name="contact_first_name[]"/></td>
               <td><input type="text" class="form-control" required="required" name="contact_last_name[]"/></td>
               <td><input type="text" class="form-control" required="required" name="contact_email[]"/></td>
               <td><input type="text" class="form-control"  class="small"  name="contact_telephone[]"/></td>
               <td><select id="contact_gender" name="contact_gender[]" class="form-control" required="required">
                        <option>....</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
               </td>
               <td><select id="contact_position" name="contact_position[]" class="form-control" required="required">
                        <option>....</option>
                        <option value="Owner">Owner</option>
                        <option value="Farm manager">Farm manager</option>
                        <option value="farm hand">farm hand</option>
                        <option value="Relative to owner">Relative to owner</option>
                        <option value="Other">Other</option>
                    </select>
               </td>
               
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <div class="modal-footer">
   
    <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
      <button type="button" class="btn btn-default" onClick="div_hidex()" >Close</button>
    </div>
  </form>
    
    </div>
    
      </div>
      
    </div>
  </div>
</div>
</body>    
</tbody>
</div>
</div>
    


