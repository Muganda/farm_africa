<?php
if (!defined('WEB_ROOT')) {
  exit;
}
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $id = (int)$_GET['id'];

} else {
  header('Location: ./index.php');
}
$fidr = "SELECT distinct(farmer_id) as cfarmer_id  FROM farms_ponds where id='$id'";
$result1 = dbQuery($dbConn,$fidr);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }

  // echo $cfarmer_id;
  // exit();
?> 
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div>
<div class="col-sm-3" style="margin-top: -15px;"><h5><font color="">Edit Cost 
  </font></h5><h4></h4></div>
  
</div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
    <div class="form-group row">
<form action= "" method="post">
  <div class="col-sm-2">
    <?php
    $sqlp = "SELECT id,pond_number from farms_ponds  where farmer_id='$cfarmer_id'";
    $resultp = dbQuery($dbConn,$sqlp);
    ?>
    <select class="form-control input-sm" name="pond"  >
      <option>Filter by Pond</option>
    <?php while($row = dbFetchAssoc($resultp)) {
      extract($row);
      ?>
      <option value="<?php echo $id; ?>"><?php echo $pond_number; ?></option>
      <?php
    }
    ?></select>
  </div>
  <div class="col-sm-2">
  <select class="form-control input-sm" name="year"  >
  <option>Filter by Year</option>
    <?php getYear($dbConn);?>
    </select>
  </div>
  <div class="col-sm-2">
  <select class="form-control input-sm" name="month"  >
    <option>Filter by Month</option>
    <?php getMonths($dbConn);?>
  </select>
  </div>
  
  <div class="col-sm-2"><input name="submit" id="submit" type="submit" value="Filter" class="btn btn-primary"  /></div>
</form> 
</div>
<div class="row">

  <?php
  
  
   if(isset($_POST['year']) && $_POST['year'] != '') {
    $year = $_POST['year'];
    $sql = "SELECT c.month,f.id as fid,c.farmer_id,f.farmer_id as farmerid,c.pond_number as pond_id,p.pond_number as pondname,f.farm_name,c.year,c.tilapia_price_per_fingerling,c.catfish_price_per_fingerling,c.feeds_costs,c.fertilizer_cost,c.lime_cost,c.manila_twine_cost,c.fishing_net_cost,c.extension_services_cost,c.water_costs,c.transportation_cost,c.sampling_cost,c.equipment_cost,c.date_created,c.modified_by,c.modified_date,f.tot_no_of_tilapia,f.tot_no_of_catfish,f.tot_no_of_tilapia * c.tilapia_price_per_fingerling as tilapia_cost,f.tot_no_of_catfish * c.catfish_price_per_fingerling as catfish_cost, (f.tot_no_of_tilapia * c.tilapia_price_per_fingerling) + (f.tot_no_of_catfish * c.catfish_price_per_fingerling) as total_cost from farm_costs c join farms f on c.farmer_id=f.farmer_id join farms_ponds p on p.id=c.pond_number where  c.year='$year' and c.farmer_id='$cfarmer_id'";
   };
  if(isset($_POST['month']) && $_POST['month'] != '') {
    $month = $_POST['month'];
    $sql = "SELECT c.month,f.id as fid,c.farmer_id,f.farmer_id as farmerid,c.pond_number as pond_id,p.pond_number as pondname,f.farm_name,c.year,c.tilapia_price_per_fingerling,c.catfish_price_per_fingerling,c.feeds_costs,c.fertilizer_cost,c.lime_cost,c.manila_twine_cost,c.fishing_net_cost,c.extension_services_cost,c.water_costs,c.transportation_cost,c.sampling_cost,c.equipment_cost,c.date_created,c.modified_by,c.modified_date,f.tot_no_of_tilapia,f.tot_no_of_catfish,f.tot_no_of_tilapia * c.tilapia_price_per_fingerling as tilapia_cost,f.tot_no_of_catfish * c.catfish_price_per_fingerling as catfish_cost, (f.tot_no_of_tilapia * c.tilapia_price_per_fingerling) + (f.tot_no_of_catfish * c.catfish_price_per_fingerling) as total_cost from farm_costs c join farms f on c.farmer_id=f.farmer_id join farms_ponds p on p.id=c.pond_number where  c.month='$month' and c.farmer_id='$cfarmer_id' ";
   };
   if(isset($_POST['pond']) && $_POST['pond'] != '') {
    $pond = $_POST['pond'];
    $sql = "SELECT c.month,f.id as fid,c.farmer_id,f.farmer_id as farmerid,c.pond_number as pond_id,p.pond_number as pondname,f.farm_name,c.year,c.tilapia_price_per_fingerling,c.catfish_price_per_fingerling,c.feeds_costs,c.fertilizer_cost,c.lime_cost,c.manila_twine_cost,c.fishing_net_cost,c.extension_services_cost,c.water_costs,c.transportation_cost,c.sampling_cost,c.equipment_cost,c.date_created,c.modified_by,c.modified_date,f.tot_no_of_tilapia,f.tot_no_of_catfish,f.tot_no_of_tilapia * c.tilapia_price_per_fingerling as tilapia_cost,f.tot_no_of_catfish * c.catfish_price_per_fingerling as catfish_cost, (f.tot_no_of_tilapia * c.tilapia_price_per_fingerling) + (f.tot_no_of_catfish * c.catfish_price_per_fingerling) as total_cost from farm_costs c join farms f on c.farmer_id=f.farmer_id join farms_ponds p on p.id=c.pond_number where c.pond_number='$pond'and c.farmer_id='$cfarmer_id' ";
   };
  

$result = dbQuery($dbConn,$sql);
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);


?>
         
<table class="table table-striped table-bordered">

   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editcost" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    

  <div class="form-group row">
    <input class="form-control" name="pond_id" type="hidden" id="pond_id" value="<?php echo $pond_id; ?>" >
  <label for="farmerid" class="col-sm-3 col-form-label">Farm ID:<input class="form-control" name="farmerid" type="text" id="farmerid" value="<?php echo $farmerid; ?>" readonly></label>
  <label for="farm_name" class="col-sm-3 col-form-label">Name of Farm:<input class="form-control" name="farm_name" type="text" id="farm_name" value="<?php echo $farm_name; ?>" readonly ></label>
  <label for="pondname" class="col-sm-3 col-form-label">Pond Name:<input class="form-control" name="pondname" type="text" id="pondname" value="<?php echo $pondname; ?>" readonly ></label>
</div>
<div class="form-group row" >
  <label for="month" class="col-sm-3 col-form-label">Month:<input class="date-own form-control" name="month" type="text" id="month" value="<?php echo $month; ?>" readonly ></label>
  <label for="year" class="col-sm-3 col-form-label">Year:<input class="date-own form-control" name="year" type="text" id="year" value="<?php echo $year; ?>" readonly ></label>
  <label for="manila_twine_cost" class="col-sm-3 col-form-label">Manilla Twine cost:<input class="form-control" name="manila_twine_cost" type="number" id="manila_twine_cost" value="<?php echo $manila_twine_cost; ?>"  ></label>
</div>
<div class="form-group row">
  <label for="feeds_costs" class="col-sm-3 col-form-label">Feeds Cost per Kg:<input class="form-control" name="feeds_costs" type="text" id="feeds_costs" value="<?php echo $feeds_costs; ?>"  ></label>
  <label for="feeds_kgs_bought" class="col-sm-3 col-form-label">Total Kgs of Feeds:<input class="form-control" name="feeds_kgs_bought" type="text" id="feeds_kgs_bought" value="<?php echo $feeds_kgs_bought; ?>" ></label>
  <label for="fertilizer_cost" class="col-sm-3 col-form-label">Fertilizer Cost:<input class="form-control" name="fertilizer_cost" type="number" id="fertilizer_cost" value="<?php echo $fertilizer_cost; ?>"  ></label>
</div>
<div class="form-group row">
<label for="catfish_price_per_fingerling" class="col-sm-3 col-form-label">Catfish Price per fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="number" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>"  ></label>
  <label for="tilapia_price_per_fingerling" class="col-sm-3 col-form-label">Tilapia Price per fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="number" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>"  ></label>
  <label for="lime_cost" class="col-sm-3 col-form-label">Lime Cost:<input class="form-control" name="lime_cost" type="number" id="lime_cost" value="<?php echo $lime_cost; ?>"  ></label>
</div>
<div class="form-group row">
  <label for="fishing_net_cost" class="col-sm-3 col-form-label">Fishing net cost:<input class="form-control" name="fishing_net_cost" type="number" id="fishing_net_cost" value="<?php echo $fishing_net_cost; ?>"  ></label>
  <label for="extension_services_cost" class="col-sm-3 col-form-label">Extension services cost:<input class="form-control" name="extension_services_cost" type="number" id="extension_services_cost" value="<?php echo $extension_services_cost; ?>"  ></label>
  <label for="sampling_cost" class="col-sm-3 col-form-label">Sampling Cost:<input class="form-control" name="sampling_cost" type="number" id="sampling_cost" value="<?php echo $sampling_cost; ?>"  ></label>
</div>
<!-- <div class="form-group row">
  <label for="equipment_cost" class="col-sm-3 col-form-label">Equipment cost:<input class="form-control" name="equipment_cost" type="number" id="equipment_cost" value="<?php  $equipment_cost; ?>"  ></label>
  <label for="water_costs" class="col-sm-3 col-form-label">Water costs:<input class="form-control" name="water_costs" type="number" id="water_costs" value="<?php  $water_costs; ?>"  ></label>
  <label for="transportation_cost" class="col-sm-3 col-form-label">Transportation cost:<input class="form-control" name="transportation_cost" type="number" id="transportation_cost" value="<?php  $transportation_cost; ?>"  ></label>
  
</div> -->


<div class="form-group row" >
 <p align="left"> 
  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Costs';" />
 </p>
 </div>
</form>
 </tbody>
</table>
<?php 

}//while
}else {
  
?>
<h4 class="text-danger">No Matching Records Found</h4>

<?php 
}
?>
</div>
</div>         
          

 </div>

</div>

</div>

</div>
</div>