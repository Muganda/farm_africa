<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id,firstname,lastname  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT s.id as sid,s.farmer_id as farmerid,s.staff_role,s.staff_age,s.staff_gender,s.contract_type,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_staff s on f.farmer_id=s.farmer_id where f_id='$f_id' ";

$result = dbQuery($dbConn,$sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h4>&nbsp;&nbsp;&nbsp;Edit Staff Information for: <font color="blue"><?php echo $firstname; ?>,<?php echo $lastname; ?></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of Farm Identification Number: <font color="blue"><?php echo $farmer_id; ?></font></h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
  <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editstaff" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
<table class="table table-striped table-bordered table-sm">
<thead>
                
                <th>#</th>
                <th>Role</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Contract type</th>
                <!-- <th></th> -->
                </thead>
   <tbody>
   
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  static $staffno=0;
  $staffno++;
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
    <input class="form-control" name="sid[]" type="hidden" id="sid" value="<?php echo $sid; ?>" readonly >
    <input class="form-control" name="f_id" type="hidden" id="f_id" value="<?php echo $f_id; ?>" readonly >
    <td><?php echo $staffno; ?></td>
     <td><input class="form-control" name="staff_role[]" type="text" id="" value="<?php echo $staff_role; ?>"></td>
     <td><select class="form-control" name="staff_gender[]">
          <option  value="<?php echo $staff_gender; ?>"><?php echo $staff_gender; ?></option>
          <?php getGender($dbConn);?>
          </select>
     </td>
     <td><input class="form-control" name="staff_age[]" type="text" id="" value="<?php echo $staff_age; ?>"></td>
     <td><select id="contract_type" name="contract_type[]" class="form-control" required="required">
        <option><?php echo $contract_type; ?></option>
        <option value="Permanent">Permanent</option>
        <option value="Temporary">Temporary</option>
        </select>
     </td>
   
  </tr>
<?php
} // end while

?>
    
</tbody></table>
<tr><td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Staff(+)" class="btn btn-default" onClick="div_showx()"></td></tr>
<tr><td colspan="24" align="center"><input name="submit" id="submit_popup" type="submit" value="SUBMIT" class="btn btn-primary" /></td></tr>
</form>
</div>


</div>

<div class="prepend-1 span-12">
<div class="col-md-12">
<tbody>

<body id="body" style="overflow:hidden;">
<div id="abcd" style="margin-top: -322px !important;">
<!-- Popup Div Starts Here -->
<div id="" class="" role="dialog" style="margin-top: 100px;" >
  <div class="modal-dialog " style="width: 60%; margin-left: 25%;">
    <!-- Start: Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <!-- <button type="button" class="close" onClick="div_hidex()">&times;</button> -->
        <h4 class="modal-title">Add Staff <div style="float: right;"><button class="btn btn-success" onClick="addRow('dataTable')" type="button" s><i class="glyphicon glyphicon-plus" ></i></button>
      <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button" ><i class="glyphicon glyphicon-remove"></i></button></div></h4>
      </div>
      
      <div class="modal-body" >
     <table class="table table-striped table-sm">
       <thead>
                <th></th>
                <th width="30%">Role</th>
                <th width="30%">Gender</th>
                <th width="30%">Age</th>
                <th width="30%">Contract type</th>
               
                </thead>
        </table>
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=addstaff" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <input type="hidden"  class="form-control" name="fid" value="<?php echo $f_id; ?>">
               <input type="hidden" class="form-control" name="farmerid" value="<?php echo $farmer_id; ?>"/>
               <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
               <td><input type="text" class="form-control" required="required" name="staff_role[]"/></td>
               <td><input type="text" class="form-control" required="required" name="staff_gender[]"/></td>
               <td><input type="text" class="form-control" required="required" name="staff_age[]"/></td>
               <td><select id="contract_type" name="contract_type[]" class="form-control" required="required">
                        <option>....</option>
                        <option value="fulltime">fulltime</option>
                        <option value="Parttime">Parttime</option>
                    </select>
               </td>
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <div class="modal-footer">
   
    <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
      <button type="button" class="btn btn-default" onClick="div_hidex()" >Close</button>
    </div>
  </form>
    
    </div>
    
      </div>
      
    </div>
  </div>
</div>
</body>
</tbody>
</div>
</div>

