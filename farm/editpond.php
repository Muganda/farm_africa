<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id,firstname,lastname  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT p.id as pid,p.farmer_id,p.pond_number,p.pond_area,p.tilapia_fingerling_stocked,p.stocking_date,p.catfish_fingerling_stocked,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_ponds p on f.farmer_id=p.farmer_id where f_id='$f_id' ";

$result = dbQuery($dbConn,$sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h4>&nbsp;&nbsp;&nbsp;Edit Pond Information for: <font color="blue"><?php echo $firstname; ?>,<?php echo $lastname; ?></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of Farm Identification Number: <font color="blue"><?php echo $farmer_id; ?></font></h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editpond" method="post" enctype="multipart/form-data" name="popform" id="popform" role="form" class="form-inline" role="form" >
<table class="table table-striped table-bordered table-sm">
                <thead>
                  <th>#</th>
                <th>Pond Name/ID</th>
                <th>Pond area(M2)</th>
                <th>Catfish Stocked</th>
                <th>Tilapia Stocked</th>
                <th>Stocking date</th>
                </thead>
   <tbody>
   
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  static $pondno=0;
  $pondno++;
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>"> 
    <input class="form-control" name="pid[]" type="hidden" id="pid" value="<?php echo $pid; ?>" readonly >
    <input class="form-control" name="f_id" type="hidden" id="f_id" value="<?php echo $f_id; ?>" readonly >
    <td><?php echo $pondno; ?></td>
     <td><input class="form-control" name="pond_number[]" type="text" id="" value="<?php echo $pond_number; ?>"></td>
     <td><input class="form-control" name="pond_area[]" type="text" id="" value="<?php echo $pond_area; ?>"></td>
     <td><input class="form-control" name="catfish_fingerling_stocked[]" type="text" id="" value="<?php echo $catfish_fingerling_stocked; ?>" readonly></td>
     <td><input class="form-control" name="tilapia_fingerling_stocked[]" type="text" id="" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly></td>
     <td><input class="form-control" name="stocking_date[]" type="text" id="" value="<?php echo $stocking_date; ?>" readonly></td>
     
   
  </tr>
<?php
} // end while

?>
    
</tbody></table>
<tr><td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Pond(+)" class="btn btn-default" onClick="div_showx()"></td></tr>
<tr><td colspan="24" align="center"><input name="submit" id="submit_popup" type="submit" value="SUBMIT" class="btn btn-primary" /></td></tr>
</form>

</div>
<div class="prepend-1 span-12">
<div class="col-md-12">
<tbody>



<body id="body" style="overflow:hidden; ">
<div id="abcd" style="margin-top: -400px !important;">
<!-- Popup Div Starts Here -->
<div id="" class="" role="dialog" style="margin-top: 100px;" >
  <div class="modal-dialog " style="width: 75%; margin-left: 20%;">
    <!-- Start: Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <!-- <button type="button" class="close" onClick="div_hidex()">&times;</button> -->
        <h4 class="modal-title">Add Pond <div style="float: right;"><button class="btn btn-success" onClick="addRow('dataTable')" type="button" s><i class="glyphicon glyphicon-plus" ></i></button>
      <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button" ><i class="glyphicon glyphicon-remove"></i></button></div></h4>
      </div>
      
      <div class="modal-body" >
     <table class="table table-striped table-sm">
       <thead>
                <th></th>
                <th >Pond Name</th>
                <th >Pond Size</th>
                
                </thead>
        </table>
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=addnpond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <input type="hidden"  class="form-control" name="fid" value="<?php echo $f_id; ?>">
               <input type="hidden" class="form-control" name="farmerid" value="<?php echo $farmer_id; ?>"/>
               <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
               <td><input type="text" class="form-control" required="required" name="pond_number[]"/></td>
               <td><input type="text" class="form-control" required="required" name="pond_area[]"/></td>
               <!-- <td><input type="text" class="form-control"  name="catfish_fingerling_stocked[]"/></td>
               <td><input type="text" class="form-control"  name="tilapia_fingerling_stocked[]"/></td>
               <td><input type="text" class="form-control"  name="stocking_date[]"></td> -->
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <div class="modal-footer">
   
    <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
      <button type="button" class="btn btn-default" onClick="div_hidex()" >Close</button>
    </div>
  </form>
    
    </div>
    
      </div>
      
    </div>
  </div>
</div>
</body>    
</tbody>
</div>
</div>
    




</div>
