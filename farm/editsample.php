<?php
if (!defined('WEB_ROOT')) {
  exit;
}
if(isset($_GET['id']) && (int)$_GET['id']>0){
$s_id = (int)$_GET['id'];
}else{
  header('Location: ../index.php');
}
$errorMessage = "";

$farmerid="select farmer_id from farms where id='$s_id'";
$resultf = dbQuery($dbConn,$farmerid);
while($row = dbFetchAssoc($resultf)) {
  extract($row);
}

$fid = "SELECT id,pond_number  FROM farms_ponds where farmer_id='$farmer_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
  extract($row);
 $tabs[]=array("pond_number"=>$pond_number,"id"=>$id);
}
  ?>
<?php require_once 'operationstab.php';?>
<div class="container">

     <div class="row">
      <div class="col-sm-12">
        <div class="panel-heading" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
        <div id="tabs">
        <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
                <?php

                $tabTitle = "";
                $tabTitleClass = "";

                foreach($tabs as $tab) {

                   if($i == 0) {
                       $tabTitleClass = "active";
                   } else {
                       $tabTitleClass = "";
                   }

                   /* Build our tab pane title */
                   $tabTitle = '<li class="'.$tabTitleClass.'"><a href="#'.$tab['id'].'" data-toggle="tab">'.$tab['pond_number'].'</a></li>';

                   echo $tabTitle;

                   $i++;

                } ?>
         </ul>

       </div>
              
    </div>
    <div class="panel-body">
        
      <div class="tab-content">
      <?php
      foreach($tabs as $tab) {
          

              if($j == 0) {
                  
                  ?>

                  <div id="<?php echo $tab['id']; ?>" class="tab-pane fade in active">
              
                 
               <div class="prepend-1 span-17">
                
                <div class="table-responsive">
                
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editsample" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

               <?php
               $sql = "SELECT id,farmer_id,pond_number,sample_date,tot_tilapia_sample_weight,(tot_tilapia_sample_weight/no_of_tilapia_sampled) as avg_tilapia_sample_weight,no_of_tilapia_sampled,tot_catfish_sample_weight,(tot_catfish_sample_weight/no_of_catfish_sampled) as avg_catfish_sample_weight,no_of_catfish_sampled,tilapia_fingerling_stocked,catfish_fingerling_stocked,tilapia_fingerling_stocking_weight,catfish_fingerling_stocking_weight,stocking_date from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."' ";
                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                extract($row);
               ?>
                  
                <div class="form-group row">
                      <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $farm_id; ?>" >
                      <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                      </div>
                      <div class="form-group row">
                      <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly ></label>
                      <label for="sample_date" class="col-md-3 col-form-label">Date Sampled:<input class="date form-control" name="sample_date" type="text" id="sample_date" value="<?php echo $sample_date; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia Stocked:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
                      <label for="no_of_tilapia_sampled" class="col-md-3 col-form-label">Number of Tilapia Sampled:<input class="form-control" name="no_of_tilapia_sampled" type="text" id="no_of_tilapia_sampled" value="<?php echo $no_of_tilapia_sampled; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish Stocked:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" readonly ></label>
                      <label for="no_of_catfish_sampled" class="col-md-3 col-form-label">Number of Catfish Sampled:<input class="form-control" name="no_of_catfish_sampled" type="text" id="no_of_catfish_sampled" value="<?php echo $no_of_catfish_sampled; ?>" required="" ></label>
                      </div>
                     
                      <div class="form-group row" >
                      <label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Tilapia Fingerlings Stocking Weight:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_tilapia_sample_weight" class="col-md-3 col-form-label">Total Weight of Tilapia Sampled:<input class="form-control" name="tot_tilapia_sample_weight" type="text" id="tot_tilapia_sample_weight" value="<?php echo $tot_tilapia_sample_weight; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Catfish Fingerlings Stocking Weight:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_catfish_sample_weight" class="col-md-3 col-form-label">Total Weight of Catfish Sampled:<input class="form-control" name="tot_catfish_sample_weight" type="text" id="tot_catfish_sample_weight" value="<?php echo $tot_catfish_sample_weight; ?>" required="" ></label>
                     
                    </div>
            
              <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Sampling';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
                
               </div>
                  <?php


              } else {
                  // $tabContentClass = "tab-pane fade";
                  ?>
                  <div id="<?php echo $tab['id']; ?>" class="tab-pane fade">
               <div class="prepend-1 span-17">
                <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editsample" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                  $sql = "SELECT id,farmer_id,pond_number,sample_date,tot_tilapia_sample_weight,(tot_tilapia_sample_weight/no_of_tilapia_sampled) as avg_tilapia_sample_weight,no_of_tilapia_sampled,tot_catfish_sample_weight,(tot_catfish_sample_weight/no_of_catfish_sampled) as avg_catfish_sample_weight,no_of_catfish_sampled,tilapia_fingerling_stocked,catfish_fingerling_stocked,tilapia_fingerling_stocking_weight,catfish_fingerling_stocking_weight,stocking_date from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."' ";

                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                   extract($row);
                 ?>
               
                  
                     <div class="form-group row">
                      <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $farm_id; ?>" >
                      <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                      </div>
                      <div class="form-group row">
                      <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly ></label>
                      <label for="sample_date" class="col-md-3 col-form-label">Date Sampled:<input class="date form-control" name="sample_date" type="text" id="sample_date" value="<?php echo $sample_date; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia Stocked:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
                      <label for="no_of_tilapia_sampled" class="col-md-3 col-form-label">Number of Tilapia Sampled:<input class="form-control" name="no_of_tilapia_sampled" type="text" id="no_of_tilapia_sampled" value="<?php echo $no_of_tilapia_sampled; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish Stocked:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" readonly ></label>
                      <label for="no_of_catfish_sampled" class="col-md-3 col-form-label">Number of Catfish Sampled:<input class="form-control" name="no_of_catfish_sampled" type="text" id="no_of_catfish_sampled" value="<?php echo $no_of_catfish_sampled; ?>" required="" ></label>
                      </div>
                     
                      <div class="form-group row" >
                      <label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Tilapia Fingerlings Stocking Weight:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_tilapia_sample_weight" class="col-md-3 col-form-label">Total Weight of Tilapia Sampled:<input class="form-control" name="tot_tilapia_sample_weight" type="text" id="tot_tilapia_sample_weight" value="<?php echo $tot_tilapia_sample_weight; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Catfish Fingerlings Stocking Weight:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_catfish_sample_weight" class="col-md-3 col-form-label">Total Weight of Catfish Sampled:<input class="form-control" name="tot_catfish_sample_weight" type="text" id="tot_catfish_sample_weight" value="<?php echo $tot_catfish_sample_weight; ?>" required="" ></label>
                     
                    </div>
                <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Sampling';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
               </div>
                  <?php
              }


                    $j++;
                
            }
        ?>
        </div>
        </div>

       </div>
      </div>
     </div>
    
<?php
  
  echo '</ul>';
  


?>
  
