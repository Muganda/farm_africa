<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$sql = "SELECT o.f_id,o.farmer_id,o.firstname,o.lastname,f.id,f.farm_name,f.farmer_id  FROM farms_owners o join farms f on o.farmer_id=f.farmer_id where f.id='$f_id'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
  extract($row);

  }
 

?>

<div class="container">
  <?php require_once 'farm/farminfotab.php';?>
  <div class="tab-content">
    
    <div id="home" class="tab-pane fade in active">
      <h2 class="catHead">Cost Information for :<?php echo $farm_name; ?></h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=cost" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  
<div class="form-group row">
  <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $id; ?>" readonly>
  <label for="firstname" class="col-sm-3 col-form-label">Name of Farmer:<input class="form-control" name="firstname" type="text" id="firstname" value="<?php echo $lastname; ?>" readonly ></label>
  <label for="lastname" class="col-sm-3 col-form-label">Name of Farmer:<input class="form-control" name="lastname" type="text" id="lastname" value="<?php echo $lastname; ?>" readonly ></label>
</div>
<div class="form-group row">
  <label for="tilapia_price_per_fingerling" class="col-sm-3 col-form-label">Tilapia Price Per Fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="number" id="tilapia_price_per_fingerling" value="" required="" ></label>
  <label for="catfish_price_per_fingerling" class="col-sm-3 col-form-label">Catfish Price Per Fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="number" id="catfish_price_per_fingerling" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="month" class="col-sm-3 col-form-label">Month:<select class="form-control" name="month" type="text" id="month"  required="" ><option  value=""></option><?php getMonths()?></select></label>
  <label for="year" class="col-sm-3 col-form-label">Year:<input class="date-own form-control" name="year" type="text" id="year" value="" required="" ></label>
</div>

<!-- <div class="form-group row" >
  <label for="amount" class="col-sm-3 col-form-label">Amount:<input class="form-control" name="amount" type="text" id="amount" value="" required="" ></label>
</div> -->
  
 
 <p align="left"> 
  <input name="submit" id="submit" type="submit" value="SUBMIT" class="btn btn-primary" />
  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=USER';" />
   
 </p>
</form>
 </tbody>
</table>
</div>


</div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h2 class="catHead">Sales Information for:<?php echo $farm_name; ?></h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=sales" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  
<div class="form-group row">
  <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $id; ?>" readonly>
  <label for="farm_name" class="col-sm-3 col-form-label">Name of Farmer:<input class="form-control" name="farm_name" type="text" id="farm_name" value="<?php echo $owner_name; ?>" readonly ></label>
</div>
<div class="form-group row">
  <label for="whole_fish_sold" class="col-sm-3 col-form-label">Whole Fish sold:<input class="form-control" name="whole_fish_sold" type="number" id="whole_fish_sold" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="whole_fish_avg_price_kg" class="col-sm-3 col-form-label">Average price(Kshs):<input class="form-control" name="whole_fish_avg_price_kg" type="number" id="whole_fish_avg_price_kg" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="value_added_fish_sold" class="col-sm-3 col-form-label">Value added fish sold:<input class="form-control" name="value_added_fish_sold" type="number" id="value_added_fish_sold" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="value_added_avg_price_kg" class="col-sm-3 col-form-label">Value added average price/kg(Kshs):<input class="form-control" name="value_added_avg_price_kg" type="number" id="value_added_avg_price_kg" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="fish_type" class="col-sm-3 col-form-label">Fish Type:<select class="form-control" name="fish_type" type="text" id="fish_type"  required="" ><option  value=""></option><?php getFishtype()?></select></label>
</div>
  
 
 <p align="left"> 
  <button class="btn btn-success" name="submit" id="" type="submit" ><i class="glyphicon glyphicon-save"></i></button>
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="btn btn-danger"  value="cancel" onClick="window.location.href='view.php?v=USER';" >    
 </p>
</form>
 </tbody>
</table>
</div>


</div>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h2 class="catHead">Harvest Information for: <?php echo $farm_name; ?></h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=harvest" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  
<div class="form-group row">
  <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $id; ?>" readonly>
  <label for="farm_name" class="col-sm-3 col-form-label">Name of Farmer:<input class="form-control" name="farm_name" type="text" id="farm_name" value="<?php echo $owner_name; ?>" readonly ></label>
</div>
<div class="form-group row">
  <label for="pond_number" class="col-sm-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="" required="" ></label>
  <label for="havest_since_last_visit" class="col-sm-3 col-form-label">Harvest Since Last Visit:<input class="form-control" name="havest_since_last_visit" type="text" id="havest_since_last_visit" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="harvest_date" class="col-sm-3 col-form-label">Harvest Date:<input class="date form-control" name="harvest_date" type="text" id="harvest_date" value="" required="" ></label>
  <label for="harvest_type" class="col-sm-3 col-form-label">Harvest Type:<input class="form-control" name="harvest_type" type="text" id="harvest_type" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="pieces_harvested" class="col-sm-3 col-form-label">Pieces Harvested:<input class="form-control" name="pieces_harvested" type="text" id="pieces_harvested" value="" required="" ></label>
  <label for="avg_weight_piece" class="col-sm-3 col-form-label">Avg Weight Piece:<input class="form-control" name="avg_weight_piece" type="text" id="avg_weight_piece" value="" required="" ></label>
</div>

<div class="form-group row" >
  <label for="total_weight_kg" class="col-sm-3 col-form-label">Total Weight KG:<input class="form-control" name="total_weight_kg" type="text" id="total_weight_kg" value="" required="" ></label>
   <label for="feed_type" class="col-sm-3 col-form-label">Feed Type:<select class="form-control" name="feed_type" type="text" id="feed_type"  required="" ><option  value=""></option><?php getFeedtype()?></select></label>
</div>
<div class="form-group row" >
  <label for="production_cycle" class="col-sm-3 col-form-label">Production Cycle:<input class="form-control" name="production_cycle" type="text" id="production_cycle" value="" required="" ></label>
  <label for="fcr" class="col-sm-3 col-form-label">FCR:<input class="form-control" name="fcr" type="text" id="fcr" value="" required="" ></label>
</div>
<div class="form-group row" >
  <p align="left"> 
    <button class="btn btn-success" name="submit" id="" type="submit" ><i class="glyphicon glyphicon-save"></i></button>
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="btn btn-danger"  value="cancel" onClick="window.location.href='view.php?v=USER';" >   

 </p>
</div> 
 
</form>
 </tbody>
</table>
</div>


</div>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h2 class="catHead">Feeds Information for: <?php echo $farm_name; ?></h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=sampling" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  
<div class="form-group row">
  <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $id; ?>" readonly>
  <label for="farm_name" class="col-sm-3 col-form-label">Name of Farmer:<input class="form-control" name="farm_name" type="text" id="farm_name" value="<?php echo $owner_name; ?>" readonly ></label>
  <label for="pond_number" class="col-sm-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="remaining_pieces" class="col-sm-3 col-form-label">Remaining Pieces:<input class="form-control" name="remaining_pieces" type="text" id="remaining_pieces" value="" required="" ></label>
  <label for="last_sample_date" class="col-sm-3 col-form-label">Last Sample Date:<input class="date form-control" name="last_sample_date" type="text" id="last_sample_date" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="sample_weight" class="col-sm-3 col-form-label">Sample Weight:<input class="form-control" name="sample_weight" type="text" id="sample_weight" value="" required="" ></label>
  <label for="main_feed_type" class="col-sm-3 col-form-label">Main Feed Type:<select class="form-control" name="main_feed_type" type="text" id="main_feed_type"  required="" ><option  value=""></option><?php getFeedtype()?></select></label>
</div>

<div class="form-group row" >
  <label for="kgs_feed" class="col-sm-3 col-form-label">Weight in KGs of Feed:<input class="form-control" name="kgs_feed" type="text" id="kgs_feed" value="" required="" ></label>
   <label for="fcr" class="col-sm-3 col-form-label">FCR:<input class="form-control" name="fcr" type="text" id="fcr" value="" required="" ></label>
</div>

  
 
 <p align="left"> 
  <button class="btn btn-success" name="submit" id="" type="submit" ><i class="glyphicon glyphicon-save"></i></button>
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="btn btn-danger"  value="cancel" onClick="window.location.href='view.php?v=USER';" >  
 </p>
</form>
 </tbody>
</table>
</div>


</div>
    </div>
    <div id="menu4" class="tab-pane fade">
      <h2 class="catHead">Market Information for: <?php echo $farm_name; ?></h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>user/processUser.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  
<div class="form-group row">
  <input class="form-control" name="farm_id" type="hidden" id="farm_id" value="<?php echo $id; ?>" readonly>
  <label for="owner_name" class="col-sm-3 col-form-label">Name of Farmer:<input class="form-control" name="owner_name" type="text" id="owner_name" value="<?php echo $owner_name; ?>" readonly ></label>
</div>
<div class="form-group row">
  <label for="cost_type" class="col-sm-3 col-form-label">Type of Cost:<input class="form-control" name="cost_type" type="text" id="cost_type" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="year" class="col-sm-3 col-form-label">Year:<input class="date-own form-control" name="year" type="text" id="year" value="" required="" ></label>
</div>
<div class="form-group row" >
  <label for="month" class="col-sm-3 col-form-label">Month:<select class="form-control" name="month" type="text" id="month"  required="" ><option  value=""></option><?php getMonths()?></select></label>
</div>
<div class="form-group row" >
  <label for="amount" class="col-sm-3 col-form-label">Amount:<input class="form-control" name="amount" type="text" id="amount" value="" required="" ></label>
</div>
  
 
 <p align="left"> 
  <button class="btn btn-success" name="submit" id="" type="submit" ><i class="glyphicon glyphicon-save"></i></button>
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="btn btn-danger"  value="cancel" onClick="window.location.href='view.php?v=USER';" >  
 </p>
</form>
 </tbody>
</table>
</div>


</div>
    </div>
  </div>
  </div>
 
</div>
