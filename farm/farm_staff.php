<?php
require_once 'farmer/addtab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

?> 
<div class="prepend-1 span-12">
<h4>&nbsp&nbsp&nbsp&nbsp&nbspAdd Staff </h4>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-sm-10">
<table class="table table-striped ">
         <thead>
          <th></th>
                <th width="16%">Farmer ID</th>
                <th width="14%">Gender</th>
                <th width="25%">Age</th>
                <th width="26%">Role</th>
                <th width="25%">Contract type</th>
                <th><button class="btn btn-success" onClick="addRow('dataTable')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
          <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button"><i class="glyphicon glyphicon-remove"></i></button></th>
                </thead>
        </table>
      </div>
      <div class="col-sm-9">
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=staff" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
          <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
            <td width="15%"><select  class="form-control" data data-live-search="true"  name="farmer_id[]" type="text" id="farmer_id" value="" required=""><?php getfarmerid($dbConn);?>
                  </select>
               </td>
            <td><select id="staff_gender" name="staff_gender[]" class="form-control" required="required">
                                <option>....</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option></select></td>
            <td><input type="text" class="form-control" required="required" class="small"  name="staff_age[]"></td>
            <td><input type="text" class="form-control" required="required" name="staff_role[]"></td>
            <td><select id="contract_type" name="contract_type[]" class="form-control" required="required">
                                <option>....</option>
                                <option value="Permanent">Permanent</option>
                                <option value="Temporary">Temporary</option></select></td>

            
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <span>
      <input name="submit" id="submit" type="submit" value="SUBMIT" class="btn btn-primary" />
      
       
    </span>
  </form>

</div>
    
    
   
    
</div>
