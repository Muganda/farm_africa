<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$agent= $_SESSION['user_id'];
$sqlgetcounty="SELECT county_of_operation from users where id='$agent'";
$rslt= dbQuery($dbConn,$sqlgetcounty);
while($row = dbFetchAssoc($rslt)) {
 extract($row);
}
$sql = "SELECT f.id as fid,f.farmer_id as farmer,f.farm_name,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_landmark,f.records_kept,f.business_plan_last_update,count(p.pond_number) as ponds_number,f.modified_by,f.date_modified,CASE WHEN p.tilapia_fingerling_stocked IS NULL THEN '0' ELSE sum(p.tilapia_fingerling_stocked) END AS tot_no_of_tilapia,c.id,c.county_name,s.id,s.subcounty_name,o.farmer_id,o.firstname,o.lastname,e.main_feeds,CASE WHEN p.catfish_fingerling_stocked IS NULL THEN '0' ELSE sum(p.catfish_fingerling_stocked) END AS tot_no_of_catfish,CASE WHEN p.pond_area IS NULL THEN '0' ELSE sum(p.pond_area) END AS tot_pond_area  from  farms f join farms_owners o on f.farmer_id=o.farmer_id left join counties c on f.farm_county=c.id left join sub_counties s on f.farm_subcounty = s.id left join farms_ponds p on f.farmer_id=p.farmer_id left join farms_extended_data e on f.farmer_id=e.farmer_id where o.farmer_id!='' and f.farm_county='$county_of_operation' group by f.farmer_id";
$result = dbQuery($dbConn,$sql);

?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><font color="">Farm Summary</font></h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">

            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
              <thead>
                  <tr>
                   <td><b>Farm ID</td>
                   <td><b>First Name</td>
                   <td><b>Last Name</td>
                   <td><b>Name of Farm</td>
                   <td><b>County</td>
                   <!--<td><b>Sub County</td>
                   <td><b>Village</td>
                   <td><b>Landmarks</td>
                   <!-<td><b>Records Kept</td> -->
                   <!-- <td><b>Records Kept</td>
                   <td><b>Last Time B/Plan was updated</td> -->
                   <td><b>Total No. of Ponds</td>
                   <td><b>Total No. of Tilapia</td>
                   <td><b>Total No. of Catfish</td>
                   <td><b>Ponds total area(M2)</td>
                    <td><b>Main Feed Used</td>
                   <!-- <td><b>Modified By</td>
                   <td><b>Date modified</td>
                   <td>Delete&nbsp;/&nbsp;Update</td> -->
                  </tr>
                  
                </thead>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>"> 
                   <td><?php echo $farmer; ?></td>
                   <td><?php echo $firstname; ?></td>
                   <td><?php echo $lastname; ?></td>
                   <td><?php echo $farm_name; ?></a></td>
                   <td><?php echo $farm_county; ?></td>
                   <!--<td><?php echo $subcounty_name; ?></td>
                   <td><?php echo $farm_village; ?></td>
                   <td><?php echo $farm_landmark; ?></td>
                   <td><?php echo $records_kept; ?></td>
                  <td><?php echo $main_feeds; ?></td>
                   <td><?php echo $records_kept; ?></td>
                   <td><?php echo $business_plan_last_update; ?></td> */?> -->
                   <td><?php echo $ponds_number; ?></td>
                   <td><?php echo $tot_no_of_tilapia; ?></td>
                   <td><?php echo $tot_no_of_catfish; ?></td>
                   <td><?php echo $tot_pond_area; ?></td>
                   <td><?php echo $main_feeds; ?></td>
                   <!-- <td><?php echo $name; ?></td>
                   <td><?php echo $date_modified; ?></td> 
                   <td align="center"><a  style="font-weight:normal;" href="">Delete</a>&nbsp;/&nbsp;<a  style="font-weight:normal;" href="javascript:editLab();">Edit</a></td> -->
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
                  </table>
                  <!-- tr><td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Farm(+)"  class="button" onClick="addfarm()"></td></tr> -->
                </div>

              </div>

            </div>
          </div>

        </div>