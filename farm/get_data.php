<?php
require_once '../library/config.php';
require_once '../library/database.php';
$filter_input = $_POST['filter_input'];
//$where = "where name like '%".$filter_input."%' or email like '%".$filter_input."%' or phone like '%".$filter_input."%'";
$where = "where f.farm_county ='".$filter_input."'";
$sql = "SELECT f.id as fid,f.farmer_id as farmer,f.farm_name,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_landmark,f.records_kept,f.business_plan_last_update,count(p.pond_number) as ponds_number,f.modified_by,f.date_modified,CASE WHEN p.tilapia_fingerling_stocked IS NULL THEN '0' ELSE sum(p.tilapia_fingerling_stocked) END AS tot_no_of_tilapia,c.id,c.county_name,s.id,s.subcounty_name,o.farmer_id,o.firstname,o.lastname,e.main_feeds,CASE WHEN p.catfish_fingerling_stocked IS NULL THEN '0' ELSE sum(p.catfish_fingerling_stocked) END AS tot_no_of_catfish,CASE WHEN p.pond_area IS NULL THEN '0' ELSE sum(p.pond_area) END AS tot_pond_area  from  farms f join farms_owners o on f.farmer_id=o.farmer_id left join counties c on f.farm_county=c.id left join sub_counties s on f.farm_subcounty = s.id left join farms_ponds p on f.farmer_id=p.farmer_id left join farms_extended_data e on f.farmer_id=e.farmer_id $where group by f.farmer_id";
$res = mysqli_query($dbConn,$sql);
$num_rows = mysqli_num_rows($res);

if ($num_rows > 0) {
    // output data of each row
    while($result = mysqli_fetch_array($res)) {
		
        echo $html = "<tr id='new_row'>
                         <td> ". $result['farmer']."</td>
                <td> ". $result['firstname']."</td>
                <td> ". $result['lastname']."</td>
                <td> ". $result['farm_name']."</td>
                <td> ". $result['county_name']."</td>
                <td> ". $result['ponds_number']."</td>
                <td> ". $result['tot_no_of_tilapia']."</td>
                <td> ". $result['tot_no_of_catfish']."</td>
                <td> ". $result['tot_pond_area']."</td>
                <td> ". $result['main_feeds']."</td>
					</tr>";
    }
} else {
    echo "No Record Found";
} 

?>