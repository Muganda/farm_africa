<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms where id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  $farmerid=$farmer_id;
}
$sql = "SELECT s.id as sid,s.pond_number,p.id,p.pond_number as name,s.farmer_id,s.stocking_date,s.harvest_type,s.expected_harvest_date,s.harvest_date,s.tilapia_fingerling_stocked,s.tilapia_harvested,s.avg_weight_tilapia_harvested,(s.tilapia_harvested * s.avg_weight_tilapia_harvested) as total_tilapia_weight, (s.tilapia_fingerling_stocked-s.tilapia_harvested) as rem_tilapia_pieces, s.catfish_fingerling_stocked,s.catfish_harvested,s.avg_weight_catfish_harvested,(s.catfish_harvested * s.avg_weight_catfish_harvested) as total_catfish_weight,(s.catfish_fingerling_stocked-s.catfish_harvested) as rem_catfish_pieces, (((s.tilapia_harvested + s.catfish_harvested)/(s.tilapia_fingerling_stocked + s.catfish_fingerling_stocked))*100) as percentage_harvested  from pond_stocking s join farms_ponds p on p.id=s.pond_number where s.farmer_id='$farmerid' order by sid";

$result = dbQuery($dbConn,$sql);

?>
<?php require_once 'farm_operationstab.php';?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <!-- <h5><font color="">Harvesting&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5> -->
          <h4><?php echo $errorMessage; ?></h4>

          <div class="table-responsive">
            <table id="paging" class="table table-striped table-bordered">
              
                  <thead>
                  <tr>
                   <td><b>#</td>
                   <td><b>Pond No</td>
                   <td><b>Stocking Date</td>
                   <td><b>Expected Harvest Date</td>
                   <td><b>Harvest Date</td>
                   <td><b>Harvest Type</td>
                   <td><b>Stocked Tilapia</td>
                   <td><b>Tilapia Harvested</td>
                   <td><b>Average tilapia weight</td>
                   <td><b>Total tilapia weight</td>
                   <td><b>Remaining tilapia pieces</td>
                   <td><b>Stocked Catfish</td>
                   <td><b>Catfish Harvested</td>
                   <td><b>Average Catfish weight</td>
                   <td><b>Total Catfish weight</td>
                   <td><b>Remaining Catfish pieces</td>
                   <td><b>Percentage of pond harvested</td>
                   

                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  while($row = dbFetchAssoc($result)) {
                  extract($row);
                  static $harvestno=0;
                  $harvestno++;
                  if ($i%2) {
                    $class = 'row1';
                  } else {
                    $class = 'row2';
                  }

                  ?>

                  <tr class="<?php echo $class; ?>">
                    <td><?php echo $harvestno; ?></td>
                    <td><a href="javascript:harvest(<?php echo $sid; ?>);"><?php echo $name; ?></a></td>
                   <td><?php echo $stocking_date; ?></td>
                   <?php
                  $td = "";
                  $tdClass = "";
                  $date1 = strtotime($expected_harvest_date);
                  $date2 = strtotime(date("Y/m/d"));
                  $secs = $date1 - $date2;// == <seconds between the two times>
                  $days = $secs / 86400;
                   if($days >= 0 && $days <=30) {
                       $tdClass = "warning";
                   } else if($days < 0 ){
                      $tdClass = "danger";
                   }
                   else {
                       $tdClass = "";
                   }
                   $td = '<td class="'.$tdClass.'">'.$expected_harvest_date.'</td>';
                   echo $td;
                   ?>
                   <td><?php echo $harvest_date; ?></td>
                   <td><?php echo $harvest_type; ?></td>
                   <td><?php echo $tilapia_fingerling_stocked; ?></td>
                   <td><?php echo $tilapia_harvested; ?></td>
                   <td><?php echo $avg_weight_tilapia_harvested; ?></td>
                   <td><?php echo $total_tilapia_weight; ?></td>
                   <td><?php echo $rem_tilapia_pieces; ?></td>
                   <td><?php echo $catfish_fingerling_stocked; ?></td>
                   <td><?php echo $catfish_harvested; ?></td>
                   <td><?php echo $avg_weight_catfish_harvested; ?></td>
                   <td><?php echo $total_catfish_weight; ?></td>
                   <td><?php echo $rem_catfish_pieces; ?></td>
                   <td><?php echo number_format((float)$percentage_harvested,2); ?>%</td>
                  </tr>
                  <?php
                  } // end while

                  ?>

                  </tbody>
                </table>
          Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/harvest.csv" >Harvesting</a>&ensp;CSV
          <form action="<?php echo WEB_ROOT; ?>farm/importharvest.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Add Harvests (+)" onclick="javascript:addharvest(<?php echo $f_id; ?>)" style="float: right;">
            </form>
          
        </div>

      </div>

    </div>
  </div>

</div>