<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
if(isset($_POST['import_data'])){
$modified_by=$_SESSION['user_id'];
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
if(is_uploaded_file($_FILES['file']['tmp_name'])){
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');

$requiredHeaders = array('Farmer ID','Feeds Cost','Fertilizer Cost','Lime Cost','Mannila Twine Cost','Fishing Net Cost','Extension Services Cost','Water Cost','Transportation Cost','Sampling Cost','Equipment Cost','Date Recorded'); //headers we expect
$firstLine = fgets($csv_file); //get first line of csv file
//fclose($csv_file);
$foundHeaders = str_getcsv(trim($firstLine), ',', '"'); //parse to array
if ($foundHeaders !== $requiredHeaders) {
   header('Location: ../view.php?v=Costs&msg=<p class="text-danger">Headers do not match</p>');
   //echo 'Headers do not match: '.implode(', ', $foundHeaders);
   die();
}
else{
$skip = 0;
while(($data = fgetcsv($csv_file)) !== FALSE){
if($skip != 0){
// Check if record exists
$sql_query = "SELECT farmer_id,date_created

FROM farm_costs 
WHERE farmer_id = '".$data[0]."'  and date_created = '".$data[11]."'";
$resultset = dbQuery($dbConn, $sql_query);
// if record exists update otherwise insert new record
if(mysqli_num_rows($resultset)) {
$sql_update = "UPDATE farm_costs 
set 
feeds_costs='".$data[1]."',
fertilizer_cost='".$data[2]."',
lime_cost='".$data[3]."',
manila_twine_cost='".$data[4]."',
fishing_net_cost='".$data[5]."', 
extension_services_cost='".$data[6]."',
water_costs='".$data[7]."',
transportation_cost='".$data[8]."',
sampling_cost='".$data[9]."',
equipment_cost='".$data[10]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[0]."' and date_created = '".$data[11]."'";
dbQuery($dbConn, $sql_update);
} else{
$mysql_insert = "INSERT INTO farm_costs 
(
farmer_id, 
feeds_costs,
fertilizer_cost,
lime_cost,
manila_twine_cost,
fishing_net_cost,
extension_services_cost,
water_costs,
transportation_cost,
sampling_cost,
equipment_cost,
date_created,
modified_by,date_modified 
)
VALUES
(
'".$data[0]."',
'".$data[1]."',
'".$data[2]."',
'".$data[3]."', 
'".$data[4]."',
'".$data[5]."', 
'".$data[6]."',
'".$data[7]."',
'".$data[8]."',
'".$data[9]."',
'".$data[10]."',
'".$data[11]."' ,
'".$modified_by."',NOW()
)";
dbQuery($dbConn, $mysql_insert);
}
}
$skip ++;
}

}


fclose($csv_file);
$import_status = '<p class="text-success">Records uploaded succesfully</p>';
} else {
$import_status = '<p class="text-danger">Error  uploading records</p>';
}
} else {
$import_status = '<p class="text-danger">Invalid file used</p>';
}
}
header("Location: ../view.php?v=Costs&msg=".$import_status);
?>