<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
if(isset($_POST['import_data'])){
$modified_by=$_SESSION['user_id'];
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
if(is_uploaded_file($_FILES['file']['tmp_name'])){
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');

$requiredHeaders = array('Farmer ID', 'Total Harvest(Kgs)','Tilapia Sold(Whole)','Price per Kg','Kgs of Value added sold','Price per Kg(Value added fish)','Form of Value addition','Kgs Consumed','Kgs spoilt','Catfish Sold(Whole)','Price per Kg','Kgs of Value added sold','Price per Kg(Value added fish)','Form of Value addition','Kgs Consumed','Kgs spoilt','Date'); //headers we expect
$firstLine = fgets($csv_file); //get first line of csv file
//fclose($csv_file);
$foundHeaders = str_getcsv(trim($firstLine), ',', '"'); //parse to array
if ($foundHeaders !== $requiredHeaders) {
   header('Location: ../view.php?v=Farmoperationstable&id=1&msg=<p class="text-danger">Headers do not match</p>');
   //echo 'Headers do not match: '.implode(', ', $foundHeaders);
   die();
}
else{
$skip = 0;
while(($data = fgetcsv($csv_file)) !== FALSE){
if($skip != 0){
// Check if record exists
$sql_query = "SELECT farmer_id,date_created

FROM farm_sales 
WHERE farmer_id = '".$data[0]."'  and date_created = '".$data[16]."'";
$resultset = dbQuery($dbConn, $sql_query);
// if record exists update otherwise insert new record
if(mysqli_num_rows($resultset)) {
$sql_update = "UPDATE farm_sales 
set 
total_harvest='".$data[1]."',
whole_tilapia_fish_sold='".$data[2]."',
whole_tilapia_fish_avg_price_kg='".$data[3]."',
value_added_tilapia_kgs_sold='".$data[4]."',
tilapia_value_added_avg_price_kg='".$data[5]."', 
tilapia_value_addition_form='".$data[6]."',
tilapia_kgs_consumed='".$data[7]."',
tilapia_kgs_spoilt='".$data[8]."',
whole_catfish_fish_sold='".$data[9]."',
whole_catfish_fish_avg_price_kg='".$data[10]."',
value_added_catfish_kgs_sold='".$data[11]."',
catfish_value_added_avg_price_kg='".$data[12]."',
catfish_value_addition_form='".$data[13]."',
catfish_kgs_consumed='".$data[14]."',
catfish_kgs_spoilt='".$data[15]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[0]."' and date_created = '".$data[16]."'";
dbQuery($dbConn, $sql_update);
} else{
$mysql_insert = "INSERT INTO farm_sales 
(
farmer_id, 
total_harvest,
whole_tilapia_fish_sold,
whole_tilapia_fish_avg_price_kg,
value_added_tilapia_kgs_sold,
tilapia_value_added_avg_price_kg,
tilapia_value_addition_form,
tilapia_kgs_consumed,
tilapia_kgs_spoilt,
whole_catfish_fish_sold,
whole_catfish_fish_avg_price_kg,
value_added_catfish_kgs_sold,
catfish_value_added_avg_price_kg,
catfish_value_addition_form,
catfish_kgs_consumed,
catfish_kgs_spoilt,
date_created,
modified_by,date_modified 
)
VALUES
(
'".$data[0]."',
'".$data[1]."',
'".$data[2]."',
'".$data[3]."', 
'".$data[4]."',
'".$data[5]."', 
'".$data[6]."',
'".$data[7]."',
'".$data[8]."',
'".$data[9]."',
'".$data[10]."',
'".$data[11]."' ,
'".$data[12]."',
'".$data[13]."',
'".$data[14]."',
'".$data[15]."',
'".$data[16]."' ,
'".$modified_by."',NOW()
)";
dbQuery($dbConn, $mysql_insert);
}
}
$skip ++;
}

}


fclose($csv_file);
$import_status = '<p class="text-success">Records uploaded succesfully</p>';
} else {
$import_status = '<p class="text-danger">Error  uploading records</p>';
}
} else {
$import_status = '<p class="text-danger">Invalid file used</p>';
}
}
header('Location: ../view.php?v=Farmoperationstable&id=1&msg='.$import_status);
?>