<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
if(isset($_POST['import_data'])){
$modified_by=$_SESSION['user_id'];
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
if(is_uploaded_file($_FILES['file']['tmp_name'])){
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');

$requiredHeaders = array('Farmer ID','Name of pond','Pond Size','Sampling Date','Number of Tilapia Sampled','Total Tilapia weight Sampled','Number of Catfish Sampled','Total Catfish weight Sampled'); //headers we expect
$firstLine = fgets($csv_file); //get first line of csv file
//fclose($csv_file);
$foundHeaders = str_getcsv(trim($firstLine), ',', '"'); //parse to array
if ($foundHeaders !== $requiredHeaders) {
   header('Location: ../view.php?v=Farmoperationstable&id=1&msg=<p class="text-danger">Headers do not match</p>');
   //echo 'Headers do not match: '.implode(', ', $foundHeaders);
   die();
}
else{
$skip = 0;
while(($data = fgetcsv($csv_file)) !== FALSE){
if($skip != 0){
// Check if record exists
$sql_query = "SELECT farmer_id, pond_number

FROM farms_ponds 
WHERE farmer_id = '".$data[0]."' && pond_number = '".$data[1]."'";
$resultset = dbQuery($dbConn, $sql_query);
// if record exists update otherwise insert new record
if(mysqli_num_rows($resultset)) {
$sql_update = "UPDATE farms_ponds 
set
sample_date='".$data[3]."',
no_of_tilapia_sampled='".$data[4]."', 
tot_tilapia_sample_weight='".$data[5]."',
no_of_catfish_sampled='".$data[6]."',
tot_catfish_sample_weight='".$data[7]."'
WHERE farmer_id = '".$data[0]."' && pond_number = '".$data[1]."'";
dbQuery($dbConn, $sql_update);

// echo"<pre>";
// 	print_r($data);
// 	echo"</pre>";
// 	exit();

$sql_insert = "INSERT INTO sampling_feeding 
(
farmer_id,
pond_number,pond_area,
sample_date,
no_of_tilapia_sampled,tilapia_sample_weight,
no_of_catfish_sampled, catfish_sample_weight,
modified_by,modified_date 
)
VALUES
(
'".$data[0]."',
'".$data[1]."','".$data[2]."',
'".$data[3]."',
'".$data[4]."','".$data[5]."', 
'".$data[6]."', '".$data[7]."',
'".$modified_by."', NOW()
)";
dbQuery($dbConn,$sql_insert);
} 
else{
header('Location: ../view.php?v=Farmoperationstable&id=1&msg=<p class="text-danger">Pond:'.$data[1].' in Farm ID:'.$data[0].' Does not have stocking information</p>');

}
}
$skip ++;
}

}


fclose($csv_file);
$import_status = '<p class="text-success">Records uploaded succesfully</p>';
} else {
$import_status = '<p class="text-danger">Error  uploading records</p>';
}
} else {
$import_status = '<p class="text-danger">Invalid file used</p>';
}
}
header('Location: ../view.php?v=Farmoperationstable&id=1&msg='.$import_status);
?>