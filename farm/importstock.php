<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
if(isset($_POST['import_data'])){
$modified_by=$_SESSION['user_id'];
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
if(is_uploaded_file($_FILES['file']['tmp_name'])){
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');

$requiredHeaders = array('Farmer ID', 'Name of Farm','Name of first pond stocked','Size of stocked pond in M2','Date stocked','Number of Tilapia stocked','Number of Catfish stocked','Source of fingerlings','Price per fingerling (kshs)','Size of fingerling (grams)'); //headers we expect
$firstLine = fgets($csv_file); //get first line of csv file
//fclose($csv_file);
$foundHeaders = str_getcsv(trim($firstLine), ',', '"'); //parse to array
if ($foundHeaders !== $requiredHeaders) {
   header('Location: ../view.php?v=Farmoperationstable&id=1&msg=<p class="text-danger">Headers do not match</p>');
   //echo 'Headers do not match: '.implode(', ', $foundHeaders);

   die();
}
else{
$skip = 0;
while(($data = fgetcsv($csv_file)) !== FALSE){
if($skip != 0){
$sql6="SELECT id from farms_ponds where pond_number='".$data[2]."' and farmer_id='".$data[0]."'";
$result6 = dbQuery($dbConn,$sql6);
while($row = dbFetchAssoc($result6)) {
  extract($row);
  }
$timestamp = strtotime($data[4]);
$stckingdate=date("Y",$timestamp);
$stckingmonth=date("F",$timestamp);
$date=strtotime(($data[4])." +6 month");
$expected_harvest_date=date("Y-m-d",$date);
// Check if record exists
$sql_query = "SELECT farmer_id,pond_number,stocking_date

FROM pond_stocking 
WHERE farmer_id = '".$data[0]."'  and pond_number = '".$id."' and stocking_date='".$data[4]."'";
$resultset = dbQuery($dbConn, $sql_query);
// if record exists update otherwise insert new record
if(mysqli_num_rows($resultset)) {
$sql_update = "UPDATE pond_stocking 
set 
pond_area='".$data[3]."',
catfish_fingerling_stocked='".$data[6]."',
tilapia_fingerling_stocked='".$data[5]."',
stocking_date='".$data[4]."',
stocking_year='".$stckingdate."', 
stocking_month='".$stckingdate."', 
expected_harvest_date='".$expected_harvest_date."',
fingerlings_source='".$data[7]."',
price_per_fingerling='".$data[8]."',
size_of_fingerlings_gms='".$data[9]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[0]."' and pond_number = '".$id."' and stocking_date='".$data[4]."'";
dbQuery($dbConn, $sql_update);
} else{
$mysql_insert = "INSERT INTO pond_stocking 
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
stocking_date,
stocking_year,
fingerlings_source,
size_of_fingerlings_gms,
expected_harvest_date,
modified_by,date_modified,date_created 
)
VALUES
(
'".$data[0]."',
'".$data[2]."',
'".$data[6]."', 
'".$data[5]."',
'".$data[4]."', 
'".$stckingdate."',
'".$stckingmonth."',
'".$data[7]."',
'".$data[9]."',
'".$expected_harvest_date."' ,
'".$modified_by."',NOW(),NOW()
)";
dbQuery($dbConn, $mysql_insert);
}
$sql_query1 = "SELECT farmer_id,pond_number

FROM farm_costs 
WHERE farmer_id = '".$data[0]."'  and pond_number = '".$id."' ";
$resultset1 = dbQuery($dbConn, $sql_query1);
// if record exists update otherwise insert new record
if(mysqli_num_rows($resultset1)) {
$sql_update1 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[8]."',
catfish_price_per_fingerling='".$data[8]."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[0]."' and pond_number = '".$id."'";
dbQuery($dbConn, $sql_update1);
} else{
$mysql_insert1 = "INSERT INTO farm_costs 
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
modified_by,modified_date,date_created 
)
VALUES
(
'".$data[0]."',
'".$id."' ,
'".$data[8]."',
'".$data[8]."',
'".$modified_by."',NOW(),NOW()
)";
dbQuery($dbConn, $mysql_insert1);
}
}
$skip ++;
}

}


fclose($csv_file);
$import_status = '<p class="text-success">Records uploaded succesfully</p>';
} else {
$import_status = '<p class="text-danger">Error  uploading records</p>';
}
} else {
$import_status = '<p class="text-danger">Invalid file used</p>';
}
}

header('Location: ../view.php?v=Farmoperationstable&id=1&msg='.$import_status);
?>