<?php
require_once 'farmer/operationstab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms where id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  $farmerid=$farmer_id;

$sqlstock="SELECT s.id as sid,p.id,p.pond_number as name,s.pond_number,s.farmer_id,s.catfish_fingerling_stocked,s.tilapia_fingerling_stocked,s.stocking_date,s.expected_harvest_date,s.fingerlings_source,s.tilapia_fingerling_stocking_weight,s.catfish_fingerling_stocking_weight from pond_stocking s join farms_ponds p on p.id=s.pond_number  where s.farmer_id='$farmerid' order by sid ";

$resultstock = dbQuery($dbConn,$sqlstock);

$sqlsample="SELECT id,farmer_id,pond_number,sample_date,tot_tilapia_sample_weight,(tot_tilapia_sample_weight/no_of_tilapia_sampled) as avg_tilapia_sample_weight,no_of_tilapia_sampled,tot_catfish_sample_weight,(tot_catfish_sample_weight/no_of_catfish_sampled) as avg_catfish_sample_weight,no_of_catfish_sampled,tilapia_fingerling_stocked,catfish_fingerling_stocked,tilapia_fingerling_stocking_weight,catfish_fingerling_stocking_weight from farms_ponds where farmer_id='$farmerid' order by id ";

$resultsample = dbQuery($dbConn,$sqlsample);

$sqlharvest = "SELECT id,pond_number,farmer_id,pond_area,stocking_date,harvest_type,harvest_date,tilapia_fingerling_stocked,tilapia_harvested,avg_weight_tilapia_harvested,(tilapia_harvested * avg_weight_tilapia_harvested) as total_tilapia_weight, (tilapia_fingerling_stocked-tilapia_harvested) as rem_tilapia_pieces, catfish_fingerling_stocked,catfish_harvested,avg_weight_catfish_harvested,(catfish_harvested * avg_weight_catfish_harvested) as total_catfish_weight,(catfish_fingerling_stocked-catfish_harvested) as rem_catfish_pieces, (((tilapia_harvested + catfish_harvested)/(tilapia_fingerling_stocked + catfish_fingerling_stocked))*100) as percentage_harvested  from farms_ponds  where farmer_id='$farmerid'";
$resultharvest = dbQuery($dbConn,$sqlharvest);

$sqlsales="SELECT id,total_harvest,farmer_id,whole_tilapia_fish_sold,whole_tilapia_fish_avg_price_kg,value_added_tilapia_kgs_sold,tilapia_value_added_avg_price_kg,tilapia_value_addition_form,tilapia_kgs_consumed,tilapia_kgs_spoilt,whole_catfish_fish_sold,whole_catfish_fish_avg_price_kg,value_added_catfish_kgs_sold,catfish_value_added_avg_price_kg,catfish_value_addition_form,catfish_kgs_consumed,catfish_kgs_spoilt,date_created,date_modified from farm_sales s where farmer_id='$farmerid' order by id ";

$resultsales = dbQuery($dbConn,$sqlsales);

?>

<div class="prepend-1 span-17">
<div class="col-md-12">
 <h4>Click <a href="javascript:addeditstock(<?php echo $f_id; ?>);"><?php echo $farmerid; ?></a>To Edit</h4>
  <div class="panel with-nav-tabs panel-default" >
    <div class="panel-heading" style="padding: 4px 4px 0 4px; border-bottom: none; margin-bottom: -1px; ">
            <ul class="nav nav-tabs" style="padding: 4px 4px 0 4px; border-bottom: none; margin-bottom: -1px; ">
                <li class="active"><a href="#tab1default" data-toggle="tab">Stocking</a></li>
                <li><a href="#tab2default" data-toggle="tab">Sampling</a></li>
                <li><a href="#tab3default" data-toggle="tab">Harvesting</a></li>
                <li><a href="#tab4default" data-toggle="tab">Sales</a></li>
            </ul>
    </div>
    <div class="panel-body">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1default">
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <h4 style="float: right;"><?php echo $errorMessage; ?></h4>
                <div class="table-responsive">
                  <table id="paging" class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>#</td>
                       <td><b>Name of pond</td>
                       <!-- <td><b>Size of pond in M2</td> -->
                       <td><b>Date stocked</td>
                       <td><b>Expected Harvest Date</td>
                       <td><b>Tilapia stocked</td>
                       <td><b>Stocking  weight (grams)</td>
                       <td><b>Catfish stocked</td>
                       <td><b>Stocking  weight (grams)</td>
                       <td><b>Source of fingerlings</td>
                       
                      
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($resultstock)) {
                      extract($row);
                      static $stockno=0;
                      $stockno++;
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                       <td><?php echo $stockno; ?></td>
                       <td><a href="javascript:editstock(<?php echo $sid; ?>);"><?php echo $name; ?></a></td>
                       <td><?php echo $stocking_date; ?></td>
                       <td><?php echo $expected_harvest_date; ?></td>
                       <td><?php echo $tilapia_fingerling_stocked; ?></td>
                       <td><?php echo $tilapia_fingerling_stocking_weight; ?></td>
                       <td><?php echo $catfish_fingerling_stocked; ?></td>
                       <td><?php echo $catfish_fingerling_stocking_weight; ?></td>
                       <td><?php echo $fingerlings_source; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                     </tbody>
                  </table> 
                  Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/stocking.csv" >Stocking</a>&ensp;CSV
            <form action="<?php echo WEB_ROOT; ?>farm/importstock.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Add Stock (+) " onclick="javascript:addstock(<?php echo $f_id; ?>)" style="float: right;">
                  
                  
            </form>
              </div>

            </div>
          </div>

        </div>
                
      </div>
      <div class="tab-pane fade" id="tab2default">
        <div class="row">
            <div class="ibox float-e-margins">
              <h4 style="float: right;"><?php echo $errorMessage; ?></h4>
                <div class="table-responsive">
                  <table id="paging" class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>#</td>
                       <td><b>Pond No</td>
                       <td><b>Sampling Date</td>
                       <td><b>No. of Tilapia</td>
                       <td><b>No. of Tilapia sampled</td>
                       <td><b>Total Tilapia weight sampled</td>
                       <td><b>Average Tilapia weight sampled</td>
                       <td><b>Average Tilapia Stocking weight</td>
                       <td><b>No. of Catfish</td>
                       <td><b>No. of Catfish sampled</td>
                       <td><b>Total Catfish weight sampled</td>
                       <td><b>Average Catfish weight sampled</td>
                       <td><b>Average Catfish Stocking weight</td>
                      
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($resultsample)) {
                      extract($row);
                      static $sampleno=0;
                      $sampleno++;
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                        <td><?php echo $sampleno; ?></td>
                       <td><?php echo $pond_number; ?></td>
                       <td><?php echo $sample_date; ?></td>
                       <td><?php echo $tilapia_fingerling_stocked; ?></td>
                       <td><?php echo $no_of_tilapia_sampled; ?></td>
                       <td><?php echo $tot_tilapia_sample_weight; ?></td>
                       <td><?php echo number_format((float)$avg_tilapia_sample_weight,2); ?></td>
                       <td><?php echo $tilapia_fingerling_stocking_weight; ?></td>
                       <td><?php echo $catfish_fingerling_stocked; ?></td>
                       <td><?php echo $no_of_catfish_sampled; ?></td>
                       <td><?php echo $tot_catfish_sample_weight; ?></td>
                       <td><?php echo number_format((float)$avg_catfish_sample_weight,2); ?></td>
                       <td><?php echo $catfish_fingerling_stocking_weight; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                     </tbody>
                  </table> 

                </div>
                Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/sampling.csv" >Sampling</a>&ensp;CSV
          <form action="<?php echo WEB_ROOT; ?>farm/importsampling.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Sample (+)" onclick="javascript:addsample(<?php echo $f_id; ?>)" style="float: right;">
            </form>
            </div>

        </div>
                
      </div>
              
      <div class="tab-pane fade" id="tab3default">
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <h4 style="float: right;"><?php echo $errorMessage; ?></h4>
                <div class="table-responsive">
                  <table id="paging" class="table table-striped table-bordered">

                  <thead>
                  <tr>
                   <td><b>#</td>
                   <td><b>Pond No</td>
                   <td><b>Size(M2)</td>
                   <td><b>Stocking Date</td>
                   <td><b>Harvest Date</td>
                   <td><b>KGs Of feed</td>
                   <td><b>Harvest Type</td>
                   <td><b>Stocked Tilapia</td>
                   <td><b>Tilapia Harvested</td>
                   <td><b>Average tilapia weight</td>
                   <td><b>Total tilapia weight</td>
                   <td><b>Remaining tilapia pieces</td>
                   <td><b>Stocked Catfish</td>
                   <td><b>Catfish Harvested</td>
                   <td><b>Average Catfish weight</td>
                   <td><b>Total Catfish weight</td>
                   <td><b>Remaining Catfish pieces</td>
                   <td><b>Percentage of pond harvested</td>
                   

                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  while($row = dbFetchAssoc($resultharvest)) {
                  extract($row);
                  static $harvestno=0;
                  $harvestno++;
                  if ($i%2) {
                    $class = 'row1';
                  } else {
                    $class = 'row2';
                  }

                  ?>

                  <tr class="<?php echo $class; ?>">
                    <td><?php echo $harvestno; ?></td>
                   <td><?php echo $pond_number; ?></td>
                   <td><?php echo $pond_area; ?></td>
                   <td><?php echo $stocking_date; ?></td>
                   <td><?php echo $harvest_date; ?></td>
                   <td><?php echo $feeds_bought; ?></td>
                   <td><?php echo $harvest_type; ?></td>
                   <td><?php echo $tilapia_fingerling_stocked; ?></td>
                   <td><?php echo $tilapia_harvested; ?></td>
                   <td><?php echo $avg_weight_tilapia_harvested; ?></td>
                   <td><?php echo $total_tilapia_weight; ?></td>
                   <td><?php echo $rem_tilapia_pieces; ?></td>
                   <td><?php echo $catfish_fingerling_stocked; ?></td>
                   <td><?php echo $catfish_harvested; ?></td>
                   <td><?php echo $avg_weight_catfish_harvested; ?></td>
                   <td><?php echo $total_catfish_weight; ?></td>
                   <td><?php echo $rem_catfish_pieces; ?></td>
                   <td><?php echo number_format((float)$percentage_harvested,2); ?>%</td>
                  </tr>
                  <?php
                  } // end while

                  ?>

                  </tbody>
                </table>
                </div>
               Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/harvest.csv" >Harvesting</a>&ensp;CSV
          <form action="<?php echo WEB_ROOT; ?>farm/importharvest.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Add Harvests (+)" onclick="javascript:addharvest(<?php echo $f_id; ?>)" style="float: right;">
            </form>
            </div>
          </div>

        </div>
                
      </div>     
      <div class="tab-pane fade" id="tab4default">
      <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <h4 style="float: right;"><?php echo $errorMessage; ?></h4>
              
                <div class="table-responsive">
                  <table id="paging" class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>#</td>
                       <td><b>Total Harvest(Kgs)</td>
                       <td><b>Whole Tilapia sold(Kgs)</td>
                       <td><b>Average price per kg</td>
                       <td><b>KGs of Value added sold</td>
                       <td><b>Average price/Kg of value added(Kshs)</td>
                       <td><b>Form of Value addition</td>
                       <td><b>Kgs consumed</td>
                       <td><b>Kgs spoilt</td>
                       <td><b>Whole Catfish sold(Kgs)</td>
                       <td><b>Average price per kg</td>
                       <td><b>KGs of Value added sold</td>
                       <td><b>Average price/Kg of value added(Kshs)</td>
                       <td><b>Form of Value addition</td>
                       <td><b>Kgs consumed</td>
                       <td><b>Kgs spoilt</td>
                          
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($resultsales)) {
                      extract($row);
                      static $salesno=0;
                      $salesno++;
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                        <td><?php echo $salesno; ?></td>
                        <td><?php echo $total_harvest; ?></td>
                       <td><?php echo $whole_tilapia_fish_sold; ?></td>
                       <td><?php echo $whole_tilapia_fish_avg_price_kg; ?></td>
                       <td><?php echo $value_added_tilapia_kgs_sold; ?></td>
                       <td><?php echo $tilapia_value_added_avg_price_kg; ?></td>
                       <td><?php echo $tilapia_value_addition_form; ?></td>
                       <td><?php echo $tilapia_kgs_consumed; ?></td>
                       <td><?php echo $tilapia_kgs_spoilt; ?></td>
                       <td><?php echo $whole_catfish_fish_sold; ?></td>
                       <td><?php echo $whole_catfish_fish_avg_price_kg; ?></td>
                       <td><?php echo $value_added_catfish_kgs_sold; ?></td>
                       <td><?php echo $catfish_value_added_avg_price_kg; ?></td>
                       <td><?php echo $catfish_value_addition_form; ?></td>
                       <td><?php echo $catfish_kgs_consumed; ?></td>
                       <td><?php echo $catfish_kgs_spoilt; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                      
                     </tbody>
                    </table>
                </div>
              Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/sales.csv" >Sales</a>&ensp;CSV
            <form action="<?php echo WEB_ROOT; ?>farm/importsales.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Addsales (+)" onclick="javascript:addsales(<?php echo $f_id; ?>)" style="float: right;">
                  
            </form>
            </div>
          </div>

        </div>
                
      </div>           
      </div>
    </div>
  </div>
 </div>
</div>
<?php

}//while


?>

