<?php
if (!defined('WEB_ROOT')) {
  exit;
}
if(isset($_GET['id']) && (int)$_GET['id']>0){
$s_id = (int)$_GET['id'];
}else{
  header('Location: ../index.php');
}
//echo '<h3>Stock Information</h3>';
$errorMessage = "";

$farmerid="select DISTINCT(farmer_id) from farms where id='$s_id'";
$resultf = dbQuery($dbConn,$farmerid);
while($row = dbFetchAssoc($resultf)) {
  extract($row);
}

$fid = "SELECT id,pond_number  FROM farms_ponds where farmer_id='$farmer_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
  extract($row);
 $tabs[]=array("pond_number"=>$pond_number,"id"=>$id);
}


  ?>
<div class="container-fluid">
  <ul class="nav md-pills nav-justified pills-secondary">
    
    
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#stocking<?php echo $tab['id']; ?>" role="tab">Stocking</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#sampling<?php echo $tab['id']; ?>" role="tab">Sampling</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#harvesting<?php echo $tab['id']; ?>" role="tab">Harvesting</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#sales<?php echo $tab['id']; ?>" role="tab">Sales</a>
    </li>
</ul>
<div class="panel-body">
<div class="tab-content">
<div class="tab-pane fade in active" id="stocking">
  <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
                <?php

                $tabTitle = "";
                $tabTitleClass = "";

                foreach($tabs as $tab) {

                   if($i == 0) {
                       $tabTitleClass = "active";
                   } else {
                       $tabTitleClass = "";
                   }

                   /* Build our tab pane title */
                   $tabTitle = '<li class="'.$tabTitleClass.'"><a href="#stocking'.$tab['id'].'" data-toggle="tab">'.$tab['pond_number'].'</a></li>';

                   echo $tabTitle;

                   $i++;

                } ?>
    </ul>
    <div class="panel-body">
    <div class="tab-content">
    <?php
      foreach($tabs as $tab) {
          

              if($j == 0) {
                  
                  ?>

                  <div id="stocking<?php echo $tab['id']; ?>" class="tab-pane fade in active">
              
                 
               <div class="prepend-1 span-17">
                
                <div class="table-responsive">
                
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editstock" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

               <?php
               $sql = "SELECT id,pond_number,pond_area,farmer_id,catfish_fingerling_stocked,tilapia_fingerling_stocked,stocking_date,expected_harvest_date,fingerlings_source,price_per_fingerling,size_of_fingerlings_gms from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."' ";

                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                extract($row);
               ?>
                      <div class="form-group row">
                      <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                      </div>
                  
                      <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      
                      <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" required="" ></label>
                      <label for="fingerlings_source" class="col-md-3 col-form-label">Source Of Fingerlings:<input class="form-control" name="fingerlings_source" type="text" id="fingerlings_source" value="<?php echo $fingerlings_source; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" required="" ></label>
                      <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
                      </div>
                     <div class="form-group row" >
                      <label for="tilapia_price_per_fingerling" class="col-md-3 col-form-label">Price per Tilapia Fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="text" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>" required="" ></label>
                      <label for="catfish_price_per_fingerling" class="col-md-3 col-form-label">Price per Catfish Fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="text" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>" required="" ></label>
                      </div>
                      <div class="form-group row" >
                      
                      <label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Tilapia Fingerlings:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" required="" ></label>
                      <label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Catfish Fingerlings:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" required="" ></label>
                     
                    </div>
            
              <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
                
               </div>
                  <?php


              } else {
                  // $tabContentClass = "tab-pane fade";
                  ?>
                  <div id="stocking<?php echo $tab['id']; ?>" class="tab-pane fade">
               <div class="prepend-1 span-17">
                <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editstock" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                  $sql = "SELECT id,pond_number,pond_area,farmer_id,catfish_fingerling_stocked,tilapia_fingerling_stocked,stocking_date,expected_harvest_date,fingerlings_source,price_per_fingerling,size_of_fingerlings_gms from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."'";

                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                   extract($row);
                 ?>
               
                  
                <div class="form-group row">
                      <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                      </div>
                  
                      <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      
                      <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" required="" ></label>
                      <label for="fingerlings_source" class="col-md-3 col-form-label">Source Of Fingerlings:<input class="form-control" name="fingerlings_source" type="text" id="fingerlings_source" value="<?php echo $fingerlings_source; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" required="" ></label>
                      <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
                      </div>
                     <div class="form-group row" >
                      <label for="tilapia_price_per_fingerling" class="col-md-3 col-form-label">Price per Tilapia Fingerling:<input class="form-control" name="tilapia_price_per_fingerling" type="text" id="tilapia_price_per_fingerling" value="<?php echo $tilapia_price_per_fingerling; ?>" required="" ></label>
                      <label for="catfish_price_per_fingerling" class="col-md-3 col-form-label">Price per Catfish Fingerling:<input class="form-control" name="catfish_price_per_fingerling" type="text" id="catfish_price_per_fingerling" value="<?php echo $catfish_price_per_fingerling; ?>" required="" ></label>
                      </div>
                      <div class="form-group row" >
                      
                      <label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Tilapia Fingerlings:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" required="" ></label>
                      <label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Size of Catfish Fingerlings:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" required="" ></label>
                     
                    </div>
                <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
               </div>
                  <?php
              }


                    $j++;
                
            }
        ?>
    </div>
    </div>
</div>
<div class="tab-pane fade" id="sampling">
  <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; margin-left: 200px; ">
                <?php

                $tabTitle = "";
                $tabTitleClass = "";

                foreach($tabs as $tab) {

                   if($i == 0) {
                       $tabTitleClass = "active";
                   } else {
                       $tabTitleClass = "";
                   }

                   /* Build our tab pane title */
                   $tabTitle = '<li class="'.$tabTitleClass.'"><a href="#sampling'.$tab['id'].'" data-toggle="tab">'.$tab['pond_number'].'</a></li>';

                   echo $tabTitle;

                   $i++;

                } ?>
  </ul>
  <div class="panel-body">
  <div class="tab-content">
  <?php
      foreach($tabs as $tab) {
          

              if($j == 0) {
                  
                  ?>

                  <div id="sampling<?php echo $tab['id']; ?>" class="tab-pane fade in active">
              
                 
               <div class="prepend-1 span-17">
                
                <div class="table-responsive">
                
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editsample" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

               <?php
               $sql = "SELECT id,farmer_id,pond_number,sample_date,tot_tilapia_sample_weight,(tot_tilapia_sample_weight/no_of_tilapia_sampled) as avg_tilapia_sample_weight,no_of_tilapia_sampled,tot_catfish_sample_weight,(tot_catfish_sample_weight/no_of_catfish_sampled) as avg_catfish_sample_weight,no_of_catfish_sampled,tilapia_fingerling_stocked,catfish_fingerling_stocked,tilapia_fingerling_stocking_weight,catfish_fingerling_stocking_weight,stocking_date from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."' ";
                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                extract($row);
               ?>
                  
                <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                      </div>
                      <div class="form-group row">
                      <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly ></label>
                      <label for="sample_date" class="col-md-3 col-form-label">Date Sampled:<input class="date form-control" name="sample_date" type="text" id="sample_date" value="<?php echo $sample_date; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="no_of_catfish_sampled" class="col-md-3 col-form-label">Number of Catfish Sampled:<input class="form-control" name="no_of_catfish_sampled" type="text" id="no_of_catfish_sampled" value="<?php echo $no_of_catfish_sampled; ?>" required="" ></label>
                      <!-- <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia Stocked:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label> -->
                      <label for="no_of_tilapia_sampled" class="col-md-3 col-form-label">Number of Tilapia Sampled:<input class="form-control" name="no_of_tilapia_sampled" type="text" id="no_of_tilapia_sampled" value="<?php echo $no_of_tilapia_sampled; ?>" required="" ></label>
                      </div>
                      <!-- <div class="form-group row">
                      <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish Stocked:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" readonly ></label>
                      <label for="no_of_catfish_sampled" class="col-md-3 col-form-label">Number of Catfish Sampled:<input class="form-control" name="no_of_catfish_sampled" type="text" id="no_of_catfish_sampled" value="<?php echo $no_of_catfish_sampled; ?>" required="" ></label>
                      </div> -->
                     
                      <div class="form-group row" >
                      <label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Tilapia Fingerlings Stocking Weight:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_tilapia_sample_weight" class="col-md-3 col-form-label">Total Weight of Tilapia Sampled:<input class="form-control" name="tot_tilapia_sample_weight" type="text" id="tot_tilapia_sample_weight" value="<?php echo $tot_tilapia_sample_weight; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Catfish Fingerlings Stocking Weight:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_catfish_sample_weight" class="col-md-3 col-form-label">Total Weight of Catfish Sampled:<input class="form-control" name="tot_catfish_sample_weight" type="text" id="tot_catfish_sample_weight" value="<?php echo $tot_catfish_sample_weight; ?>" required="" ></label>
                     
                    </div>
            
              <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
                
               </div>
                  <?php


              } else {
                  // $tabContentClass = "tab-pane fade";
                  ?>
                  <div id="sampling<?php echo $tab['id']; ?>" class="tab-pane fade">
               <div class="prepend-1 span-17">
                <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editsample" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                  $sql = "SELECT id,farmer_id,pond_number,sample_date,tot_tilapia_sample_weight,(tot_tilapia_sample_weight/no_of_tilapia_sampled) as avg_tilapia_sample_weight,no_of_tilapia_sampled,tot_catfish_sample_weight,(tot_catfish_sample_weight/no_of_catfish_sampled) as avg_catfish_sample_weight,no_of_catfish_sampled,tilapia_fingerling_stocked,catfish_fingerling_stocked,tilapia_fingerling_stocking_weight,catfish_fingerling_stocking_weight,stocking_date from farms_ponds where farmer_id='$farmer_id' and id='".$tab['id']."' ";

                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                   extract($row);
                 ?>
               
                  
                     <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                      </div>
                      <div class="form-group row">
                      <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly ></label>
                      <label for="sample_date" class="col-md-3 col-form-label">Date Sampled:<input class="date form-control" name="sample_date" type="text" id="sample_date" value="<?php echo $sample_date; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="no_of_catfish_sampled" class="col-md-3 col-form-label">Number of Catfish Sampled:<input class="form-control" name="no_of_catfish_sampled" type="text" id="no_of_catfish_sampled" value="<?php echo $no_of_catfish_sampled; ?>" required="" ></label>
                      <!-- <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Number of Tilapia Stocked:<input class="form-control" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label> -->
                      <label for="no_of_tilapia_sampled" class="col-md-3 col-form-label">Number of Tilapia Sampled:<input class="form-control" name="no_of_tilapia_sampled" type="text" id="no_of_tilapia_sampled" value="<?php echo $no_of_tilapia_sampled; ?>" required="" ></label>
                      </div>
                      <!-- <div class="form-group row">
                      <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Number of Catfish Stocked:<input class="form-control" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" readonly ></label>
                      <label for="no_of_catfish_sampled" class="col-md-3 col-form-label">Number of Catfish Sampled:<input class="form-control" name="no_of_catfish_sampled" type="text" id="no_of_catfish_sampled" value="<?php echo $no_of_catfish_sampled; ?>" required="" ></label>
                      </div> -->
                     
                      <div class="form-group row" >
                      <label for="tilapia_fingerling_stocking_weight" class="col-md-3 col-form-label">Tilapia Fingerlings Stocking Weight:<input class="form-control" name="tilapia_fingerling_stocking_weight" type="text" id="tilapia_fingerling_stocking_weight" value="<?php echo $tilapia_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_tilapia_sample_weight" class="col-md-3 col-form-label">Total Weight of Tilapia Sampled:<input class="form-control" name="tot_tilapia_sample_weight" type="text" id="tot_tilapia_sample_weight" value="<?php echo $tot_tilapia_sample_weight; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="catfish_fingerling_stocking_weight" class="col-md-3 col-form-label">Catfish Fingerlings Stocking Weight:<input class="form-control" name="catfish_fingerling_stocking_weight" type="text" id="catfish_fingerling_stocking_weight" value="<?php echo $catfish_fingerling_stocking_weight; ?>" readonly ></label>
                      <label for="tot_catfish_sample_weight" class="col-md-3 col-form-label">Total Weight of Catfish Sampled:<input class="form-control" name="tot_catfish_sample_weight" type="text" id="tot_catfish_sample_weight" value="<?php echo $tot_catfish_sample_weight; ?>" required="" ></label>
                     
                    </div>
                <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
               </div>
                  <?php
              }


                    $j++;
                
            }
        ?>
  </div>
  </div>
</div>
<div class="tab-pane fade" id="harvesting">
  <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; margin-left: 400px; ">
                <?php

                $tabTitle = "";
                $tabTitleClass = "";

                foreach($tabs as $tab) {

                   if($i == 0) {
                       $tabTitleClass = "active";
                   } else {
                       $tabTitleClass = "";
                   }

                   /* Build our tab pane title */
                   $tabTitle = '<li class="'.$tabTitleClass.'"><a href="#harvesting'.$tab['id'].'" data-toggle="tab">'.$tab['pond_number'].'</a></li>';

                   echo $tabTitle;

                   $i++;

                } ?>
  </ul>
  <div class="panel-body">
  <div class="tab-content">
  <?php
      foreach($tabs as $tab) {
          

              if($j == 0) {
                  
                  ?>

                  <div id="harvesting<?php echo $tab['id']; ?>" class="tab-pane fade in active">
              
                 
               <div class="prepend-1 span-17">
                
                <div class="table-responsive">
                
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editharvest" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

               <?php
               $sqltab = "SELECT pond_number,farmer_id,pond_area,stocking_date,harvest_type,expected_harvest_date,tilapia_fingerling_stocked,tilapia_harvested,avg_weight_tilapia_harvested,(tilapia_harvested * avg_weight_tilapia_harvested) as total_tilapia_weight, (tilapia_fingerling_stocked-tilapia_harvested) as rem_tilapia_pieces, catfish_fingerling_stocked,catfish_harvested,avg_weight_catfish_harvested,(catfish_harvested * avg_weight_catfish_harvested) as total_catfish_weight,(catfish_fingerling_stocked-catfish_harvested) as rem_catfish_pieces, (((tilapia_harvested + catfish_harvested)/(tilapia_fingerling_stocked + catfish_fingerling_stocked))*100) as percentage_harvested  from farms_ponds  where farmer_id='$farmer_id' and  id='".$tab['id']."'";

                   $resulttab = dbQuery($dbConn,$sqltab);
                   while($row = dbFetchAssoc($resulttab)) {
                extract($row);
               ?>
                  
                <div class="form-group row">
                <input class="form-control input-sm" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
                <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control input-sm" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                <label for="pond_area" class="col-md-3 col-form-label">Pond Size(M2):<input class="form-control input-sm" name="pond_area" type="text" id="pond_area" value="<?php echo $pond_area; ?>" readonly></label>
                </div>
                <div class="form-group row">
                <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control input-sm" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly></label>
                <label for="expected_harvest_date" class="col-md-3 col-form-label">Expected Date of Harvest:<input class="date form-control input-sm" name="expected_harvest_date" type="text" id="expected_harvest_date" value="<?php echo $expected_harvest_date; ?>" readonly ></label>
                <label for="harvest_date" class="col-md-3 col-form-label">Date Harvested:<input class="date form-control input-sm" name="harvest_date" type="text" id="harvest_date" value=""  ></label>
                <label for="harvest_type" class="col-md-3 col-form-label">Harvest type:<select class="form-control input-sm" name="harvest_type" value="<?php echo $harvest_type; ?>">
                <option value="<?php echo $harvest_type; ?>"><?php echo $harvest_type; ?></option>
                <option value="Partial">Partial</option>
                <option value="Complete">Complete</option>
                </select></label>
                <!-- <input type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                <input type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()"/><br> -->
               
                </div>
                <div class="form-group row">
                <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Tilapia Stocked:<input class="form-control input-sm" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
                <input class="form-control input-sm" name="tilapia_harvested" type="hidden" id="tilapia_harvested" value="<?php echo $tilapia_harvested; ?>" >
                <label for="tilapia_harvest" class="col-md-3 col-form-label">Tilapia Harvested:<input class="form-control input-sm" name="tilapia_harvest" type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()" value=""  >
                  <p id="msg"></p>
                </label>
                <label for="rem_tilapia_pieces" class="col-md-3 col-form-label">Tilapia pieces remaining:<input class="form-control input-sm" name="rem_tilapia_pieces" type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()" value="<?php echo $rem_tilapia_pieces; ?>" readonly ></label>
                <label for="avg_weight_tilapia_harvested" class="col-md-3 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_tilapia_harvested" type="text" id="avg_weight_tilapia_harvested" value="<?php echo $avg_weight_tilapia_harvested; ?>" required="" ></label>
                
                </div>
               <div class="form-group row">
                <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Catfish Stocked:<input class="form-control input-sm" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
                <input class="form-control input-sm" name="catfish_harvested" type="hidden" id="catfish_harvested" value="<?php echo $catfish_harvested; ?>" >
                <label for="catfish_harvest" class="col-md-3 col-form-label">Catfish Harvested:<input class="form-control input-sm" name="catfish_harvest" type="text" id="catfish_harvest" value=""  ></label>
                <label for="rem_catfish_pieces" class="col-md-3 col-form-label">Catfish pieces remaining:<input class="form-control input-sm" name="rem_catfish_pieces" type="text" id="rem_catfish_pieces" value="<?php echo $rem_catfish_pieces; ?>" readonly ></label>
                <label for="avg_weight_catfish_harvested" class="col-md-3 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_catfish_harvested" type="text" id="avg_weight_catfish_harvested" value="<?php echo $avg_weight_catfish_harvested; ?>" required="" ></label>
                
                </div>
               
              
              <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
                
               </div>
                  <?php


              } else {
                  // $tabContentClass = "tab-pane fade";
                  ?>
                  <div id="harvesting<?php echo $tab['id']; ?>" class="tab-pane fade">
               <div class="prepend-1 span-17">
                <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editharvest" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                 $sqltab = "SELECT pond_number,farmer_id,pond_area,stocking_date,harvest_type,expected_harvest_date,tilapia_fingerling_stocked,tilapia_harvested,avg_weight_tilapia_harvested,(tilapia_harvested * avg_weight_tilapia_harvested) as total_tilapia_weight, (tilapia_fingerling_stocked-tilapia_harvested) as rem_tilapia_pieces, catfish_fingerling_stocked,catfish_harvested,avg_weight_catfish_harvested,(catfish_harvested * avg_weight_catfish_harvested) as total_catfish_weight,(catfish_fingerling_stocked-catfish_harvested) as rem_catfish_pieces,(((tilapia_harvested + catfish_harvested)/(tilapia_fingerling_stocked + catfish_fingerling_stocked))*100) as percentage_harvested  from farms_ponds  where farmer_id='$farmer_id' and  id='".$tab['id']."'";

                   $resulttab = dbQuery($dbConn,$sqltab);
                   while($row = dbFetchAssoc($resulttab)) {
                extract($row);
               ?>
               
                  
                <div class="form-group row">
                <input class="form-control input-sm" name="farmer_id" type="hidden" id="farmer_id" value="<?php echo $farmer_id; ?>" >
                <label for="pond_number" class="col-md-3 col-form-label">Pond Number:<input class="form-control input-sm" name="pond_number" type="text" id="pond_number" value="<?php echo $pond_number; ?>" readonly></label>
                <label for="pond_area" class="col-md-3 col-form-label">Pond Size(M2):<input class="form-control input-sm" name="pond_area" type="text" id="pond_area" value="<?php echo $pond_area; ?>" readonly></label>
                </div>
                <div class="form-group row">
                <label for="stocking_date" class="col-md-3 col-form-label">Date Stocked:<input class="date form-control input-sm" name="stocking_date" type="text" id="stocking_date" value="<?php echo $stocking_date; ?>" readonly></label>
                <label for="expected_harvest_date" class="col-md-3 col-form-label">Expected Date of Harvest:<input class="date form-control input-sm" name="expected_harvest_date" type="text" id="expected_harvest_date" value="<?php echo $expected_harvest_date; ?>" readonly ></label>
                <label for="harvest_date" class="col-md-3 col-form-label">Date Harvested:<input class="date form-control input-sm" name="harvest_date" type="text" id="harvest_date" value=""  ></label>
                <label for="harvest_type" class="col-md-3 col-form-label">Harvest type:<select class="form-control input-sm" name="harvest_type" value="<?php echo $harvest_type; ?>">
                <option value="<?php echo $harvest_type; ?>"><?php echo $harvest_type; ?></option>
                <option value="Partial">Partial</option>
                <option value="Complete">Complete</option>
                </select></label>
                <!-- <input type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                <input type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()"/><br>
                 -->
                </div>
                <div class="form-group row">
                <label for="tilapia_fingerling_stocked" class="col-md-3 col-form-label">Tilapia Stocked:<input class="form-control input-sm" name="tilapia_fingerling_stocked" type="text" id="tilapia_fingerling_stocked" value="<?php echo $tilapia_fingerling_stocked; ?>" readonly ></label>
                <input class="form-control input-sm" name="tilapia_harvested" type="hidden" id="tilapia_harvested" value="<?php echo $tilapia_harvested; ?>" >
                <label for="tilapia_harvest" class="col-md-3 col-form-label">Tilapia Harvested:<input class="form-control input-sm" name="tilapia_harvest" type='number' id='first' onkeyup="validateNumber()" onclick="validateNumber()" value=""  >
                  <p id="msg"></p>
                </label>
                <label for="rem_tilapia_pieces" class="col-md-3 col-form-label">Tilapia pieces remaining:<input class="form-control input-sm" name="rem_tilapia_pieces" type='number' id='second' onkeyup="validateNumber()" onclick="validateNumber()" value="<?php echo $rem_tilapia_pieces; ?>" readonly ></label>
                <label for="avg_weight_tilapia_harvested" class="col-md-3 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_tilapia_harvested" type="text" id="avg_weight_tilapia_harvested" value="<?php echo $avg_weight_tilapia_harvested; ?>" required="" ></label>
                
                </div>
               <div class="form-group row">
                <label for="catfish_fingerling_stocked" class="col-md-3 col-form-label">Catfish Stocked:<input class="form-control input-sm" name="catfish_fingerling_stocked" type="text" id="catfish_fingerling_stocked" value="<?php echo $catfish_fingerling_stocked; ?>" required="" ></label>
                <input class="form-control input-sm" name="catfish_harvested" type="hidden" id="catfish_harvested" value="<?php echo $catfish_harvested; ?>" >
                <label for="catfish_harvest" class="col-md-3 col-form-label">Catfish Harvested:<input class="form-control input-sm" name="catfish_harvest" type="text" id="catfish_harvest" value=""  ></label>
                <label for="rem_catfish_pieces" class="col-md-3 col-form-label">Catfish pieces remaining:<input class="form-control input-sm" name="rem_catfish_pieces" type="text" id="rem_catfish_pieces" value="<?php echo $rem_catfish_pieces; ?>" readonly ></label>
                <label for="avg_weight_catfish_harvested" class="col-md-3 col-form-label">Average weight harvested:<input class="form-control input-sm" name="avg_weight_catfish_harvested" type="text" id="avg_weight_catfish_harvested" value="<?php echo $avg_weight_catfish_harvested; ?>" required="" ></label>
                
                </div>
                <?php
                } // end while

                ?>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>
            
                </div>
               </div>
                  <?php
              }

                    /* Build our tab content */
                    
                    // $tabContent = 
                    // '';

                    // echo $tabContent;

                    $j++;
                
            }
        ?>
  </div>
  </div>
</div>
<div class="tab-pane fade" id="sales">
 <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; margin-left: 600px; ">
              
  </ul>
  <div class="panel-body">
  <div class="tab-content">
  <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <tbody>
                 <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editsales" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
                  <?php
                  $sql = "SELECT * from farm_sales where farmer_id='$farmer_id' ";

                   $result = dbQuery($dbConn,$sql);
                   while($row = dbFetchAssoc($result)) {
                   extract($row);
                 ?>
               
                  
                <div class="form-group row">
                      <label for="total_harvest" class="col-md-3 col-form-label">Total Harvest(KGs):<input class="form-control" name="total_harvest" type="text" id="total_harvest" value="<?php echo $total_harvest; ?>" readonly></label>
                      </div>
                  
                      <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      
                      <label for="whole_tilapia_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_sold" type="text" id="whole_tilapia_fish_sold" value="<?php echo $whole_tilapia_fish_sold; ?>" required="" ></label>
                      <label for="whole_tilapia_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_avg_price_kg" type="text" id="whole_tilapia_fish_avg_price_kg" value="<?php echo $whole_tilapia_fish_avg_price_kg; ?>" required="" ></label>
                      <label for="whole_catfish_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_sold" type="text" id="whole_catfish_fish_sold" value="<?php echo $whole_catfish_fish_sold; ?>" required="" ></label>
                      <label for="whole_catfish_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_avg_price_kg" type="text" id="whole_catfish_fish_avg_price_kg" value="<?php echo $whole_catfish_fish_avg_price_kg; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="value_added_tilapia_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Tilapia Sold:<input class="form-control" name="value_added_tilapia_kgs_sold" type="text" id="value_added_tilapia_kgs_sold" value="<?php echo $value_added_tilapia_kgs_sold; ?>" required="" ></label>
                      <label for="tilapia_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Tilapia per Kg:<input class="form-control" name="tilapia_value_added_avg_price_kg" type="text" id="tilapia_value_added_avg_price_kg" value="<?php echo $tilapia_value_added_avg_price_kg; ?>" required="" ></label>
                      <label for="value_added_catfish_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Catfish Sold:<input class="form-control" name="value_added_catfish_kgs_sold" type="text" id="value_added_catfish_kgs_sold" value="<?php echo $value_added_catfish_kgs_sold; ?>" required="" ></label>
                      <label for="catfish_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Catfish per Kg:<input class="form-control" name="catfish_value_added_avg_price_kg" type="text" id="catfish_value_added_avg_price_kg" value="<?php echo $catfish_value_added_avg_price_kg; ?>" required="" ></label>
                      </div>
                     <div class="form-group row" >
                      <label for="tilapia_value_addition_form" class="col-md-3 col-form-label">Form of Tilapia Value addition:<input class="form-control" name="tilapia_value_addition_form" type="text" id="tilapia_value_addition_form" value="<?php echo $tilapia_value_addition_form; ?>" required="" ></label>
                      <label for="tilapia_kgs_consumed" class="col-md-3 col-form-label">KGs of Tilapia Consumed:<input class="form-control" name="tilapia_kgs_consumed" type="text" id="tilapia_kgs_consumed" value="<?php echo $tilapia_kgs_consumed; ?>" required="" ></label>
                      <label for="catfish_value_addition_form" class="col-md-3 col-form-label">Form of Catfish Value addition:<input class="form-control" name="catfish_value_addition_form" type="text" id="catfish_value_addition_form" value="<?php echo $catfish_value_addition_form; ?>" required="" ></label>
                      <label for="catfish_kgs_consumed" class="col-md-3 col-form-label">KGs of Catfish Consumed:<input class="form-control" name="catfish_kgs_consumed" type="text" id="catfish_kgs_consumed" value="<?php echo $catfish_kgs_consumed; ?>" required="" ></label>
                      </div>
                      <div class="form-group row" >
                      
                      <label for="tilapia_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Tilapia spoilt:<input class="form-control" name="tilapia_kgs_spoilt" type="text" id="tilapia_kgs_spoilt" value="<?php echo $tilapia_kgs_spoilt; ?>" required="" ></label>
                      <label for="catfish_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Catfish spoilt:<input class="form-control" name="catfish_kgs_spoilt" type="text" id="catfish_kgs_spoilt" value="<?php echo $catfish_kgs_spoilt; ?>" required="" ></label>
                      
                     
                    </div>
                <?php
                } // end while

                ?>
                <div class="form-group row">
                      <label for="total_harvest" class="col-md-3 col-form-label">Total Harvest(KGs):<input class="form-control" name="total_harvest" type="text" id="total_harvest" value="<?php echo $total_harvest; ?>" required=""></label>
                      </div>
                  
                      <div class="form-group row">
                      <input class="form-control" name="s_id" type="hidden" id="s_id" value="<?php echo $s_id; ?>" >
                      
                      <label for="whole_tilapia_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_sold" type="text" id="whole_tilapia_fish_sold" value="<?php echo $whole_tilapia_fish_sold; ?>" required="" ></label>
                      <label for="whole_tilapia_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Tilapia Sold:<input class="form-control" name="whole_tilapia_fish_avg_price_kg" type="text" id="whole_tilapia_fish_avg_price_kg" value="<?php echo $whole_tilapia_fish_avg_price_kg; ?>" required="" ></label>
                      <label for="whole_catfish_fish_sold" class="col-md-3 col-form-label">Quantity of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_sold" type="text" id="whole_catfish_fish_sold" value="<?php echo $whole_catfish_fish_sold; ?>" required="" ></label>
                      <label for="whole_catfish_fish_avg_price_kg" class="col-md-3 col-form-label">Avg Price of Whole Catfish Sold:<input class="form-control" name="whole_catfish_fish_avg_price_kg" type="text" id="whole_catfish_fish_avg_price_kg" value="<?php echo $whole_catfish_fish_avg_price_kg; ?>" required="" ></label>
                      </div>
                      <div class="form-group row">
                      <label for="value_added_tilapia_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Tilapia Sold:<input class="form-control" name="value_added_tilapia_kgs_sold" type="text" id="value_added_tilapia_kgs_sold" value="<?php echo $value_added_tilapia_kgs_sold; ?>" required="" ></label>
                      <label for="tilapia_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Tilapia per Kg:<input class="form-control" name="tilapia_value_added_avg_price_kg" type="text" id="tilapia_value_added_avg_price_kg" value="<?php echo $tilapia_value_added_avg_price_kg; ?>" required="" ></label>
                      <label for="value_added_catfish_kgs_sold" class="col-md-3 col-form-label">Quantity of VA Catfish Sold:<input class="form-control" name="value_added_catfish_kgs_sold" type="text" id="value_added_catfish_kgs_sold" value="<?php echo $value_added_catfish_kgs_sold; ?>" required="" ></label>
                      <label for="catfish_value_added_avg_price_kg" class="col-md-3 col-form-label">Price of VA Catfish per Kg:<input class="form-control" name="catfish_value_added_avg_price_kg" type="text" id="catfish_value_added_avg_price_kg" value="<?php echo $catfish_value_added_avg_price_kg; ?>" required="" ></label>
                      </div>
                     <div class="form-group row" >
                      <label for="tilapia_value_addition_form" class="col-md-3 col-form-label">Form of Tilapia Value addition:<input class="form-control" name="tilapia_value_addition_form" type="text" id="tilapia_value_addition_form" value="<?php echo $tilapia_value_addition_form; ?>" required="" ></label>
                      <label for="tilapia_kgs_consumed" class="col-md-3 col-form-label">KGs of Tilapia Consumed:<input class="form-control" name="tilapia_kgs_consumed" type="text" id="tilapia_kgs_consumed" value="<?php echo $tilapia_kgs_consumed; ?>" required="" ></label>
                      <label for="catfish_value_addition_form" class="col-md-3 col-form-label">Form of Catfish Value addition:<input class="form-control" name="catfish_value_addition_form" type="text" id="catfish_value_addition_form" value="<?php echo $catfish_value_addition_form; ?>" required="" ></label>
                      <label for="catfish_kgs_consumed" class="col-md-3 col-form-label">KGs of Catfish Consumed:<input class="form-control" name="catfish_kgs_consumed" type="text" id="catfish_kgs_consumed" value="<?php echo $catfish_kgs_consumed; ?>" required="" ></label>
                      </div>
                      <div class="form-group row" >
                      
                      <label for="tilapia_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Tilapia spoilt:<input class="form-control" name="tilapia_kgs_spoilt" type="text" id="tilapia_kgs_spoilt" value="<?php echo $tilapia_kgs_spoilt; ?>" required="" ></label>
                      <label for="catfish_kgs_spoilt" class="col-md-3 col-form-label">Kgs of Catfish spoilt:<input class="form-control" name="catfish_kgs_spoilt" type="text" id="catfish_kgs_spoilt" value="<?php echo $catfish_kgs_spoilt; ?>" required="" ></label>
                      
                     
                    </div>
                <p align="left"> 
                  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
                  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Operations';" />
                  
                 </p>
              </form>

               </tbody>

              </table>
              
                </div>  
  </div>
  </div> 
</div>
</div>
</div>
</div>
    

  
