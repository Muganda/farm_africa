<?php
require_once 'farmer/addtab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = '&nbsp;';
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
$sql = "SELECT id, farm_name FROM farms order by id desc
limit 1";
$result = dbQuery($dbConn,$sql);

   while($row = dbFetchAssoc($result)) {
    extract($row);
   }

?>
<div class="prepend-1 span-12">

<div class="modal-header">
  <p class="errorMessage"><?php echo $errorMessage; ?></p>
        <!-- <button type="button" class="close" onClick="div_hidex()">&times;</button> -->
        <h4 class="modal-title">Add Pond information<div style="float: right;"><button class="btn btn-success" onClick="addRow('dataTable')" type="button" s><i class="glyphicon glyphicon-plus" ></i></button>
      <button class="btn btn-danger remove" onClick="deleteRow('dataTable')" type="button" ><i class="glyphicon glyphicon-remove"></i></button></div></h4>
      </div>
<table class="table table-striped table-sm">
       <thead>
                <th></th>
                <th>Farmer ID</th>
                <th >Pond Name</th>
                <th >Pond Size</th>
                
                </thead>
        </table>
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=pond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <td><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
               <td width="15%"><select  class="form-control" data data-live-search="true"  name="farmer_id[]" type="text" id="farmer_id" value="" required=""><?php getfarmerid($dbConn);?>
                  </select>
               </td>
               
               <td><input type="text" class="form-control" required="required" name="pond_number[]"/></td>
               <td><input type="text" class="form-control" required="required" name="pond_area[]"/></td>
               <!-- <td><input type="text" class="form-control"  name="catfish_fingerling_stocked[]"/></td>
               <td><input type="text" class="form-control"  name="tilapia_fingerling_stocked[]"/></td>
               <td><input type="text" class="form-control"  name="stocking_date[]"></td> -->
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <div class="modal-footer">
   
    <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
    </div>
  </form>

</div>
    
    
   
    
</div>
