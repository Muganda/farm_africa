<?php
require_once '../library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {

case 'add' :
addFarm($dbConn);
break;

case 'pond' :
addPond($dbConn);
break;

case 'addnpond' :
addnPond($dbConn);
break;
case 'editassesments':
editassesments($dbConn);
break;

case 'editpond' :
editpond($dbConn);
break;

case 'contact' :
contactPerson($dbConn);
break;

case 'addcontact' :
adcontactPerson($dbConn);
break;

case 'editcontact' :
editcontactperson($dbConn);
break;


case 'staff' :
farmStaff($dbConn);
break;

case 'addstaff' :
addstaff($dbConn);
break;

case 'editstaff' :
editstaff($dbConn);
break;

case 'farminfo' :
extFarminfo($dbConn);
break;

case 'addproductioncycle' :
addproductioncycle($dbConn);
break;

case 'cycle' :
prodCycle($dbConn);
break;

case 'assesments' :
assesments($dbConn);
break;

case 'extrainfo' :
editextfarminfo($dbConn);
break;

case 'basicinfo' :
editbasicfarminfo($dbConn);
break;

case 'addcost' :
addcostinfo($dbConn);
break;

case 'editcost' :
editcost($dbConn);
break;

case 'sales' :
addsales($dbConn);
break;

case 'sampling' :
addsampling($dbConn);
break;

case 'harvest' :
harvestinfo($dbConn);
break;

case 'editharvest' :
editharvestinfo($dbConn);
break;

case 'doassesments' :
doassesments($dbConn);
break;


case 'editstock' :
editstockinfo($dbConn);
break;

case 'addstock' :
addstock($dbConn);
break;


case 'editsample' :
editsample($dbConn);
break;

case 'editsales' :
editsales($dbConn);
break;	

case 'addrecurrentcost' :
addrecurrentcost($dbConn);
break;

case 'operatingcost' :
addOpcost($dbConn);
break;


case 'delete' :
deleteLab();
break;

default :
// if action is not defined or unknown
// move to main user page
header('Location: ../index.php');
}

/*
this function will add farm entry 
*/
function addFarm($dbConn)
{  
$target_dir="images/";
$target_file=$target_dir .basename($_FILES["file"]["name"]);
$uploadOk=1;
$imageFileType =pathinfo($target_file,PATHINFO_EXTENSION); 
$farmer_id =$_POST['farmer_id']; 
$farm_name=$_POST['farm_name']; 
$farm_county=$_POST['farm_county']; 
$farm_subcounty=$_POST['farm_subcounty'];
$tot_no_of_tilapia=$_POST['tot_no_of_tilapia'];
$tot_no_of_catfish=$_POST['tot_no_of_catfish']; 
$farm_village=$_POST['farm_village'];
$farm_landmark=$_POST['farm_landmark'];
$ponds_number=$_POST['ponds_number']; 
$ponds_stocked=$_POST['ponds_stocked']; 
// $keeps_records=$_POST['keeps_records'];
$records_kept=$_POST['records_kept']; 
$record_keeping=$_POST['record_keeping'];  
$has_business_plan=$_POST['has_business_plan']; 
$business_plan_last_update=$_POST['business_plan_last_update'];
$modified_by=$_SESSION['user_id'];

if(!empty($_FILES['file']['name'])){

$check =getimagesize($_FILES["files"]["tmp_name"]);
if($check !== false){
$uploadOk=1;
}else{
header('Location: ../add.php?v=addfarm&msg='. urlencode('<p class="text-danger">File uploaded is not image</p>'));
$uploadOk=0;
}
if(file_exists($target_file)){
header('Location: ../add.php?v=addfarm&msg='. urlencode('<p class="text-danger">Image already exists!</p>'));
$uploadOk= 0;
}
if($_FILES["files"]["size"]>500000){
header('Location: ../add.php?v=addfarm&msg='. urlencode('<p class="text-danger">Selected Image is too Large!</p>'));
$uploadOk= 0;
}
if($imageFileType !="jpg" && $imageFileType !="png" && $imageFileType !="jpeg" && $imageFileType !="gif"){
header('Location: ../add.php?v=addfarm&msg='. urlencode('<p class="text-danger">File selected is not an image!</p>'));
$uploadOk= 0;
}
if ($uploadOk == 0){
header('Location: ../add.php?v=addfarm&msg='. urlencode('<p class="text-danger">Image Upload was not succesful!</p>'));
}


if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)){
$sql = "SELECT farmer_id, farm_name
FROM farms
WHERE farmer_id = '$farmer_id' and farm_name = '$farm_name' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=addfarm&msg=' . urlencode('<p class="text-danger">There exists another farm with that ID and Name</p>'));	
} else {	

$sql   = "INSERT INTO farms (farmer_id, farm_name,farm_county, farm_subcounty, farm_village, farm_landmark,tot_no_of_tilapia,tot_no_of_catfish,ponds_number, keeps_records, ponds_stocked, record_keeping, records_kept, has_business_plan, business_plan_last_update,farmphoto,modified_by,date_modified)
VALUES ('$farmer_id', '$farm_name', '$farm_county', '$farm_subcounty', '$farm_village', '$farm_landmark','$tot_no_of_tilapia',$tot_no_of_catfish,'$ponds_number', '$keeps_records', '$ponds_stocked','$record_keeping', '$records_kept', '$has_business_plan', '$business_plan_last_update','$target_file','$modified_by',NOW())";

dbQuery($dbConn,$sql);
header('Location: ../add.php?v=addfarm&msg=' . urlencode('<p class="text-success">Record has been added successfully!</p>'));	
}
}
else{

header('Location: ../add.php?v=addfarm&msg=' . urlencode('<p class="text-danger">Image Upload was not succesful!</p>'));
}

}
else{
$sql   = "INSERT INTO farms (farmer_id, farm_name,farm_county, farm_subcounty, farm_village, farm_landmark,tot_no_of_tilapia,tot_no_of_catfish,ponds_number, keeps_records, ponds_stocked, record_keeping, records_kept, has_business_plan, business_plan_last_update,modified_by,date_modified)
VALUES ('$farmer_id', '$farm_name', '$farm_county', '$farm_subcounty', '$farm_village', '$farm_landmark','$tot_no_of_tilapia',$tot_no_of_catfish,'$ponds_number', '$keeps_records', '$ponds_stocked','$record_keeping', '$records_kept', '$has_business_plan', '$business_plan_last_update','$modified_by',NOW())";

dbQuery($dbConn,$sql);
header('Location: ../add.php?v=addfarm&msg=' . urlencode('<p class="text-success">Record has been added successfully!</p>'));
}

}


function contactPerson($dbConn)
{   
$farmer_id = $_POST['farmer_id'];
$contact_id=$_POST['contact_id'];
$contact_first_name=$_POST['contact_first_name'];
$contact_last_name=$_POST['contact_last_name'];
$contact_email=$_POST['contact_email'];
$contact_telephone=$_POST['contact_telephone'];
$contact_gender=$_POST['contact_gender'];
$contact_position=$_POST['contact_position'];
$modified_by=$_SESSION['user_id'];

// echo "<pre>";
//  print_r($_POST);
//  echo"</pre>";
//  exit();
for($i=0;$i<count($contact_first_name);$i++)
{
if($contact_first_name[$i]!="" && $contact_telephone[$i]!="" && $contact_gender[$i]!="" && $contact_position[$i]!="")
{
$sql = "SELECT farmer_id, contact_first_name, contact_last_name
FROM farms_contact_persons
WHERE farmer_id = '$farmer_id[$i]' and contact_first_name = '$contact_first_name[$i]' and contact_last_name ='$contact_last_name[$i]' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=contactperson&msg=' . urlencode('<p class="text-success">There exists another person with Identification Number&nbsp&nbsp(' .$contact_first_name[$i].$contact_last_name[$i]. ')&nbsp&nbspfor the farm ID'.$farmer_id[$i].'</p>'));	
} else {
$sql= ("INSERT into farms_contact_persons(farmer_id,contact_first_name,contact_last_name,contact_email,contact_telephone,contact_gender,contact_position,modified_by,date_created,modified_date) values('$farmer_id[$i]','$contact_first_name[$i]','$contact_last_name[$i]','$contact_email[$i]','$contact_telephone[$i]','$contact_gender[$i]','$contact_position[$i]','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../add.php?v=contactperson&msg=' . urlencode('<p class="text-success">Record added successfully</p>'));
}
}
else
{
header('Location: ../add.php?v=contactperson&msg=' . urlencode('<p class="text-danger">Error uploading records!</p>'));
}
}
}

function adcontactPerson($dbConn)
{
$f_id=$_POST['fid'];   
$farmer_id = $_POST['farmerid'];
$contact_first_name=$_POST['contact_first_name'];
$contact_last_name=$_POST['contact_last_name'];
$contact_email=$_POST['contact_email'];
$contact_telephone=$_POST['contact_telephone'];
$contact_gender=$_POST['contact_gender'];
$contact_position=$_POST['contact_position'];
$modified_by=$_SESSION['user_id'];

//  	echo "<pre>";
// print_r($f_id);
// echo"</pre>";
// exit();

for($i=0;$i<count($contact_first_name);$i++)
{
if($contact_first_name[$i]!="" && $contact_last_name[$i]!="" && $contact_telephone[$i]!="" && $contact_gender[$i]!="" && $contact_position[$i]!="")
{
$sql = "SELECT farmer_id, contact_first_name,contact_last_name
FROM farms_contact_persons
WHERE farmer_id = '$farmer_id[$i]' and contact_first_name = '$contact_first_name[$i]' and contact_last_name='$contact_last_name[$i]' ";
$result = dbQuery($dbConn,$sql);
if (dbNumRows($result) == 1) {


header('Location: ../edit.php?v=edicontactperson&id='.$f_id.'&error=' . urlencode('&nbsp&nbsp&nbspThere exists another person with Name&nbsp&nbsp(' .$contact_first_name[$i]. ')&nbsp&nbspfor the farm selected'));	
} else {

$sql= ("INSERT into farms_contact_persons(farmer_id,contact_first_name,contact_last_name,contact_email,contact_telephone,contact_gender,contact_position,date_created,modified_by,modified_date) values('$farmer_id','$contact_first_name[$i]','$contact_last_name[$i]','$contact_email[$i]','$contact_telephone[$i]','$contact_gender[$i]','$contact_position[$i]',NOW(),$modified_by,NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=edicontactperson&id='.$f_id);	
}
}
else
{

header('Location: ../edit.php?v=edicontactperson&id='.$f_id.'=' . urlencode('Error uploading records'));
}
}
}
function editcontactperson($dbConn)
{ 
$f_id=$_POST['fid'];  
$cid = $_POST["cid"];
$contact_first_name = $_POST['contact_first_name'];
$contact_last_name = $_POST['contact_last_name'];
$contact_telephone = $_POST['contact_telephone'];
$contact_email = $_POST['contact_email'];
$contact_gender=$_POST['contact_gender'];
$modified_by=$_SESSION['user_id'];

for($i=0;$i<count($cid);$i++)
{
$sql   = "UPDATE farms_contact_persons

SET contact_first_name='$contact_first_name[$i]',
contact_last_name='$contact_last_name[$i]',
contact_telephone='$contact_telephone[$i]',
contact_email='$contact_email[$i]',
contact_gender='$contact_gender[$i]',
modified_by='$modified_by', 
modified_date=NOW()

WHERE id = '$cid[$i]'";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=edicontactperson&id='.$f_id);	

}
}
/*
this function will add pond entry 
*/
function addPond($dbConn)
{   

$farmer_id =$_POST['farmer_id']; 
$pond_number=$_POST['pond_number'];
$pond_area=$_POST['pond_area']; 
// $catfish_no=$_POST['catfish_no']; 
// $tilapia_no=$_POST['tilapia_no']; 
// $tilapia_fingerling_stocked=$_POST['tilapia_fingerling_stocked'];
// $tilapia_stocking_date=$_POST['tilapia_stocking_date'];
// $catfish_fingerling_stocked=$_POST['catfish_fingerling_stocked'];
// $catfish_stocking_date=$_POST['catfish_stocking_date'];

//   echo "<pre>";
// print_r($_POST);
// echo"</pre>";
// exit();
for($i=0;$i<count($pond_number);$i++)
{
if($pond_number[$i]!="" && $pond_area[$i]!="" )
{

$sql = "SELECT farmer_id, pond_number
FROM farms_ponds
WHERE farmer_id = '$farmer_id[$i]' and pond_number = '$pond_number[$i]' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {	
header('Location: ../add.php?v=addpond&msg=' . urlencode('<p class="text-danger">Another Pond exists with name '.$pond_number[$i].' for selected farmer</p>'));

} else {			
$sql= "INSERT INTO farms_ponds (farmer_id, pond_number, pond_area )
VALUES ('$farmer_id[$i]', '$pond_number[$i]', '$pond_area[$i]')";

dbQuery($dbConn,$sql);
header('Location: ../add.php?v=addpond&msg=' . urlencode('<p class="text-success">Pond Information added</p>'));
}
}
else
{
header('Location: ../add.php?v=addpond&msg=' . urlencode('<p class="text-danger">Error uploading records</p>'));
}
}

}
function addnPond($dbConn)
{   
$f_id=$_POST['fid']; 
$farmer_id =$_POST['farmerid']; 
$pond_number=$_POST['pond_number'];
$pond_area=$_POST['pond_area']; 
$tilapia_fingerling_stocked=$_POST['tilapia_fingerling_stocked'];
$catfish_fingerling_stocked=$_POST['catfish_fingerling_stocked'];
$stocking_date=$_POST['stocking_date'];
$modified_by=$_SESSION['user_id'];

for($i=0;$i<count($pond_number);$i++)
{
if($pond_number[$i]!="" && $pond_area[$i]!="" )
{

$sql = "SELECT farmer_id, pond_number
FROM farms_ponds
WHERE farmer_id = '$farmer_id[$i]' and pond_number = '$pond_number[$i]' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../edit.php?v=editpond&id='.$f_id . urlencode('&nbsp&nbsp&nbspThere exists another pond with Identification Number&nbsp&nbsp(' .$pond_number[$i]. ')&nbsp&nbspfor the farm selected'));	
} else {			
$sql= "INSERT INTO farms_ponds (farmer_id, pond_number, pond_area, tilapia_fingerling_stocked, catfish_fingerling_stocked, stocking_date,modified_by,date_modified,date_created )
VALUES ('$farmer_id', '$pond_number[$i]', '$pond_area[$i]', '$tilapia_fingerling_stocked[$i]','$catfish_fingerling_stocked[$i]','$stocking_date[$i]','$modified_by',NOW(),NOW())";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editpond&id='.$f_id);	
}
}
else
{
header('Location: ../edit.php?v=editpond&id='.$f_id.'=' . urlencode('Error uploading records'));
}
}

}


function farmStaff($dbConn)
{   
$farmer_id=$_POST['farmer_id'];
// $staff_name=$_POST['staff_name'];
$contract_type=$_POST['contract_type'];
$staff_gender=$_POST['staff_gender'];
$staff_age=$_POST['staff_age'];
$staff_role=$_POST['staff_role'];


//   echo "<pre>";
// print_r($_POST);
// echo"</pre>";
// exit();

for($i=0;$i<count($staff_role);$i++)
{
if($staff_gender[$i]!="" && $staff_age[$i]!="" && $staff_role[$i])
{

$sql = "SELECT farmer_id, staff_role
FROM farms_staff
WHERE farmer_id = '$farmer_id[$i]' and staff_role = '$staff_role[$i]' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
	header('Location: ../add.php?v=farmstaff&msg=' . urlencode('<p class="text-danger">Ehere exists a Staff with Identification Number&nbsp&nbsp(' .$staff_role[$i]. ')&nbsp&nbspfor the farm selected</p>'));

} else {		

$sql   = "INSERT INTO farms_staff (farmer_id, staff_role, staff_gender, staff_age,contract_type)
VALUES ('$farmer_id[$i]', '$staff_role[$i]', '$staff_gender[$i]', '$staff_age[$i]','$contract_type[$i]')";

dbQuery($dbConn,$sql);
header('Location: ../add.php?v=farmstaff&msg=' . urlencode('<p class="text-success">Record added successfully</p>'));
}
}
else
{
header('Location: ../add.php?v=farmstaff&msg=' . urlencode('<p class="text-danger">Error uploading records</p>'));
}
}
}
function addstaff($dbConn)
{
$f_id=$_POST['fid'];   
$farmer_id = $_POST['farmerid'];
$staff_role=$_POST['staff_role'];
$staff_age=$_POST['staff_age'];
$staff_gender=$_POST['staff_gender'];
$modified_by=$_SESSION['user_id'];

//  	echo "<pre>";
// print_r($f_id);
// echo"</pre>";
// exit();

for($i=0;$i<count($staff_role);$i++)
{
if($staff_role[$i]!="" && $staff_age[$i]!="")
{
$sql = "SELECT farmer_id, staff_role,staff_age
FROM farms_staff
WHERE farmer_id = '$farmer_id[$i]' and staff_role = '$staff_role[$i]' and staff_age='$staff_age[$i]' ";
$result = dbQuery($dbConn,$sql);
if (dbNumRows($result) == 1) {


header('Location: ../edit.php?v=editstaff&id='.$f_id.'&error=' . urlencode('&nbsp&nbsp&nbspThere exists another person with Name&nbsp&nbsp(' .$staff_role[$i]. ')&nbsp&nbspfor the farm selected'));	
} else {

$sql= ("INSERT into farms_staff(farmer_id,staff_role,staff_age,staff_gender,date_modified,modified_by) values('$farmer_id','$staff_role[$i]','$staff_age[$i]','$staff_gender[$i]',NOW(),$modified_by)");	 
dbQuery($dbConn,$sql);

header('Location: ../edit.php?v=editstaff&id='.$f_id);	
}
}
else
{

header('Location: ../edit.php?v=editstaff&id='.$f_id.'=' . urlencode('Error uploading records'));
}
}
}

function editpond($dbConn)
{ 
$f_id=$_POST['f_id'];  
$pid = $_POST['pid'];
$pond_number=$_POST['pond_number'];
$pond_area=$_POST['pond_area']; 
$modified_by=$_SESSION['user_id'];

for($i=0;$i<count($pid);$i++)
{


$sql   = "UPDATE farms_ponds

SET 
pond_number='$pond_number[$i]',
pond_area='$pond_area[$i]',
modified_by='$modified_by',
date_modified=NOW()


WHERE id = '$pid[$i]'";
	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editpond&id='.$f_id);	

}

}

function editstaff($dbConn)
{ 
$f_id=$_POST['f_id'];  
$sid = $_POST['sid'];
$staff_gender=$_POST['staff_gender'];
$staff_age=$_POST['staff_age'];
$staff_role=$_POST['staff_role'];
$modified_by=$_SESSION['user_id']; 
$contract_type=$_POST['contract_type'];

for($i=0;$i<count($staff_role);$i++)
{


$sql   = "UPDATE farms_staff

SET 
staff_gender='$staff_gender[$i]',
staff_age='$staff_age[$i]',
staff_role='$staff_role[$i]',
modified_by='$modified_by', 
contract_type='$contract_type[$i]',
date_modified=NOW()


WHERE id = '$sid[$i]'";
	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editstaff&id='.$f_id);	

}

}

function extFarminfo($dbConn)
{   
$farmer_id =$_POST['farmer_id']; 
$water_availability=$_POST['water_availability'];
$water_sources = $_POST['water_sources'];
$sources=implode( ", ", $water_sources );
$water_mechanism=$_POST['water_mechanism']; 
$farm_equipments=$_POST['farm_equipments']; 
$otherequipments = $_POST['otherequipments'];
// $staff_total=$_POST['staff_total']; 
$staff_fulltime=$_POST['staff_fulltime'];
$staff_parttime=$_POST['staff_parttime'];
$feeds_source=$_POST['feeds_source'];
$farm_greenhouse=$_POST['farm_greenhouse'];
$security_types = $_POST['security_types'];
$security=implode( ", ", $security_types );
$has_security=$_POST['has_security'];
//$security_types=$_POST['security_types']; 
$customers=$_POST['customers']; 
$customers_others=$_POST['customers_others'];
$challenges=$_POST['challenges'];
$five_year_target=$_POST['five_year_target']; 
$needs_to_reach_target=$_POST['needs_to_reach_target']; 
$receive_updates=$_POST['receive_updates'];
$can_host_trainings=$_POST['can_host_trainings'];
$modified_by=$_SESSION['user_id'];

$sql = "SELECT farmer_id
FROM farms_extended_data
WHERE farmer_id = '$farmer_id' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=extfarminfo&msg=' . urlencode('<p class="text-danger">There exists another Farm record with that ID!</p>'));	
} else {			
$sql   = "INSERT INTO farms_extended_data (farmer_id, water_availability, water_sources, water_mechanism, 
farm_equipments, staff_fulltime, staff_parttime,feeds_source,farm_greenhouse, has_security, security_types, customers_others, customers, challenges,five_year_target,needs_to_reach_target,receive_updates,can_host_trainings,modified_by,date_modified)
VALUES ('$farmer_id', '$water_availability', '$sources', '$water_mechanism', '$farm_equipments $otherequipments','$staff_fulltime','$staff_parttime','$feeds_source','$farm_greenhouse', '$has_security','$security', '$customers_others', '$customers','$challenges','$five_year_target','$needs_to_reach_target','$receive_updates','$can_host_trainings','$modified_by',NOW())";

dbQuery($dbConn,$sql);
header('Location: ../add.php?v=extfarminfo&msg=' . urlencode('<p class="text-success">Record added successfully</p>'));
}
}
function addproductioncycle($dbConn)
{
$f_id=$_POST['fid'];   
$farmer_id = $_POST['farmerid'];
$year=$_POST['year'];
$species=$_POST['species']; 
$length=$_POST['length']; 
$ponds_harvested_size=$_POST['ponds_harvested_size']; 
$fingerlings_bought=$_POST['fingerlings_bought']; 
$fingerlings_source=$_POST['fingerlings_source'];
$feeds_bought=$_POST['feeds_bought'];
$feeds_source=$_POST['feeds_source'];
$fish_harvested=$_POST['fish_harvested'];
$modified_by=$_SESSION['user_id'];

//  	echo "<pre>";
// print_r($f_id);
// echo"</pre>";
// exit();

for($i=0;$i<count($year);$i++)
{
if($year[$i]!="" && $species[$i]!="" && $length[$i]!="" && $ponds_harvested_size[$i]!="" && $fingerlings_bought[$i]!="")
{
$sql = "SELECT farmer_id, year,species
FROM farms_production_cycles
WHERE farmer_id = '$farmer_id[$i]' and year = '$year[$i]' and species='$species[$i]' ";
$result = dbQuery($dbConn,$sql);
if (dbNumRows($result) == 1) {


header('Location: ../edit.php?v=editprodcycle&id='.$f_id.'&error=' . urlencode('&nbsp&nbsp&nbspThere exists another record for the year&nbsp&nbsp(' .$year[$i]. ')&nbsp&nbspfor the farm selected'));	
} else {

$sql= ("INSERT into farms_production_cycles(farmer_id,year,species,length,ponds_harvested_size,fingerlings_bought,fingerlings_source,feeds_bought,feeds_source,fish_harvested,modified_by,date_modified) values('$farmer_id','$year[$i]','$species[$i]','$length[$i]','$ponds_harvested_size[$i]','$fingerlings_bought[$i]','$fingerlings_source[$i]','$feeds_bought[$i]','$feeds_source[$i]','$fish_harvested[$i]',$modified_by,NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editprodcycle&id='.$f_id);	
}
}
else
{

header('Location: ../edit.php?v=editprodcycle&id='.$f_id.'=' . urlencode('Error uploading records'));
}
}
}
function prodCycle($dbConn)
{   
$farmer_id =$_POST['id']; 
$year=$_POST['year'];
$species=$_POST['species']; 
$length=$_POST['length']; 
$ponds_harvested_size=$_POST['ponds_harvested_size']; 
$fingerlings_bought=$_POST['fingerlings_bought']; 
$fingerlings_source=$_POST['fingerlings_source'];
$feeds_bought=$_POST['feeds_bought'];
$feeds_source=$_POST['feeds_source'];
$fish_harvested=$_POST['fish_harvested']; 

$sql = "SELECT farmer_id, year
FROM farms_production_cycles
WHERE farmer_id = '$farmer_id' and year = '$year' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=prodcycle&error=' . urlencode('Production Cycle for that year already exists For the farm selected '));	
} else {			
$sql   = "INSERT INTO farms_production_cycles (farmer_id, year, species, length, ponds_harvested_size, fingerlings_bought, fingerlings_source, feeds_bought, feeds_source, fish_harvested)
VALUES ('$farmer_id', '$year', '$species', '$length', '$ponds_harvested_size', '$fingerlings_bought', '$fingerlings_source', '$feeds_bought', '$feeds_source','$fish_harvested')";

dbQuery($dbConn,$sql);
header('Location: ../add.php?v=assesments');	
}
}
function assesments($dbConn)
{   
$farmer_id =$_POST['id']; 
$assessor_id=$_SESSION['user_id'];
$assessor_observations=$_POST['assessor_observations']; 
$reason_farmer_qualifies=$_POST['reason_farmer_qualifies']; 
$assessor_remarks=$_POST['assessor_remarks'];

$sql = "SELECT farmer_id
FROM farms_assessments
WHERE farmer_id = '$farmer_id' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=assesments&error=' . urlencode('Farm already assessed'));	
} else {			
$sql   = "INSERT INTO farms_assessments (farmer_id, assessor_id, assessor_observations, reason_farmer_qualifies, assessor_remarks)
VALUES ('$farmer_id', '$assessor_id', '$assessor_observations', '$reason_farmer_qualifies', '$assessor_remarks')";

dbQuery($dbConn,$sql);
header('Location: ../View.php?v=Farmer');	
}
}

function editextfarminfo($dbConn)
{   
$f_id=$_POST['f_id'];
$farmer_id = $_POST["farmer_id"];
$water_availability = $_POST['water_availability'];
$water_sources = $_POST['water_sources'];
$sources=implode( ", ", $water_sources );
$water_mechanism = $_POST['water_mechanism'];
$date_registered = $_POST['date_registered'];
$farm_equipments = $_POST['farm_equipments'];
$otherequipments = $_POST['otherequipments'];
//$equipments=implode( ", ", $farm_equipments );
$farm_greenhouse = $_POST['farm_greenhouse'];
// $staff_total = $_POST['staff_total'];
$staff_fulltime = $_POST['staff_fulltime'];
$staff_parttime = $_POST["staff_parttime"];
$main_feed = $_POST['main_feed'];
$feeds_source=$_POST['feeds_source'];
$has_security = $_POST['has_security'];
$security_types = $_POST['security_types'];
$security=implode( ", ", $security_types );
$customers = $_POST['customers'];
$customers_others = $_POST['customers_others'];
$challenges = $_POST['challenges'];
$five_year_target = $_POST['five_year_target'];
$needs_to_reach_target=$_POST['needs_to_reach_target']; 
$receive_updates = $_POST['receive_updates'];
$can_host_trainings = $_POST['can_host_trainings'];
$modified_by=$_SESSION['user_id']; 

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';
// exit();


$sql   = "UPDATE farms_extended_data

SET  water_availability = '$water_availability',
water_sources = '$sources',
water_mechanism = '$water_mechanism',
farm_equipments = '$farm_equipments $otherequipments',
farm_greenhouse = '$farm_greenhouse',
-- staff_total = '$staff_total',
staff_fulltime = '$staff_fulltime',
staff_parttime = '$staff_parttime',
main_feeds = '$main_feed',
feeds_source='$feeds_source',
has_security = '$has_security',
security_types = '$security',
customers = '$customers',
customers_others = '$customers_others',
challenges = '$challenges',
five_year_target = '$five_year_target',
needs_to_reach_target='$needs_to_reach_target',
receive_updates = '$receive_updates',
can_host_trainings = '$can_host_trainings',
modified_by='$modified_by', 
date_modified=NOW()


WHERE farmer_id = '$farmer_id'";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editextfarminfo&id='.$f_id);		

}
function editbasicfarminfo($dbConn)
{   
$target_dir="images/";
$target_file=$target_dir .basename($_FILES["file"]["name"]);
$uploadOk=1;
$imageFileType =pathinfo($target_file,PATHINFO_EXTENSION);
$f_id=$_POST['f_id'];
$farmer_id = $_POST["farmer_id"];
$farm_name = $_POST['farm_name'];
$farm_county = $_POST['farm_county'];
$farm_subcounty = $_POST['farm_subcounty'];
$farm_village = $_POST['farm_village'];
$tot_no_of_tilapia=$_POST['tot_no_of_tilapia'];
$tot_no_of_catfish=$_POST['tot_no_of_catfish'];
$farm_longitude = $_POST['farm_longitude'];
$farm_latitude = $_POST['farm_latitude'];
$farm_landmark = $_POST['farm_landmark'];
$pond_sample_temperature = $_POST["pond_sample_temperature"];
$pond_sample_time = $_POST['pond_sample_time'];
$ponds_number = $_POST['ponds_number'];
$keeps_records = $_POST['keeps_records'];
$record_keeping = $_POST['record_keeping'];
$records_kept = $_POST['records_kept'];
$has_business_plan = $_POST['has_business_plan'];
$business_plan_last_update=$_POST['business_plan_last_update'];
$modified_by=$_SESSION['user_id']; 
if(!empty($_FILES['file']['name'])){
$check =getimagesize($_FILES["files"]["tmp_name"]);
if($check !== false){
$uploadOk=1;
}else{
header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id.'=' . urlencode('File uploaded is not image'));
$uploadOk=0;
}
if(file_exists($target_file)){
header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id.'=' . urlencode('Image already exists!'));
$uploadOk= 0;
}
if($_FILES["files"]["size"]>500000){
header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id.'=' . urlencode('File is too Large!'));
$uploadOk= 0;
}
if($imageFileType !="jpg" && $imageFileType !="png" && $imageFileType !="jpeg" && $imageFileType !="gif"){
header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id.'=' . urlencode('Invalid File!'));
$uploadOk= 0;
}
if ($uploadOk == 0){
header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id.'=' . urlencode('Upload not succesful!'));
}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';
// exit();
if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)){

$sql   = "UPDATE farms

SET  farm_name = '$farm_name',
farm_county = '$farm_county',
farm_subcounty = '$farm_subcounty',
farm_village = '$farm_village',
tot_no_of_tilapia='$tot_no_of_tilapia',
tot_no_of_catfish='$tot_no_of_catfish',
farm_longitude = '$farm_longitude',
farm_latitude = '$farm_latitude',
farm_landmark = '$farm_landmark',
pond_sample_temperature = '$pond_sample_temperature',
pond_sample_time = '$pond_sample_time',
ponds_number = '$ponds_number',
keeps_records = '$keeps_records',
record_keeping = '$record_keeping',
records_kept = '$records_kept',
has_business_plan = '$has_business_plan',
business_plan_last_update='$business_plan_last_update',
farmphoto='$target_file',
modified_by='$modified_by', 
date_modified=NOW()


WHERE farmer_id = '$farmer_id'";

dbQuery($dbConn,$sql);

header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id);

}
else{
header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id.'=' . urlencode('Upload not succesful!'));
}
}
else{
$sql   = "UPDATE farms

SET  farm_name = '$farm_name',
farm_county = '$farm_county',
farm_subcounty = '$farm_subcounty',
farm_village = '$farm_village',
tot_no_of_tilapia='$tot_no_of_tilapia',
tot_no_of_catfish='$tot_no_of_catfish',
farm_longitude = '$farm_longitude',
farm_latitude = '$farm_latitude',
farm_landmark = '$farm_landmark',
pond_sample_temperature = '$pond_sample_temperature',
pond_sample_time = '$pond_sample_time',
ponds_number = '$ponds_number',
keeps_records = '$keeps_records',
record_keeping = '$record_keeping',
records_kept = '$records_kept',
has_business_plan = '$has_business_plan',
business_plan_last_update='$business_plan_last_update',
modified_by='$modified_by', 
date_modified=NOW()


WHERE farmer_id = '$farmer_id'";

dbQuery($dbConn,$sql);

header('Location: ../edit.php?v=editbasicfarminfo&id='.$f_id);

}

}

function addcostinfo($dbConn)
{
	$pond=$_POST['pond'];  
	$cost_date=$_POST['cost_date'];  
	$farmer=$_POST['farmer'];  
	$feeds_costs=$_POST['feeds_costs'];  
	$fertilizer_cost=$_POST['fertilizer_cost'];  
	$manila_twine_cost=$_POST['manila_twine_cost'];  
	$catfish_price_per_fingerling=$_POST['catfish_price_per_fingerling'];  
	$tilapia_price_per_fingerling=$_POST['tilapia_price_per_fingerling'];  
	$lime_cost=$_POST['lime_cost'];  
	$fishing_net_cost=$_POST['fishing_net_cost']; 
	$extension_services_cost=$_POST['extension_services_cost'];  
	$sampling_cost=$_POST['sampling_cost']; 
	$newdate = str_replace('-', '/', $cost_date);
	$timestamp= date('Y-m-d', strtotime($newdate));
	$date=strtotime($timestamp);
	$year=date("Y",$date);
	$month=date("F",$date);
	$modified_by=$_SESSION['user_id']; 
	// echo "<pre>";
	//  print_r($_POST);
	//  echo"</pre>";
	//  exit();

$sql = "SELECT *
FROM farm_costs
WHERE farmer_id = '$farmer' and year='$year' and month='$month' and pond_number='$pond'";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {

header('Location: ../add.php?v=operatingcost&msg=<p class="text-danger">Farm Cost Information for selected period already exists.');
} 
else {
$sql= ("INSERT into farm_costs(pond_number,cost_date,farmer_id,feeds_costs,fertilizer_cost,manila_twine_cost,catfish_price_per_fingerling,tilapia_price_per_fingerling,lime_cost,fishing_net_cost,extension_services_cost,sampling_cost,year,month,modified_by,modified_date,date_created) values('$pond','$cost_date','$farmer','$feeds_costs','$fertilizer_cost','$manila_twine_cost','$catfish_price_per_fingerling','$tilapia_price_per_fingerling
    ','$lime_cost','$fishing_net_cost','$extension_services_cost','$sampling_cost','$year','$month','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../add.php?v=operatingcost&msg=<p class="text-success">Record Saved </p>');

}
}
function addrecurrentcost($dbConn)
{
	$pond=$_POST['pond'];  
	$date_cost_incurred=$_POST['date_cost_incurred'];  
	$farmer=$_POST['farmer'];  
	$annual_salary=$_POST['annual_salary'];  
	$other_labour=$_POST['other_labour'];  
	$electricity=$_POST['electricity'];  
	$fuel=$_POST['fuel'];  
	$rent_oxygen_tank=$_POST['rent_oxygen_tank'];  
	$price_per_oxygen_tank=$_POST['price_per_oxygen_tank'];  
	$transport=$_POST['transport']; 
	$administration_cost=$_POST['administration_cost'];
	$newdate = str_replace('-', '/', $date_cost_incurred);
	$timestamp= date('Y-m-d', strtotime($newdate));
	$date=strtotime($timestamp);
	$year_cost_incurred=date("Y",$date);
	$month_cost_incurred=date("F",$date);
	$modified_by=$_SESSION['user_id']; 
	 // echo "<pre>";
	 // print_r($_POST);
	 // echo"</pre>";
	 // exit();

$sql = "SELECT *
FROM recurrent_costs
WHERE farmer_id = '$farmer' and year_cost_incurred='$year_cost_incurred' and month_cost_incurred='$month_cost_incurred' and pond_number='$pond'";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {

header('Location: ../add.php?v=recurrentcost&msg=<p class="text-danger">Recurrent Cost Information for selected period already exists.');
} 
else {
$sql= ("INSERT into recurrent_costs(pond_number,date_cost_incurred,farmer_id,annual_salary,other_labour,electricity,fuel,rent_oxygen_tank,price_per_oxygen_tank,transport,administration_cost,year_cost_incurred,month_cost_incurred,created_by,modified_by,date_modified,date_created) values('$pond','$date_cost_incurred','$farmer','$annual_salary','$other_labour','$electricity','$fuel','$rent_oxygen_tank
    ','$price_per_oxygen_tank','$transport','$administration_cost','$year_cost_incurred','$month_cost_incurred',$created_by,'$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../add.php?v=recurrentcost&msg=<p class="text-success">Record Saved </p>');

}
}
function editcost($dbConn)
{
$pond_id = $_POST['pond_id'];   
$farmer_id = $_POST['farmerid'];
$tilapia_price_per_fingerling = $_POST['tilapia_price_per_fingerling'];
$catfish_price_per_fingerling = $_POST['catfish_price_per_fingerling'];
$feeds_costs = $_POST["feeds_costs"];
$feeds_kgs_bought = $_POST["feeds_kgs_bought"];
$fertilizer_cost = $_POST['fertilizer_cost'];
$lime_cost = $_POST['lime_cost'];
$manila_twine_cost = $_POST["manila_twine_cost"];
$fishing_net_cost = $_POST['fishing_net_cost'];
$extension_services_cost = $_POST['extension_services_cost'];
$equipment_cost = $_POST["equipment_cost"];
$water_costs = $_POST['water_costs'];
$transportation_cost = $_POST['transportation_cost'];
$sampling_cost = $_POST['sampling_cost'];
$month = $_POST['month'];
$year = $_POST['year'];
$modified_by=$_SESSION['user_id']; 
 
$sqlselect= "SELECT year,month,pond_number from farm_costs where farmer_id='$farmer_id' and month='$month' and year='$year' and pond_number='$pond_id'";
$result = dbQuery($dbConn,$sqlselect);

if (dbNumRows($result) == 1) {

$sql   = "UPDATE farm_costs

SET  
feeds_costs = '$feeds_costs',
feeds_kgs_bought = '$feeds_kgs_bought',
fertilizer_cost = '$fertilizer_cost',
lime_cost = '$lime_cost',
manila_twine_cost = '$manila_twine_cost',
tilapia_price_per_fingerling = '$tilapia_price_per_fingerling',
catfish_price_per_fingerling = '$catfish_price_per_fingerling',
fishing_net_cost = '$fishing_net_cost',
extension_services_cost = '$extension_services_cost',
-- equipment_cost = '$equipment_cost',
-- water_costs = '$water_costs',
-- transportation_cost = '$transportation_cost',
sampling_cost = '$sampling_cost',
month = '$month',
year = '$year',
modified_by='$modified_by', 
modified_date=NOW()



WHERE farmer_id='$farmer_id' and month='$month' and year='$year' and pond_number='$pond_id'";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editcost&id='.$pond_id);
}
else
{
$sql= ("INSERT into farm_costs(farmer_id,feeds_costs,feeds_kgs_bought,fertilizer_cost,lime_cost,manila_twine_cost,year,fishing_net_cost,extension_services_cost,sampling_cost,month,modified_by,modified_date,date_created) values('$farmer_id','$feeds_costs','$feeds_kgs_bought','$fertilizer_cost','$lime_cost','$manila_twine_cost','$year','$fishing_net_cost','$extension_services_cost','$sampling_cost','$month','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editcost&id='.$pond_id);
}

}


function addsales($dbConn)
{

$farmer_id = $_POST['farmer_id'];
$s_id=$_POST['s_id'];
$sales_date = $_POST['sales_date'];
$newdate = str_replace('-', '/', $sales_date);
$timestamp= date('Y-m-d', strtotime($newdate));
$date=strtotime($timestamp);
$sales_year=date("Y",$date);
$sales_month=date("F",$date);
$harvest_remaining=$_POST['remaining_harvest'];
$whole_tilapia = $_POST['whole_tilapia_fish_sold'];
$whole_tilapia_fish_avg_price=$_POST['whole_tilapia_fish_avg_price_kg'];
$whole_tilapia_income=$whole_tilapia * $whole_tilapia_fish_avg_price;
$value_added_tilapia=$_POST['value_added_tilapia_kgs_sold'];
$tilapia_value_added_avg_price=$_POST['tilapia_value_added_avg_price_kg'];
$va_tilapia_income=$value_added_tilapia*$tilapia_value_added_avg_price;
$tilapia_value_addition_form=$_POST['tilapia_value_addition_form'];
$tilapia_consumed=$_POST['tilapia_kgs_consumed'];
$spoilt_tilapia = $_POST['tilapia_kgs_spoilt'];
$whole_catfish = $_POST['whole_catfish_fish_sold'];
$whole_catfish_fish_avg_price=$_POST['whole_catfish_fish_avg_price_kg'];
$whole_catfish_income=$whole_catfish * $whole_catfish_fish_avg_price;
$value_added_catfish=$_POST['value_added_catfish_kgs_sold'];
$catfish_value_added_avg_price=$_POST['catfish_value_added_avg_price_kg'];
$va_catfish_income=$value_added_catfish*$catfish_value_added_avg_price;
$catfish_value_addition_form=$_POST['catfish_value_addition_form'];
$catfish_consumed=$_POST['catfish_kgs_consumed'];
$spoilt_catfish = $_POST['catfish_kgs_spoilt'];
$modified_by=$_SESSION['user_id'];
// echo "<pre>";
//  print_r($_POST);
//  echo"</pre>";
//  exit();
$sqlsales= ("INSERT into farm_sales_record(farmer_id,sales_month,sales_year,sales_date,whole_tilapia_fish_sold,whole_tilapia_fish_avg_price_kg,whole_tilapia_total_income,value_added_tilapia_kgs_sold,tilapia_value_added_avg_price_kg,va_tilapia_total_income,tilapia_value_addition_form,tilapia_kgs_consumed,tilapia_kgs_spoilt,whole_catfish_fish_sold,whole_catfish_total_income,whole_catfish_fish_avg_price_kg,value_added_catfish_kgs_sold,catfish_value_added_avg_price_kg,va_catfish_total_income,catfish_value_addition_form,catfish_kgs_consumed,catfish_kgs_spoilt,modified_by,date_modified,date_created) values('$farmer_id','$sales_month','$sales_year','$sales_date','$whole_tilapia','$whole_tilapia_fish_avg_price','$whole_tilapia_income','$value_added_tilapia','$tilapia_value_added_avg_price','$va_tilapia_income','$tilapia_value_addition_form','$tilapia_consumed','$spoilt_tilapia','$whole_catfish','$whole_catfish_income','$whole_catfish_fish_avg_price','$value_added_catfish','$catfish_value_added_avg_price','$va_catfish_income','$catfish_value_addition_form','$catfish_consumed','$spoilt_catfish','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sqlsales);

$sql = "SELECT sales_date as sdate,sales_year as syear,whole_tilapia_fish_sold as wholetilapiafishsold,whole_tilapia_fish_avg_price_kg as wholetilapiafishavgpricekg,whole_tilapia_total_income as wholetilapiatotalincome,value_added_tilapia_kgs_sold as valueaddedtilapiakgssold,tilapia_value_added_avg_price_kg as tilapiavalueaddedavgpricekg,va_tilapia_total_income as vatilapiatotalincome,tilapia_value_addition_form as tilapiavalueadditionform,tilapia_kgs_consumed as tilapiakgsconsumed,tilapia_kgs_spoilt as tilapiakgsspoilt,whole_catfish_fish_sold as wholecatfishfishsold,whole_catfish_fish_avg_price_kg as wholecatfishfishavgpricekg,whole_catfish_total_income as wholecatfishtotalincome,value_added_catfish_kgs_sold as valueaddedcatfishkgssold,catfish_value_added_avg_price_kg as catfishvalueaddedavgpricekg,va_catfish_total_income as vacatfishtotalincome,catfish_value_addition_form as catfishvalueadditionform,catfish_kgs_consumed as catfishkgsconsumed,catfish_kgs_spoilt as catfishkgsspoilt,remaining_harvest as remainingharvest
FROM farm_sales
WHERE farmer_id = '$farmer_id' and id='$s_id' ";
$result = dbQuery($dbConn,$sql);

if($row = dbFetchAssoc($result)) {
extract($row);
$newwhole_tilapia_fish_sold= $whole_tilapia+$wholetilapiafishsold;
$newwholetilapiatotalincome= $whole_tilapia_income+$wholetilapiatotalincome;
$newvalueaddedtilapiakgssold=$value_added_tilapia+$valueaddedtilapiakgssold;
$newva_tilapia_total_income=$va_tilapia_income+$vatilapiatotalincome;
$newtilapia_kgs_consumed=$tilapia_consumed+$tilapiakgsconsumed;
$newtilapia_kgs_spoilt=$spoilt_tilapia+$tilapiakgsspoilt;
$newwhole_catfish_fish_sold= $whole_catfish+$wholecatfishfishsold;
$newwholecatfishtotalincome= $whole_catfish_income+$wholecatfishtotalincome;
$newvalueaddedcatfishkgssold=$value_added_catfish+$valueaddedcatfishkgssold;
$newva_catfish_total_income=$va_catfish_income+$vacatfishtotalincome;
$newcatfish_kgs_consumed=$catfish_consumed+$catfishkgsconsumed;
$newcatfish_kgs_spoilt=$spoilt_catfish+$catfishkgsspoilt;
$totkgssold=$whole_tilapia+$value_added_tilapia+$tilapia_consumed+$spoilt_tilapia+$whole_catfish+$value_added_catfish+$catfish_consumed+$spoilt_catfish;
$newremaining_harvest=$harvest_remaining-$totkgssold;


$sql   = "UPDATE farm_sales

SET 
sales_date='$sales_date',
sales_month='$sales_month',
sales_year='$sales_year',
remaining_harvest='$newremaining_harvest',
whole_tilapia_fish_sold='$newwhole_tilapia_fish_sold',
whole_tilapia_fish_avg_price_kg='$whole_tilapia_fish_avg_price',
whole_catfish_fish_sold='$newwhole_catfish_fish_sold',
whole_catfish_fish_avg_price_kg='$whole_catfish_fish_avg_price',
value_added_tilapia_kgs_sold='$newvalueaddedtilapiakgssold',
tilapia_value_added_avg_price_kg='$tilapia_value_added_avg_price',
value_added_catfish_kgs_sold='$newvalueaddedcatfishkgssold',
catfish_value_added_avg_price_kg='$catfish_value_added_avg_price',
tilapia_value_addition_form='$tilapia_value_addition_form',
tilapia_kgs_consumed='$newtilapia_kgs_consumed',
catfish_value_addition_form='$catfish_value_addition_form',
catfish_kgs_consumed='$newcatfish_kgs_consumed',
tilapia_kgs_spoilt='$newtilapia_kgs_spoilt',
catfish_kgs_spoilt='$newcatfish_kgs_spoilt',
whole_tilapia_total_income='$newwholetilapiatotalincome',
va_tilapia_total_income='$newva_tilapia_total_income',
whole_catfish_total_income='$newwholecatfishtotalincome',
va_catfish_total_income='$newva_catfish_total_income',
modified_by='$modified_by', 
date_modified=NOW()
WHERE farmer_id='$farmer_id' and id='$s_id'";
dbQuery($dbConn,$sql);
header('Location: ../add.php?v=addsales&id='.$s_id.'&msg=<p class="text-success">Sales records updated</p>');
} else {
header('Location: ../add.php?v=addsales&id='.$s_id.'&msg=<p class="text-danger">Sales records Does not exist</p>');	
}
}

function addsampling($dbConn)
{

$pond_number = $_POST['pond_number'];
$farmer_id = $_POST['farmer_id'];
$remaining_pieces=$_POST['remaining_pieces'];
$last_sample_date=$_POST['last_sample_date'];
$sample_weight=$_POST['sample_weight'];
$main_feed_type=$_POST['main_feed_type'];
$kgs_feed=$_POST['kgs_feed'];
$fcr=$_POST['fcr'];
$modified_by=$_SESSION['user_id'];

// echo "<pre>";
//  print_r($_POST);
//  echo"</pre>";
//  exit();
$sql = "SELECT farmer_id, pond_number
FROM sampling_feeding
WHERE farmer_id = '$farmer_id[$i]' and pond_number ='$pond_number[$i]'";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../view.php?v=Sampling&error=' . urlencode('&nbsp&nbsp&nbspFarm Sampling Information for selected farm already exists'));	
} else {
$sql= ("INSERT into sampling_feeding(farmer_id,pond_number,main_feed_type,remaining_pieces,last_sample_date,sample_weight,kgs_feed,fcr,modified_by,modified_date,date_created) values('$farmer_id','$pond_number','$main_feed_type','$remaining_pieces','$last_sample_date','$sample_weight','$kgs_feed','$fcr','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../add.php?v=addfarminfo&id='.$farmer_id);	
}
}


function harvestinfo($dbConn)
{	
$pond_number = $_POST['pond_number'];
$farmer_id = $_POST['farmer_id'];
$havest_since_last_visit=$_POST['havest_since_last_visit'];
$harvest_date=$_POST['harvest_date'];
$harvest_type=$_POST['harvest_type'];
$pieces_harvested=$_POST['pieces_harvested'];
$avg_weight_piece=$_POST['avg_weight_piece'];
$total_weight_kg=$_POST['total_weight_kg'];
$feed_type=$_POST['feed_type'];
$production_cycle=$_POST['production_cycle'];
$fcr=$_POST['fcr'];
$modified_by=$_SESSION['user_id'];

// echo "<pre>";
//  print_r($_POST);
//  echo"</pre>";
//  exit();
$sql = "SELECT farmer_id, pond_number,harvest_date
FROM harvest_information
WHERE farmer_id = '$farmer_id' and pond_number ='$pond_number' and harvest_date='$harvest_date'";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=addfarminfo&id='.$farmer_id . urlencode('&nbsp&nbsp&nbspFarm Harvest Information for selected farm already exists'));	
} else {
$sql= ("INSERT into harvest_information(pond_number,farmer_id,pieces_harvested,havest_since_last_visit,harvest_date,harvest_type,avg_weight_piece,total_weight_kg,feed_type,production_cycle,fcr,modified_by,modified_date,date_created) values('$pond_number','$farmer_id','$pieces_harvested','$havest_since_last_visit','$harvest_date','$harvest_type','$avg_weight_piece','$total_weight_kg','$feed_type','$production_cycle','$fcr','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../add.php?v=addfarminfo&id='.$farmer_id);	
}

}

function editharvestinfo($dbConn)
{   
$farmer_id = $_POST["farmer_id"];
$s_id=$_POST['sid'];
$pond_number = $_POST['pondnumber'];
$harvest_date = $_POST['harvest_date'];
$harvest_type = $_POST["harvest_type"];
$tilapia_harvested = $_POST['tilapia_harvested'];
$avg_weight_tilapia_harvested = $_POST['avg_weight_tilapia_harvested'];
$catfish_harvested=$_POST['catfish_harvested'];
$avg_weight_catfish_harvested=$_POST['avg_weight_catfish_harvested'];
$tilapia_harvest=$_POST['tilapia_harvest'];
$catfish_harvest=$_POST['catfish_harvest'];
$tilapia_total_weight_kg=$tilapia_harvest*$avg_weight_tilapia_harvested;
$catfish_total_weight_kg=$catfish_harvest*$avg_weight_catfish_harvested;
$modified_by=$_SESSION['user_id']; 
$totalharvest=$tilapia_total_weight_kg + $catfish_total_weight_kg;
$tilapia=$tilapia_harvested + $tilapia_harvest;
$catfish=$catfish_harvested + $catfish_harvest;
$newdate = str_replace('-', '/', $harvest_date);
$timestamp= date('Y-m-d', strtotime($newdate));
$date=strtotime($timestamp);
$year=date("Y",$date);
// echo "<pre>";
//    print_r($_POST);
//    echo"</pre>";
//    exit();

$sql   = "UPDATE pond_stocking

SET harvest_date='$harvest_date',
harvest_type='$harvest_type',
tilapia_harvested='$tilapia',
avg_weight_tilapia_harvested='$avg_weight_tilapia_harvested',
catfish_harvested='$catfish',
avg_weight_catfish_harvested='$avg_weight_catfish_harvested',
modified_by='$modified_by', 
date_modified=NOW()
WHERE id='$s_id'";
dbQuery($dbConn,$sql);

$sqls = "SELECT total_harvest
FROM farm_sales
WHERE farmer_id = '$farmer_id' and harvest_year='$year'";
$result = dbQuery($dbConn,$sqls);

if($row = dbFetchAssoc($result)) {
extract($row);
$newtotal= $total_harvest+$totalharvest;
$sql   = "UPDATE farm_sales

SET 
total_harvest='$newtotal',
modified_by='$modified_by', 
date_modified=NOW()
WHERE farmer_id='$farmer_id' and harvest_year='$year'";
dbQuery($dbConn,$sql);
	
} else {
$sqlsales= ("INSERT into farm_sales(farmer_id,harvest_year,total_harvest,remaining_harvest,modified_by,date_modified,date_created) values('$farmer_id','$year','$totalharvest','$totalharvest','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sqlsales);
}
$sqlharvest= ("INSERT into harvest_information(pond_number,farmer_id,harvest_date,harvest_type,tilapia_avg_weight_piece,catfish_avg_weight_piece,tilapia_total_weight_kg,catfish_total_weight_kg,tilapia_pieces_harvested,catfish_pieces_harvested,modified_by,modified_date,date_created) values('$pond_number','$farmer_id','$harvest_date','$harvest_type','$avg_weight_tilapia_harvested','$avg_weight_catfish_harvested','$tilapia_total_weight_kg','$catfish_total_weight_kg','$tilapia_harvest','$catfish_harvest','$modified_by',NOW(),NOW())");	 
dbQuery($dbConn,$sqlharvest);
header('Location: ../edit.php?v=edit_harvest&id='.$s_id.'&msg=<p class="text-success">Harvest records updated</p>');

}
function editassesments($dbConn)
{ 
$f_id=$_POST['fid'];  
$aid = $_POST['aid'];
$date_assesed=$_POST['date_assesed'];
$assessor_name=$_POST['assessor_name'];
$remarks=$_POST['remarks'];
$modified_by=$_SESSION['user_id']; 
$recommendation=$_POST['recommendation'];

for($i=0;$i<count($aid);$i++)
{


$sql   = "UPDATE assessments

SET 
date_assesed='$date_assesed[$i]',
assessor_name='$assessor_name[$i]',
remarks='$remarks[$i]',
modified_by='$modified_by', 
recommendation='$recommendation[$i]',
date_modified=NOW()


WHERE id = '$aid[$i]'";
	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editassesmnt&id='.$f_id);	

}

}
function doassesments($dbConn)
{
$f_id=$_POST['fid'];   
$farmer_id = $_POST['farmerid'];
$date_assesed=$_POST['date_assesed'];
$assessor_name=$_POST['assessor_name']; 
$remarks=$_POST['remarks']; 
$recommendation=$_POST['recommendation'];
$modified_by=$_SESSION['user_id'];

//  	echo "<pre>";
// print_r($f_id);
// echo"</pre>";
// exit();

for($i=0;$i<count($date_assesed);$i++)
{
if($date_assesed[$i]!="" && $assessor_name[$i]!="" && $remarks[$i]!="" && $recommendation[$i]!="" )
{
$sql = "SELECT farmer_id, date_assesed,assessor_name
FROM assessments
WHERE farmer_id = '$farmer_id[$i]' and date_assesed = '$date_assesed[$i]' and assessor_name='$assessor_name[$i]' ";
$result = dbQuery($dbConn,$sql);
if (dbNumRows($result) == 1) {


header('Location: ../edit.php?v=editassesmnt&id='.$f_id.'&error=' . urlencode('&nbsp&nbsp&nbspThere exists another record for the date&nbsp&nbsp(' .$date_assesed[$i]. ')&nbsp&nbspfor the farm selected'));	
} else {

$sql= ("INSERT into assessments(farmer_id,date_assesed,assessor_name,remarks,recommendation,date_created,modified_by,date_modified) values('$farmer_id','$date_assesed[$i]','$assessor_name[$i]','$remarks[$i]','$recommendation[$i]',NOW(),$modified_by,NOW())");	 
dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=editassesmnt&id='.$f_id);	
}
}
else
{

header('Location: ../edit.php?v=editassesmnt&id='.$f_id.'=' . urlencode('Error uploading records'));
}
}
}
function addstock($dbConn)
{   
$farmer_id = $_POST["farmer_id"];
$s_id=$_POST['s_id'];
$pond_number = $_POST['id'];
$stocking_date = $_POST['stocking_date'];
$newdate = str_replace('-', '/', $stocking_date);
$timestamp= date('Y-m-d', strtotime($newdate));
$date1=strtotime($timestamp);
$date=strtotime(($timestamp)." +6 month");
$expected_harvest_date=date("Y-m-d",$date);
$stckingyear=date("Y",$date1);
$stckingmonth=date("F",$date1);
$expected_harvest_date=date("Y-m-d",$date);
$tilapia_fingerling_stocked = $_POST["tilapia_fingerling_stocked"];
$catfish_fingerling_stocked = $_POST['catfish_fingerling_stocked'];
$tilapia_price_per_fingerling = $_POST['tilapia_price_per_fingerling'];
$catfish_price_per_fingerling=$_POST['catfish_price_per_fingerling'];
$fingerlings_source=$_POST['fingerlings_source'];
$tilapia_fingerling_stocking_weight=$_POST['tilapia_fingerling_stocking_weight'];
$catfish_fingerling_stocking_weight=$_POST['catfish_fingerling_stocking_weight'];
$modified_by=$_SESSION['user_id'];



$sql = "SELECT farmer_id, stocking_year,stocking_month,pond_number
FROM pond_stocking
WHERE farmer_id = '$farmer_id' and stocking_year = '$stckingyear' and stocking_month='$stckingmonth' and pond_number='$pond_number' ";
$result = dbQuery($dbConn,$sql);

if (dbNumRows($result) == 1) {
header('Location: ../add.php?v=addstock&id='.$s_id.'&msg=' . urlencode('<p class="text-danger">Stock Information exists!</p>'));	
} else {			
$sql   = "INSERT INTO pond_stocking (farmer_id,pond_number,stocking_date,stocking_year,stocking_month,expected_harvest_date,tilapia_fingerling_stocked,catfish_fingerling_stocked,fingerlings_source,tilapia_fingerling_stocking_weight, catfish_fingerling_stocking_weight,modified_by,date_modified,date_created)
VALUES ('$farmer_id','$pond_number','$stocking_date','$stckingyear','$stckingmonth','$expected_harvest_date','$tilapia_fingerling_stocked','$catfish_fingerling_stocked','$fingerlings_source','$tilapia_fingerling_stocking_weight','$catfish_fingerling_stocking_weight','$modified_by',NOW(),NOW())";
dbQuery($dbConn,$sql);
$sql1   = "INSERT INTO farm_costs (farmer_id,pond_number,cost_date,year,month,tilapia_price_per_fingerling,catfish_price_per_fingerling,modified_by,modified_date,date_created)
VALUES ('$farmer_id','$pond_number','$stocking_date','$stckingyear','$stckingmonth','$tilapia_price_per_fingerling','$catfish_price_per_fingerling','$modified_by',NOW(),NOW())";

dbQuery($dbConn,$sql1);
header('Location: ../add.php?v=addstock&id='.$s_id.'&msg=' . urlencode('<p class="text-success">Stock Information Added</p>'));
}
}
function editstockinfo($dbConn)
{   
$farmer_id = $_POST["farmer_id"];
$s_id=$_POST['sid'];
$pond_number = $_POST['pondnumber'];
$stocking_date = $_POST['stocking_date'];
$timestamp = strtotime($stocking_date);
$stckingyear=date("Y",$timestamp);
$stckingmonth=date("F",$timestamp);
$tilapia_fingerling_stocked = $_POST["tilapia_fingerling_stocked"];
$catfish_fingerling_stocked = $_POST['catfish_fingerling_stocked'];
$tilapia_price_per_fingerling = $_POST['tilapia_price_per_fingerling'];
$catfish_price_per_fingerling=$_POST['catfish_price_per_fingerling'];
$fingerlings_source=$_POST['fingerlings_source'];
$tilapia_fingerling_stocking_weight=$_POST['tilapia_fingerling_stocking_weight'];
$catfish_fingerling_stocking_weight=$_POST['catfish_fingerling_stocking_weight'];
$modified_by=$_SESSION['user_id'];



// echo "<pre>";
//    print_r($_POST);
//    echo"</pre>";
//    exit();

$sql   = "UPDATE pond_stocking 

SET 
stocking_date='$stocking_date',
stocking_year='$stckingyear',
stocking_month='$stckingmonth',
tilapia_fingerling_stocked='$tilapia_fingerling_stocked',
catfish_fingerling_stocked='$catfish_fingerling_stocked',
fingerlings_source='$fingerlings_source',
catfish_fingerling_stocking_weight='$catfish_fingerling_stocking_weight',
tilapia_fingerling_stocking_weight='$tilapia_fingerling_stocking_weight',
modified_by='$modified_by', 
date_modified=NOW()


WHERE id='$s_id'";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=addeditstock&id='.$s_id.'&msg=' . urlencode('<p class="text-success">Stock updated</p>'));

}

function editsample($dbConn)
{   
$farmer_id = $_POST["farmer_id"];
$s_id=$_POST['s_id'];
$pond_number = $_POST['pond_number'];
$sample_date = $_POST['sample_date'];
$no_of_catfish_sampled = $_POST["no_of_catfish_sampled"];
$no_of_tilapia_sampled = $_POST['no_of_tilapia_sampled'];
$tot_tilapia_sample_weight = $_POST['tot_tilapia_sample_weight'];
$tot_catfish_sample_weight=$_POST['tot_catfish_sample_weight'];
$modified_by=$_SESSION['user_id'];


$sql   = "UPDATE farms_ponds 

SET sample_date='$sample_date',
no_of_catfish_sampled='$no_of_catfish_sampled',
no_of_tilapia_sampled='$no_of_tilapia_sampled',
tot_tilapia_sample_weight='$tot_tilapia_sample_weight',
tot_catfish_sample_weight='$tot_catfish_sample_weight',
modified_by='$modified_by', 
date_modified=NOW()


WHERE farmer_id = '$farmer_id' and pond_number='$pond_number' and id='$s_id' ";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=operation&id='.$s_id.'=' . urlencode('Pond sampling data  updated'));

}

function editsales($dbConn)
{   
$farmer_id = $_POST["farmer_id"];
$s_id=$_POST['s_id'];
$total_harvest = $_POST['total_harvest'];
$whole_tilapia_fish_sold = $_POST['whole_tilapia_fish_sold'];
$whole_tilapia_fish_avg_price_kg = $_POST["whole_tilapia_fish_avg_price_kg"];
$whole_catfish_fish_sold = $_POST['whole_catfish_fish_sold'];
$whole_catfish_fish_avg_price_kg = $_POST['whole_catfish_fish_avg_price_kg'];
$value_added_tilapia_kgs_sold=$_POST['value_added_tilapia_kgs_sold'];
$tilapia_value_added_avg_price_kg=$_POST['tilapia_value_added_avg_price_kg'];
$value_added_catfish_kgs_sold=$_POST['value_added_catfish_kgs_sold'];
$catfish_value_added_avg_price_kg=$_POST['catfish_value_added_avg_price_kg'];
$tilapia_value_addition_form=$_POST['tilapia_value_addition_form'];
$tilapia_kgs_consumed=$_POST['tilapia_kgs_consumed'];
$catfish_value_addition_form=$_POST['catfish_value_addition_form'];
$catfish_kgs_consumed=$_POST['catfish_kgs_consumed'];
$tilapia_kgs_spoilt=$_POST['tilapia_kgs_spoilt'];
$catfish_kgs_spoilt=$_POST['catfish_kgs_spoilt'];
$modified_by=$_SESSION['user_id'];


$sql   = "UPDATE farm_sales 

SET 
total_harvest='$total_harvest',
whole_tilapia_fish_sold='$whole_tilapia_fish_sold',
whole_tilapia_fish_avg_price_kg='$whole_tilapia_fish_avg_price_kg',
whole_catfish_fish_sold='$whole_catfish_fish_sold',
whole_catfish_fish_avg_price_kg='$whole_catfish_fish_avg_price_kg',
value_added_tilapia_kgs_sold='$value_added_tilapia_kgs_sold',
tilapia_value_added_avg_price_kg='$tilapia_value_added_avg_price_kg',
value_added_catfish_kgs_sold='$value_added_catfish_kgs_sold',
catfish_value_added_avg_price_kg='$catfish_value_added_avg_price_kg',
tilapia_value_addition_form='$tilapia_value_addition_form',
tilapia_kgs_consumed='$tilapia_kgs_consumed',
catfish_value_addition_form='$catfish_value_addition_form',
catfish_kgs_consumed='$catfish_kgs_consumed',
tilapia_kgs_spoilt='$tilapia_kgs_spoilt',
catfish_kgs_spoilt='$catfish_kgs_spoilt',
modified_by='$modified_by', 
date_modified=NOW()


WHERE farmer_id = '$farmer_id' ";

dbQuery($dbConn,$sql);
header('Location: ../edit.php?v=operation&id='.$s_id.'=' . urlencode('Pond updated'));

}



/*
function used to delete farm
*/
function deleteLab()
{
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
$id = (int)$_GET['id'];
} else {
header('Location: index.php');
}

$sql = "DELETE FROM tbl_depts 
WHERE id = $id";
dbQuery($dbConn,$sql);

header('Location: ../menu.php?v=LABS');
}
?>