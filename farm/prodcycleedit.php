<?php
require_once 'farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}

$errorMessage = "";

$fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);

  }
  $sql="SELECT p.id as pid,  p.farmer_id,p.year,p.species,p.length,p.ponds_harvested_size,p.fingerlings_bought,p.fingerlings_source,p.feeds_bought,p.feeds_source,p.fish_harvested,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_production_cycles p on f.farmer_id=p.farmer_id where f_id='$f_id' ";

$result = dbQuery($dbConn,$sql);

?> 

<div class="prepend-1 span-12">
<!--  -->
<h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Edit Additional information for:<?php echo $farm_name; ?> Farm</h6>
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-12">
<table class="table table-striped table-bordered table-sm">
                <thead>
               
                <th>Cycle Year</th>
                <th>Species</th>
                <th>Length of Cycle</th>
                <th>Size of ponds harvested (m2)</th>
                <th>Number of Fingerlings bought</th>
                <th>Source of Fingerlings</th>
                <th>Kgs of Feeds bought</th>
                <th>Source of Feeds</th>
                <th>Kgs of fish harvested</th>
                </thead>
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=pond" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
         <?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  
?>
  <tr class="<?php echo $class; ?>">
   <td><?php echo $year; ?></td>
   <td><a href="javascript:(<?php echo $pid; ?>);" onClick="div_show()"><?php echo $species; ?></a></td>
   <td><?php echo $length;?></td>
   <td><?php echo $ponds_harvested_size;?></td>
   <td><?php echo $fingerlings_bought;?></td>
   <td><?php echo $fingerlings_source; ?></td>
   <td><?php echo $feeds_bought;?></td>
   <td><?php echo $feeds_source;?></td>
   <td><?php echo $fish_harvested;?></td>
  </tr>
<?php
} // end while

?>
    </form>
</tbody></table>
<tr><td colspan="24" align="center"><input name="btnAddUser" type="button" id="btnAddUser" value="Add Production cycle(+)"  class="button" onClick="div_showx()"></td></tr>
</div>


</div>
<div class="prepend-1 span-12">
<div class="col-md-12">
<tbody>

<body id="body" style="overflow:hidden; ">

<div id="abc">
<!-- Popup Div Starts Here -->

<div id="" class="" role="dialog" style="margin-top: 100px;">

  <div class="modal-dialog ">
   
 
    <!-- Start: Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" onClick="div_hide()">&times;</button>
        <h4 class="modal-title">Edit Production Cycle Information</h4>
      </div>
      <div id="validation-error"></div>
    <div class="cl"></div>
      <div class="modal-body" >
    
    <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=editproductioncycle" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser" role="form" class="form-inline" role="form" >
    
    <input class="form-control" type="hidden" name="cid" type="text" id="cid" value="<?php echo $id; ?>" readonly />
    <div class="form-group row">
    <label for="year" class="col-sm-5 col-form-label">Cycle Year :<input class="form-control" name="year" type="text" id="" value="<?php echo $year; ?>"></label>
    <label for="length" class="col-sm-5 col-form-label">Length of Cycle :<input class="form-control" name="length" type="text" id="" value="<?php echo $length; ?>"></label>
    </div>
    <div class="form-group row">
    <label for="species" class="col-sm-5 col-form-label">Species &nbsp; :<input class="form-control" name="species"  id="" value="<?php echo $species; ?>"></label>
    <label for="ponds_harvested_size" class="col-sm-5 col-form-label">Size of ponds harvested :<input class="form-control" name="ponds_harvested_size" type="email" id="" value="<?php echo $ponds_harvested_size; ?>"></label>
    </div>
    <div class="form-group row">
    <label for="fingerlings_bought" class="col-sm-5 col-form-label">Number of Fingerlings bought :<input class="form-control" name="fingerlings_bought" type="text" id="" value="<?php echo $fingerlings_bought; ?>"></label>
    <label for="fingerlings_source" class="col-sm-5 col-form-label">Source of Fingerlings :<input class="form-control" name="fingerlings_source" type="text" id="" value="<?php echo $fingerlings_source; ?>"></label>
    </div>
    <div class="form-group row">
    <label for="fingerlings_bought" class="col-sm-5 col-form-label">Kgs of Feeds bought :<input class="form-control" name="fingerlings_bought" type="tel" id="" value="<?php echo $fingerlings_bought; ?>"></label>
    <label for="feeds_source" class="col-sm-5 col-form-label">Source of Feeds :<input class="form-control" name="feeds_source" type="email" id="" value="<?php echo $feeds_source; ?>"></label>
    </div>
    <div class="form-group row">
    <label for="fish_harvested" class="col-sm-5 col-form-label">Kgs of fish harvested :<input class="form-control" name="fish_harvested" type="tel" id="" value="<?php echo $fish_harvested; ?>"></label>
    </div>
    
    <div class="modal-footer">
   
    <input name="submit" id="submit_popup" type="submit" value="SUBMIT" class="btn btn-primary" />
      <button type="button" class="btn btn-default" onClick="div_hide()" >Close</button>
    </div>
    
  </form>
  
    </div>
    
      </div>
      
    </div>
  </div>
  
</div>

</body>

<body id="body" style="overflow:hidden;">
<div id="abcd" style="margin-top: -250px !important;">
<!-- Popup Div Starts Here -->
<div id="" class="" role="dialog" style="margin-top: 100px;" >
  <div class="modal-dialog " style="width: 80%; margin-left: 15%;">
    <!-- Start: Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <!-- <button type="button" class="close" onClick="div_hidex()">&times;</button> -->
        <h4 class="modal-title">Add Production Cycle <div style="float: right;"><button class="btn btn-success" id="AddRow" type="button" s><i class="glyphicon glyphicon-plus" ></i></button>
      <button class="btn btn-danger remove" id="deleteRow" type="button" ><i class="glyphicon glyphicon-remove"></i></button></div></h4>
        
      </div>
      
      <div class="modal-body" >
     <table class="table table-striped table-sm">
       <thead>
                <th width="11%">Year</th>
                <th width="11%">Length</th>
                <th width="11%">Species</th>
                <th width="11%">Size of pond harvested</th>
                <th width="11%">Fingerlings bought</th>
                <th width="11%">Source</th>
                <th width="11%">Kgs of Feeds</th>
                <th width="11%">Source of Feeds</th>
                <th width="11%">Kgs of fish harvested</th>
                
                
                
                </thead>
        </table>
        <form action="<?php echo WEB_ROOT; ?>farm/processfarm.php?action=addproductioncycle" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
               <input type="hidden"  class="form-control" name="fid" value="<?php echo $f_id; ?>">
               <input type="hidden" class="form-control" name="farmerid" value="<?php echo $farmer_id; ?>"/>
               <td><input type="text" class="form-control" required="required" name="year[]"/></td>
               <td><input type="text" class="form-control" required="required" name="length[]"/></td>
               <td><input type="text" class="form-control" required="required" name="species[]"/></td>
               <td><input type="text" class="form-control"  class="small"  name="ponds_harvested_size[]"/></td>
               <td><input type="text" class="form-control" required="required" name="fingerlings_bought[]"/></td>
               <td><input type="text" class="form-control" required="required" name="fingerlings_source[]"/></td>
               <td><input type="text" class="form-control" required="required" name="feeds_bought[]"/></td>
               <td><input type="text" class="form-control"  class="small"  name="feeds_source[]"/></td>
               <td><input type="text" class="form-control" required="required" name="fish_harvested[]"></td>
          
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <div class="modal-footer">
   
    <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
      <button type="button" class="btn btn-default" onClick="div_hidex()" >Close</button>
    </div>
  </form>
    
    </div>
    
      </div>
      
    </div>
  </div>
</div>
</body>    
</tbody>
</div>
</div>
    




