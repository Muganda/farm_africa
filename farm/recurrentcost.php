<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
$sql = "SELECT r.id as rid,r.farmer_id,r.pond_number as pond_id,p.pond_number as pond,r.year_cost_incurred,r.annual_salary,r.other_labour,r.electricity,r.fuel,r.rent_oxygen_tank,r.price_per_oxygen_tank,r.transport,r.administration_cost from recurrent_costs r join farms_ponds p on r.pond_number=p.id where r.farmer_id!='' group by r.id,r.year_cost_incurred";

 $result = dbQuery($dbConn,$sql);

?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><font color="">Recurrent Costs &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; <a href="<?php echo WEB_ROOT; ?>csv/cost.csv" >Download</a>&ensp;CSV&ensp; &ensp; &ensp; &ensp; &ensp;&ensp; &ensp; &ensp; &ensp; &ensp;</font></h5><h4><?php echo $errorMessage; ?></h4>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">

            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%; ">
              <thead>
                  <tr>
                   <td><b>Farmer ID</td>
                   <td><b>Pond </td>
                   <td><b>Year Cost incurred</td>
                   <td><b>Annual Salary</td>
                   <td><b>Other Labour</td>
                   <td><b>Electricity</td>
                   <td><b>Fuel</td>
                   <td><b>Oxygen Tank Rent</td>
                   <td><b>Price Per Oxygen Tank</td>
                   <td><b>Transport</td>
                   <td><b>Administration Cost</td>
                  </tr>
                  
                </thead>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>">
                   <td><a href="javascript:editfarmcosts(<?php echo $pond_id; ?>);"><?php echo $farmer_id; ?></a></td>
                   <td><?php echo $pond; ?></td>
                   <td><?php echo $year_cost_incurred; ?></td>
                   <td><?php echo $annual_salary; ?></td>
                   <td><?php echo $other_labour; ?></td>
                   <td><?php echo $electricity; ?></td>
                   <td><?php echo $fuel; ?></td>
                   <td><?php echo $rent_oxygen_tank; ?></td>
                   <td><?php echo $price_per_oxygen_tank; ?></td>
                   <td><?php echo $transport; ?></td>
                   <td><?php echo $administration_cost; ?></td>
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
          </table>
  <form action="<?php echo WEB_ROOT; ?>farm/importrcrcost.php" method="post" enctype="multipart/form-data" id="import_form">
  <div class="col-md-3">
  <input type="file" name="file" style="float: left;"/>
  </div>
  <div class="col-md-5">
  <input type="submit" class="btn btn-primary" name="import_data" value="Import">
  </form>
   <input name="btnAddUser" type="button" id="btnAddUser" value="Add Recurrent Costs (+)" class="btn btn-default" onClick="addrcrcost()">
  </div>

</div>

</div>
</div>

</div>
