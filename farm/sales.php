<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms where id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  $farmerid=$farmer_id;
}
$sql = "SELECT id,harvest_year,total_harvest,farmer_id,whole_tilapia_fish_sold,whole_tilapia_fish_avg_price_kg,value_added_tilapia_kgs_sold,tilapia_value_added_avg_price_kg,tilapia_value_addition_form,tilapia_kgs_consumed,tilapia_kgs_spoilt,whole_catfish_fish_sold,whole_catfish_fish_avg_price_kg,value_added_catfish_kgs_sold,catfish_value_added_avg_price_kg,catfish_value_addition_form,catfish_kgs_consumed,catfish_kgs_spoilt,date_created,date_modified from farm_sales  where farmer_id='$farmerid' order by id";

$result = dbQuery($dbConn,$sql);

?>
<?php require_once 'farm_operationstab.php';?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <!-- <h5><font color="">Sales&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5> -->
          <h4><?php echo $errorMessage; ?></h4>

          <div class="table-responsive">
            <table id="paging" class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>#</td>
                       <td><b>Year Harvested</td>
                       <td><b>Total Harvest(Kgs)</td>
                       <td><b>Whole Tilapia sold(Kgs)</td>
                       <td><b>Average price per kg</td>
                       <td><b>KGs of Value added sold</td>
                       <td><b>Average price/Kg of value added(Kshs)</td>
                       <td><b>Form of Value addition</td>
                       <td><b>Kgs consumed</td>
                       <td><b>Kgs spoilt</td>
                       <td><b>Whole Catfish sold(Kgs)</td>
                       <td><b>Average price per kg</td>
                       <td><b>KGs of Value added sold</td>
                       <td><b>Average price/Kg of value added(Kshs)</td>
                       <td><b>Form of Value addition</td>
                       <td><b>Kgs consumed</td>
                       <td><b>Kgs spoilt</td>
                          
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($result)) {
                      extract($row);
                      static $salesno=0;
                      $salesno++;
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                        <td><?php echo $salesno; ?></td>
                        <td><a href="javascript:addsales(<?php echo $id; ?>);"><?php echo $harvest_year; ?></a></td>
                        <td><?php echo $total_harvest; ?></td>
                       <td><?php echo $whole_tilapia_fish_sold; ?></td>
                       <td><?php echo $whole_tilapia_fish_avg_price_kg; ?></td>
                       <td><?php echo $value_added_tilapia_kgs_sold; ?></td>
                       <td><?php echo $tilapia_value_added_avg_price_kg; ?></td>
                       <td><?php echo $tilapia_value_addition_form; ?></td>
                       <td><?php echo $tilapia_kgs_consumed; ?></td>
                       <td><?php echo $tilapia_kgs_spoilt; ?></td>
                       <td><?php echo $whole_catfish_fish_sold; ?></td>
                       <td><?php echo $whole_catfish_fish_avg_price_kg; ?></td>
                       <td><?php echo $value_added_catfish_kgs_sold; ?></td>
                       <td><?php echo $catfish_value_added_avg_price_kg; ?></td>
                       <td><?php echo $catfish_value_addition_form; ?></td>
                       <td><?php echo $catfish_kgs_consumed; ?></td>
                       <td><?php echo $catfish_kgs_spoilt; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                      
                     </tbody>
                    </table>
              Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/sales.csv" >Sales</a>&ensp;CSV
            <form action="<?php echo WEB_ROOT; ?>farm/importsales.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Addsales (+)" onclick="javascript:addsales(<?php echo $f_id; ?>)" style="float: right;">
                  
            </form>
          
        </div>

      </div>

    </div>
  </div>

</div>