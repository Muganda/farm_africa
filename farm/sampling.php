<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms where id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  $farmerid=$farmer_id;
}
$sql = "SELECT s.id as sid,s.farmer_id,p.id,p.pond_number as name,s.pond_number,s.sample_date,s.tot_tilapia_sample_weight,(s.tot_tilapia_sample_weight/s.no_of_tilapia_sampled) as avg_tilapia_sample_weight,s.no_of_tilapia_sampled,s.tot_catfish_sample_weight,(s.tot_catfish_sample_weight/s.no_of_catfish_sampled) as avg_catfish_sample_weight,s.no_of_catfish_sampled,s.tilapia_fingerling_stocked,s.catfish_fingerling_stocked,s.tilapia_fingerling_stocking_weight,s.catfish_fingerling_stocking_weight from pond_stocking s join farms_ponds p on p.id=s.pond_number  where s.farmer_id='$farmerid' order by id ";

$result = dbQuery($dbConn,$sql);

?>
<?php require_once 'farm_operationstab.php';?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <!-- <h5><font color="">Pond Sampling&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5> -->
          <h4><?php echo $errorMessage; ?></h4>

          <div class="table-responsive">
            <table id="paging" class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>#</td>
                       <td><b>Pond No</td>
                       <td><b>Sampling Date</td>
                       <td><b>No. of Tilapia</td>
                       <td><b>No. of Tilapia sampled</td>
                       <td><b>Total Tilapia weight sampled</td>
                       <td><b>Average Tilapia weight sampled</td>
                       <td><b>Average Tilapia Stocking weight</td>
                       <td><b>No. of Catfish</td>
                       <td><b>No. of Catfish sampled</td>
                       <td><b>Total Catfish weight sampled</td>
                       <td><b>Average Catfish weight sampled</td>
                       <td><b>Average Catfish Stocking weight</td>
                      
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($result)) {
                      extract($row);
                      static $sampleno=0;
                      $sampleno++;
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                        <td><?php echo $sampleno; ?></td>
                        <td><a href="javascript:harvest(<?php echo $sid; ?>);"><?php echo $name; ?></a></td>
                       <td><?php echo $sample_date; ?></td>
                       <td><?php echo $tilapia_fingerling_stocked; ?></td>
                       <td><?php echo $no_of_tilapia_sampled; ?></td>
                       <td><?php echo $tot_tilapia_sample_weight; ?></td>
                       <td><?php echo number_format((float)$avg_tilapia_sample_weight,2); ?></td>
                       <td><?php echo $tilapia_fingerling_stocking_weight; ?></td>
                       <td><?php echo $catfish_fingerling_stocked; ?></td>
                       <td><?php echo $no_of_catfish_sampled; ?></td>
                       <td><?php echo $tot_catfish_sample_weight; ?></td>
                       <td><?php echo number_format((float)$avg_catfish_sample_weight,2); ?></td>
                       <td><?php echo $catfish_fingerling_stocking_weight; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                     </tbody>
                  </table> 
          Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/sampling.csv" >Sampling</a>&ensp;CSV
          <form action="<?php echo WEB_ROOT; ?>farm/importsampling.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Sample (+)" onclick="javascript:addsample(<?php echo $f_id; ?>)" style="float: right;">
            </form>
          
        </div>

      </div>

    </div>
  </div>

</div>