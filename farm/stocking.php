<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms where id='$f_id'";
$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  $farmerid=$farmer_id;
}
$sql = "SELECT s.id as sid,p.id,p.pond_number as name,s.pond_number,s.farmer_id,s.catfish_fingerling_stocked,s.tilapia_fingerling_stocked,s.stocking_date,s.expected_harvest_date,s.fingerlings_source,s.tilapia_fingerling_stocking_weight,s.catfish_fingerling_stocking_weight from pond_stocking s join farms_ponds p on p.id=s.pond_number  where s.farmer_id='$farmerid' order by sid";

$result = dbQuery($dbConn,$sql);

?>
<?php require_once 'farm_operationstab.php';?>
<div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <!-- <h5><font color="">Pond Stocking&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5> -->
          <h4><?php echo $errorMessage; ?></h4>

          <div class="table-responsive">
            <table id="paging" class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>#</td>
                       <td><b>Name of pond</td>
                       <!-- <td><b>Size of pond in M2</td> -->
                       <td><b>Date stocked</td>
                       <td><b>Expected Harvest Date</td>
                       <td><b>Tilapia stocked</td>
                       <td><b>Stocking  weight (grams)</td>
                       <td><b>Catfish stocked</td>
                       <td><b>Stocking  weight (grams)</td>
                       <td><b>Source of fingerlings</td>
                       
                      
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($result)) {
                      extract($row);
                      static $stockno=0;
                      $stockno++;
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                       <td><?php echo $stockno; ?></td>
                       <td><a href="javascript:editstock(<?php echo $sid; ?>);"><?php echo $name; ?></a></td>
                       <td><?php echo $stocking_date; ?></td>
                       <td><?php echo $expected_harvest_date; ?></td>
                       <td><?php echo $tilapia_fingerling_stocked; ?></td>
                       <td><?php echo $tilapia_fingerling_stocking_weight; ?></td>
                       <td><?php echo $catfish_fingerling_stocked; ?></td>
                       <td><?php echo $catfish_fingerling_stocking_weight; ?></td>
                       <td><?php echo $fingerlings_source; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                     </tbody>
                  </table> 
          Download&ensp;<a href="<?php echo WEB_ROOT; ?>csv/stocking.csv" >Stocking</a>&ensp;CSV
            <form action="<?php echo WEB_ROOT; ?>farm/importstock.php" method="post" enctype="multipart/form-data" id="import_form">

                  <input type="file" class="btn btn-default" name="file" style="float: left;"/>&ensp;
                  <input type="submit" class="btn btn-default" name="import_data" value="Import" style="float: center;">
                  <input type="button" class="btn btn-primary" name="btnAddUser" value="Add Stock (+) " onclick="javascript:addstock(<?php echo $f_id; ?>)" style="float: right;">
                  
                  
            </form>
          
        </div>

      </div>

    </div>
  </div>

</div>