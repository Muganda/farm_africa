<?php
require_once 'addtab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = '&nbsp;';
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

$sql = "SELECT id, type FROM farmer_reg_type";
$result = dbQuery($dbConn,$sql);
?> 

<h3 class="catHead"><b>&nbsp;&nbsp;Enrol Farmer</h3>
<div class="prepend-1 span-12">
<div class="errorMessage">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $errorMessage; ?></div>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farmer/processFarmer.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group row" >
  <label for="farmer_id" class="col-md-3 col-form-label">Recommended Farmer ID:<input class="form-control" name="farmer_id" type="text" id="farmer_id" value="" required ></label>
  <label for="registration" class="col-md-3 col-form-label">Registration Type:<select class="form-control" name="registration"><?php getRegistrationtype($dbConn);?></select></label></label>
</div>
<div class="form-group row">
  <label for="txtFname" class="col-md-3 col-form-label">First Name:<input class="form-control" name="txtFname" type="text" id="txtFname" value="" required ></label>
  <label for="txtLname" class="col-md-3 col-form-label">Last Name: <input class="form-control" name="txtLname" type="text" id="txtLname" value="" required ></label>
  <label for="owner_telephone" class="col-md-3 col-form-label">Telephone:<input class="form-control" name="owner_telephone" type="text" id="owner_telephone" value="" required="" ></label>
  </div>
<div class="form-group row" >
  <label for="owner_email" class="col-md-3 col-form-label">Email:<input class="form-control" name="owner_email" type="email" id="owner_email" value="" ></label>
  <label for="owner_gender" class="col-md-3 col-form-label">Gender:<select class="form-control" name="owner_gender"><?php getGender($dbConn);?></select></label>
  <label for="age" class="col-md-3 col-form-label">Age:<input class="form-control" name="age" type="number" id="age" value=""  ></label>
 
</div>

 <div class="form-group row" >
  <label for="type_of_enterprise" class="col-md-3 col-form-label">Type of Enterprise:<select class="form-control" name="type_of_enterprise"><?php getEnterprise($dbConn);?></select></label>  <label for="occupation" class="col-md-3 col-form-label">Occupation:<input class="form-control" name="occupation" type="text" id="occupation" value="" ></label>

  <label for="finance_source" class="col-sm-3 control-label">Source of Finances:<select class="selectpicker" data-show-subtext="true" data-live-search="true" id="" name="finance_source[]" multiple="multiple"><?php getFinancesources($dbConn);?>
      </select></label>

  
  
  </div>


 <p align="left"> 
  <input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
  <input name="btnCancel" id="btnCancel" type="button" value="Cancel" class="btn btn-danger" onClick="window.location.href='view.php?v=Farmer';" />
  
 </p>
</form>
 </tbody>
</table>
</div>


</div>