<?php
//require_once '../farmer/edittab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";


if (isset($_GET['farmer_id']) && (int)$_GET['farmer_id'] > 0) {
  $farmer_id = (int)$_GET['farmer_id'];
} else {
  header('Location: ../index.php');
}

$sql = "SELECT farmer_id,owner_name,owner_telephone,owner_gender,owner_email,registration,finance_source
        FROM farms_owners
        WHERE farmer_id = '$farmer_id'";
$result = dbQuery($dbConn,$sql);


?> 


<div class="prepend-1 span-12">
<p align="center"><strong><font color="#660000"><?php echo $errorMessage; ?></font></strong></p>
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($result)){
extract($row);
?>
<div class="col-md-12">
<table class="table table-striped table-bordered">
   <tbody>
   <form action="<?php echo WEB_ROOT; ?>farmer/processFarmer.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group " >
  <label for="recruitment_source" class="col-md-3 col-form-label">Recruitment Source</label>
  <select name="recruitment_source">
  <option >Select</option>
  <?php
  while($row = dbFetchAssoc($result)) {
    extract($row);
  ?>
  <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
  <?php
  }
  ?>
  </select>
</div>

<div class="form-group ">
  <label for="farmer_id" class="col-md-3 col-form-label">Recommended Farmer ID:<input class="form-control" name="farmer_id" type="text" id="farmer_id" value="" required ></label>
  <label for="txtFname" class="col-md-3 col-form-label">First Name:<input class="form-control" name="txtFname" type="text" id="txtFname" value="" required ></label>
  <label for="txtLname" class="col-md-3 col-form-label">Last Name: <input class="form-control" name="txtLname" type="text" id="txtLname" value="" required ></label>
  </div>
<div class="form-group " >
  <label for="owner_telephone" class="col-md-3 col-form-label">Telephone:<input class="form-control" name="owner_telephone" type="text" id="owner_telephone" value="" required="" ></label>
  <label for="owner_email" class="col-md-3 col-form-label">Email:<input class="form-control" name="owner_email" type="email" id="owner_email" value="" required="" ></label>
  <label for="owner_gender" class="col-md-3 col-form-label">Gender:<select class="form-control" name="owner_gender">
  <option >Select</option>
  <option value="M">Male</option>
  <option value="F">Female</option>
  </select></label>
 
</div>

<div class="form-group " >
  <label for="type_of_enterprise" class="col-md-3 col-form-label">Type of Enterprise:<input class="form-control" name="type_of_enterprise" type="text" id="type_of_enterprise" value="" required="" ></label>
  <label for="finance_source" class="col-md-3 col-form-label">Source of Finances:<input class="form-control" name="finance_source" type="text" id="finance_source" value="" required="" ></label>
  <label for="registration" class="col-md-3 col-form-label">Registration Type:<select class="form-control"  name="registration">
  <option >Select</option>
  <option value="Not Registered">Not Registered</option>
  <option value="LTD">LTD</option>
  <option value="SHG">SHG</option>
  </select></label>
  
</div>
<div class="form-group ">
 <p align="left"> 
  <input name="btnAddUser" type="button"   class="button" id="btnAddUser" value="Save (✔)" onClick="checkAddFarmerForm()" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="button"  value="Cancel" onClick="window.location.href='view.php?v=v=profile&id=<?php echo $f_id; ?>';" class="box">  
 </p>
 </div>
</form>
 </tbody>
</table>
</div>
<?php 
}//while
}else {
?>
<p> No user found.</p>
<?php 
} 
?>
</div>