
<?php
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];

} else {
  header('Location: ./index.php');
}
?>
<div class="panel-heading" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
<ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; background-color: #324159; width: 100% ">
<li  role="presentation" ><a href="javascript:editprofile(<?php echo $f_id; ?>);">Farm Owner</a></li>
<li  role="presentation"><a href="javascript:editbasicfarminfo(<?php echo $f_id; ?>);">Basic Info</a></li>
<li  role="presentation"><a href="javascript:editextfarminfo(<?php echo $f_id; ?>);">Other Farm info</a></li>
<li  role="presentation"><a href="javascript:edicontactperson(<?php echo $f_id; ?>);">Contact Persons</a></li>
<li  role="presentation"><a href="javascript:editstaff(<?php echo $f_id; ?>);">Staff</a></li>
<li  role="presentation"><a href="javascript:editpond(<?php echo $f_id; ?>);">Pond Information</a></li>
<!-- <li  role="presentation"><a href="javascript:editprodcycle(<?php echo $f_id; ?>);">Production Cycles</a></li> -->
<li  role="presentation"><a href="javascript:editassesmnt(<?php echo $f_id; ?>);">Extension</a></li>
</ul>
</div>


