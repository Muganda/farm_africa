<?php
//error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
//$date_modified=NOW();
if(isset($_POST['import_data'])){  
$modified_by=$_SESSION['user_id'];  
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain','text/xlsx');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){                                  
if(is_uploaded_file($_FILES['file']['tmp_name'])){ 
// Reading file  
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');   
$i = 0;
$Data_arr = array();
while(! feof($csv_file)) {

foreach(fgetcsv($csv_file) as $key=>$value){
$Data_arr[$i][] = $value;
}

$i++;
}
fclose($csv_file);

$skip = 0;
foreach($Data_arr as $data){
if($skip != 0){
$data = str_replace("'", "", $data);
if ($data[20]) {
$county_id = getCountyid($dbConn,$data[20]);
}
if ($data[21]) {
$subcounty_id = getsubCountyid($dbConn,$data[21]);
} 
if ($data[3]) {
$registration = getRegistrationtypeID($dbConn,$data[3]);
}
$module1='1';
$module2='2';
$module3='3';
$module4='4';
$attendance1='';
$attendance2='';
$attendance3='';
$attendance4='';
$year = date("Y");
$password = md5($data[10].'#'.$year);

$sqlfarmer = "SELECT farmer_id FROM farms_owners  WHERE farmer_id = '".$data[5]."'";
$resultfarmer = dbQuery($dbConn,$sqlfarmer);
// if farmer already exist then update details otherwise insert new record
if(dbNumRows($resultfarmer) ==1) {                     
$sql_updatefarmer = "UPDATE farms_owners
set 
firstname='".$data[9]."',
lastname='".$data[10]."', 
owner_telephone='".$data[11]."',
owner_gender='".$data[12]."',
owner_email='".$data[13]."',
pass='".$password."',
age='".$data[14]."', 
registration='".$registration."',
date_enrolled='".$data[0]."',
recruitment_source='".$data[1]."',
type_of_enterprise='".$data[2]."',
finance_source='".$data[26]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."'";
dbQuery($dbConn,$sql_updatefarmer);

} else{

$mysql_farmer = "INSERT INTO farms_owners 
(
farmer_id,
firstname,
lastname,
owner_gender,
owner_email,
pass,
age,
owner_telephone, 
registration,
date_enrolled,
recruitment_source,
type_of_enterprise,
finance_source,
modified_by,
date_modified 
)
VALUES
(
'".$data[5]."',
'".$data[9]."',
'".$data[10]."',
'".$data[12]."',
'".$data[13]."',
'".$password."',
'".$data[14]."',
'".$data[11]."',
'".$registration."', 
'".$data[0]."', 
'".$data[1]."', 
'".$data[2]."',
 '".$data[26]."', 
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_farmer);

}
$sqlfarm = "SELECT
farmer_id,farm_name,farm_county 
FROM farms  
WHERE farmer_id = '".$data[5]."' && 
farm_name = '".$data[8]."' && 
farm_county = '".$county_id."'";
$resultfarm = dbQuery($dbConn,$sqlfarm);
if(dbNumRows($resultfarm) ==1) {

$sql_updatefarm = "UPDATE 
farms set 
farm_subcounty='".$subcounty_id."',
farm_village='".$data[22]."',
farm_landmark='".$data[23]."',
keeps_records='".$data[24]."',
farm_longitude='".$data[6]."',
farm_latitude='".$data[7]."',
record_keeping='".$data[25]."',
has_business_plan='".$data[27]."',
pond_sample_temperature='".$data[28]."',
pond_sample_time='".$data[29]."',
ponds_number='".$data[30]."',
staff_total='".$data[75]."',
tot_no_of_tilapia='".$data[59]."',
tot_no_of_catfish='".$data[60]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
catfish_fingerling_stocking_weight='".$data[70]."',
tot_pond_area='".$data[32]."',
ponds_stocked='".$data[33]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."'";
dbQuery($dbConn,$sql_updatefarm);
}else{

$mysql_farm = "INSERT INTO farms 
(
farmer_id,
farm_name, 
farm_county, 
farm_subcounty,
farm_village,
farm_landmark,
keeps_records,
farm_longitude,
farm_latitude,
record_keeping,
tot_no_of_tilapia,
tot_no_of_catfish,
tilapia_fingerling_stocking_weight,
catfish_fingerling_stocking_weight,
has_business_plan,
pond_sample_temperature,
pond_sample_time,
ponds_number,
tot_pond_area,
ponds_stocked,
staff_total,
modified_by,
date_modified 
)
VALUES
(
'".$data[5]."','".$data[8]."',
'".$county_id."','".$subcounty_id."',
'".$data[22]."', '".$data[23]."', 
'".$data[24]."', '".$data[6]."', 
'".$data[7]."','".$data[25]."',
'".$data[59]."','".$data[60]."', 
'".$data[69]."','".$data[70]."',
'".$data[27]."','".$data[28]."', 
'".$data[29]."', '".$data[30]."',
'".$data[32]."', '".$data[33]."',
'".$data[75]."',
'".$modified_by."',NOW()
)";
dbQuery($dbConn,$mysql_farm);
}
if ($data[34]!="" and $data[34]!="0" and $data[34]!="None"){
$number1="Pond 1"; 
$timestamp1 = strtotime($data[37]);
$stckingyear1=date("Y",$timestamp1);
$stckingmonth1=date("F",$timestamp1);

// $harvestdate1=strtotime(($data[37])." +6 month");
// $expected_harvest_date1=date("Y-m-d",$harvestdate1);
$sqlpond1 = "SELECT farmer_id,pond_number
FROM farms_ponds  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$number1."' ";
$resultpond1 = dbQuery($dbConn,$sqlpond1);
if(dbNumRows($resultpond1) ==1) {
$sql_updatepond1 = "UPDATE farms_ponds 
set 
pond_area='".$data[34]."',
catfish_fingerling_stocked='".$data[36]."',
tilapia_fingerling_stocked='".$data[35]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[61]."',
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$number1."' and stocking_date='".$data[37]."'";

dbQuery($dbConn,$sql_updatepond1);
}else{
$mysql_pond1 = "INSERT INTO farms_ponds
(
farmer_id,
pond_number, 
pond_area,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
modified_by,
date_modified
)
VALUES
(
'".$data[5]."',
'".$number1."',
'".$data[34]."',
'".$data[36]."', 
'".$data[35]."',
'".$data[69]."', 
'".$data[37]."', 
'".$stckingyear1."',
'".$stckingmonth1."',
'".$data[61]."', 
'".$data[70]."',
'".$modified_by."',
NOW()
)";
dbQuery($dbConn,$mysql_pond1);

}

}
else
$skip++;
if ($data[38]!="" and $data[38]!="0" and $data[38]!="None"){
$number2="Pond 2";
$timestamp2 = strtotime($data[41]);
$stckingyear2=date("Y",$timestamp2);
$stckingmonth2=date("F",$timestamp2);
// $harvestdate2=strtotime(($data[41])." +6 month");
// $expected_harvest_date2=date("Y-m-d",$harvestdate2);
$sqlpond2 = "SELECT farmer_id,pond_number,stocking_date
FROM farms_ponds  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$number2."' and stocking_date='".$data[41]."' ";
$resultpond2 = dbQuery($dbConn,$sqlpond2);
if(dbNumRows($resultpond2) ==1) {
$sql_updatepond2 = "UPDATE farms_ponds 
set 
pond_area='".$data[38]."',
catfish_fingerling_stocked='".$data[40]."',
tilapia_fingerling_stocked='".$data[39]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[62]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$number2."' and stocking_date='".$data[41]."'";

dbQuery($dbConn,$sql_updatepond2);
}else{
$mysql_pond2 = "INSERT INTO farms_ponds
(
farmer_id, pond_number, 
pond_area,catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,stocking_year,stocking_month,expected_harvest_date,
catfish_fingerling_stocking_weight,
modified_by,date_modified
)
VALUES
(
'".$data[5]."','".$number2."',
'".$data[38]."','".$data[40]."', 
'".$data[39]."','".$data[69]."', 
'".$data[41]."', 
'".$stckingyear2."','".$stckingmonth2."', '".$data[62]."',
'".$data[70]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_pond2);

}

}
else
$skip++;
if ($data[42]!="" and $data[42]!="0" and $data[42]!="None"){
$number3="Pond 3";
$timestamp3 = strtotime($data[45]);
$stckingyear3=date("Y",$timestamp3);
$stckingmonth3=date("F",$timestamp3);
// $harvestdate3=strtotime(($data[45])." +6 month");
// $expected_harvest_date3=date("Y-m-d",$harvestdate3);
$sqlpond3 = "SELECT farmer_id,pond_number,stocking_date 
FROM farms_ponds  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$number3."' and stocking_date='".$data[45]."'";
$resultpond3 = dbQuery($dbConn,$sqlpond3);
if(dbNumRows($resultpond3) ==1) {
$sql_updatepond3 = "UPDATE farms_ponds 
set 
pond_area='".$data[42]."',
catfish_fingerling_stocked='".$data[44]."',
tilapia_fingerling_stocked='".$data[43]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[63]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$number3."'  and stocking_date='".$data[45]."'";

dbQuery($dbConn,$sql_updatepond3);
}else{
$mysql_pond3 = "INSERT INTO farms_ponds
(
farmer_id, pond_number, 
pond_area,catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,stocking_year,stocking_month,expected_harvest_date,
catfish_fingerling_stocking_weight,
modified_by,date_modified
)
VALUES
(
'".$data[5]."','".$number3."',
'".$data[42]."','".$data[44]."', 
'".$data[43]."','".$data[69]."', 
'".$data[45]."', 
'".$stckingyear3."','".$stckingmonth3."','".$data[63]."', '".$data[70]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_pond3);

}

}
else
$skip++;
if ($data[46]!="" and $data[46]!="0" and $data[46]!="None"){
$number4="Pond 4";
$timestamp4 = strtotime($data[49]);
$stckingyear4=date("Y",$timestamp4);
$stckingmonth4=date("F",$timestamp4);
// $harvestdate4=strtotime(($data[49])." +6 month");
// $expected_harvest_date4=date("Y-m-d",$harvestdate4);
$sqlpond4 = "SELECT farmer_id,pond_number,stocking_date
FROM farms_ponds  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$number4."' 
and stocking_date='".$data[49]."'";
$resultpond4 = dbQuery($dbConn,$sqlpond4);
if(dbNumRows($resultpond4) ==1) {
$sql_updatepond4 = "UPDATE farms_ponds 
set 
pond_area='".$data[46]."',
catfish_fingerling_stocked='".$data[48]."',
tilapia_fingerling_stocked='".$data[47]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[64]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$number4."' and stocking_date='".$data[49]."'";

dbQuery($dbConn,$sql_updatepond4);
}else{
$mysql_pond4 = "INSERT INTO farms_ponds
(
farmer_id, pond_number, 
pond_area,catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,stocking_year,stocking_month,expected_harvest_date,
catfish_fingerling_stocking_weight,
modified_by,date_modified
)
VALUES
(
'".$data[5]."','".$number4."',
'".$data[46]."','".$data[48]."', 
'".$data[47]."','".$data[69]."', 
'".$data[49]."', 
'".$stckingyear4."','".$stckingmonth4."', '".$data[64]."','".$data[70]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_pond4);

}

}
else
$skip++;
if ($data[50]!="" and $data[50]!="0" and $data[50]!="None"){
$number5="Pond 5";
$timestamp5 = strtotime($data[53]);
$stckingyear5=date("Y",$timestamp5);
$stckingmonth5=date("F",$timestamp5);
// $harvestdate5=strtotime(($data[53])." +6 month");
// $expected_harvest_date5=date("Y-m-d",$harvestdate5);
$sqlpond5 = "SELECT farmer_id,pond_number,stocking_date
FROM farms_ponds  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$number5."' and stocking_date='".$data[53]."'";
$resultpond5 = dbQuery($dbConn,$sqlpond5);
if(dbNumRows($resultpond5) ==1) {
$sql_updatepond5 = "UPDATE farms_ponds 
set 
pond_area='".$data[50]."',
catfish_fingerling_stocked='".$data[52]."',
tilapia_fingerling_stocked='".$data[51]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[65]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$number5."' and stocking_date='".$data[53]."'";

dbQuery($dbConn,$sql_updatepond5);
}else{
$mysql_pond5 = "INSERT INTO farms_ponds
(
farmer_id, pond_number, 
pond_area,catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,stocking_year,stocking_month,expected_harvest_date,
catfish_fingerling_stocking_weight,
modified_by,date_modified
)
VALUES
(
'".$data[5]."','".$number5."',
'".$data[50]."','".$data[52]."', 
'".$data[51]."','".$data[69]."', 
'".$data[53]."', 
'".$stckingyear5."','".$stckingmonth5."','".$data[65]."', '".$data[70]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_pond5);

}

}
else
$skip++;
if ($data[54]!="" and $data[54]!="0" and $data[54]!="None"){
$number6="Pond 6";
$timestamp6 = strtotime($data[57]);
$stckingyear6=date("Y",$timestamp6);
$stckingmonth6=date("F",$timestamp6);
// $harvestdate6=strtotime(($data[57])." +6 month");
// $expected_harvest_date6=date("Y-m-d",$harvestdate6);
$sqlpond6 = "SELECT farmer_id,pond_number,stocking_date
FROM farms_ponds  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$number6."' and stocking_date='".$data[57]."'";
$resultpond6 = dbQuery($dbConn,$sqlpond6);
if(dbNumRows($resultpond6) ==1) {
$sql_updatepond6 = "UPDATE farms_ponds 
set 
pond_area='".$data[54]."',
catfish_fingerling_stocked='".$data[56]."',
tilapia_fingerling_stocked='".$data[55]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[66]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$number6."' and stocking_date='".$data[57]."'";

dbQuery($dbConn,$sql_updatepond6);
}else{
$mysql_pond6 = "INSERT INTO farms_ponds
(
farmer_id, pond_number, 
pond_area,catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,stocking_year,stocking_month,expected_harvest_date,
catfish_fingerling_stocking_weight,
modified_by,date_modified
)
VALUES
(
'".$data[5]."','".$number6."',
'".$data[54]."','".$data[56]."', 
'".$data[55]."','".$data[69]."', 
'".$data[57]."', 
'".$stckingyear6."','".$stckingmonth6."','".$data[66]."', '".$data[70]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_pond6);

}

}
else
$skip++;
if ($data[34]!="" and $data[34]!="0" and $data[34]!="None"){
$sql="SELECT id from farms_ponds where pond_number='Pond 1' and farmer_id='".$data[5]."'";
$result1 = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  }
$number1="Pond 1"; 
$timestamp1 = strtotime($data[37]);
$stckingyear1=date("Y",$timestamp1);
$stckingmonth1=date("F",$timestamp1);
$sqlstockpond1 = "SELECT farmer_id,pond_number,stocking_date
FROM pond_stocking  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and stocking_date='".$data[37]."' ";
$resultstockpond1 = dbQuery($dbConn,$sqlstockpond1);
if(dbNumRows($resultstockpond1) ==1) {
$sql_updatestockpond1 = "UPDATE pond_stocking 
set 
catfish_fingerling_stocked='".$data[36]."',
tilapia_fingerling_stocked='".$data[35]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[61]."',
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and stocking_date='".$data[37]."'";

dbQuery($dbConn,$sql_updatestockpond1);
}else{
$mysql_stockpond1 = "INSERT INTO pond_stocking
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
date_created,
modified_by,
date_modified
)
VALUES
(
'".$data[5]."',
'".$id."',
'".$data[36]."', 
'".$data[35]."',
'".$data[69]."', 
'".$data[37]."', 
'".$stckingyear1."',
'".$stckingmonth1."',
'".$data[61]."', 
'".$data[70]."',
NOW(),
'".$modified_by."',
NOW()
)";
dbQuery($dbConn,$mysql_stockpond1);

}

}
else
$skip++;
if ($data[38]!="" and $data[38]!="0" and $data[38]!="None"){
$sql2="SELECT id from farms_ponds where pond_number='Pond 2' and farmer_id='".$data[5]."'";
$result2 = dbQuery($dbConn,$sql2);
while($row = dbFetchAssoc($result2)) {
  extract($row);
  }
$number2="Pond 2";
$timestamp2 = strtotime($data[41]);
$stckingyear2=date("Y",$timestamp2);
$stckingmonth2=date("F",$timestamp2);
$sqlstockpond2 = "SELECT farmer_id,pond_number,stocking_date 
FROM pond_stocking  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and stocking_date='".$data[41]."' ";
$resultstockpond2 = dbQuery($dbConn,$sqlstockpond2);
if(dbNumRows($resultstockpond2) ==1) {
$sql_updatestockpond2 = "UPDATE pond_stocking 
set 
catfish_fingerling_stocked='".$data[40]."',
tilapia_fingerling_stocked='".$data[39]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[62]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and stocking_date='".$data[41]."'";

dbQuery($dbConn,$sql_updatestockpond2);
}else{
$mysql_stockpond2 = "INSERT INTO pond_stocking
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
date_created,
modified_by,
date_modified
)
VALUES
(
'".$data[5]."',
'".$id."',
'".$data[40]."', 
'".$data[39]."',
'".$data[69]."', 
'".$data[41]."', 
'".$stckingyear2."',
'".$stckingmonth2."', 
'".$data[62]."',
'".$data[70]."',
NOW(),
'".$modified_by."',
NOW()
)";
dbQuery($dbConn,$mysql_stockpond2);

}

}
else
$skip++;
if ($data[42]!="" and $data[42]!="0" and $data[42]!="None"){
$sql3="SELECT id from farms_ponds where pond_number='Pond 3' and farmer_id='".$data[5]."'";
$result3 = dbQuery($dbConn,$sql3);
while($row = dbFetchAssoc($result3)) {
  extract($row);
  }
$number3="Pond 3";
$timestamp3 = strtotime($data[45]);
$stckingyear3=date("Y",$timestamp3);
$stckingmonth3=date("F",$timestamp3);
$sqlstockpond3 = "SELECT farmer_id,pond_number,stocking_date 
FROM pond_stocking  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and stocking_date='".$data[45]."'";
$resultstockpond3 = dbQuery($dbConn,$sqlstockpond3);
if(dbNumRows($resultstockpond3) ==1) {
$sql_updatestockpond3 = "UPDATE pond_stocking 
set 
catfish_fingerling_stocked='".$data[44]."',
tilapia_fingerling_stocked='".$data[43]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[63]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and stocking_date='".$data[45]."'";

dbQuery($dbConn,$sql_updatestockpond3);
}else{
$mysql_stockpond3 = "INSERT INTO pond_stocking
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
date_created,
modified_by,
date_modified
)
VALUES
(
 '".$data[5]."',
 '".$id."',
'".$data[44]."', 
'".$data[43]."',
'".$data[69]."', 
'".$data[45]."', 
'".$stckingyear3."',
'".$stckingmonth3."',
'".$data[63]."', 
'".$data[70]."',
NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_stockpond3);

}

}
else
$skip++;
if ($data[46]!="" and $data[46]!="0" and $data[46]!="None"){
$sql4="SELECT id from farms_ponds where pond_number='Pond 4' and farmer_id='".$data[5]."'";
$result4 = dbQuery($dbConn,$sql4);
while($row = dbFetchAssoc($result4)) {
  extract($row);
  }
$number4="Pond 4";
$timestamp4 = strtotime($data[49]);
$stckingyear4=date("Y",$timestamp4);
$stckingmonth4=date("F",$timestamp4);
$sqlstockpond4 = "SELECT farmer_id,pond_number,stocking_date 
FROM pond_stocking  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and stocking_date='".$data[49]."' ";
$resultstockpond4 = dbQuery($dbConn,$sqlstockpond4);
if(dbNumRows($resultstockpond4) ==1) {
$sql_updatestockpond4 = "UPDATE pond_stocking 
set 
catfish_fingerling_stocked='".$data[48]."',
tilapia_fingerling_stocked='".$data[47]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[64]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and stocking_date='".$data[49]."'";

dbQuery($dbConn,$sql_updatestockpond4);
}else{
$mysql_stockpond4 = "INSERT INTO pond_stocking
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
date_created,
modified_by,
date_modified
)
VALUES
(
 '".$data[5]."',
 '".$id."',
'".$data[48]."', 
'".$data[47]."',
'".$data[69]."', 
'".$data[49]."', 
'".$stckingyear4."',
'".$stckingmonth4."', 
'".$data[64]."',
'".$data[70]."',
NOW(),
'".$modified_by."',
NOW()
)";
dbQuery($dbConn,$mysql_stockpond4);

}

}
else
$skip++;
if ($data[50]!="" and $data[50]!="0" and $data[50]!="None"){
$sql5="SELECT id from farms_ponds where pond_number='Pond 5' and farmer_id='".$data[5]."'";
$result5 = dbQuery($dbConn,$sql5);
while($row = dbFetchAssoc($result5)) {
  extract($row);
  }
$number5="Pond 5";
$timestamp5 = strtotime($data[53]);
$stckingyear5=date("Y",$timestamp5);
$stckingmonth5=date("F",$timestamp5);
$sqlstockpond5 = "SELECT farmer_id,pond_number,stocking_date 
FROM pond_stocking  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and stocking_date='".$data[53]."'";
$resultstockpond5 = dbQuery($dbConn,$sqlstockpond5);
if(dbNumRows($resultstockpond5) ==1) {
$sql_updatestockpond5 = "UPDATE pond_stocking 
set 
catfish_fingerling_stocked='".$data[52]."',
tilapia_fingerling_stocked='".$data[51]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[65]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and stocking_date='".$data[53]."'";

dbQuery($dbConn,$sql_updatestockpond5);
}else{
$mysql_stockpond5 = "INSERT INTO pond_stocking
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
date_created,
modified_by,
date_modified
)
VALUES
(
 '".$data[5]."',
 '".$id."',
'".$data[52]."', 
'".$data[51]."',
'".$data[69]."', 
'".$data[53]."', 
'".$stckingyear5."',
'".$stckingmonth5."',
'".$data[65]."', 
'".$data[70]."',
NOW(),
'".$modified_by."',
NOW()
)";
dbQuery($dbConn,$mysql_stockpond5);

}

}
else
$skip++;
if ($data[54]!="" and $data[54]!="0" and $data[54]!="None"){
$sql6="SELECT id from farms_ponds where pond_number='Pond 6' and farmer_id='".$data[5]."'";
$result6 = dbQuery($dbConn,$sql6);
while($row = dbFetchAssoc($result6)) {
  extract($row);
  }
$number6="Pond 6";
$timestamp6 = strtotime($data[57]);
$stckingyear6=date("Y",$timestamp6);
$stckingmonth6=date("F",$timestamp6);
$sqlstockpond6 = "SELECT farmer_id,pond_number,stocking_date 
FROM pond_stocking  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and stocking_date='".$data[57]."'";
$resultstockpond6 = dbQuery($dbConn,$sqlstockpond6);
if(dbNumRows($resultstockpond6) ==1) {
$sql_updatestockpond6 = "UPDATE pond_stocking 
set 
catfish_fingerling_stocked='".$data[56]."',
tilapia_fingerling_stocked='".$data[55]."',
tilapia_fingerling_stocking_weight='".$data[69]."',
expected_harvest_date='".$data[66]."', 
catfish_fingerling_stocking_weight='".$data[70]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and stocking_date='".$data[57]."'";

dbQuery($dbConn,$sql_updatestockpond6);
}else{
$mysql_stockpond6 = "INSERT INTO pond_stocking
(
farmer_id,
pond_number,
catfish_fingerling_stocked,
tilapia_fingerling_stocked,
tilapia_fingerling_stocking_weight,
stocking_date,
stocking_year,
stocking_month,
expected_harvest_date,
catfish_fingerling_stocking_weight,
date_created,
modified_by,
date_modified
)
VALUES
(
 '".$data[5]."',
 '".$id."',
'".$data[56]."', 
'".$data[55]."',
'".$data[69]."', 
'".$data[57]."', 
'".$stckingyear6."',
'".$stckingmonth6."',
'".$data[66]."', 
'".$data[70]."',
NOW(),
'".$modified_by."',
NOW()
)";
dbQuery($dbConn,$mysql_stockpond6);

}

}
else
$skip++;
$equiments=array($data[71],$data[72],$data[73],$data[74]);
$farm_equipments=implode( ", ", $equiments );
$challenges_array=array($data[103],$data[104],$data[105]);
$challenges=implode(",", $challenges_array);
$target=array($data[107],$data[108]);
$needs_to_reach_target=implode(",", $target);
$sqlext = "SELECT farmer_id  FROM farms_extended_data  
WHERE farmer_id = '".$data[5]."'";
$resultext = dbQuery($dbConn,$sqlext);
if(dbNumRows($resultext) ==1) {
$sql_updatext = "UPDATE farms_extended_data 
set 
farm_equipments='".$farm_equipments."', 
staff_fulltime='".$data[76]."',
staff_parttime='".$data[77]."',
main_feeds='".$data[92]."',
feeds_source='".$data[100]."',
challenges='".$challenges."',
five_year_target='".$data[106]."', 
needs_to_reach_target='".$needs_to_reach_target."', 
receive_updates='".$data[109]."',
can_host_trainings='".$data[110]."',
modified_by='".$modified_by."',
date_modified = NOW() 
WHERE farmer_id = '".$data[5]."'";
dbQuery($dbConn,$sql_updatext);
}else{
$mysql_ext = "INSERT INTO farms_extended_data 
(
farmer_id,
farm_equipments,
staff_fulltime,
staff_parttime,
main_feeds,
feeds_source,
challenges,
five_year_target,
needs_to_reach_target,
receive_updates,
can_host_trainings,
modified_by,
date_modified
)
VALUES
(
'".$data[5]."',
'".$farm_equipments."',
'".$data[76]."',
'".$data[77]."',
'".$data[92]."',
'".$data[100]."',
'".$challenges."',
'".$data[106]."',
'".$needs_to_reach_target."',
'".$data[109]."', 
'".$data[110]."',
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_ext);
} 
$sqlcycle = "SELECT farmer_id,year FROM farms_production_cycles 
WHERE farmer_id = '".$data[5]."'";
$resultcycle = dbQuery($dbConn,$sqlcycle);
if(dbNumRows($resultcycle) ==1) {
$sql_updatecycle = "UPDATE farms_production_cycles 
set
species='".$data[94]."',
length='".$data[95]."',
fingerlings_bought='".$data[97]."',
fingerlings_source='".$data[98]."',
feeds_bought='".$data[99]."',
main_feeds='".$data[92]."',
feeds_source='".$data[100]."', 
fish_harvested='".$data[101]."',
main_buyer='".$data[102]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' && year='".$data[93]."'";
dbQuery($dbConn,$sql_updatecycle);
}else{
$mysql_cycle = "INSERT INTO farms_production_cycles 
(
farmer_id,year,
species,length,
fingerlings_bought,fingerlings_source,
feeds_bought,feeds_source,
fish_harvested,main_buyer,
main_feeds,
modified_by,date_modified 
)
VALUES
(
'".$data[5]."',  '".$data[93]."',
'".$data[94]."', '".$data[95]."',
'".$data[97]."', '".$data[98]."', 
'".$data[99]."', '".$data[100]."', 
'".$data[101]."','".$data[102]."',
'".$data[92]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_cycle);
} 


$sqlassesments = "SELECT farmer_id FROM assessments 
WHERE farmer_id = '".$data[5]."'";
$resultassesment = dbQuery($dbConn,$sqlassesments);
if(dbNumRows($resultassesment) ==1) {
$sql_updateassesment = "UPDATE assessments 
set
assessor_name='".$data[1]."', 
date_assesed='".$data[0]."',
remarks='".$data[111]."',
recommendation='".$data[112]."',
date_created=NOW(),
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' && date_assesed='".$data[0]."' ";
dbQuery($dbConn,$sql_updateassesment);
}else{
$mysql_assesment = "INSERT INTO assessments 
(
farmer_id,
assessor_name,
date_assesed,
remarks,
recommendation,
date_created,
modified_by,
date_modified 
)
VALUES
(
'".$data[5]."', 
'".$data[1]."', 
'".$data[0]."',
'".$data[111]."',
'".$data[112]."',
NOW(),
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_assesment);
}

$sqlmodule1 = "SELECT farmer_id FROM farmer_training 
WHERE farmer_id = '".$data[5]."' && training_module='".$module1."'";
if ($data[113]!="" and $data[113]!="0")  {
$attendance1=$data[113];
}
else
{
 $attendance1='No';
}
$resultmodule1 = dbQuery($dbConn,$sqlmodule1);
if(dbNumRows($resultmodule1) ==1) {
$sql_updatemodule1 = "UPDATE farmer_training 
set
comments='".$data[117]."',
attendance='".$attendance1."',
date_created=NOW(),
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' && training_module='".$module1."' ";
dbQuery($dbConn,$sql_updatemodule1);
}else{
$mysql_module1 = "INSERT INTO farmer_training 
(
farmer_id,
training_module,
attendance,
comments,
date_created,
modified_by,
modified_date 
)
VALUES
(
'".$data[5]."', 
'".$module1."',
'".$attendance1."',
'".$data[117]."',
NOW(),
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_module1);
}
$sqlmodule2 = "SELECT farmer_id FROM farmer_training 
WHERE farmer_id = '".$data[5]."' && training_module='".$module2."'";
if ($data[114]!="" and $data[114]!= "0") {
$attendance2=$data[114];
}
else
{
 $attendance2='No';
}
$resultmodule2 = dbQuery($dbConn,$sqlmodule2);
if(dbNumRows($resultmodule2) ==1) {
$sql_updatemodule2 = "UPDATE farmer_training 
set
comments='".$data[117]."',
attendance='".$attendance2."',
date_created=NOW(),
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' && training_module='".$module2."' ";
dbQuery($dbConn,$sql_updatemodule2);
}else{
$mysql_module2 = "INSERT INTO farmer_training 
(
farmer_id,
training_module,
attendance,
comments,
date_created,
modified_by,
modified_date 
)
VALUES
(
'".$data[5]."', 
'".$module2."',
'".$attendance2."',
'".$data[117]."',
NOW(),
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_module2);
}
$sqlmodule3 = "SELECT farmer_id FROM farmer_training 
WHERE farmer_id = '".$data[5]."' && training_module='".$module3."'";
$resultmodule3 = dbQuery($dbConn,$sqlmodule3);
if ($data[115]!="" and $data[115]!="0") {
$attendance3=$data[113];
}
else
{
 $attendance3='No';
}
if(dbNumRows($resultmodule3) ==1) {
$sql_updatemodule3 = "UPDATE farmer_training 
set
comments='".$data[117]."',
attendance='".$attendance3."',
date_created=NOW(),
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' && training_module='".$module3."' ";
dbQuery($dbConn,$sql_updatemodule3);
}else{
$mysql_module3 = "INSERT INTO farmer_training 
(
farmer_id,
training_module,
attendance,
comments,
date_created,
modified_by,
modified_date 
)
VALUES
(
'".$data[5]."', 
'".$module3."',
'".$attendance3."',
'".$data[117]."',
NOW(),
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_module3);
}
$sqlmodule4 = "SELECT farmer_id FROM farmer_training 
WHERE farmer_id = '".$data[5]."' && training_module='".$module4."'";
if ($data[116]!="" and $data[116]!= "0") {
$attendance4=$data[113];
}
else
{
 $attendance4='No';
}
$resultmodule4 = dbQuery($dbConn,$sqlmodule4);
if(dbNumRows($resultmodule4) ==1) {
$sql_updatemodule4 = "UPDATE farmer_training 
set
comments='".$data[117]."',
attendance='".$attendance4."',
date_created=NOW(),
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' && training_module='".$module4."' ";
dbQuery($dbConn,$sql_updatemodule4);
}else{
$mysql_module4 = "INSERT INTO farmer_training 
(
farmer_id,
training_module,
attendance,
comments,
date_created,
modified_by,
modified_date 
)
VALUES
(
'".$data[5]."', 
'".$module4."',
'".$attendance4."',
'".$data[117]."',
NOW(),
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_module4);
}

if ($data[34]!="" and $data[34]!="0" and $data[34]!="None"){
$sql="SELECT id from farms_ponds where pond_number='Pond 1' and farmer_id='".$data[5]."'";
$result1 = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  }
$number1="Pond 1"; 
$timestamp1 = strtotime($data[37]);
$stckingyear1=date("Y",$timestamp1);
$stckingmonth1=date("F",$timestamp1);
$totaltilapiacost1=$data[35]*$data[67];
$totalcatfishcost1=$data[36]*$data[67];
$sqlcostspond1 = "SELECT farmer_id,pond_number,cost_date
FROM farm_costs  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and cost_date='".$data[37]."' ";
$resultcostpond1 = dbQuery($dbConn,$sqlcostspond1);
if(dbNumRows($resultcostpond1) ==1) {
$sql_updatecostpond1 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[67]."',
catfish_price_per_fingerling='".$data[68]."',
total_tilapia_fingerling_cost='".$totaltilapiacost1."',
total_catfish_fingerling_cost='".$totalcatfishcost1."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and cost_date='".$data[37]."'";

dbQuery($dbConn,$sql_updatecostpond1);
}else{
$mysql_costpond1 = "INSERT INTO farm_costs
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
total_tilapia_fingerling_cost,
total_catfish_fingerling_cost,
cost_date,
year,
month,
date_created,
modified_by,
modified_date
)
VALUES
(
'".$data[5]."',
'".$id."',
'".$data[67]."',
'".$data[68]."',
'".$totaltilapiacost1."', 
'".$totalcatfishcost1."',
'".$data[37]."',
'".$stckingyear1."',
'".$stckingmonth1."',
 NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_costpond1);

}

}
else
$skip++;
if ($data[38]!="" and $data[38]!="0" and $data[38]!="None"){
$sql2="SELECT id from farms_ponds where pond_number='Pond 2' and farmer_id='".$data[5]."'";
$result2 = dbQuery($dbConn,$sql2);
while($row = dbFetchAssoc($result2)) {
  extract($row);
  }
$number2="Pond 2";
$timestamp2 = strtotime($data[41]);
$stckingyear2=date("Y",$timestamp2);
$stckingmonth2=date("F",$timestamp2);
$totaltilapiacost2=$data[39]*$data[67];
$totalcatfishcost2=$data[40]*$data[67];
$sqlcostspond2 = "SELECT farmer_id,pond_number,cost_date 
FROM farm_costs  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and cost_date='".$data[41]."' ";
$resultcostpond2 = dbQuery($dbConn,$sqlcostspond2);
if(dbNumRows($resultcostpond2) ==1) {
$sql_updatecostpond2 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[40]."',
catfish_price_per_fingerling='".$data[39]."',
total_tilapia_fingerling_cost='".$totaltilapiacost2."',
total_catfish_fingerling_cost='".$totalcatfishcost2."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and cost_date='".$data[41]."'";

dbQuery($dbConn,$sql_updatecostpond2);
}else{
$mysql_costpond2 = "INSERT INTO farm_costs
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
total_tilapia_fingerling_cost,
total_catfish_fingerling_cost,
cost_date,
year,
month,
date_created,
modified_by,
modified_date
)
VALUES
(
'".$data[5]."',
'".$id."',
'".$data[67]."',
'".$data[68]."', 
'".$totaltilapiacost2."', 
'".$totalcatfishcost2."',
'".$data[41]."',
'".$stckingyear2."',
'".$stckingmonth2."',
 NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_costpond2);

}

}
else
$skip++;
if ($data[42]!="" and $data[42]!="0" and $data[42]!="None"){
$sql3="SELECT id from farms_ponds where pond_number='Pond 3' and farmer_id='".$data[5]."'";
$result3 = dbQuery($dbConn,$sql3);
while($row = dbFetchAssoc($result3)) {
  extract($row);
  }
$number3="Pond 3";
$timestamp3 = strtotime($data[45]);
$stckingyear3=date("Y",$timestamp3);
$stckingmonth3=date("F",$timestamp3);
$totaltilapiacost3=$data[43]*$data[67];
$totalcatfishcost3=$data[44]*$data[67];
$sqlcostspond3 = "SELECT farmer_id,pond_number,cost_date 
FROM farm_costs  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and cost_date='".$data[45]."'";
$resultcostpond3 = dbQuery($dbConn,$sqlcostspond3);
if(dbNumRows($resultcostpond3) ==1) {
$sql_updatecostpond3 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[67]."',
catfish_price_per_fingerling='".$data[68]."',
total_tilapia_fingerling_cost='".$totaltilapiacost3."',
total_catfish_fingerling_cost='".$totalcatfishcost3."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and cost_date='".$data[45]."'";

dbQuery($dbConn,$sql_updatecostpond3);
}else{
$mysql_costpond3 = "INSERT INTO farm_costs
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
total_tilapia_fingerling_cost,
total_catfish_fingerling_cost,
cost_date,
year,
month,
date_created,
modified_by,
modified_date
)
VALUES
(
 '".$data[5]."',
'".$id."',
'".$data[67]."',
'".$data[68]."', 
'".$totaltilapiacost3."', 
'".$totalcatfishcost3."',
'".$data[45]."',
'".$stckingyear3."',
'".$stckingmonth3."',
 NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_costpond3);

}

}
else
$skip++;
if ($data[46]!="" and $data[46]!="0" and $data[46]!="None"){
$sql4="SELECT id from farms_ponds where pond_number='Pond 4' and farmer_id='".$data[5]."'";
$result4 = dbQuery($dbConn,$sql4);
while($row = dbFetchAssoc($result4)) {
  extract($row);
  }
$number4="Pond 4";
$timestamp4 = strtotime($data[49]);
$stckingyear4=date("Y",$timestamp4);
$stckingmonth4=date("F",$timestamp4);
$totaltilapiacost4=$data[47]*$data[67];
$totalcatfishcost4=$data[48]*$data[67];
$sqlcostspond4 = "SELECT farmer_id,pond_number,cost_date 
FROM farm_costs  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and cost_date='".$data[49]."' ";
$resultcostpond4 = dbQuery($dbConn,$sqlcostspond4);
if(dbNumRows($resultcostpond4) ==1) {
$sql_updatecostpond4 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[67]."',
catfish_price_per_fingerling='".$data[68]."',
total_tilapia_fingerling_cost='".$totaltilapiacost4."',
total_catfish_fingerling_cost='".$totalcatfishcost4."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and cost_date='".$data[49]."'";

dbQuery($dbConn,$sql_updatecostpond4);
}else{
$mysql_costpond4 = "INSERT INTO farm_costs
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
total_tilapia_fingerling_cost,
total_catfish_fingerling_cost,
cost_date,
year,
month,
date_created,
modified_by,
modified_date
)
VALUES
(
 '".$data[5]."',
'".$id."',
'".$data[67]."',
'".$data[68]."', 
'".$totaltilapiacost4."', 
'".$totalcatfishcost4."',
'".$data[49]."',
'".$stckingyear4."',
'".$stckingmonth4."',
 NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_costpond4);

}

}
else
$skip++;
if ($data[50]!="" and $data[50]!="0" and $data[50]!="None"){
$sql5="SELECT id from farms_ponds where pond_number='Pond 5' and farmer_id='".$data[5]."'";
$result5 = dbQuery($dbConn,$sql5);
while($row = dbFetchAssoc($result5)) {
  extract($row);
  }
$number5="Pond 5";
$timestamp5 = strtotime($data[53]);
$stckingyear5=date("Y",$timestamp5);
$stckingmonth5=date("F",$timestamp5);
$totaltilapiacost5=$data[51]*$data[67];
$totalcatfishcost5=$data[52]*$data[67];
$sqlcostspond5 = "SELECT farmer_id,pond_number,cost_date 
FROM farm_costs  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and cost_date='".$data[53]."'";
$resultcostpond5 = dbQuery($dbConn,$sqlcostspond5);
if(dbNumRows($resultcostpond5) ==1) {
$sql_updatecostpond5 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[67]."',
catfish_price_per_fingerling='".$data[68]."',
total_tilapia_fingerling_cost='".$totaltilapiacost5."',
total_catfish_fingerling_cost='".$totalcatfishcost5."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and cost_date='".$data[53]."'";

dbQuery($dbConn,$sql_updatecostpond5);
}else{
$mysql_costpond5 = "INSERT INTO farm_costs
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
total_tilapia_fingerling_cost,
total_catfish_fingerling_cost,
cost_date,
year,
month,
date_created,
modified_by,
modified_date
)
VALUES
(
 '".$data[5]."',
'".$id."',
'".$data[67]."',
'".$data[68]."', 
'".$totaltilapiacost5."', 
'".$totalcatfishcost5."',
'".$data[53]."',
'".$stckingyear5."',
'".$stckingmonth5."',
 NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_costpond5);

}

}
else
$skip++;
if ($data[54]!="" and $data[54]!="0" and $data[54]!="None"){
$sql6="SELECT id from farms_ponds where pond_number='Pond 6' and farmer_id='".$data[5]."'";
$result6 = dbQuery($dbConn,$sql6);
while($row = dbFetchAssoc($result6)) {
  extract($row);
  }
$number6="Pond 6";
$timestamp6 = strtotime($data[57]);
$stckingyear6=date("Y",$timestamp6);
$stckingmonth6=date("F",$timestamp6);
$totaltilapiacost6=$data[55]*$data[67];
$totalcatfishcost6=$data[56]*$data[67];
$sqlcostspond6 = "SELECT farmer_id,pond_number,cost_date 
FROM farm_costs  
WHERE farmer_id = '".$data[5]."' 
and pond_number = '".$id."' and cost_date='".$data[57]."'";
$resultcostpond6 = dbQuery($dbConn,$sqlcostspond6);
if(dbNumRows($resultcostpond6) ==1) {
$sql_updatecostpond6 = "UPDATE farm_costs 
set 
tilapia_price_per_fingerling='".$data[67]."',
catfish_price_per_fingerling='".$data[68]."',
total_tilapia_fingerling_cost='".$totaltilapiacost6."',
total_catfish_fingerling_cost='".$totalcatfishcost6."',
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' and pond_number='".$id."' and cost_date='".$data[57]."'";

dbQuery($dbConn,$sql_updatecostpond6);
}else{
$mysql_costpond6 = "INSERT INTO farm_costs
(
farmer_id,
pond_number,
tilapia_price_per_fingerling,
catfish_price_per_fingerling,
total_tilapia_fingerling_cost,
total_catfish_fingerling_cost,
cost_date,
year,
month,
date_created,
modified_by,
modified_date
)
VALUES
(
 '".$data[5]."',
'".$id."',
'".$data[67]."',
'".$data[68]."', 
'".$totaltilapiacost6."', 
'".$totalcatfishcost6."',
'".$data[57]."',
'".$stckingyear6."',
'".$stckingmonth6."',
 NOW(),
'".$modified_by."',
 NOW()
)";
dbQuery($dbConn,$mysql_costpond6);

}

}
else
$skip++;
$sqlcontactperson = "SELECT farmer_id,contact_first_name,contact_last_name FROM farms_contact_persons 
WHERE farmer_id = '".$data[5]."' && contact_first_name='".$data[15]."' && contact_last_name='".$data[16]."'  ";
$resultcontact = dbQuery($dbConn,$sqlcontactperson);
if(dbNumRows($resultcontact) ==1) {
$sql_updatecontact = "UPDATE farms_contact_persons 
set 
contact_first_name='".$data[15]."',
contact_last_name='".$data[16]."',
contact_telephone='".$data[17]."',
contact_email='".$data[19]."',
contact_gender='".$data[18]."',
date_created=NOW(),
modified_by='".$modified_by."',
modified_date=NOW() 
WHERE farmer_id = '".$data[5]."' ";
dbQuery($dbConn,$sql_updatecontact);
}else{
$mysql_contact = "INSERT INTO farms_contact_persons 
(
farmer_id,
contact_first_name,
contact_last_name,
contact_telephone,
contact_email,
contact_gender,
date_created,
modified_by,
modified_date 
)
VALUES
(
'".$data[5]."',  
'".$data[15]."',
'".$data[16]."',
'".$data[17]."',
'".$data[19]."',
'".$data[18]."',
NOW(),
'".$modified_by."',
NOW())";
dbQuery($dbConn,$mysql_contact);
}
if ($data[79]!="" and $data[79]!="0" and $data[79]!="None"){
$sqlstaff = "SELECT farmer_id,staff_role
FROM farms_staff  
WHERE farmer_id = '".$data[5]."' and staff_role='".$data[79]."' and staff_gender='".$data[81]."'";
$resultstaff = dbQuery($dbConn,$sqlstaff);
if(dbNumRows($resultstaff) ==1) {
$sql_updatestaff = "UPDATE farms_staff 
set 
staff_role='".$data[79]."',
staff_age='".$data[80]."',
staff_gender='".$data[81]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and staff_role='".$data[79]."' and staff_gender='".$data[81]."'";

dbQuery($dbConn,$sql_updatestaff);
}else{
$mysql_staff = "INSERT INTO farms_staff
(
farmer_id, 
staff_role,
staff_age,
staff_gender,
modified_by,date_modified 
)
VALUES
(
'".$data[5]."',
'".$data[79]."',
'".$data[80]."', 
'".$data[81]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_staff);

}
}
else
$skip++;
if ($data[82]!="" and $data[82]!="0" and $data[82]!="None"){
$sqlstaff2 = "SELECT farmer_id,staff_role
FROM farms_staff  
WHERE farmer_id = '".$data[5]."' and staff_role='".$data[82]."' and staff_gender='".$data[84]."'";
$resultstaff2 = dbQuery($dbConn,$sqlstaff2);
if(dbNumRows($resultstaff2) ==1) {
$sql_updatestaff2 = "UPDATE farms_staff 
set 
staff_role='".$data[82]."',
staff_age='".$data[83]."',
staff_gender='".$data[84]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and staff_role='".$data[82]."' and staff_gender='".$data[84]."'";

dbQuery($dbConn,$sql_updatestaff2);
}else{
$mysql_staff2 = "INSERT INTO farms_staff
(
farmer_id, 
staff_role,
staff_age,
staff_gender,
modified_by,date_modified 
)
VALUES
(
'".$data[5]."',
'".$data[82]."',
'".$data[83]."', 
'".$data[84]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_staff2);

}
}
else
$skip++;
if ($data[85]!="" and $data[85]!="0" and $data[85]!="None"){
$sqlstaff3 = "SELECT farmer_id,staff_role
FROM farms_staff  
WHERE farmer_id = '".$data[5]."' and staff_role='".$data[85]."' and staff_gender='".$data[87]."'";
$resultstaff3 = dbQuery($dbConn,$sqlstaff3);
if(dbNumRows($resultstaff3) ==1) {
$sql_updatestaff3 = "UPDATE farms_staff 
set 
staff_role='".$data[85]."',
staff_age='".$data[86]."',
staff_gender='".$data[87]."',
modified_by='".$modified_by."',
date_modified=NOW() 
WHERE farmer_id = '".$data[5]."' and staff_role='".$data[85]."' and staff_gender='".$data[87]."'";

dbQuery($dbConn,$sql_updatestaff3);
}else{
$mysql_staff3 = "INSERT INTO farms_staff
(
farmer_id, 
staff_role,
staff_age,
staff_gender,
modified_by,date_modified 
)
VALUES
(
'".$data[5]."',
'".$data[85]."',
'".$data[86]."', 
'".$data[87]."',
'".$modified_by."',NOW())";
dbQuery($dbConn,$mysql_staff3);

}
}
else
$skip++;

}
$skip ++;  
}

$import_status = '&msg=<p class="text-success">Data upload successfull</p>';

} else {
$import_status = '&msg=<p class="text-danger">Error uploading file</p>';
}
} else {
$import_status = '&msg=<p class="text-danger">Invalid file</p>';
}
}
header('Location: ../view.php?v=Farmer&msg='.$import_status);

?>