<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
//$date_modified=NOW();
if(isset($_POST['import_data'])){  
   $modified_by=$_SESSION['user_id'];  
    // validate to check uploaded file is a valid csv file
    $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){                                  
        if(is_uploaded_file($_FILES['file']['tmp_name'])){ 
              // Reading file  
        $csv_file = fopen($_FILES['file']['tmp_name'], 'r');   
         $i = 0;
        $Data_arr = array();
        while(! feof($csv_file)) {

            foreach(fgetcsv($csv_file) as $key=>$value){
                $Data_arr[$i][] = $value;
            }

            $i++;
            }
            fclose($csv_file);

            $skip = 0;
             foreach($Data_arr as $data){
                if($skip != 0){
                <?php
//error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
if(isset($_POST['import_data'])){
$modified_by=$_SESSION['user_id'];
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
if(is_uploaded_file($_FILES['file']['tmp_name'])){
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');

$requiredHeaders = array('When Enrolled', 'Recruitment Source','Type of Enterprise','Registration','Recruitment No.','Recommended FarmerID','Longitudes','Latitudes','Name of Enterprise/farm','Owner/Investors First Name','Owner/Investors Last Name','Telephone','Gender','Email address','Age','Contact person First Name','Contact person Last Name','Telephone','Gender','Email address','County','Sub County','Village','Landmark','Keep records','Record keeping method','Finance Source','Business Plan','Temparature','Time of day (mon, mid, eve)','Total number of ponds','Type','Total Pond Area','Number of Ponds stocked','Size of pond 1 m2','Number of Tilapia in pond 1','Number of Catfish in pond 1','Pond 1 Stocking date','Size of pond 2 m2','Number of tilapia in pond 2','Number of catfish in pond 2','Pond 2 Stocking date','Size of pond 3 m2','Number of Tilapia in pond 3','Number of catfish in pond 3','Pond 3 Stocking date','Size of pond 4 m2','Number of Tilapia in pond 4','Number of Catfish in pond 4','Pond 4 Stocking date','Size of pond 5 m2','Number of Tilapia in pond 5','Number of catfish in pond 5','Pond 5 Stocking date','Size of pond 6 m2','Number of Tilapia in pond 6','Number of catfish in pond 6','Pond 6 Stocking date','Total Pond Size','TOTAL TILAPIA','TOTAL CATFISH','E.H pond 1','E.H pond 2','E.H pond 3','E.H pond 4','E.H pond 5','E.H pond 6','Tilapia fingerling price','Catfish fingerling price','Tilapia fingerling weight','Catfish fingerling weight','Essential equipment available on farm: Equipment 1','Equipment 2','Equipment 3, Equipment 4','Total number of staff','Full time, Part time','','Staff 1 function','Age','Sex','Staff 2 function','Age','Sex','Staff 3 Function ','Age ','Sex','Total Male','Total female','Total youth ','Total 35+','Main feed','Year cycle completed','species','cycle length (Months)','pond size ','Number of fingerlings bought last year','Fingerling source','Kg of feeds bought last year','Feed source','Kgs of fish harvested last year','Main buyer','Challenge 1','Challenge 2','Challenge 3','What is your target for your fish enterprise in 5 years','What is needed to accomplish this target','What is needed to accomplish this target','Would you like to receive updates on Farm Africa Aquaculture Project','Are you willing to host a practical trainings on your farm','Remarks/Expectations','Recommendation','Trained Module 1 ','Trained Module 2','Trained Module 3','Trained Module 4','M&E comments'); //headers we expect
$firstLine = fgets($csv_file); //get first line of csv file
//fclose($csv_file);
$foundHeaders = str_getcsv(trim($firstLine), ',', '"'); //parse to array
if ($foundHeaders !== $requiredHeaders) {
//header('Location: ../view.php?v=Farmer&msg=<p class="text-danger">Headers do not match</p>');
echo 'Headers do not match: '.implode(', ', $foundHeaders);
die();
}
else{
$skip = 0;
while(($data = fgetcsv($csv_file)) !== FALSE){
if($skip != 0){
$data = str_replace("'", "", $data);
                if ($data[20]) {
                $county_id = getCountyid($data[20]);
                }
                if ($data[21]) {
                $subcounty_id = getsubCountyid($data[21]);
                } 
                if ($data[3]) {
                $registration = getRegistrationtypeID($data[3]);
                }  
                $timestamp = strtotime($data[37]);
                $date=date("Y", $timestamp);
                

                // echo"<pre>";
                // print_r($data);
                // echo"</pre>";
                // exit();  
              
                $sqlfarmer = "SELECT farmer_id FROM farms_owners  WHERE farmer_id = '".$data[5]."'";
                $resultfarmer = dbQuery($sqlfarmer);
                // if farmer already exist then update details otherwise insert new record
                if(dbNumRows($resultfarmer) ==1) {                     
                $sql_updatefarmer = "UPDATE farms_owners
                 set owner_name='".$data[9]."".$data[10]."', 
                 owner_telephone='".$data[11]."',
                 owner_gender='".$data[12]."',
                 owner_email='".$data[13]."',
                 age='".$data[14]."', 
                 registration='".$registration."',
                 date_enrolled='".$data[0]."',
                 recruitment_source='".$data[1]."',
                 type_of_enterprise='".$data[2]."',
                 finance_source='".$data[26]."',
                 modified_by='".$modified_by."',
                 date_modified=NOW() 
                 WHERE farmer_id = '".$data[5]."'";
                    dbQuery($sql_updatefarmer);
                    
                } else{
               
                $mysql_farmer = "INSERT INTO farms_owners 
                (
                farmer_id,
                owner_name,owner_gender,
                owner_email,age,
                owner_telephone, registration,
                date_enrolled,recruitment_source,
                type_of_enterprise,finance_source,
                modified_by,date_modified 
                 )
                 VALUES
                 (
                 '".$data[5]."',
                 '".$data[9]."".$data[10]."',
                 '".$data[12]."','".$data[13]."',
                 '".$data[14]."',
                 '".$data[11]."','".$registration."', 
                 '".$data[0]."', '".$data[1]."', 
                 '".$data[2]."', '".$data[26]."', 
                 '".$modified_by."', NOW()
                  )";
                 dbQuery($mysql_farmer);
                     
                }
                $sqlfarm = "SELECT
                 farmer_id,farm_name,farm_county 
                 FROM farms  
                 WHERE farmer_id = '".$data[5]."' && 
                 farm_name = '".$data[8]."' && 
                 farm_county = '".$county_id."'";
                $resultfarm = dbQuery($sqlfarm);
                if(dbNumRows($resultfarm) ==1) {
               
                $sql_updatefarm = "UPDATE 
                farms set 
                farm_subcounty='".$subcounty_id."',
                farm_village='".$data[22]."',
                farm_landmark='".$data[23]."',
                keeps_records='".$data[24]."',
                farm_longitude='".$data[6]."',
                farm_latitude='".$data[7]."',
                record_keeping='".$data[25]."',
                has_business_plan='".$data[27]."',
                pond_sample_temperature='".$data[28]."',
                pond_sample_time='".$data[29]."',
                ponds_number='".$data[30]."',
                tot_pond_area='".$data[32]."',
                ponds_stocked='".$data[33]."',
                modified_by='".$modified_by."',
                date_modified=NOW() 
                WHERE farmer_id = '".$data[5]."'";
                dbQuery($sql_updatefarm);
                }else{
                
                $mysql_farm = "INSERT INTO farms 
                (
                farmer_id, farm_name, 
                farm_county, farm_subcounty,
                farm_village,farm_landmark,
                keeps_records,farm_longitude,
                farm_latitude,record_keeping,
                has_business_plan,pond_sample_temperature,
                pond_sample_time,ponds_number,
                tot_pond_area,ponds_stocked,
                modified_by,date_modified 
                 )
                 VALUES
                 (
                 '".$data[5]."','".$data[8]."',
                 '".$county_id."','".$subcounty_id."',
                 '".$data[22]."', '".$data[23]."', 
                 '".$data[24]."', '".$data[6]."', 
                 '".$data[7]."','".$data[25]."', 
                 '".$data[27]."','".$data[28]."', 
                 '".$data[29]."', '".$data[30]."',
                 '".$data[32]."', '".$data[33]."',
                 '".$modified_by."',NOW()
                  )";
                    dbQuery($mysql_farm);
                }
                $sqlf_id="SELECT id 
                from farms 
                where farmer_id='".$data[5]."' and 
                modified_by= '".$modified_by."'";
                $resultid = dbQuery($sqlf_id);
                while($row = dbFetchAssoc($resultid)) {
                extract($row);
                $number="6";
                $sqlpond = "SELECT farm_id,pond_number 
                FROM farms_ponds  
                WHERE farm_id = '".$id."' 
                and pond_number = '".$number."' ";
                $resultfarm = dbQuery($sqlpond);
                if(dbNumRows($resultpond) ==1) {
                $sql_updatepond = "UPDATE farms_ponds 
                set 
                pond_area='".$data[34]."',
                catfish_no='".$data[36]."',
                tilapia_no='".$data[35]."',
                tilapia_price_per_fingerling='".$data[67]."',
                tilapia_fingerling_stocking_weight='".$data[69]."',
                tilapia_stocking_date='".$data[37]."',
                t_stck_year='".$date."', 
                catfish_price_per_fingerling='".$data[68]."', 
                catfish_fingerling_stocking_weight='".$data[70]."',
                catfish_stocking_date='".$data[37]."',
                c_stck_year='".$date."',
                modified_by='".$modified_by."',
                date_modified=NOW() 
                WHERE farmer_id = '".$data[5]."'";
                 
                    dbQuery($sql_updatepond);
                }else{
                $mysql_pond = "INSERT INTO farms_ponds
                (
                farm_id, pond_number, 
                pond_area,catfish_no,
                tilapia_no,tilapia_price_per_fingerling,
                tilapia_fingerling_stocking_weight,
                tilapia_stocking_date,t_stck_year,
                catfish_price_per_fingerling,catfish_fingerling_stocking_weight,
                catfish_stocking_date,c_stck_year,
                modified_by,date_modified 
                )
                VALUES
                (
                '".$id."','".$number."',
                '".$data[34]."','".$data[36]."', 
                '".$data[35]."','".$data[67]."', 
                '".$data[69]."', '".$data[37]."', 
                '".$date."', '".$data[68]."',
                '".$data[70]."', '".$data[37]."',
                '".$date."',
                '".$modified_by."',NOW())";
                dbQuery($mysql_pond);
                    
                }
                $sqlext = "SELECT farm_id  FROM farms_extended_data  
                WHERE farm_id = '".$id."'";
                $resultext = dbQuery($sqlext);
                if(dbNumRows($resultext) ==1) {
                $sql_updatext = "UPDATE farms_extended_data 
                set farm_equipments='".$data[71]."".$data[72]."".$data[73]."".$data[74]."', 
                staff_total='".$data[75]."',
                staff_fulltime='".$data[76]."',
                staff_parttime='".$data[77]."',
                main_feeds='".$data[92]."',
                challenges='".$data[103].",".$data[104].",".$data[105]."',
                five_year_target='".$data[106]."', 
                needs_to_reach_target='".$data[107].",".$data[108]."', 
                receive_updates='".$data[109]."',
                can_host_trainings='".$data[110]."',
                modified_by='".$modified_by."',
                date_modified = '".$date_modified."' 
                WHERE farm_id = '".$farm_id."'";
                dbQuery($sql_updatext);
                }else{
                $mysql_ext = "INSERT INTO farms_extended_data 
                (
                farm_id,
                farm_equipments,
                staff_total,
                staff_fulltime,
                staff_parttime,
                main_feeds,
                challenges,
                five_year_target,
                needs_to_reach_target,
                receive_updates,
                can_host_trainings,
                modified_by,
                date_modified
                 )
                VALUES
                (
                '".$id."',
                '".$data[71].",".$data[72].",".$data[73].",".$data[74]."',
                '".$data[75]."',
                '".$data[76]."',
                '".$data[77]."',
                '".$data[92]."',
                '".$data[103].",".$data[104].",".$data[105]."',
                '".$data[106]."',
                '".$data[107].",".$data[108]."',
                '".$data[109]."', 
                '".$data[110]."',
                '".$modified_by."',
                NOW())";
                    dbQuery($mysql_ext);
                } 
                $sqlcycle = "SELECT farm_id  FROM farms_production_cycles 
                WHERE farm_id = '".$id."'";
                $resultcycle = dbQuery($sqlcycle);
                if(dbNumRows($resultcycle) ==1) {
                $sql_updatext = "UPDATE farms_production_cycles 
                set 
                year='".$data[93]."',
                species='".$data[94]."',
                length='".$data[95]."',
                fingerlings_bought='".$data[97]."',
                fingerlings_source='".$data[98]."',
                feeds_bought='".$data[99]."',
                feeds_source='".$data[100]."', 
                fish_harvested='".$data[101]."',
                main_buyer='".$data[102]."',
                modified_by='".$modified_by."',
                date_modified=NOW() 
                WHERE farm_id = '".$farm_id."'";
                dbQuery($resultcycle);
                }else{
                $mysql_cycle = "INSERT INTO farms_production_cycles 
                (
                farm_id,year,
                species,length,
                fingerlings_bought,fingerlings_source,
                feeds_bought,feeds_source,
                fish_harvested,main_buyer,
                modified_by,date_modified 
                )
                VALUES
                (
                '".$id."',  '".$data[93]."',
                '".$data[94]."', '".$data[95]."',
                '".$data[97]."', '".$data[98]."', 
                '".$data[99]."', '".$data[100]."', 
                '".$data[101]."','".$data[102]."',
                '".$modified_by."',NOW())";
                    dbQuery($mysql_cycle);
                } 
                
                }
               

                }
$skip ++;
}

}


fclose($csv_file);
$import_status = '<p class="text-success">Records uploaded succesfully</p>';
} else {
$import_status = '<p class="text-danger">Error  uploading records</p>';
}
} else {
$import_status = '<p class="text-danger">Invalid file used</p>';
}
}
header("Location: ../view.php?v=Farmer&msg=".$import_status);
?>
                
                 $skip ++;  
                }
            $import_status = '&msg=<p class="text-success">success</p>';
           
        } else {
            $import_status = '&msg=<p class="text-danger">Error uploading file</p>';
        }
    } else {
        $import_status = '&msg=<p class="text-danger">Invalid file</p>';
    }
}
 header('Location: ../view.php?v=Farmer&='.$import_status);
?>