<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

$sql = "SELECT o.f_id,o.farmer_id,o.firstname,o.lastname,o.registration,o.owner_telephone,o.owner_gender,o.finance_source,o.modified_by,o.date_modified,o.date_registered,o.date_enrolled,r.id,r.type,u.id,u.name from  farms_owners o join farmer_reg_type r on o.registration=r.id  join users u on u.id=o.modified_by where o.farmer_id!=''  ORDER BY o.farmer_id";
$result = dbQuery($dbConn,$sql);
?>
    <div class="row" >
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title" style="margin-top: -10px;">
          <div><h5><font color="">Farmer Profiles&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4><?php echo $errorMessage; ?></h4></div>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
              <thead>
                  <tr>
                   <td><b>ID </td>

                   <td><b>First Name</td>
                   <td><b>Last Name</td>
                   <!-- <td><b>Name of Farm</td> -->
                   <td><b>Gender</td>
                   <!-- <td><b>County</td>
                   <td><b>SubCounty</td>
                   <td><b>Village</td>
                   <td><b>Telephone</td>
                   <td><b>Registration</td>
                   <td><b>Date Registered</td>
                   <td><b>Date Enrolled</td>
                   <td><b>Source of Finance</td>
                   <td><b>Modified By</td>
                   <td><b>Date modified</td>
                   <td><b>Delete</td> -->
                  </tr>
                  
                </thead>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>"> 
                   <td><a href="javascript:viewprofile(<?php echo $f_id; ?>);"><?php echo $farmer_id; ?></a></td>
                   <td><?php echo $firstname; $user;?></td>
                   <td><?php echo $lastname; ?></td> 
                   <td><?php echo $owner_gender; ?></td>
                   <!-- <td><?php  $county_name; ?></td>
                   <td><?php  $subcounty_name; ?></td>
                   <td><?php  $farm_village; ?></td> 
                   <td><?php  $owner_telephone; ?></td>
                   <td><?php  $type; ?></td>
                   <td><?php  $date_registered; ?></td> 
                   <td><?php  $date_enrolled; ?></td>
                   <td><?php  $finance_source; ?></td>
                   <td><?php  $name; ?></td>
                   <td><?php  $date_modified; ?></td>

                   <td align="center"><a href="javascript:delete(<?php echo $id; ?>);">Delete</a></td> -->
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
                  </table>
                  <form action="<?php echo WEB_ROOT; ?>farmer/import.php" method="post" enctype="multipart/form-data" id="import_data">
                  <div class="col-md-2"><input type="file" name="file" id="file" style="float: center;" /></div>
                  <div class="col-md-3"><input type="submit" class="btn btn-primary" name="import_data" id="import_data" value="Import"></div>
                  </form> 
                  
            <input name="btnAddUser" type="button" id="btnAddUser" value="Add Farmer (+)" class="button" onClick="addfarmer()">
                  </div>
                  
                </div>

              </div>

            </div>
          </div>

        </div>
        </div>
    </div>
</div>