<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
	case 'add' :
		addFarmer($dbConn);
		break;
		
	case 'modify' :
		editprofile($dbConn);
		break;
		
	case 'delete' :
		deleteHardware();
		break;
    

	default :
	    // if action is not defined or unknown
		// move to main user page
		header('Location: ../index.php');
}

/*
Function used to add entry in tbl_hardwares table.
*/
function addFarmer($dbConn)
{
   $farmer_id=$_POST['farmer_id']; 
   $txtFname=$_POST['txtFname'];
   $txtLname=$_POST['txtLname'];
   $registration=$_POST['registration'];  
   $owner_telephone=$_POST['owner_telephone']; 
   $owner_gender=$_POST['owner_gender']; 
   $type_of_enterprise=$_POST['type_of_enterprise']; 
   $finance_source=$_POST['finance_source']; 
   $year = date("Y");
   $password = md5($txtLname.'#'.$year);
   $insert_pass = json_encode($password);
   $occupation=$_POST['occupation'];
   $modified_by=$_SESSION['user_id']; 
   $age=$_POST['age']; 
   $owner_email=$_POST['owner_email'];
   $finance=implode( ", ", $finance_source );
   
	$sql = "SELECT farmer_id
	        FROM farms_owners
			WHERE farmer_id = '$farmer_id'";
	$result = dbQuery($dbConn,$sql);
	
	if (dbNumRows($result) == 1) {

		header('Location: ../add.php?v=addfarmer&msg=' . urlencode('<p class="text-danger">Farmer Identification entered exists!</p>'));	
	} else {			
           $sql   = "INSERT INTO farms_owners (registration,recruitment_source,pass,farmer_id, firstname,lastname, owner_telephone,
         owner_gender,owner_email,type_of_enterprise,finance_source,occupation,modified_by,date_modified,age,date_registered,date_enrolled)
          VALUES ('$registration','$modified_by','$password','$farmer_id', '$txtFname', '$txtLname', '$owner_telephone', '$owner_gender', '$owner_email','$type_of_enterprise','$finance','$occupation','$modified_by',NOW(),$age,NOW(),'Not Yet')";
	
		dbQuery($dbConn,$sql);
		header('Location: ../add.php?v=addfarmer&msg='. urlencode('<p class="text-success">Farmer record saved</p>'));	
	}
}

//Edit farmer profile

function editprofile($dbConn)
{
	$f_id=$_POST['fid'];
	$farmer_id = $_POST["farmer_id"];
	$firstname = $_POST["firstname"];
	$lastname =$_POST['lastname'];
    $registration = $_POST['type'];
	$date_registered = $_POST['date_registered'];
	$date_enrolled = $_POST['date_enrolled'];
	$age = $_POST['age'];
	$owner_email = $_POST['owner_email'];
	$owner_telephone = $_POST['owner_telephone'];
	$owner_gender = $_POST["owner_gender"];
    $occupation = $_POST['occupation'];
	$finance_source = $_POST['finance_source'];
	$finance=implode( ", ", $finance_source );
	$type_of_enterprise = $_POST['type_of_enterprise'];
	$modified_by=$_SESSION['user_id']; 

	// echo "<pre>";
 // print_r($_POST);
 // echo"</pre>";
 // exit();
	//echo $id;
    //exit();
	//$utype = 'USER';
	//$did = (int)$_POST['did'];
	
		$sql   = "UPDATE farms_owners
		   SET  firstname = '$firstname',
		        lastname = '$lastname',
			    registration='$registration',
			    date_registered='$date_registered',
			    age='$age',
			    date_enrolled='$date_enrolled',
			    owner_email='$owner_email',
			    owner_telephone='$owner_telephone',
			    owner_gender='$owner_gender',
			    occupation='$occupation',
			    finance_source='$finance',
			    type_of_enterprise='$type_of_enterprise',
			    modified_by='$modified_by',
			    date_modified=NOW()


				WHERE farmer_id = '$farmer_id'";
	
		dbQuery($dbConn,$sql);
		
		header('Location: ../edit.php?v=editfarmer&id='.$f_id);	
	
}

/*
	Remove a Hardware
*/
function deleteHardware()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$id = (int)$_GET['id'];
	} else {
		header('Location: index.php');
	}
	
	
	$sql = "DELETE FROM tbl_hardwares
	        WHERE id = $id";
	dbQuery($dbConn,$sql);
	
	header('Location: ../menu.php?v=HRWR');
}
?>