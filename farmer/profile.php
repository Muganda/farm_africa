<?php
require_once 'farmer/profiletab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id as ofarmer_id FROM farms_owners where f_id='$f_id'";

$result1 = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  }
$sqlpond="SELECT pond_number,pond_area,expected_harvest_date,tilapia_fingerling_stocked,catfish_fingerling_stocked,stocking_date FROM  farms_ponds where farmer_id='$ofarmer_id' ";
$resultpond = dbQuery($dbConn,$sqlpond);



$sqlfarm="SELECT f.farm_name,f.farm_county,f.farm_subcounty,f.farmphoto,f.farm_village,f.farm_landmark,f.keeps_records,f.farm_longitude,f.farm_latitude,f.record_keeping,f.records_kept,f.has_business_plan,f.business_plan_last_update,f.pond_sample_temperature,f.pond_sample_time,count(p.pond_number) as ponds,(count(CASE WHEN p.tilapia_fingerling_stocked > 0 THEN 1 END) + count(CASE WHEN p.catfish_fingerling_stocked > 0 THEN 1 END)) as pondstocked, f.id, c.id, c.county_name, s.id,s.subcounty_name FROM farms f left join counties c on f.farm_county=c.id left join sub_counties s on f.farm_subcounty=s.id inner join farms_ponds p on f.farmer_id= p.farmer_id  where f.farmer_id='$ofarmer_id' group by p.farmer_id ";

$resultfarm = dbQuery($dbConn,$sqlfarm);

$sqlstaff="SELECT s.farmer_id,s.staff_role,s.staff_age,s.staff_gender,s.contract_type,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id, e.staff_parttime,e.staff_fulltime FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_staff s on f.farmer_id=s.farmer_id right join farms_extended_data e on f.farmer_id=e.farmer_id where f_id='$f_id' ";

$resultstaff = dbQuery($dbConn,$sqlstaff);

$sql = "SELECT o.registration,o.recruitment_source,o.farmer_id, o.firstname,o.lastname, o.owner_telephone,o.date_enrolled,
         o.owner_gender,o.owner_email,o.type_of_enterprise,o.finance_source,o.occupation,o.modified_by,o.date_modified,o.age,o.date_registered,r.id,r.type,s.id,s.source_name
        FROM farmer_reg_type r inner join farms_owners o on r.id = o.registration left join farmer_finance_sources s on o.finance_source=s.id  where f_id='$f_id'";
$result = dbQuery($dbConn,$sql);

$sqlpersons="SELECT c.farmer_id,c.contact_first_name,c.contact_last_name,c.contact_telephone,c.contact_email,c.contact_gender,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_contact_persons c on f.farmer_id=c.farmer_id where f_id='$f_id' ";

  $resultpersons = dbQuery($dbConn,$sqlpersons);

$sqlext = "SELECT e.farmer_id,e.water_availability,e.water_sources,e.feeds_source,e.main_feeds, e.water_mechanism,e.farm_greenhouse, e.farm_equipments,
      e.staff_fulltime,e.staff_parttime,e.has_security,e.security_types,e.customers,e.customers_others,e.challenges,e.five_year_target,e.needs_to_reach_target,e.receive_updates,e.can_host_trainings,o.f_id,f.id,o.farmer_id,f.farmer_id,f.farm_name
      FROM farms_owners o inner join farms f on o.farmer_id = f.farmer_id inner join farms_extended_data e on f.farmer_id=e.farmer_id  where f_id='$f_id'";
$resultext = dbQuery($dbConn,$sqlext);



?>

<div class="row col-md-12" style="margin-top: 10px">
<div class="prepend-1 span-17">
<div class="col-md-6">
    <div class="panel with-nav-tabs panel-default" >
        <div class="panel-heading" style="padding: 4px 4px 0 4px; border-bottom: none; margin-bottom: -1px; ">
                <ul class="nav nav-tabs" style="padding: 4px 4px 0 4px; border-bottom: none; margin-bottom: -1px; ">
                    <li class="active"><a href="#tab1default" data-toggle="tab">Farmer Infomation</a></li>
                    <li><a href="#tab2default" data-toggle="tab">Farm Information</a></li>
                    <li><a href="#tab3default" data-toggle="tab">Contact Persons</a></li>
                    <li><a href="#tab4default" data-toggle="tab">Staff Information</a></li>
                </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab1default">
                 <div class="prepend-1 span-17">
                      <?php
                      while($row = dbFetchAssoc($result)){
                      extract($row);
                      ?>
                      <form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
                      <div class="table-responsive">
                        <!-- <h4>Farm Information</h4> -->
                      <table  id="paging" class="table table-striped table-bordered">
                      <tbody>
                      <tr><td><b>Farmer ID</td><td><a href="javascript:editprofile(<?php echo $f_id; ?>);"><?php echo $farmer_id; ?></a></td></tr>
                      <tr><td><b>Name</td><td><?php echo $firstname; ?>,<?php echo $lastname; ?></td></tr>
                      <tr><td><b>Age</td><td><?php echo $age; ?></td></tr>
                      <tr><td><b>Genger</td><td><?php echo $owner_gender; ?></td></tr>
                      <tr><td><b>Telephone</td><td><?php echo $owner_telephone; ?></td></tr>
                      <tr><td><b>Email</td><td><?php echo $owner_email; ?></td></tr>
                      <tr><td><b>Registration</td><td><?php echo $type; ?></td></tr>
                      <tr><td><b>Date Enrolled</td><td><?php echo $date_enrolled; ?></td></tr>
                      <tr><td><b>Source of Finance</td><td><?php echo $source_name;?></td></tr>
                      <tr><td><b>Type of Enterprise</td><td><?php echo $type_of_enterprise; ?></td></tr>
                      </tbody>
                      </table>
                      </div>
                      </form>
                      <?php
                      }//while
                      ?>
                      </div> 
                </div>
                <div class="tab-pane fade" id="tab2default">
                 <div class="prepend-1 span-17">
                      <?php
                      while($row = dbFetchAssoc($resultfarm)){
                      extract($row);
                      ?>
                      <form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
                      <div class="table-responsive">
                        <!-- <h4>Farm Information</h4> -->
                      <table class="table table-striped table-bordered">
                      <tbody>
                        <tr><td><b>Name of Farm</td><td><?php echo $farm_name; ?></td></tr>
                        <tr><td><b>County</td><td><?php echo $county_name; ?></td></tr>
                        <tr><td><b>Sub County</td><td><?php echo $subcounty_name; ?></td></tr>
                        <tr><td><b>Village</td><td><?php echo $farm_village; ?></td></tr>
                        <tr><td><b>Number of staff</td><td><?php echo $total; ?></td></tr>
                        <tr><td><b>Total no. of ponds</td><td><?php echo $ponds; ?></td></tr>
                        <tr><td><b>Ponds stocked</td><td><?php echo $pondstocked; ?></td></tr>
                        <tr><td><b>Which records do you keep?</td><td><?php echo $record_keeping;?></td></tr>
                        <tr><td><b>Have you recieved a record keeping book from KMAP?</td><td><?php echo $records_kept; ?></td></tr>
                        <tr><td><b>Do they have a Business plan?</td><td><?php echo $has_business_plan; ?></td></tr>
                        <tr><td><b>Last time business plan was updated</td><td><?php echo $business_plan_last_update; ?></td></tr>
                        <tr><td><b>Farm Photo</td><td><img  src="<?php echo WEB_ROOT;?>farm/<?php echo $farmphoto; ?>" width="120px" height="100px" ></td></tr>
                      </tbody>
                      </table>
                      </div>
                      </form>
                      <?php
                      }//while
                      ?>
                      </div> 
                </div>
                <div class="tab-pane fade" id="tab3default">
                  <div class="prepend-1 span-17">
                    <form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
                    <div class="table-responsive">
                      <!-- <h4>Contact Persons</h4> -->
                    <table class="table table-striped table-bordered">

                    <thead>
                      <tr>
                       <td><b>Name</td>
                       <td><b>Telephone</td>
                       <td><b>Gender</td>
                       <td><b>Position</td>
                      
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = dbFetchAssoc($resultpersons)) {
                      extract($row);
                      if ($i%2) {
                        $class = 'row1';
                      } else {
                        $class = 'row2';
                      }
                     
                    ?>

                      <tr class="<?php echo $class; ?>">
                       <td><?php echo $contact_first_name .','.$contact_last_name; ?></td>
                       <td><?php echo $contact_telephone; ?></td>
                       <td><?php echo $contact_gender; ?></td>
                       <td><?php echo $contact_position; ?></td>
                      </tr>
                    <?php
                    } // end while

                    ?>
                      
                     </tbody>
                    </table>
                    </div>
                    </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab4default">
                 <div class="prepend-1 span-17">
                  <form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
                  <div class="table-responsive">
                  <!-- <h4>Staff Information</h4> -->
                  <table class="table table-striped table-bordered">

                  <thead>
                  <tr>
                   <td><b>Age</td>
                   <td><b>Gender</td>
                   <td><b>Role</td>
                   <td><b>Contract Type</td>
                   

                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  while($row = dbFetchAssoc($resultstaff)) {
                  extract($row);
                  if ($i%2) {
                    $class = 'row1';
                  } else {
                    $class = 'row2';
                  }

                  ?>

                  <tr class="<?php echo $class; ?>">
                   <td><?php echo $staff_age; ?></td>
                   <td><?php echo $staff_gender; ?></td>
                   <td><?php echo $staff_role; ?></td>
                   <td><?php echo $contract_type; ?></td>
                  </tr>
                  <?php
                  } // end while

                  ?>

                  </tbody>
                  </table>
                  </div>
                  </form>
                  </div>
                </div>
            </div>
        </div>
      </div>
        </div>
<div class="prepend-1 span-17">
<div class="col-md-6">
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px;">
            <ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
                <li class="active"><a href="#tab1" data-toggle="tab">Other Farm Information</a></li>
                <li><a href="#tab2" data-toggle="tab">Pond Information</a></li>
            </ul>
    </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1">
                        <div class="prepend-1 span-17">
                          <?php
                          while($row = dbFetchAssoc($resultext)){
                          extract($row);
                          ?>
                            <form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                            <tbody>
                              <tr><td><b>Water availability</td><td><?php echo $water_availability; ?></td></tr>
                              <tr><td><b>Sources of Water</td><td><?php echo $water_sources; ?></td></tr>
                              <tr><td><b>How does water get to the farm?</td><td><?php echo $water_mechanism; ?></td></tr>
                              <tr><td><b>Main feeds</td><td><?php echo $main_feeds; ?></td></tr>
                              <tr><td><b>Source of feeds</td><td><?php echo $feeds_source; ?></td></tr>
                              <tr><td><b>Equipments</td><td><?php echo $farm_equipments; ?></td></tr>
                              <!-- <tr><td><b>Number of staff</td><td><?php echo $staff_total; ?></td></tr> -->
                              <tr><td><b>Does he/she farm in a greenhouse</td><td><?php echo $farm_greenhouse; ?></td></tr>
                              <tr><td><b>Type of security</td><td><?php echo $security_types; ?></td></tr>
                              <tr><td><b>Customers</td><td><?php echo $customers;?></td></tr>
                              <tr><td><b>Challenges</td><td><?php echo $challenges; ?></td></tr>
                              <tr><td><b>5 year target</td><td><?php echo $five_year_target; ?></td></tr>
                              <tr><td><b>Requirements to archieve target</td><td><?php echo $needs_to_reach_target; ?></td></tr>
                            </tbody>
                            </table>
                            </div>
                            </form>
                            <?php
                            } // end while
                          ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab2">
                        <div class="prepend-1 span-17">
                          <form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
                          <div class="table-responsive">
                          <!-- <h4>Pond Information</h4> -->
                          <table class="table table-striped table-bordered">

                          <thead>
                          <tr>
                           <td><b>Pond Name/Number</td>
                           <td><b>Area(M2)</td>
                           <td><b>Catfish stocked</td>
                           <td><b>Tilapia Stocked</td>
                           <td><b>Stocking date</td>
                           <td><b>Expected harvest date</td>

                          </tr>
                          </thead>
                          <tbody>
                          <?php
                          while($row = dbFetchAssoc($resultpond)) {
                          extract($row);
                          if ($i%2) {
                            $class = 'row1';
                          } else {
                            $class = 'row2';
                          }

                          ?>

                          <tr class="<?php echo $class; ?>">
                           <td><?php echo $pond_number; ?></td>
                           <td><?php echo $pond_area; ?></td>
                           <td><?php echo $catfish_fingerling_stocked; ?></td>
                           <td><?php echo $tilapia_fingerling_stocked; ?></td>
                           <td><?php echo $stocking_date; ?></td>
                           <td><?php echo $expected_harvest_date; ?></td>
                           
                          </tr>
                          <?php
                          } // end while

                          ?>

                          </tbody>
                          </table>
                          </div>
                          </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
</div>
</div>

  
