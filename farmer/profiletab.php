
<?php

if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];

} else {
  header('Location: ./index.php');
}
?>
<div class="panel-heading" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; ">
<ul class="nav nav-tabs" style="padding: 5px 5px 0 5px; border-bottom: none; margin-bottom: -1px; background-color: #324159; width: 100% ">
<li  role="presentation" ><a href="javascript:viewprofile(<?php echo $f_id; ?>);">Basic Profile</a></li>
<li  role="presentation"><a href="javascript:costreport(<?php echo $f_id; ?>);">Cost Reports</a></li>
<li  role="presentation"><a href="javascript:salesreport(<?php echo $f_id; ?>);">Sales Reports</a></li>
</ul>
</div>
