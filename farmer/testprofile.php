<?php
require_once 'farmer/profiletab.php';
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = "";
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$fid = "SELECT farmer_id  FROM farms_owners where f_id='$f_id'";

$result1 = dbQuery($fid);
while($row = dbFetchAssoc($result1)) {
  extract($row);
  }
$sqlpond="SELECT p.farm_id,p.pond_number,p.pond_area,p.expected_harvest_date,p.tilapia_fingerling_stocked,p.catfish_fingerling_stocked,               p.stocking_date,f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_ponds p on f.id=p.farm_id where f_id='$f_id' ";

$resultpond = dbQuery($sqlpond);

$sqlfarm="SELECT f.farm_name,f.farm_county,f.farm_subcounty,f.farm_village,f.farm_landmark,f.keeps_records,f.farm_longitude,f.farm_latitude,f.record_keeping,f.records_kept,f.has_business_plan,f.business_plan_last_update,f.ponds_number,f.ponds_stocked,f.pond_sample_temperature,f.pond_sample_time,f.farmer_id, f.id,o.farmer_id, o.f_id, c.id, c.county_name, s.id,s.subcounty_name FROM farms f inner join farms_owners o on f.farmer_id=o.farmer_id join counties c on f.farm_county=c.id join sub_counties s on f.farm_subcounty=s.id where f_id='$f_id' ";

$resultfarm = dbQuery($sqlfarm);

$sqlstaff="SELECT s.farm_id,s.staff_role,s.staff_age,s.staff_gender,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_staff s on f.id=s.farm_id where f_id='$f_id' ";

$resultstaff = dbQuery($sqlstaff);

$sql = "SELECT o.registration,o.recruitment_source,o.farmer_id, o.firstname,o.lastname, o.owner_telephone,o.date_enrolled,
         o.owner_gender,o.owner_email,o.type_of_enterprise,o.finance_source,o.occupation,o.modified_by,o.date_modified,o.age,o.date_registered,r.id,r.type
        FROM farmer_reg_type r inner join farms_owners o on r.id = o.registration  where f_id='$f_id'";
$result = dbQuery($sql);

$sqlpersons="SELECT c.farm_id,c.contact_first_name,c.contact_last_name,c.contact_telephone,c.contact_email,c.contact_gender,
  f.farmer_id, f.id,f.farm_name,o.farmer_id, o.f_id FROM farms_owners o inner join farms f on o.farmer_id=f.farmer_id inner join farms_contact_persons c on f.id=c.farm_id where f_id='$f_id' ";

  $resultpersons = dbQuery($sqlpersons);

$sqlext = "SELECT e.farm_id,e.water_availability,e.water_sources,e.feeds_source,e.main_feeds, e.water_mechanism,e.farm_greenhouse, e.farm_equipments,e.staff_total,
      e.staff_fulltime,e.staff_parttime,e.has_security,e.security_types,e.customers,e.customers_others,e.challenges,e.five_year_target,e.needs_to_reach_target,e.receive_updates,e.can_host_trainings,o.f_id,f.id,o.farmer_id,f.farmer_id,f.farm_name
      FROM farms_owners o inner join farms f on o.farmer_id = f.farmer_id inner join farms_extended_data e on f.id=e.farm_id  where f_id='$f_id'";
$resultext = dbQuery($sqlext);



?>
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
  <h3>Farmer Profile</h3>
<table class="table table-striped table-bordered">

<thead>
  <tr>
   <td><b>ID</td>
   <td><b>First Name</td>
   <td><b>Last Name</td>
   <td><b>Gender</td>
   <td><b>Age</td>
   <td><b>Telephone</td>
   <td><b>Email</td>
   <td><b>Registration</td>
   <!-- <td><b>Date registered</td> -->
   <td><b>Date Enrolled</td>
   <!-- <td><b>Occupation</td> -->
   <td><b>Source of Finance</td>
   <td><b>Type of Enterprise</td>
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  //echo $farmer_id;
?>

  <tr class="<?php echo $class; ?>">
    <td><a href="javascript:editprofile(<?php echo $f_id; ?>);"><?php echo $farmer_id; ?></a></td>
   <td><?php echo $firstname; ?></td>
   <td><?php echo $lastname; ?></td>
   <td><?php echo $owner_gender; ?></td>
   <td><?php echo $age; ?></td>
   <td><?php echo $owner_telephone; ?></td>
   <td><?php echo $owner_email; ?></td>
   <td><?php echo $type; ?></td>
   <!-- <td><?php echo $date_registered; ?></td> -->
   <td><?php echo $date_enrolled; ?></td>
   <!-- <td><?php echo $occupation; ?></td> -->
   <td><?php echo $finance_source; ?></td>
   <td><?php echo $type_of_enterprise; ?></td>
  </tr>
<?php
} // end while

?>
  
 </tbody>
</table>
</div>
</div>
</form>
</div>
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($resultfarm)){
extract($row);
?>
<div class="prepend-1 span-17">
<div class="col-md-4">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Farm Information</h4>
<table class="table table-striped table-bordered">
<tbody>
  <tr><td><b>Name of Farm</td><td><?php echo $farm_name; ?></td></tr>
  <tr><td><b>County</td><td><?php echo $county_name; ?></td></tr>
  <tr><td><b>Sub County</td><td><?php echo $subcounty_name; ?></td></tr>
  <tr><td><b>Village</td><td><?php echo $farm_village; ?></td></tr>
  <tr><td><b>Total no. of ponds</td><td><?php echo $ponds_number; ?></td></tr>
  <tr><td><b>Ponds stocked</td><td><?php echo $ponds_stocked; ?></td></tr>
  <tr><td><b>Which records do you keep?</td><td><?php echo $record_keeping;?></td></tr>
  <tr><td><b>Have you recieved a record keeping book from KMAP?</td><td><?php echo $records_kept; ?></td></tr>
  <tr><td><b>Do they have a Business plan?</td><td><?php echo $has_business_plan; ?></td></tr>
  <tr><td><b>Last time business plan was updated</td><td><?php echo $business_plan_last_update; ?></td></tr>
</tbody>
</table>
</div>
</form>
</div>
</div>
<?php

}//while
}else {
}

?>
<div class="col-md-8">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Pond Information</h4>
<table class="table table-striped table-bordered">

<thead>
  <tr>
   <td><b>Pond Name/Number</td>
   <td><b>Area(M2)</td>
   <td><b>Catfish stocked</td>
   <td><b>Tilapia Stocked</td>
   <td><b>Stocking date</td>
   <td><b>Expected harvest date</td>
  
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($resultpond)) {
  extract($row);
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
 
?>

  <tr class="<?php echo $class; ?>">
   <td><?php echo $pond_number; ?></td>
   <td><?php echo $pond_area; ?></td>
   <td><?php echo $catfish_fingerling_stocked; ?></td>
   <td><?php echo $tilapia_fingerling_stocked; ?></td>
   <td><?php echo $stocking_date; ?></td>
   <td><?php echo $expected_harvest_date; ?></td>
   
  </tr>
<?php
} // end while

?>
  
 </tbody>
</table>
</div>
</form>
</div>
</div>
</div>
<?php
if(dbAffectedRows() == 1){
while($row = dbFetchAssoc($resultext)){
extract($row);
?>
<div class="prepend-1 span-17">
<div class="col-md-8">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Other Farm Information</h4>
<table class="table table-striped table-bordered">
<tbody>
  <tr><td><b>Water availability</td><td><?php echo $water_availability; ?></td></tr>
  <tr><td><b>Sources of Water</td><td><?php echo $water_sources; ?></td></tr>
  <tr><td><b>Water mechanisim</td><td><?php echo $water_mechanism; ?></td></tr>
  <tr><td><b>Main feeds</td><td><?php echo $main_feeds; ?></td></tr>
  <tr><td><b>Source of feeds</td><td><?php echo $feeds_source; ?></td></tr>
  <tr><td><b>Equipments</td><td><?php echo $farm_equipments; ?></td></tr>
  <tr><td><b>Number of staff</td><td><?php echo $staff_total; ?></td></tr>
  <tr><td><b>Does he/she farm in a greenhouse</td><td><?php echo $farm_greenhouse; ?></td></tr>
  <tr><td><b>Type of security</td><td><?php echo $security_types; ?></td></tr>
  <tr><td><b>Customers</td><td><?php echo $customers;?></td></tr>
  <tr><td><b>Challenges</td><td><?php echo $challenges; ?></td></tr>
  <tr><td><b>5 year target</td><td><?php echo $five_year_target; ?></td></tr>
  <tr><td><b>Requirements to archieve target</td><td><?php echo $needs_to_reach_target; ?></td></tr>
</tbody>
</table>
</div>
</form>
</div>
</div>
<?php
}//while
}else {

}

?>
<div class="container-fluid content">
<div class="col-md-6">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Staff Information</h4>
<table class="table table-striped table-bordered">

<thead>
  <tr>
   <td><b>Age</td>
   <td><b>Gender</td>
   <td><b>Role</td>
  
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($resultstaff)) {
  extract($row);
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
 
?>

  <tr class="<?php echo $class; ?>">
   <td><?php echo $staff_age; ?></td>
   <td><?php echo $staff_gender; ?></td>
   <td><?php echo $staff_role; ?></td>
  </tr>
<?php
} // end while

?>
  
 </tbody>
</table>
</div>
</form>
</div>
</div>
<div class="col-md-6">
<div class="prepend-1 span-17">
<form action="processFarmer.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="table-responsive">
  <h4>Contact Persons</h4>
<table class="table table-striped table-bordered">

<thead>
  <tr>
   <td><b>Name</td>
   <td><b>Telephone</td>
   <td><b>Gender</td>
   <td><b>Position</td>
  
  </tr>
</thead>
<tbody>
<?php
while($row = dbFetchAssoc($resultpersons)) {
  extract($row);
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
 
?>

  <tr class="<?php echo $class; ?>">
   <td><?php echo $contact_first_name .','.$contact_last_name; ?></td>
   <td><?php echo $contact_telephone; ?></td>
   <td><?php echo $contact_gender; ?></td>
   <td><?php echo $contact_position; ?></td>
  </tr>
<?php
} // end while

?>
  
 </tbody>
</table>
</div>
</form>
</div>
</div>
</div>
</div>