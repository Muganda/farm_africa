<?php
require_once 'library/config.php';
require_once 'library/functions.php';
require_once 'library/common.php';

checkUser();

$content = 'main.php';

$pageTitle = 'Farm Africa';
$script = array();

require_once 'template.php';
?>
