/*
Strip whitespace from the beginning and end of a string
Input : a string
*/
function trim(str)
{
  return str.replace(/^\s+|\s+$/g,'');
}

/*
Make sure that textBox only contain number
*/
function checkNumber(textBox)
{
  while (textBox.value.length > 0 && isNaN(textBox.value)) {
    textBox.value = textBox.value.substring(0, textBox.value.length - 1)
  }
  
  textBox.value = trim(textBox.value);
/*  if (textBox.value.length == 0) {
    textBox.value = 0;    
  } else {
    textBox.value = parseInt(textBox.value);
  }*/
}

/*
  Check if a form element is empty.
  If it is display an alert box and focus
  on the element
*/
function checkPass()
{
    //Store the password field objects into variables ...
    var npassword = document.getElementById('npassword');
    var cpassword = document.getElementById('cpassword');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(npassword.value == cpassword.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        cpassword.style.borderColor  = goodColor;
        message.style.color = goodColor;
        message.innerHTML = " "
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        cpassword.style.borderColor  = badColor;
        message.style.color = badColor;
        message.innerHTML = "New and confirmation passwords do not match!"
    }
}  

// $(document).ready(function () {
//     toggle(); //call this first so we start out with the correct visibility depending on the selected form values
//     //this will call our toggleFields function every time the selection value of our underAge field changes
//     $("#catfish_fingerling_stocked").change(function () {
//         toggle();
//     });

// });
// //this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
// function togglet() {
//     if ($("#catfish_fingerling_stocked").val() > 0)
//         $("#catfish_price_per_fingerling").show();
//     else
//         $("#catfish_price_per_fingerling").hide();
// };
// $(document).ready(function () {
//     togglesc(); //call this first so we start out with the correct visibility depending on the selected form values
//     //this will call our toggleFields function every time the selection value of our underAge field changes
//     $("#catfish_fingerling_stocked").change(function () {
//         togglesc();
//     });

// });
// //this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
// function togglescx() {
//     if ($("#catfish_fingerling_stocked").val() > 0)
//         $("#catfish_fingerling_stocking_weight").show();
//     else
//         $("#catfish_fingerling_stocking_weight").hide();
// };

// $(document).ready(function () {
//     toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
//     //this will call our toggleFields function every time the selection value of our underAge field changes
//     $("#tilapia_fingerling_stocked").change(function () {
//         toggleFields();
//     });

// });
// //this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
// function toggleFieldsx() {
//     if ($("#tilapia_fingerling_stocked").val() > 0)
//         $("#tilapia_price_per_fingerling").show();
//     else
//         $("#tilapia_price_per_fingerling").hide();
// };

// $(document).ready(function () {
//     toggletw(); //call this first so we start out with the correct visibility depending on the selected form values
//     //this will call our toggleFields function every time the selection value of our underAge field changes
//     $("#tilapia_fingerling_stocked").change(function () {
//         toggletw();
//     });

// });
// //this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
// function toggletw() {
//     if ($("#tilapia_fingerling_stocked").val() > 0)
//         $("#tilapia_fingerling_stocking_weight").show();
//     else
        
//         $("#tilapia_fingerling_stocking_weight").hide();
// };

function isEmpty(formElement, message) {
  formElement.value = trim(formElement.value);
  
  _isEmpty = false;
  if (formElement.value == '') {
    _isEmpty = true;
    alert(message);
    formElement.focus();
  }
  
  return _isEmpty;
}

/*
  Set one value in combo box as the selected value
*/
function setSelect(listElement, listValue)
{
  for (i=0; i < listElement.options.length; i++) {
    if (listElement.options[i].value == listValue)  {
      listElement.selectedIndex = i;
    }
  } 
}


function geoFindMe() {
  var output = document.getElementById("out");

  if (!navigator.geolocation){
    output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
    return;
  }

  function success(position) {
    var latitude  = position.coords.latitude;
    var longitude = position.coords.longitude;

    output.innerHTML = '<p>Latitude is ' + latitude + '° <br>Longitude is ' + longitude + '°</p>';

    var img = new Image();
    img.src = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=300x300&sensor=false";

    output.appendChild(img);
  }

  function error() {
    output.innerHTML = "Unable to retrieve your location";
  }

  output.innerHTML = "<p>Locating…</p>";

  navigator.geolocation.getCurrentPosition(success, error);
}




//Function To Display Popup
function div_show() {
document.getElementById('abc').style.display = "block";
}
//Function to Hide Popup
function div_hide(){
document.getElementById('abc').style.display = "none";
}
function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
function div_showx() {
document.getElementById('abcd').style.display = "block";
}
//Function to Hide Popup
function div_hidex(){
document.getElementById('abcd').style.display = "none";
}

$(document).ready(function() {
    $('.date').datepicker({  
           format: 'mm-dd-yyyy'  
         });
  });

$(document).ready(function() {
$('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
       });
});


function getUsers(type,val){
        $.ajax({
            type: 'POST',
            url: './fethData/fetchFarmer.php',
            data: 'type='+type+'&val='+val,
            beforeSend:function(html){
                $('.loading-overlay').show();
            },
            success:function(html){
                $('.loading-overlay').hide();
                $('#userData').html(html);
            }
        });
        }
      $(document).ready(function() {
        $('#paging').DataTable( {
          scrollY:        "90%",
          scrollX:        true,
          scrollCollapse: true,
          "pagingType": "full_numbers",
          dom: 'Bfrtip',
          buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
          ],
            initComplete: function () {
            this.api().columns().every( function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
            .appendTo( $(column.footer()).empty() )
            .on( 'change', function () {
            var val = $.fn.dataTable.util.escapeRegex(
            $(this).val()
            );

          column
          .search( val ? '^'+val+'$' : '', true, false )
          .draw();
          } );

          column.data().unique().sort().each( function ( d, j ) {
          select.append( '<option value="'+d+'">'+d+'</option>' )
          } );
          } );
          }
        } );
      } );


$(document).ready(function() {
$("#other").keypress(function(e){
    var value = $(this).val().replace(" ", "");
    var words = value.split(",");
    
    if(words.length > 3){
        alert("Maximum of three items allowed!");
        e.preventDefault();
    }
});

} );
 
 function display(obj,id1) {
    txt = obj.options[obj.selectedIndex].value;
    document.getElementById(id1).style.display = 'none';
    if ( txt.match(id1) ) {
    document.getElementById(id1).style.display = 'block';
    }
    };



   
  function validateTilapia()
      {
      var first = document.getElementById('tilapiaharvest').value;
      var second = document.getElementById('remtilapiapieces').value;

      if(parseInt(first) > parseInt(second)){
      document.getElementById("msg").innerHTML = "Value cannot exceed pieces remaining";
      }else{
      document.getElementById("msg").innerHTML = "";   
      }


};
function validateCatfish()
      {
      var first = document.getElementById('catfishharvest').value;
      var second = document.getElementById('remcatfishpieces').value;

      if(parseInt(first) > parseInt(second)){
      document.getElementById("message").innerHTML = "Value cannot exceed pieces remaining";
      }else{
      document.getElementById("message").innerHTML = "";   
      }


};
function validateTilapia2()
      {
      var first = document.getElementById('tilapiaharvest2').value;
      var second = document.getElementById('remtilapiapieces2').value;

      if(parseInt(first) > parseInt(second)){
      document.getElementById("msg1").innerHTML = "invalid ";
      }else{
      document.getElementById("msg1").innerHTML = "";   
      }


};
function validateCatfish2()
      {
      var first1 = document.getElementById('catfishharvest2').value;
      var second1 = document.getElementById('remcatfishpieces2').value;

      if(parseInt(first1) > parseInt(second1)){
      document.getElementById("message1").innerHTML = "invalid ";
      }else{
      document.getElementById("message1").innerHTML = "";   
      }


};


function getUrlVars() {
var vars = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
vars[key] = value;
});
return vars;
};

function addRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  if(rowCount < 20){             // limit the user from creating fields more than your limits
    var row = table.insertRow(rowCount);
    var colCount = table.rows[0].cells.length;
    for(var i=0; i<colCount; i++) {
      var newcell = row.insertCell(i);
      newcell.innerHTML = table.rows[0].cells[i].innerHTML;
    }
  }else{
     alert("Maximum Upload number reached");
         
  }
};

function deleteRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 1) {             // limit the user from removing all the fields
        alert("You cannot delete all rows");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
};

