<?php
/*
	Contain the common functions 
	required in shop and admin pages
*/
require_once 'config.php';
require_once 'database.php';

if(function_exists($_GET['f'])) {
   $_GET['f']();
}

/*
	Make sure each key name in $requiredField exist
	in $_POST and the value is not empty
*/
function checkRequiredPost($requiredField) {
	$numRequired = count($requiredField);
	$keys        = array_keys($_POST);
	
	$allFieldExist  = true;
	for ($i = 0; $i < $numRequired && $allFieldExist; $i++) {
		if (!in_array($requiredField[$i], $keys) || $_POST[$requiredField[$i]] == '') {
			$allFieldExist = false;
		}
	}
	
	return $allFieldExist;
}



function displayAmount($amount)
{
	global $shopConfig;
	return $shopConfig['currency'] . number_format($amount);
}

/*
	Join up the key value pairs in $_GET
	into a single query string
*/
function queryString()
{
	$qString = array();
	
	foreach($_GET as $key => $value) {
		if (trim($value) != '') {
			$qString[] = $key. '=' . trim($value);
		} else {
			$qString[] = $key;
		}
	}
	
	$qString = implode('&', $qString);
	
	return $qString;
}

/*
	Put an error message on session 
*/
function setError($errorMessage)
{
	if (!isset($_SESSION['plaincart_error'])) {
		$_SESSION['plaincart_error'] = array();
	}
	
	$_SESSION['plaincart_error'][] = $errorMessage;

}

/*
	print the error message
*/
function displayError()
{
	if (isset($_SESSION['plaincart_error']) && count($_SESSION['plaincart_error'])) {
		$numError = count($_SESSION['plaincart_error']);
		
		echo '<table id="errorMessage" width="550" align="center" cellpadding="20" cellspacing="0"><tr><td>';
		for ($i = 0; $i < $numError; $i++) {
			echo '&#8226; ' . $_SESSION['plaincart_error'][$i] . "<br>\r\n";
		}
		echo '</td></tr></table>';
		
		// remove all error messages from session
		$_SESSION['plaincart_error'] = array();
	}
}

/**************************
	Paging Functions
***************************/

function getPagingQuery($sql, $itemPerPage = 10)
{
	if (isset($_GET['page']) && (int)$_GET['page'] > 0) {
		$page = (int)$_GET['page'];
	} else {
		$page = 1;
	}
	
	// start fetching from this row number
	$offset = ($page - 1) * $itemPerPage;
	
	return $sql . " LIMIT $offset, $itemPerPage";
}


function getPagingLink($dbConn,$sql, $itemPerPage = 10, $strGet = '')
{
	$result        = dbQuery($dbConn,$sql);
	$pagingLink    = '';
	$totalResults  = dbNumRows($result);
	$totalPages    = ceil($totalResults / $itemPerPage);
	
	// how many link pages to show
	$numLinks      = 10;

		
	// create the paging links only if we have more than one page of results
	if ($totalPages > 1) {
	
		$self = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ;
		

		if (isset($_GET['page']) && (int)$_GET['page'] > 0) {
			$pageNumber = (int)$_GET['page'];
		} else {
			$pageNumber = 1;
		}
		
		// print 'previous' link only if we're not
		// on page one
		if ($pageNumber > 1) {
			$page = $pageNumber - 1;
			if ($page > 1) {
				$prev = " <a href=\"$self?page=$page&$strGet/\">[Prev]</a> ";
			} else {
				$prev = " <a href=\"$self?$strGet\">[Prev]</a> ";
			}	
				
			$first = " <a href=\"$self?$strGet\" class=\"pagBox\" >[First]</a> ";
		} else {
			$prev  = ''; // we're on page one, don't show 'previous' link
			$first = ''; // nor 'first page' link
		}
	
		// print 'next' link only if we're not
		// on the last page
		if ($pageNumber < $totalPages) {
			$page = $pageNumber + 1;
			$next = " <a href=\"$self?page=$page&$strGet\" class=\"pagBox\">[Next]</a> ";
			$last = " <a href=\"$self?page=$totalPages&$strGet\" class=\"pagBox\">[Last]</a> ";
		} else {
			$next = ''; // we're on the last page, don't show 'next' link
			$last = ''; // nor 'last page' link
		}

		$start = $pageNumber - ($pageNumber % $numLinks) + 1;
		$end   = $start + $numLinks - 1;		
		
		$end   = min($totalPages, $end);
		
		$pagingLink = array();
		for($page = $start; $page <= $end; $page++)	{
			if ($page == $pageNumber) {
				$pagingLink[] = " $page ";   // no need to create a link to current page
			} else {
				if ($page == 1) {
					$pagingLink[] = " <a href=\"$self?$strGet\" class=\"pagBox\">$page</a> ";
				} else {	
					$pagingLink[] = " <a href=\"$self?page=$page&$strGet\" class=\"pagBox\">$page</a> ";
				}	
			}
	
		}
		
		$pagingLink = implode(' | ', $pagingLink);
		
		// return the page navigation link
		$pagingLink = $first . $prev . $pagingLink . $next . $last;
	}
	
	return $pagingLink;
}
function getfarmerid($dbConn){
$sql = "SELECT farmer_id  FROM farms_owners ";
$result = dbQuery($dbConn,$sql);
$farmer = array();
while($row = dbFetchAssoc($result)) {
    extract($row);

   $farmerid[] = array("farmer_id" => $farmer_id);

   }
   $keys = array_keys($farmerid);
   foreach ($farmerid as $farmer){

    echo "<option value=".$farmer["farmer_id"].">".$farmer["farmer_id"]."</option>";
   }
    
   return $farmerid;
}
function getYear(){
    $years = range(date("Y"), 1985);
    $year=array();
    foreach ($years as $year) {
     echo "<option value=".$year.">".$year."</option>"; 
    }
    }
function getCounties($dbConn){
$sql = "SELECT id, county_name  FROM counties ";
$result = dbQuery($dbConn,$sql);
$county = array();
while($row = dbFetchAssoc($result)) {
    extract($row);

   $counties[] = array("id" => $id, "county_name" => $county_name);

   }
   $keys = array_keys($counties);
   foreach ($counties as $county){

   	echo "<option value=".$county["id"].">".$county["county_name"]."</option>";
   }
    
   return $counties;
}
function getCountyid($dbConn,$countyname){
  
$sql = "SELECT id FROM counties where REPLACE(county_name,'\'', '') LIKE '$countyname%'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
    extract($row);

    $county=$id;
   }
   	return $county;
}

function getCountiest($dbConn){
$sql = "SELECT id, county_name  FROM counties ";
$result = dbQuery($dbConn,$sql);
$county = array();
while($row = dbFetchAssoc($result)) {
    extract($row);

   $counties[] = array("id" => $id, "county_name" => $county_name);

   }
   
    
   return $counties;
}


function getSubCounties($dbConn){

$sql = "SELECT id, subcounty_name  FROM sub_counties  ";
$result = dbQuery($dbConn,$sql);
$subcounty = array();
while($row = dbFetchAssoc($result)) {
    extract($row);

    $subcounties[] = array("id" => $id, "subcounty_name" => $subcounty_name);
   }
   foreach ($subcounties as $subcounty){
   	echo "<option value=".$subcounty["id"].">".$subcounty["subcounty_name"]."</option>";
   }
   return $subcounties;

}
function getsubCountyid($dbConn,$subcounty){
$sql = "SELECT id FROM sub_counties where subcounty_name='".$subcounty."' ";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
    extract($row);
    $subcounty=$id;
   }
   
   	return $subcounty;
}
function getEnterprise($dbConn){
$sql = "SELECT id, ent_name FROM farmer_enterprise";
$result = dbQuery($dbConn,$sql);
$enterprise=array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $enterprises[]=array("id" => $id, "ent_name" => $ent_name);
    }
    foreach ($enterprises as $enterprise){
    echo "<option value=".$enterprise["ent_name"].">".$enterprise["ent_name"]."</option>";
    }
    return $enterprises;

}
function getFishtype($dbConn){
$sql = "SELECT id, type FROM fish_type";
$result = dbQuery($dbConn,$sql);
$types=array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $types[]=array("id" => $id, "type" => $type);
    }
    foreach ($types as $fish){
    echo "<option value=".$fish["type"].">".$fish["type"]."</option>";
    }
    return $types;

}
function getFeedtype($dbConn){
$sql = "SELECT id, feed_type FROM types_of_feeds";
$result = dbQuery($dbConn,$sql);
$feed_types=array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $types[]=array("id" => $id, "feed_type" => $feed_type);
    }
    foreach ($types as $feed){
    echo "<option value=".$feed["feed_type"].">".$feed["feed_type"]."</option>";
    }
    return $feed_types;

}

function getRegistrationtype($dbConn){
$sql = "SELECT id, type FROM farmer_reg_type";
$result = dbQuery($dbConn,$sql);
$regtype = array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $regtypes[] = array("id" => $id, "type" => $type);
    }
    foreach ($regtypes as $regtype){
    echo "<option value=".$regtype["id"].">".$regtype["type"]."</option>";
    }
    return $regtypes;

}

function getRegistrationtypeID($dbConn,$type){
$sql = "SELECT id FROM farmer_reg_type where type='".$type."'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
    extract($row);
    $type=$id;
   }
   
   	return $type;
}


function getFinancesources($dbConn){
$sql = "SELECT id, source_name FROM farmer_finance_sources";
$result = dbQuery($dbConn,$sql);
$source = array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $sources[] = array("id" => $id, "source_name" => $source_name);
    }
    foreach ($sources as $source){
    echo "<option value=".$source["source_name"].">".$source["source_name"]."</option>";
    }
    return $sources;

}
function getGender($dbConn){
$sql = "SELECT id, gender_type FROM gender";
$result = dbQuery($dbConn,$sql);
$type = array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $types[] = array("id" => $id, "gender_type" => $gender_type);
    }
    foreach ($types as $type){
    echo "<option value=".$type["gender_type"].">".$type["gender_type"]."</option>";
    }
    return $types;

}
function getfarmer_id($dbConn){
$sql = "SELECT farmer_id, owner_name FROM farms_owners";
$result = dbQuery($dbConn,$sql);
$type = array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $types[] = array("farmer_id" => $farmer_id, "owner_name" => $owner_name);
    }
    foreach ($types as $type){
    echo "<option value=".$type["farmer_id"].">".$type["owner_name"]."</option>";
    }
    return $types;
   }
function getFarmerids($dbConn){
$sql = "SELECT  farmer_id  FROM farms_owners ";
$result = dbQuery($dbConn,$sql);
$farmer = array();
while($row = dbFetchAssoc($result)) {
    extract($row);

   $farmers[] = array("farmer_id" => $farmer_id);

   }
   
    
   return $farmers;
}
  function getmodule($dbConn){
$sql = "SELECT id, training_module FROM training_modules";
$result = dbQuery($dbConn,$sql);
$type = array();
while($row = dbFetchAssoc($result)) {
    extract($row);
    $types[] = array("id" => $id, "training_module" => $training_module);
    }
    foreach ($types as $type){
    echo "<option value=".$type["id"].">".$type["training_module"]."</option>";
    }
    return $types;
   }

function getFarmer($dbConn){
if (isset($_REQUEST['query'])) {
    $query = $_REQUEST['query'];
    $sql = "SELECT farmer_id, owner_name FROM farms_owners WHERE owner_name LIKE '%{$query}%' OR farmer_id LIKE '%{$query}%'";
    $result = dbQuery($dbConn,$sql);
	$array = array();
    while ($row = dbFetchArray($result)) {
    	extract($row);
        $array[] = array (
            'label' => $owner_name, $farmer_id,                                    
            'value' => $farmer_id,
        );
    }
    //RETURN JSON ARRAY
    echo json_encode ($array);
}
}

function getFarmerx($dbConn){

    $sql = "SELECT farmer_id, owner_name FROM farms_owners WHERE owner_name LIKE '%".$_GET['query']."%' OR farmer_id LIKE '%".$_GET['query']."%'";
    $result = dbQuery($dbConn,$sql);
	$array = [];
    while ($row = dbFetchArray($result)) {
    	extract($row);
    	$array[] = $owner_name;
    }
    //RETURN JSON ARRAY
    echo json_encode ($array);
    //return $array;

}


function getYears(){
    $years = range(date("Y"), 1985);
    
    return $years;

    }
function getMontsc(){
    $months = array("January", "February", "March", "April", "May","June","July","August","September","October","November","December");
    
    return $months;

    }
  function getMonths(){

    $months = array("January", "February", "March", "April", "May","June","July","August","September","October","November","December");
    foreach ($months as $month){
    echo "<option value=".$month.">".$month."</option>";
    }

    return $months;

    }
   
  function getfishstocked($dbConn){
	$sql = "SELECT p.farmer_id, sum(p.tilapia_fingerling_stocked) as tilapia ,p.stocking_year,sum(p.catfish_fingerling_stocked) as catfish,f.farmer_id,f.farm_county  FROM farms_ponds p inner join farms f on p.farmer_id=f.farmer_id group by p.stocking_year";
	$result = dbQuery($dbConn,$sql);
	$stocked = array();
	while($row = dbFetchAssoc($result)) {
	    extract($row);

	   $fishstocked[] = array("farmer_id" => $farmer_id, "tilapia" => $tilapia, "catfish"=> $catfish, "stocking_year"=>$stocking_year, "farm_county"=>$farm_county);

   }
   
    
   return $fishstocked;
}
function getffishstockingdensity($dbConn){
  $sql = "SELECT p.farmer_id,f.farmer_id,p.stocking_year,f.farm_county,sum(p.tilapia_fingerling_stocked)as totaltilapia,sum(p.catfish_fingerling_stocked)as totalcatfish,p.pond_area,(sum(p.tilapia_fingerling_stocked)/sum(p.pond_area)) as tilapia_stocking_density,(sum(p.catfish_fingerling_stocked)/sum(p.pond_area)) as catfish_stocking_density,p.expected_harvest_date,c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id  group by f.farm_county,p.stocking_year";
  $result = dbQuery($dbConn,$sql);
  $stocked = array();
  while($row = dbFetchAssoc($result)) {
      extract($row);

     $stockingdensity[] = array("farmer_id" => $farmer_id, "totaltilapia" => $totaltilapia, "totalcatfish"=> $totalcatfish, "stocking_year"=>$stocking_year, "farm_county"=>$farm_county,"catfish_stocking_density"=>$catfish_stocking_density,"tilapia_stocking_density"=>$tilapia_stocking_density);

   }
   
    
   return $stockingdensity;
}

function getharvestingreport($dbConn){
  $sql = "SELECT p.farmer_id,f.farmer_id,p.stocking_year,f.farm_county,(sum(y.feeds_bought)/(((sum(p.tot_tilapia_sample_weight)+sum(p.tot_catfish_sample_weight))/(sum(p.no_of_tilapia_sampled)+sum(p.no_of_catfish_sampled)))*(sum(p.tilapia_fingerling_stocked)+sum(p.catfish_fingerling_stocked)))) as fcr,(sum(h.tilapia_total_weight_kg)+sum(h.catfish_total_weight_kg))as total_fish_weight,(sum(co.tilapia_price_per_fingerling)+sum(co.catfish_price_per_fingerling)+sum(co.feeds_costs)+sum(co.fertilizer_cost)+sum(co.lime_cost)+sum(co.manila_twine_cost)+sum(co.fishing_net_cost)+sum(co.extension_services_cost)+sum(co.water_costs)+sum(co.transportation_cost)+sum(co.sampling_cost)+sum(co.equipment_cost))as input_cycle_costs,(sum(co.fuel_electricyt_cost)+sum(co.salaries)+sum(co.part_time_labour)) as recurrent_cost,(((sum(co.tilapia_price_per_fingerling)+sum(co.catfish_price_per_fingerling)+sum(co.feeds_costs)+sum(co.fertilizer_cost)+sum(co.lime_cost)+sum(co.manila_twine_cost)+sum(co.fishing_net_cost)+sum(co.extension_services_cost)+sum(co.water_costs)+sum(co.transportation_cost)+sum(co.sampling_cost)+sum(co.equipment_cost)))+(((sum(co.fuel_electricyt_cost)+sum(co.salaries)+sum(co.part_time_labour))*y.length))) as total_cost,((((sum(co.tilapia_price_per_fingerling)+sum(co.catfish_price_per_fingerling)+sum(co.feeds_costs)+sum(co.fertilizer_cost)+sum(co.lime_cost)+sum(co.manila_twine_cost)+sum(co.fishing_net_cost)+sum(co.extension_services_cost)+sum(co.water_costs)+sum(co.transportation_cost)+sum(co.sampling_cost)+sum(co.equipment_cost)))+(((sum(co.fuel_electricyt_cost)+sum(co.salaries)+sum(co.part_time_labour))*y.length)))/(sum(h.tilapia_total_weight_kg)+sum(h.catfish_total_weight_kg))) as cost_per_kilo,((((sum(co.tilapia_price_per_fingerling)+sum(co.catfish_price_per_fingerling)+sum(co.feeds_costs)+sum(co.fertilizer_cost)+sum(co.lime_cost)+sum(co.manila_twine_cost)+sum(co.fishing_net_cost)+sum(co.extension_services_cost)+sum(co.water_costs)+sum(co.transportation_cost)+sum(co.sampling_cost)+sum(co.equipment_cost)))+(((sum(co.fuel_electricyt_cost)+sum(co.salaries)+sum(co.part_time_labour))*y.length)))/(sum(h.tilapia_total_weight_kg*1000)+sum(h.catfish_total_weight_kg*1000))) as cost_per_m2,((sum(h.tilapia_total_weight_kg)+sum(h.catfish_total_weight_kg))/((sum(s.whole_tilapia_fish_avg_price_kg)+sum(s.whole_catfish_fish_avg_price_kg))/2)) as revenue_per_kilo,c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id left join farm_costs co on p.farmer_id=co.farmer_id left join farm_sales s on p.farmer_id=s.farmer_id
 left join farms_production_cycles y on p.farmer_id=y.farmer_id left join harvest_information h on p.farmer_id=h.farmer_id group by f.farm_county,p.stocking_year";
  $result = dbQuery($dbConn,$sql);
  $stocked = array();
  while($row = dbFetchAssoc($result)) {
      extract($row);

     $harvestingreport[] = array("farmer_id" => $farmer_id, "fcr" => $fcr, "total_fish_weight"=> $total_fish_weight, "input_cycle_costs"=>$input_cycle_costs, "farm_county"=>$farm_county,"recurrent_cost"=>$recurrent_cost,"total_cost"=>$total_cost,"cost_per_kilo"=>$cost_per_kilo,"cost_per_m2"=>$cost_per_m2,"revenue_per_kilo"=>$revenue_per_kilo,"stocking_year"=>$stocking_year,);

   }
   
    
   return $harvestingreport;
}

function getpondsincounty($dbConn){
	$sql = "SELECT farm_county, SUM(ponds_number) AS no_total FROM farms GROUP BY farm_county ";
	$result = dbQuery($dbConn,$sql);
	$pondsno = array();
	while($row = dbFetchAssoc($result)) {
	    extract($row);

	   $ponds[] = array("farm_county" => $farm_county, "no_total" => $no_total);

   }
   
    
   return $ponds;
}
function user($dbConn){
    $sql = "SELECT id,name,last_name from  users where id= '".$_SESSION['user_id']."'";
    $result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)) {
    extract($row);
    $user=$name.$last_name;
    }
   return $user;
    }

  function farmersssion($dbConn){
    $sql = "SELECT f_id,firstname,lastname from  farms_owners where f_id= '".$_SESSION['farmer_id']."'";
    $result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)) {
    extract($row);
    $user=$firstname.$lastname;
    }
   return $user;
    }
function userid($dbConn){
    $sql = "SELECT id from  users where id= '".$_SESSION['user_id']."'";
    $result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)) {
    extract($row);
    $userid=$id;
    }
   return $userid;
    }

  function farmersessionid($dbConn){
    $sql = "SELECT f_id from  farms_owners where f_id= '".$_SESSION['farmer_id']."'";
    $result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)) {
    extract($row);
    $fmuserid=$f_id;
    }
   return $fmuserid;
    }

function userroleid($dbConn){
    $sql = "SELECT user_role from  users where id= '".$_SESSION['user_id']."'";
    $result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)) {
    extract($row);
    $roleid=$user_role;
    }
   return $roleid;
    }
function userrole($dbConn){
    $sql = "SELECT u.id,u.user_role,r.usr_role_id,r.usr_role_title from  users u join user_roles r 
    on u.user_role=r.usr_role_id
    where u.id= '".$_SESSION['user_id']."'";
    $result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)) {
    extract($row);
    $role=$usr_role_title;
    }
   return $role;
    }

function getUserRole($dbConn){
$sql = "SELECT usr_role_id, usr_role_title  FROM user_roles ";
$result = dbQuery($dbConn,$sql);
$usrole = array();
while($row = dbFetchAssoc($result)) {
    extract($row);

   $userole[] = array("usr_role_id" => $usr_role_id, "usr_role_title" => $usr_role_title);

   }
   $keys = array_keys($userole);
   foreach ($userole as $usrole){

    echo "<option value=".$usrole["usr_role_id"].">".$usrole["usr_role_title"]."</option>";
   }
    
   return $userole;
}
function getRoleid($dbConn,$usr_role_title){
  
$sql = "SELECT usr_role_id FROM user_roles where REPLACE(usr_role_title,'\'', '') LIKE '$usr_role_title%'";
$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)) {
    extract($row);

    $role=$usr_role_id;
   }
    return $role;
}
function bargraph(){
    $sql = "SELECT COUNT(DISTINCT f.farmer_id) AS  farmer,f.farm_county,c.id,c.county_name from farms f join counties c on f.farm_county = c.id Group by f.farm_county";
    $result  = mysqli_query($dbConn,$sql);
           $arr1  = array();
           $arr['name'] = 'County';
         $arr1['name'] = 'Farmers';
      while($row = mysqli_fetch_assoc($result)){
        
        $arr['data'][] = $row['county_name'];
        $arr1['data'][] = $row['farmer'];

        }
        $rslt = array();
        array_push($rslt,$arr);
        array_push($rslt,$arr1);

        print json_encode($rslt, JSON_NUMERIC_CHECK);
    }

    function training(){
    $sql = "SELECT COUNT(DISTINCT t.farmer_id) AS  farmer,t.training_module,m.id,(m.training_module) as module from farmer_training t join training_modules m on t.training_module = m.id Group by t.training_module";
      $result  = mysqli_query($dbConn,$sql);
           $arr1  = array();
           $arr['name'] = 'Module';
         $arr1['name'] = 'Farmer';
      while($row = mysqli_fetch_assoc($result)){
        
        $arr['data'][] = $row['module'];
        $arr1['data'][] = $row['farmer'];

        }
        $rslt = array();
        array_push($rslt,$arr);
        array_push($rslt,$arr1);

        print json_encode($rslt, JSON_NUMERIC_CHECK);
    }

    function farmerlocations(){
    $sql = "SELECT farmer_id,farm_name,farm_county,farm_longitude,farm_latitude,farm_village from farms";
      $result  = mysqli_query($dbConn,$sql);
      while($row = mysqli_fetch_assoc($result)){
          extract($row);
        }
        echo json_encode( $row );
    }
