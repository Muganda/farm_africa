<?php
include 'config.php';
$years=getYears();
$sql = "SELECT c.year,c.month,c.farmer_id,f.farm_county,sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs) as total_cost,s.sales_year,s.sales_month,s.farmer_id,sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income) as total_sales,((sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income))-(sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs))) as revenue,y.id,y.county_name from  farms f left join farm_sales_record s on f.farmer_id=s.farmer_id left join farm_costs c on f.farmer_id=c.farmer_id left join counties y on y.id=f.farm_county  group by f.farm_county,s.sales_year,c.year";
    $result  = mysqli_query($dbConn,$sql);
           $TT  = array();
           $arr['name'] = 'County';
           $yr2['name'] = 'Sales Year';
           $yr['name'] = 'Cost Year';
           $TT['name'] = 'Total Sales';
           $TC['name'] = 'Total Cost';
      while($row = mysqli_fetch_assoc($result)){
        
        $arr['data'][] = $row['county_name'];
        $yr2['data'][] = $row['sales_year'];
        $yr['data'][] = $row['year'];
        $TT['data'][] = $row['total_sales'];
         $TC['data'][] = $row['total_cost'];

        }
        
  
    
        $rslt = array();
        array_push($rslt,$arr);
        array_push($rslt,$TT);
        array_push($rslt,$TC);

        print json_encode($rslt, JSON_NUMERIC_CHECK);
        mysql_close($dbConn);


?>