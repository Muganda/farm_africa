// JavaScript Document
function checkAddFarmForm()
{
	with (window.document.frmAddUser) {
		if (isEmpty(farmer_id, 'Select Farmer.')) {
			return;
		} else if (isEmpty(farm_name, 'Enter Farm name; if no name input `None`')) {
			return;
		} else if (isEmpty(farm_county, 'Enter County')) {
			return;
		} else if (isEmpty(farm_subcounty, 'Enter Sub County')) {
			return;
		} else if (isEmpty(farm_village, 'Enter name of village')) {
			return;
		} else if (isEmpty(ponds_number, 'Enter Number of Ponds')) {
			return;
		} else if (isEmpty(ponds_stocked, 'Enter number of ponds stocked')) {
			return;
	    } else 
	 //   if ((Number(ponds_stocked) > Number(ponds_number), 'Ponds stocked should not be greater than number of ponds')) {
		// 	return;
		// } else 
		if (isEmpty(keeps_records, 'Specify whether or not records are kept')) {
			return;
		} else if (isEmpty(has_business_plan, 'Specify business plan')) {
			return;
		
		}else {
			submit();
		}
	}
}
function checkeditFarmForm()
{
	with (window.document.frmAddUser) {
		if (isEmpty(farm_name, 'Enter Farm name; if no name input `None`')) {
			return;
		} else if (isEmpty(farm_county, 'Enter County')) {
			return;
		} else if (isEmpty(farm_subcounty, 'Enter Sub County')) {
			return;
		} else if (isEmpty(farm_village, 'Enter name of village')) {
			return;
		} else if (isEmpty(ponds_number, 'Enter Number of Ponds')) {
			return;
		} else if (isEmpty(ponds_stocked, 'Enter number of ponds stocked')) {
			return;
		} else if (isEmpty(keeps_records, 'Specify whether or not records are kept')) {
			return;
		} else if (isEmpty(has_business_plan, 'Specify business plan')) {
			return;
		
		}else {
			submit();
		}
	}
}
function checkAddPondForm()
{

	with (window.document.frmAddUser) {
		// if (isEmpty(id, 'Enter Farm ID.')) {
		// 	return;
		// } else 
		if (isEmpty(pond_number, 'Specify pond ID')) {
			return;
		} else if (isEmpty(pond_area, 'Indicate pond size')) {
			return;
		} else if (isEmpty(catfish_no, 'Indicate number of catfish')) {
			return;
		} else if (isEmpty(tilapia_no, 'Indicate number of tilapia')) {
			return;
		} else if (isEmpty(tilapia_fingerling_stocked, 'Indicate number of tilapia fingerlings stocked ')) {
			return;
		} else if (isEmpty(tilapia_stocking_date, 'Specify the tilapia stocking date')) {
			return;
		} else if (isEmpty(catfish_fingerling_stocked, 'Indicate number of catfish fingerlings stocked ')) {
			return;
		} else if (isEmpty(catfish_stocking_date, 'Specify the catfish stocking date')) {
			return;
		
		}else {
			submit();
		}
	}
}
function checkAddcycleForm()
{

	with (window.document.frmAddUser) {
		if (isEmpty(year, 'Indicate year the cycle was completed')) {
			return;
		} else if (isEmpty(species, 'Species')) {
			return;
		} else if (isEmpty(length, 'Length of cycle')) {
			return;
		} else if (isEmpty(ponds_harvested_size, 'Size')) {
			return;
		} else if (isEmpty(fingerlings_bought, 'Number of Fingerlings bought ')) {
			return;
		} else if (isEmpty(fingerlings_source, 'Source of Fingerlings')) {
			return;
		} else if (isEmpty(feeds_bought, 'Source of Feeds ')) {
			return;
		} else if (isEmpty(fish_harvested, 'Kgs of fish hasrvested in last production cycle')) {
			return;
		
		}else {
			submit();
		}
	}
}
function checkAddContactForm()
{

	with (window.document.frmAddUser) {
		
		if (isEmpty(contact_name, 'Enter Name of Person')) {
			return;
		} else 
		if (isEmpty(contact_telephone, 'Enter Phone number')) {
			return;
		} else if (isEmpty(contact_gender, 'Specify gender')) {
			return;
		} else if (isEmpty(contact_position, 'Indicate position/role ')) {
			return;
		
		}else {
             console.log(jQuery('form#frmAddUser').serialize());
			return false;
			submit();
		}

	}
}
function checkAddStaffForm()
{

	with (window.document.frmAddUser) {
		
		if (isEmpty(staff_name, 'Enter Name of Staff')) {
			return;
		} else if (isEmpty(staff_gender, 'Specify gender')) {
			return;
		} else if (isEmpty(staff_role, 'Indicate position/role ')) {
			return;
		
		}else {
			submit();
		}

	}
}
function checkAssessmentForm()
{

	with (window.document.frmAddUser) {
		
		if (isEmpty(assessor_observations, 'Observations')) {
			return;
		} else if (isEmpty(reason_farmer_qualifies, 'Does the farmer qualify and why')) {
			return;
		} else if (isEmpty(assessor_remarks, 'Remarks ')) {
			return;
		
		}else {
			submit();
		}

	}
}
function checkAddextdataForm()

{
	with (window.document.frmAddUser) {
		if (isEmpty(water_availability, 'Water availability.')) {
			return;
		} else if (isEmpty(water_sources, 'Water sources')) {
			return;
		} else if (isEmpty(water_mechanism, 'Water mechanism')) {
			return;
		} else if (isEmpty(farm_equipments, 'Farm equipments')) {
			return;
		} else if (isEmpty(staff_total, 'Number of staff')) {
			return;
		} else if (isEmpty(has_security, 'Security of farm')) {
			return;
		// } else if (isEmpty(customers_others, 'Who are your Customers')) {
		// 	return;
		} else if (isEmpty(challenges, 'Challenges faced')) {
			return;
		} else if (isEmpty(five_year_target, '5 year target')) {
			return;
		} else if (isEmpty(needs_to_reach_target, 'Requirements to reach target')) {
			return;
		} else if (isEmpty(receive_updates, 'Would you like to recieve updates from farm africa?')) {
			return;
	    } else if (isEmpty(can_host_trainings, 'Are you ok with hosting trainings?')) {
			return;		
		
		}else {
			submit();
		}
	}
}


function checkeditExtdataForm()

{
	with (window.document.frmAddUser) {
		if (isEmpty(water_availability, 'Water availability.')) {
			return;
		} else if (isEmpty(water_sources, 'Water sources')) {
			return;
		} else if (isEmpty(water_mechanism, 'Water mechanism')) {
			return;
		} else if (isEmpty(farm_equipments, 'Farm equipments')) {
			return;
		} else if (isEmpty(staff_total, 'Number of staff')) {
			return;
		} else if (isEmpty(has_security, 'Security of farm')) {
			return;
		// } else if (isEmpty(customers_others, 'Who are your Customers')) {
		// 	return;
		} else if (isEmpty(challenges, 'Challenges faced')) {
			return;
		} else if (isEmpty(five_year_target, '5 year target')) {
			return;
		} else if (isEmpty(needs_to_reach_target, 'Requirements to reach target')) {
			return;
		} else if (isEmpty(receive_updates, 'Would you like to recieve updates from farm africa?')) {
			return;
	    } else if (isEmpty(can_host_trainings, 'Are you ok with hosting trainings?')) {
			return;		
		
		}else {
			submit();
		}
	}
	
}

function addfarm()
{
	window.location.href = 'add.php?v=addfarm';
}
function addpond()
{
	window.location.href = 'add.php?v=addpond';
}
function addpond1()
{
	window.location.href = 'add.php?v=addpond1';
}
function extfarminfo()
{
	window.location.href = 'add.php?v=extfarminfo';
}
function contactperson()
{
	window.location.href = 'add.php?v=contactperson';
}

function farmstaff()
{
	window.location.href = 'add.php?v=farmstaff';
}
function prodcycle()
{
	window.location.href = 'add.php?v=prodcycle';
}
function assesments()
{
	window.location.href = 'add.php?v=assesments';
}
function addfarminfo(id)
{
	window.location.href = 'add.php?v=addfarminfo&id=' +id;
}
function getid(id)
{
	//document.frmAddUser.cid.value = +id;
	 window.location.href = 'edit.php?v=edicontactperson&id=' +id;
	// window.document.frmAddUser.cid.value= +id;
	 //alert(id);
}
function addOpcost()
{
	window.location.href = 'add.php?v=operatingcost';
}
function addrcrcost()
{
	window.location.href = 'add.php?v=recurrentcost';
}
	

function deleteLab(id)
{
	if (confirm('Delete this Lab?')) {
		window.location.href = 'labs/processLab.php?action=delete&id=' + id;
	}
}

function editextfarminfo(id)
{
	window.location.href = 'edit.php?v=editextfarminfo&id=' + id;
	
}
function editbasicfarminfo(id)
{
	window.location.href = 'edit.php?v=editbasicfarminfo&id=' + id;
	
}
function edicontactperson(id)
{
	window.location.href = 'edit.php?v=edicontactperson&id=' + id;

	
}
function editpond(id)
{
	window.location.href = 'edit.php?v=editpond&id=' + id;
	
}
function editpondinfo(id){
    
    document.popform.pond_number.value = id;

}

function editfarmcosts(id)
{
	window.location.href = 'edit.php?v=editcost&id=' + id;
	
}
function editprodcycle(id)
{
	window.location.href = 'edit.php?v=editprodcycle&id=' + id;
	
}
function editstaff(id)
{
	window.location.href = 'edit.php?v=editstaff&id=' + id;
	
}
function editassesmnt(id)
{
	window.location.href = 'edit.php?v=editassesmnt&id=' + id;
	
}
function operation(id)
{
	window.location.href = 'edit.php?v=operation&id=' + id;
	
}
function editfarmsampling(id)
{
	window.location.href = 'edit.php?v=editsample&id=' + id;
	
}
function harvest(id)
{
	window.location.href = 'edit.php?v=edit_harvest&id=' + id;
	
}
function farmoperationstable(id)
{
	window.location.href = 'view.php?v=Farmoperationstable&id=' + id;
	
}
function stocking(id)
{
	window.location.href = 'view.php?v=stocking&id=' + id;
	
}
function sampling(id)
{
	window.location.href = 'view.php?v=sampling&id=' + id;
	
}
function harvesting(id)
{
	window.location.href = 'view.php?v=Harvest&id=' + id;
	
}
function sales(id)
{
	window.location.href = 'view.php?v=Sales&id=' + id;
	
}
function editstock(id)
{
	window.location.href = 'edit.php?v=addeditstock&id=' + id;
}
function editsample(id)
{
	window.location.href = 'edit.php?v=editsample&id=' + id;
}
function edit_harvest(id)
{
	window.location.href = 'edit.php?v=edit_harvest&id=' + id;
}
function edit_sales(id)
{
	window.location.href = 'edit.php?v=edit_sales&id=' + id;
}
function addstock(id)
{
	window.location.href = 'add.php?v=addstock&id=' + id;
}
function addsample(id)
{
	window.location.href = 'add.php?v=addsample&id=' + id;
}
function addharvest(id)
{
	window.location.href = 'add.php?v=addharvest&id=' + id;
}
function addsales(id)
{
	window.location.href = 'add.php?v=addsales&id=' + id;
}
