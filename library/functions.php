<?php
//error_reporting(E_ALL);
function checkUser()
{
	if (!isset($_SESSION['user_id']) && !isset($_SESSION['farmer_id'])) {
		header('Location: ' . WEB_ROOT . 'login.php');
		exit;
	}
	
	if (isset($_GET['logout'])) {
		doLogout();
	}
}



function doLogin($dbConn)
{
	// if we found an error save the error message in this variable
	
	$errorMessage = '';
	
	$email = $_POST['txt_email'];
	$password = $_POST['txt_password'];
	$usertype = $_POST['usertype'];
	$pass=md5($password);

	// first, make sure the email & password are not empty
	if ($email == '') {
		$errorMessage = '<div class="alert alert-danger"> You must enter your email</div>';

	} 
	else if ($password == '') {
		$errorMessage = '<div class="alert alert-danger"> You must enter the password</div>';
	} 
	else {
          
          if ($usertype=='farmer') {
          	
          $sql = "SELECT f_id, owner_email
		        FROM farms_owners 
				WHERE owner_email = '$email' AND pass = '$pass' ";
		$result = dbQuery($dbConn,$sql);
	
		if (dbNumRows($result) == 1) {
			
			$row = dbFetchAssoc($result);
			$_SESSION['farmer_id'] = $row['f_id'];
			$_SESSION['farmer_email'] = $row['owner_email'];
		
			header('Location: view.php?v=farmerProfile');
				exit;
			
		} else {
			$errorMessage = '<div class="alert alert-danger"> Wrong email or password</div>';

		}			

          }
          else{

           $sql = "SELECT id, email
		        FROM users 
				WHERE email = '$email' AND password = '$pass' ";
		$result = dbQuery($dbConn,$sql);
	
		if (dbNumRows($result) == 1) {
			
			$row = dbFetchAssoc($result);
			$_SESSION['user_id'] = $row['id'];
			$_SESSION['user_email'] = $row['email'];
		
			header('Location: index.php');
				exit;
			
		} else {
			$errorMessage = '<div class="alert alert-danger"> Wrong email or password</div>';

		}		

          }
		// check the database and see if the email and password combo do match
		
			
	}
	
	return $errorMessage;
}

/*
	Logout a user
*/
function doLogout()
{
	 
	session_destroy(); 
	header("location: ../login.php");
	//exit;
}



function getPagingNav($sql, $pageNum, $rowsPerPage, $queryString = '')
{
	$result  = mysql_query($sql) or die('Error, query failed. ' . mysql_error());
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];
	
	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);
	
	$self = $_SERVER['PHP_SELF'];

	// on page one
	if ($pageNum > 1)
	{
		$page = $pageNum - 1;
		$prev = " <a href=\"$self?page=$page{$queryString}\">[Prev]</a> ";
	
		$first = " <a href=\"$self?page=1{$queryString}\">[First Page]</a> ";
	}
	else
	{
		$prev  = ' [Prev] ';       // we're on page one, don't enable 'previous' link
		$first = ' [First Page] '; // nor 'first page' link
	}
	
	// print 'next' link only if we're not
	// on the last page
	if ($pageNum < $maxPage)
	{
		$page = $pageNum + 1;
		$next = " <a href=\"$self?page=$page{$queryString}\">[Next]</a> ";
	
		$last = " <a href=\"$self?page=$maxPage{$queryString}{$queryString}\">[Last Page]</a> ";
	}
	else
	{
		$next = ' [Next] ';      // we're on the last page, don't enable 'next' link
		$last = ' [Last Page] '; // nor 'last page' link
	}
	
	// return the page navigation link
	return $first . $prev . " Showing page <strong>$pageNum</strong> of <strong>$maxPage</strong> pages " . $next . $last; 
}
function checkTimeout($timeout = 10) {
  if ($timeout !== 0 && isset($_SESSION['last_time']) && time() - $_SESSION['last_time'] > $timeout)  {
    // Log user out.
    session_destroy();
    session_unset();
  }
  
  $_SESSION['last_time'] = time();
}



?>