<?php
include 'config.php';
$years=getYears();
$sql = "SELECT p.farmer_id,f.farmer_id,p.stocking_year,f.farm_county,sum(p.tilapia_fingerling_stocked)as totaltilapia,sum(p.catfish_fingerling_stocked)as totalcatfish,p.pond_area,(sum(p.tilapia_fingerling_stocked)/sum(p.pond_area)) as tilapia_stocking_density,(sum(p.catfish_fingerling_stocked)/sum(p.pond_area)) as catfish_stocking_density,c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id  group by f.farm_county,p.stocking_year";
    $result  = mysqli_query($dbConn,$sql);
           $TT  = array();
           $arr['name'] = 'County';
           $yr['name'] = 'Year';
           $TT['name'] = 'Tot Tilapia';
           $TC['name'] = 'Tot CatFish';
      while($row = mysqli_fetch_assoc($result)){
        
        $arr['data'][] = $row['county_name'];
        $yr['data'][] = $row['stocking_year'];
        $TT['data'][] = $row['totaltilapia'];
         $TC['data'][] = $row['totalcatfish'];

        }
        
        $rslt = array();
        array_push($rslt,$arr);
        array_push($rslt,$TT);
        array_push($rslt,$TC);

        print json_encode($rslt, JSON_NUMERIC_CHECK);
        mysql_close($dbConn);


?>