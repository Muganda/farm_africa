// JavaScript Document
function checkAddUserForm()
{
	with (window.document.frmAddUser) {
		if (isEmpty(usr_role_id, 'Enter user role')) {
			return;
		}  else if (isEmpty(txtEmail, 'Enter Email')) {
			return;
		} else if (isEmpty(txtFname, 'Enter First name')) {
			return;
		} else if (isEmpty(txtLname, 'Enter Last name')) {
			return;
		}else {
			submit();
		}
	}
}

function addUser()
{
	window.location.href = 'add.php?v=adduser';
}

function editUser(id)
{
	window.location.href = 'edit.php?v=edit&id=' + id;
	//alert(id);
}
function userprofile(id)
{
	window.location.href = 'view.php?v=Userprofile&id=' + id;
	
}
function deleteUser(userId)
{
	if (confirm('Delete this user?')) {
		window.location.href = 'user/processUser.php?action=delete&userId=' + userId;
	}
}

