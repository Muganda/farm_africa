<?php
 include 'library/config.php';
  $apikey = "AIzaSyAY20zO50qiCxe5-RqOt5nzOy77jIAekeI";
  $lat = 0.788652;
  $long = 37.12851;
  $zoom = 7;
$sql="SELECT farmer_id,farm_name,farm_county,farm_longitude,farm_latitude,farm_village from farms where farmer_id!='' and farm_longitude!='' and farm_latitude!='' ";
$result = mysqli_query($dbConn, $sql) or die (mysqli_error($dbConn));
while ($data = mysqli_fetch_array($result))
{
$farmer_id = $data['farmer_id'];
$farm_name = $data['farm_name'];
$lat = $data['farm_latitude'];
$long = $data['farm_longitude'];
  }   
 
?>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
            body { font: normal 10pt Helvetica, Arial; }
            #map-canvas { width: 750px; height: 600px; border: 0px; padding: 0px; }
        </style>

    
<script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=
          <?php echo $apikey; ?>">
    </script>
        <script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(<?php echo $lat.', '.$long; ?>),
          zoom: <?php echo $zoom; ?>
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
        <?php
          $getpoints = "SELECT farmer_id,farm_name,farm_county,farm_longitude,farm_latitude,farm_village from farms where farmer_id!='' and farm_longitude!='' and farm_latitude!='' ";
          $result = mysqli_query($dbConn, $getpoints) or die (mysqli_error($dbConn));
while ($row = mysqli_fetch_array($result))
{
echo '  var myLatlng1 = new google.maps.LatLng('. $row['farm_latitude'].', '.$row['farm_longitude'].'); 
          var marker1 = new google.maps.Marker({
            icon: "assets/img/farmer.png", 
            position: myLatlng1, 
            map: map, 
            title:"'.$row['farmer_id'].'"
          });';
           } 
        ?>


      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</head>
<body class="pace-done">
        <div class="row">
          <div  class="table-responsive col-lg-8">
            <div id="map-canvas"></div>
          </div>
          <div class="table-responsive col-lg-4">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <span class="label label-warning pull-right">Data has changed</span>
                <h5>Field Agents activity</h5>
              </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-xs-4">
                    <small class="stats-label">Ponds / Visit</small>
                    <h4>130</h4>
                  </div>

                  <div class="col-xs-4">
                    <small class="stats-label">% New Visits</small>
                    <h4>46.11%</h4>
                  </div>
                  <div class="col-xs-4">
                    <small class="stats-label">Last week</small>
                    <h4>98</h4>
                  </div>
                </div>
              </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-xs-4">
                    <small class="stats-label">Harvest / Visit</small>
                    <h4>4550</h4>
                  </div>

                  <div class="col-xs-4">
                    <small class="stats-label">% New Visits</small>
                    <h4>92.43%</h4>
                  </div>
                  <div class="col-xs-4">
                    <small class="stats-label">Last week</small>
                    <h4>900</h4>
                  </div>
                </div>
              </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-xs-4">
                    <small class="stats-label">Sales / Visit</small>
                    <h4>436 547.20</h4>
                  </div>

                  <div class="col-xs-4">
                    <small class="stats-label">% New Visits</small>
                    <h4>150.23%</h4>
                  </div>
                  <div class="col-xs-4">
                    <small class="stats-label">Last week</small>
                    <h4>124.990</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
  
</body>
</html>
