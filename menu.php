<?php
require_once 'library/config.php';
require_once 'library/functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '';

switch ($view) {
	case 'USER' :
		$content 	= 'user/list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
		break;

	case 'Farmer' :
		$content 	= 'farmer/list.php';		
		$pageTitle 	= 'Farm Africa - View Farmers';
		break;

	case 'Costs' :
		$content 	= 'farm/costs.php';		
		$pageTitle 	= 'Farm Africa - View Farm Costs';
		break;

	// case 'SFWR' :
	// 	$content 	= 'software/list.php';		
	// 	$pageTitle 	= 'Asset Management - View Softwares List';
	// 	break;

	// case 'LABS' :
	// 	$content 	= 'labs/list.php';		
	// 	$pageTitle 	= 'Asset Management - View Labs List';
	// 	break;

	// case 'STCK' :
	// 	$content 	= 'stock/list.php';		
	// 	$pageTitle 	= 'Asset Management - View Labs List';
	// 	break;

}

$script    = array('user.js', 'farmer.js', 'software.js');

require_once 'template.php';

?>