<?php
$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];

$years=getYears();
$counties=getCountiest();
$fishinfo=getfishstocked();
$colspan="2";
$rowspan="2";
function clean($string) {
    $string = trim($string);
    $string = str_replace(' ', '', $string);
    $string = str_replace('-', '', $string);
    $str1 = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    $tolower = strtolower($str1);
    return $tolower;
}
?>

 <div class="wrapper wrapper-content">
        <div class="row">
          <div class="col-md-2">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <span class="label label-success pull-right">Monthly</span>
                <h5>Visits</h5>
              </div>
              <div class="ibox-content">
                <h1 class="no-margins">1200</h1>
                <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                <small>Total visits</small>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <span class="label label-info pull-right hidden">Annual</span>
                <h5>Fish Sales</h5>
              </div>
              <div class="ibox-content">
                <h1 class="no-margins">800,800</h1>
                <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                <small>New Sales</small>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <span class="label label-primary pull-right">Today</span>
                <h5>Visits</h5>
              </div>
              <div class="ibox-content">

                <div class="row">
                  <div class="col-md-6">
                    <h1 class="no-margins">22 Farms</h1>
                    <div class="font-bold text-navy">44% <i class="fa fa-level-up"></i> <small>Farm Visits</small></div>
                  </div>
                  <div class="col-md-6">
                    <h1 class="no-margins">56,120</h1>
                    <div class="font-bold text-navy">22% <i class="fa fa-level-up"></i> <small>Fish Sales</small></div>
                  </div>
                </div>


              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5>Monthly visits</h5>
                <div class="ibox-tools">
                  <span class="label label-primary">Updated 7.2017</span>
                </div>
              </div>
              <div class="ibox-content no-padding">
                <div class="flot-chart m-t-lg" style="height: 55px;">
                  <div class="flot-chart-content" id="flot-chart1"></div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <div>
                  <span class="pull-right text-right">
                                        <small>Average value of sales in the past month in: <strong>Kenya</strong></small>
                                            <br/>
                                            All sales: 162,862
                                        </span>
                  <h3 class="font-bold no-margins">
                                Half-year Farm Profiles Analysis
                            </h3>
                  <small>Sales marketing.</small>
                </div>

                <div class="m-t-sm">

                  <div class="row">
                    <div class="col-md-8">
                      <div>
                        <canvas id="lineChart" height="114"></canvas>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <ul class="stat-list m-t-lg">
                        <li>
                          <h2 class="no-margins">2,346</h2>
                          <small>Farm costs in period</small>
                          <div class="progress progress-mini">
                            <div class="progress-bar" style="width: 48%;"></div>
                          </div>
                        </li>
                        <li>
                          <h2 class="no-margins ">4,422</h2>
                          <small>Harvest yields in last month</small>
                          <div class="progress progress-mini">
                            <div class="progress-bar" style="width: 60%;"></div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>

                </div>

                <div class="m-t-md">
                  <small class="pull-right">
                                <i class="fa fa-clock-o"> </i>
                                Update on 7.7.2017
                            </small>
                  <small>
                                <strong>Analysis of sales:</strong> The value has been changed over time, and last month reached a level over Kshs 50,000.
                            </small>
                </div>

              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <span class="label label-warning pull-right">Data has changed</span>
                <h5>Field Agents activity</h5>
              </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-xs-4">
                    <small class="stats-label">Ponds / Visit</small>
                    <h4>130</h4>
                  </div>

                  <div class="col-xs-4">
                    <small class="stats-label">% New Visits</small>
                    <h4>46.11%</h4>
                  </div>
                  <div class="col-xs-4">
                    <small class="stats-label">Last week</small>
                    <h4>98</h4>
                  </div>
                </div>
              </div>
              <div class="ibox-content">
                <div class="row">
                  
                    <div class="col-md-12">
                     <div class="table-responsive ">
                    <table id="" class="table table-striped table-bordered table-sm"  style="width: 60%">
                      <thead >
                              <tr>
                                <th ></th>
                              <?php foreach ($counties as  $county): ?>
                                <th value="<?php echo $county["id"]?>"> <?php echo $county["county_name"]?></th>
                                <?php endforeach; ?>
                              </tr>
                           </thead>
                          <tbody >
                        <tr>
                        <th >Ponds</th>
                        <?php foreach ($counties as $key => $county):?>
                        <td>
                        <?php
                          $pondsinfo =getpondsincounty();
                            foreach ($pondsinfo as $key => $pond) {
                              if ($pond['farm_county'] == $county['id']) {
                                  echo $pond['no_total'];
                              }
                            }     
                        ?>
                      </td>
                    <?php endforeach; ?>
                     </tr>
                     </tbody>
                    </table>
                  </div>
                  </div>
                  </div>
                
              </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-xs-4">
                    <small class="stats-label">Sales / Visit</small>
                    <h4>436 547.20</h4>
                  </div>

                  <div class="col-xs-4">
                    <small class="stats-label">% New Visits</small>
                    <h4>150.23%</h4>
                  </div>
                  <div class="col-xs-4">
                    <small class="stats-label">Last week</small>
                    <h4>124.990</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5>Farm Profiles Overview</h5>
                <div class="ibox-tools">
                  <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">Config option 1</a>
                    </li>
                    <li><a href="#">Config option 2</a>
                    </li>
                  </ul>
                  <a class="close-link hidden">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </div>
              <div class="ibox-content">
                
                <div class="table-responsive">
                  <table id="good" class="table table-striped table-bordered table-sm"  style="width: 60%; ">
                    <thead >
                      <tr>
                          <th></th>
                              <?php foreach ($years as $key => $year): ?>
                          <th colspan="2"> <?php echo $year ?></th><?php endforeach; ?>
                      </tr>
                      <tr>
                          <td >County</td>
                            <?php foreach ($years as $key => $year): ?>
                              <td >Tilapia</td>
                              <td >Catfish</td>
                            <?php endforeach; ?>
                      </tr>
     
                    </thead>
                  <tbody >
                    <?php foreach ($counties as $key=> $county):?>
                    <tr >
                     <th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>
         
                   <?php foreach ($years as $key => $year): ?>
                    <td>
                      <?php
                     $fishstock =getfishstocked(); 
                     foreach ($fishstock as $key => $stock) {
                              if ($stock['farm_county'] == $county['id']&& $stock['t_stck_year'] == $year) {
                                  echo $stock['tilapia_fingerling_stocked'];
                              }
                            }      
                     ?>
                  </td>
                  <td > 
                  <?php
                     
                     foreach ($fishstock as $key => $stock) {
                              if ($stock['farm_county'] == $county['id']&& $stock['c_stck_year'] == $year) {
                                  echo $stock['catfish_fingerling_stocked'];
                              }
                            }      
                     ?>
                 </td> 
                          
                   <?php endforeach; ?>
              

                      </tr>
                     <?php endforeach; ?>
                     </tbody>
                    
                  </table>
                </div>

              </div>
            </div>
          </div>

        </div>


      </div>

