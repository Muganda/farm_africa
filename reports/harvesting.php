<?php
if (!defined('WEB_ROOT')) {
exit;
}
$years=getYears();
$counties=getCountiest($dbConn);
$harvests=getharvestingreport($dbConn);
$colspan="8";
$rowspan="2";
?>
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div><h5><font color="">Harvesting Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4></h4></div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">
<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
<thead >
<tr>
<th></th>
<?php foreach ($years as $key => $year): ?>
<th colspan="<?php echo $colspan ?>"> <?php echo $year ?></th><?php endforeach; ?>
</tr>
<tr>
<td >County</td>
<?php foreach ($years as $key => $year): ?>
<td><b>FCR</td>
<td><b>Total Weight Harvested</td>
<td><b>Input Cycle Cost</td>
<td><b>Recurrent Cost</td>
<td><b>Total Costs</td>
<td><b>Cost per Kilo</td>
<td><b>Cost per M2</td>
<td><b>Cost per M2</td>
<?php endforeach; ?>
</tr>
</thead>
<tfoot>
<tr>
<th>County</th>
</tr>
</tfoot>
<tbody >
                   
<?php foreach ($counties as $key=> $county):?>

<tr >
<th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>

<?php foreach ($years as $key => $year): ?>
<td>
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['fcr'],2); 
}
}      
?>
</td>
<td>
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['total_fish_weight'],2); 
}
}      
?>
</td>
<td>
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['input_cycle_costs'],2);
}
}      
?>
</td>
<td > 
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['recurrent_cost'],2);
}
}      
?>
</td>
<td>
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['total_cost'],2);
}
}      
?>
</td>
<td>
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['cost_per_kilo'],2); 
}
}      
?>
</td>
<td>
<?php
foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['cost_per_m2'],2);
}
}      
?>
</td>
<td > 
<?php

foreach ($harvests as $key => $harvest) {
if ($harvest['farm_county'] == $county['id']&& $harvest['stocking_year'] == $year) {
echo number_format((float)$harvest['revenue_per_kilo'],2);
}
}      
?>
</td>

<?php endforeach; ?>


</tr>
<?php endforeach; ?>
                  
</tbody>

</table>
</div>

</div>

</div>

</div>
</div>

</div>
</div>
</div>
</div>