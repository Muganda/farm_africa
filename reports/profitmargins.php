<?php
include("./assets/fusioncharts/fusioncharts.php"); 
if (!defined('WEB_ROOT')) {
exit;
}
$counties=getCountiest($dbConn);
$months=getMontsc();
$years=getYears();
$colspan="36";
$col="3";
$rowspan="2";

$sql = "SELECT c.year,c.month,c.farmer_id,f.farm_county,sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs) as total_cost,s.sales_year,s.sales_month,s.farmer_id,sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income) as total_sales,((sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income))-(sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs))) as revenue from  farms f left join farm_sales_record s on f.farmer_id=s.farmer_id left join farm_costs c on f.farmer_id=c.farmer_id left join counties y on y.id=f.farm_county  group by s.sales_year,s.sales_month,c.year,c.month";

  $result = dbQuery($dbConn,$sql);
  while($row = dbFetchAssoc($result)) {
      extract($row);

     $profitarray[] = array("farm_county" => $farm_county,"sales_year" => $sales_year,"sales_month" => $sales_month,"total_sales"=>$total_sales,"total_cost"=>$total_cost,"year" => $year,"month" => $month,"revenue" => $revenue);

   }
?>
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div><h5><font color="">Profit Margins Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4></h4></div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-content">
<div>
<span class="pull-right text-right"><h3 class="font-bold no-margins"></h3>
<br/></span>
</div>
<div class="m-t-sm">
<div class="row">
<div class="">
<div id="">
<?php
$strQuery = "SELECT c.year,c.month,c.farmer_id,f.farm_county,sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs) as total_cost,s.sales_year,s.sales_month,s.farmer_id,sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income) as total_sales,((sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income))-(sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs))) as revenue,y.id,y.county_name from  farms f left join farm_sales_record s on f.farmer_id=s.farmer_id left join farm_costs c on f.farmer_id=c.farmer_id left join counties y on y.id=f.farm_county  group by s.sales_year,c.year";
$result = dbQuery($dbConn,$strQuery);
if ($result) {
//`$arrData` holds the Chart Options and Data.
$arrData = array(
"chart" => array(
"xAxisName"=> "Year",
"yAxisName"=> "Amount in Kshs",
"paletteColors"=> "#1FD6D1,#1aaf5d",
"yAxisMaxValue"=> "50000",
"baseFont"=> "Open Sans",
"theme" => "elegant"

)
);

//Create an array for Parent Chart.
$arrData["data"] = array();

// Push data in array.
while ($row = mysqli_fetch_array($result)) {

array_push(

    $arrData["data"],
 array(
"label" => $row["sales_year"],
"value" => $row["total_sales"],
"link" => "newchart-json-" . $row["sales_year"]
),array(
"label" => $row["year"],
"value" => $row["total_cost"],
"link" => "newchart-json-" . $row["year"]
)

);

}

// echo"<pre>";
// print_r($arrData);
// echo"</pre>";
// exit();

//Data for Linked Chart will start from here, SQL query from quarterly_sales table 
$year = 1970;
$strQuarterly = "SELECT c.year,c.month,c.farmer_id,f.farm_county,sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs) as total_cost,s.sales_year,s.sales_month,s.farmer_id,sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income) as total_sales,((sum(s.whole_tilapia_total_income+s.va_tilapia_total_income+s.whole_catfish_total_income+s.va_catfish_total_income))-(sum(c.total_tilapia_fingerling_cost+c.total_catfish_fingerling_cost+(c.feeds_costs*c.feeds_kgs_bought)+c.fertilizer_cost+c.lime_cost+c.manila_twine_cost+c.fishing_net_cost+c.extension_services_cost+c.water_costs))) as revenue,y.id,y.county_name from  farms f left join farm_sales_record s on f.farmer_id=s.farmer_id left join farm_costs c on f.farmer_id=c.farmer_id left join counties y on y.id=f.farm_county  group by s.sales_year,c.year,f.farm_county";
$resultQuarterly = dbQuery($dbConn,$strQuarterly);
//$resultQuarterly = $dbhandle->query($strQuarterly) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");

//If the query returns a valid response, preparing the JSON array.
if ($resultQuarterly) {
$arrData["linkeddata"] = array(); //"linkeddata" is responsible for feeding data and chart options to child charts.
$arrData["linkeddata2"] = array();
$i = 0;
if ($resultQuarterly) {
while ($row = mysqli_fetch_array($resultQuarterly)) {
$year = $row['sales_year'];
if (isset($arrData["linkeddata"][$i-1]) && $arrData["linkeddata"][$i-1]["id"] == $year) {
array_push($arrData["linkeddata"][$i-1]["linkedchart"]["data"], array(
"label" => $row["county_name"],
"value" => $row["total_sales"],
"value" => $row["total_cost"]
));
} else {
array_push($arrData["linkeddata"], array(
"id" => "$year",
"linkedchart" => array(
    "chart" => array(
        "caption" => "$year",
        "xAxisName"=> "Counties",
        "yAxisName"=> "Amount in Kshs",
        "paletteColors"=> "#1FD6D1",
        "baseFont"=> "Open Sans",
        "theme" => "elegant"
    ),
    "data" => array(
        array(
            "label" => $row["county_name"],
            "value" => $row["total_sales"],
            "value" => $row["total_cost"]
        )
    )
)
));

$i++;
}
}
}


$jsonEncodedData = json_encode($arrData, JSON_PRETTY_PRINT);

$columnChart = new FusionCharts("column2d", "myFirstChart" , "100%", "500", "linked-chart", "json", "$jsonEncodedData"); 

$columnChart->render();    //Render Method
mysqli_close($dbConn);   
//$dbhandle->close(); //Closing DB Connection

}
}
?> 

<!-- DOM element for Chart -->
<?php echo "<script type=\"text/javascript\" >
FusionCharts.ready(function () {
FusionCharts('myFirstChart').configureLink({     
overlayButton: {            
message: 'Back',
padding: '13',
fontSize: '16',
fontColor: '#F7F3E7',
bold: '0',
bgColor: '#22252A',           
borderColor: '#D5555C'         }     });
});
</script>" 
?>
<div id="linked-chart">

</div>

</div>
</div>

</div>

</div>
</div>
</div>
</div>



</div>

</div>

</div>

</div>
</div>
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: 5px;">
<div>
<div class="col-sm-3"><h5><font color="">Profit Margins Report
  </font></h5><h4></h4></div>
  
</div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">
<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
<thead >
<tr>
<th></th>
<?php foreach ($years as $key => $year): ?>
<th colspan="<?php echo $colspan ?>"> <?php echo $year ?></th><?php endforeach; ?>
</tr>
<tr>
<th></th>
<?php foreach ($years as $key => $year): ?>
<?php foreach ($months as $key => $month): ?>
<td colspan="<?php echo $col ?>"> <?php echo $month ?></td>
<?php endforeach; ?>
<?php endforeach; ?>
</tr>
<tr>
<td >County</td>
<?php foreach ($years as $key => $year): ?>
<?php foreach ($months as $key => $month): ?>
<td>Revenue</td>
<td>Cost</td>
<td>Profit/Loss</td>
<?php endforeach; ?>
<?php endforeach; ?>
</tr>

</thead>
<tfoot>
<tr>
<th>County</th>
</tr>
</tfoot>
<tbody >
<?php foreach ($counties as $key=> $county):?>
<tr >
<th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>
<?php foreach ($years as $key => $year): ?>
<?php foreach ($months as $key => $month): ?>
<td>
<?php
foreach ($profitarray as $key => $profit) {
if ($profit['farm_county'] == $county['id']&& $profit['sales_year'] == $year && $profit['sales_month'] == $month) {
$amount=$profit['total_sales'];
if (empty($amount)) {
    echo "0.00";
} else {
    echo $amount;
}
}
}      
?>
</td>
<td>
<?php
foreach ($profitarray as $key => $profit) {
if ($profit['farm_county'] == $county['id']&& $profit['year'] == $year && $profit['month'] == $month) {
$amount=$profit['total_cost'];
if (empty($amount)) {
    echo "0.00";
} else {
    echo $amount;
}
}
}      
?>
</td>
<td>
<?php
foreach ($profitarray as $key => $profit) {
if ($profit['farm_county'] == $county['id']&& $profit['year'] == $year && $profit['month'] == $month) {

$amount=$profit['revenue'];
if (empty($amount)) {
    echo "0.00";
} else {
    echo $amount;
}
}
}      
?>
</td>
<?php endforeach; ?>
<?php endforeach; ?>


</tr>
<?php endforeach; ?>
                  
</tbody>

</table>
</div>

</div>

</div>

</div>
</div>

</div>
</div>
</div>
</div>