<?php
require_once 'farmer/profiletab.php';
if (!defined('WEB_ROOT')) {
exit;
}
if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
  $f_id = (int)$_GET['id'];
} else {
  header('Location: ../index.php');
}
$months=getMontsc();
$fid = "SELECT farmer_id as pfarmer_id  FROM farms_owners where f_id='$f_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
  extract($row);

  }
$fid = "SELECT pond_number  FROM farms_ponds where farmer_id='$pfarmer_id'";
$result = dbQuery($dbConn,$fid);
while($row = dbFetchAssoc($result)) {
extract($row);
$ponds[]=array("pond_number"=>$pond_number);
}
$sql = "SELECT s.farmer_id as farmer,s.sales_year,s.pond_number,s.sales_month,(s.whole_tilapia_fish_sold+s.value_added_tilapia_kgs_sold) as tilapia_kg_sold,(s.whole_catfish_fish_sold+s.value_added_catfish_kgs_sold) as catfish_kg_sold,((s.whole_tilapia_fish_sold*s.whole_tilapia_fish_avg_price_kg)+(s.value_added_tilapia_kgs_sold*s.tilapia_value_added_avg_price_kg)) as tilapia_revenue,((s.whole_catfish_fish_sold*s.whole_catfish_fish_avg_price_kg)+(s.value_added_catfish_kgs_sold*s.catfish_value_added_avg_price_kg)) as catfish_revenue from  farm_sales s join farms_ponds p on p.farmer_id=s.farmer_id  where s.farmer_id='$pfarmer_id' and s.pond_number=p.id group by p.pond_number,s.sales_year,s.sales_month";
  $result = dbQuery($dbConn,$sql);
  while($row = dbFetchAssoc($result)) {
      extract($row);

     $salesrray[] = array("farmer" => $farmer, "sales_year" => $sales_year, "sales_month"=> $sales_month, "pond_number"=>$pond_number, "tilapia_kg_sold"=>$tilapia_kg_sold,"catfish_kg_sold"=>$catfish_kg_sold,"tilapia_revenue"=>$tilapia_revenue,"catfish_revenue"=>$catfish_revenue);

   }

// echo"<pre>";
// print_r($costsarray);
// echo"</pre>";
// exit();

$colspan="4";
$rowspan="2";
?>

<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: 5px;">
<div>
<div class="col-sm-3" style="margin-top: -15px;"><h5><font color="">Sales
  </font></h5><h4></h4></div>
  <form action= "" method="post">
  <div class="col-sm-2">
  <select class="form-control input-sm" name="year" style="margin-top: -15px;" >
  <option>Filter by Year</option>
    <?php getYear($dbConn);?>
    </select>
  </div>
  
  
  <div class="col-sm-2"><input name="submit" id="submit" type="submit" value="Filter" class="btn btn-primary" style="margin-top: -15px;" /></div>
</form>
</div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">
<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
<thead >
<tr>
<th></th>
<th></th>
<?php foreach ($months as $key => $month): ?>
<th colspan="<?php echo $colspan ?>"> <?php echo $month ?></th><?php endforeach; ?>
</tr>
<tr>
<td >Pond Name</td>
<td >Year</td>
<?php foreach ($months as $key => $month): ?>
<td >Tilapia Sold(Kgs)</td>
<td >Tilapia Revenue</td>
<td >Catfish Sold(Kgs)</td>
<td >Catfish Revenue</td>
<?php endforeach; ?>
</tr>
</thead>
<tfoot>
<tr>
<th>Pond Name</th>
</tr>
</tfoot>
<tbody >
<?php foreach ($ponds as $key=> $pond):?>
<tr >
<th value="<?php echo $pond["pond_number"]?>"><?php echo $pond['pond_number'] ?></th>
<th value="<?php echo $pond["sales_year"]?>"><?php echo $pond['sales_year'] ?></th>

<?php foreach ($months as $key => $month): ?>
<td>
<?php
foreach ($salesrray as $key => $sales) {
if ($sales['pond_number'] == $pond['pond_number']&& $sales['cmonth'] == $month) {
echo number_format((float)$sales['tilapia_kg_sold']);
}
}
?>
</td>
<td>
<?php
foreach ($salesrray as $key => $sales) {
if ($sales['pond_number'] == $pond['pond_number']&& $sales['cmonth'] == $month) {
echo number_format((float)$sales['tilapia_revenue']);
}
}
?>
</td>
<td>
<?php
foreach ($salesrray as $key => $sales) {
if ($sales['pond_number'] == $pond['pond_number']&& $sales['cmonth'] == $month) {
echo number_format((float)$sales['catfish_kg_sold'],2);
}
}
?>
</td>
<td > 
<?php
foreach ($salesrray as $key => $sales) {
if ($sales['pond_number'] == $pond['pond_number']&& $sales['cmonth'] == $month) {
echo number_format((float)$sales['catfish_revenue']);
}
}
?>
</td>

<?php endforeach; ?>


</tr>
<?php endforeach; ?>
                  
</tbody>

</table>
</div>

</div>

</div>

</div>
</div>

</div>
</div>
</div>
</div>