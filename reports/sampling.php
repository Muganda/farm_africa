<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

$sql = "SELECT p.sample_date,p.farmer_id,f.farmer_id,((sum(p.tot_tilapia_sample_weight)+sum(p.tot_catfish_sample_weight))/(sum(p.no_of_tilapia_sampled)+sum(p.no_of_catfish_sampled))) as avg_weight,(p.sample_date-p.stocking_date) as sample_period,
(((sum(p.tot_tilapia_sample_weight)+sum(p.tot_catfish_sample_weight))/(sum(p.no_of_tilapia_sampled)+sum(p.no_of_catfish_sampled)))*(sum(p.tilapia_fingerling_stocked)+sum(p.catfish_fingerling_stocked))) as total_weight_gain,
 ((((sum(p.tot_tilapia_sample_weight)+sum(p.tot_catfish_sample_weight))/(sum(p.no_of_tilapia_sampled)+sum(p.no_of_catfish_sampled)))*(sum(p.tilapia_fingerling_stocked)+sum(p.catfish_fingerling_stocked)))/sum(p.pond_area)) as productivity,
 c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id group by p.farmer_id,p.sample_date";
$result = dbQuery($dbConn,$sql);
?>
    <div class="row" >
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title" style="margin-top: -10px;">
          <div><h5><font color="">Sampling Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4><?php echo $errorMessage; ?></h4></div>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
              <thead>
                  <tr>
                   <td><b>County</td>
                   <td><b>Farmer ID</td>
                   <td><b>Sampling Date</td>
                   <td><b>Average weight of fish</td>
                   <td><b>Sample Period</td>
                   <td><b>Total Weight Gain</td>
                   <td><b>Productivity</td>
                  </tr>
                  
                </thead>
                <tfoot>
                  <tr>
                   <th>County</th>
                   <th>Farmer ID</th>
                   <th></th>
                   <th></th>
                   <th></th>
                   <th></th>
                   <th></th>
                  </tr>
                  
                </tfoot>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>">
                   <td><?php echo $county_name; ?></td>
                   <td><?php echo $farmer_id; ?></td>
                   <td><?php echo $sample_date; ?></td>
                   <td><?php echo number_format((float)$avg_weight,2); ?></td>
                   <td><?php echo $sample_period; ?></td>
                   <td><?php echo number_format((float)$total_weight_gain,2); ?></td>
                   <td><?php echo number_format((float)$productivity,2); ?></td>
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
                  </table>
                  </div>
                  
                </div>

              </div>

            </div>
          </div>

        </div>
        </div>
    </div>
</div>