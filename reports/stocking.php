<?php
include("./assets/fusioncharts/fusioncharts.php"); 
if (!defined('WEB_ROOT')) {
exit;
}
$years=getYears();
$counties=getCountiest($dbConn);
$fishstocks=getffishstockingdensity($dbConn);
$colspan="4";
$rowspan="2";
?>
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div><h5><font color="">Stocking Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4></h4></div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-content">
<div>
<span class="pull-right text-right"><h3 class="font-bold no-margins"></h3>
<br/></span>
</div>
<div class="m-t-sm">
<div class="row">
<div class="">
<div id="">
<?php
$strQuery = "SELECT p.farmer_id,f.farmer_id,p.stocking_year,f.farm_county,sum(p.tilapia_fingerling_stocked)as totaltilapia,sum(p.catfish_fingerling_stocked)as totalcatfish,p.pond_area,(sum(p.tilapia_fingerling_stocked)/sum(p.pond_area)) as tilapia_stocking_density,(sum(p.catfish_fingerling_stocked)/sum(p.pond_area)) as catfish_stocking_density,c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id  group by p.stocking_year";
$result = dbQuery($dbConn,$strQuery);
if ($result) {
//`$arrData` holds the Chart Options and Data.
$arrData = array(
"chart" => array(
"xAxisName"=> "Year",
"yAxisName"=> "Quantity of Fish in Pieces",
"paletteColors"=> "#1FD6D1,#1aaf5d",
"yAxisMaxValue"=> "50000",
"baseFont"=> "Open Sans",
"theme" => "elegant"

)
);

//Create an array for Parent Chart.
$arrData["data"] = array();

// Push data in array.
while ($row = mysqli_fetch_array($result)) {

array_push(

    $arrData["data"],
 array(
"label" => $row["stocking_year"],
"value" => $row["totaltilapia"],
"link" => "newchart-json-" . $row["stocking_year"]
),array(
"label" => $row["stocking_year"],
"value" => $row["totalcatfish"],
"link" => "newchart-json-" . $row["stocking_year"]
)

);

}

// echo"<pre>";
// print_r($arrData);
// echo"</pre>";
// exit();

//Data for Linked Chart will start from here, SQL query from quarterly_sales table 
$year = 1970;
$strQuarterly = "SELECT p.farmer_id,f.farmer_id,p.stocking_year,f.farm_county,sum(p.tilapia_fingerling_stocked)as totaltilapia,sum(p.catfish_fingerling_stocked)as totalcatfish,p.pond_area,(sum(p.tilapia_fingerling_stocked)/sum(p.pond_area)) as tilapia_stocking_density,(sum(p.catfish_fingerling_stocked)/sum(p.pond_area)) as catfish_stocking_density,c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id  group by p.stocking_year,f.farm_county";
$resultQuarterly = dbQuery($dbConn,$strQuarterly);
//$resultQuarterly = $dbhandle->query($strQuarterly) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");

//If the query returns a valid response, preparing the JSON array.
if ($resultQuarterly) {
$arrData["linkeddata"] = array(); //"linkeddata" is responsible for feeding data and chart options to child charts.
$arrData["linkeddata2"] = array();
$i = 0;
if ($resultQuarterly) {
while ($row = mysqli_fetch_array($resultQuarterly)) {
$year = $row['stocking_year'];
if (isset($arrData["linkeddata"][$i-1]) && $arrData["linkeddata"][$i-1]["id"] == $year) {
array_push($arrData["linkeddata"][$i-1]["linkedchart"]["data"], array(
"label" => $row["county_name"],
"value" => $row["totaltilapia"],
"value" => $row["totalcatfish"]
));
} else {
array_push($arrData["linkeddata"], array(
"id" => "$year",
"linkedchart" => array(
    "chart" => array(
        "caption" => "$year",
        "xAxisName"=> "Counties",
        "yAxisName"=> "Quantity of Fish in Pieces",
        "paletteColors"=> "#1FD6D1",
        "baseFont"=> "Open Sans",
        "theme" => "elegant"
    ),
    "data" => array(
        array(
            "label" => $row["county_name"],
            "value" => $row["totaltilapia"],
            "value" => $row["totalcatfish"]
        )
    )
)
));

$i++;
}
}
}


$jsonEncodedData = json_encode($arrData, JSON_PRETTY_PRINT);

$columnChart = new FusionCharts("column2d", "myFirstChart" , "100%", "500", "linked-chart", "json", "$jsonEncodedData"); 

$columnChart->render();    //Render Method
mysqli_close($dbConn);   
//$dbhandle->close(); //Closing DB Connection

}
}
?> 

<!-- DOM element for Chart -->
<?php echo "<script type=\"text/javascript\" >
FusionCharts.ready(function () {
FusionCharts('myFirstChart').configureLink({     
overlayButton: {            
message: 'Back',
padding: '13',
fontSize: '16',
fontColor: '#F7F3E7',
bold: '0',
bgColor: '#22252A',           
borderColor: '#D5555C'         }     });
});
</script>" 
?>
<div id="linked-chart">

</div>

</div>
</div>

</div>

</div>
</div>
</div>
</div>



</div>

</div>

</div>

</div>
</div>
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -20px;">
<div><h5><font color="">Stocking &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4></h4></div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">
<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
<thead >
<tr>
<th></th>
<?php foreach ($years as $key => $year): ?>
<th colspan="<?php echo $colspan ?>"> <?php echo $year ?></th><?php endforeach; ?>
</tr>
<tr>
<td >County</td>
<?php foreach ($years as $key => $year): ?>
<td >Tilapia</td>
<td >Tilapia SD</td>
<td >Catfish</td>
<td >Catfish SD</td>
<?php endforeach; ?>
</tr>
</thead>
<tfoot>
<tr>
<th>County</th>
</tr>
</tfoot>
<tbody >
<?php foreach ($counties as $key=> $county):?>
<tr >
<th value="<?php echo $county["id"]?>"><?php echo $county['county_name'] ?></th>

<?php foreach ($years as $key => $year): ?>

<td>
<?php
foreach ($fishstocks as $key => $stock) {
if ($stock['farm_county'] == $county['id']&& $stock['stocking_year'] == $year) {
echo $stock['totaltilapia'];
}
}      
?>
</td>
<td>
<?php
foreach ($fishstocks as $key => $stock) {
if ($stock['farm_county'] == $county['id']&& $stock['stocking_year'] == $year) {
echo number_format((float)$stock['tilapia_stocking_density'],3); 
}
}      
?>
</td>
<td>
<?php
foreach ($fishstocks as $key => $stock) {
if ($stock['farm_county'] == $county['id']&& $stock['stocking_year'] == $year) {
echo $stock['totalcatfish'];
}
}      
?>
</td>
<td > 
<?php

foreach ($fishstocks as $key => $stock) {
if ($stock['farm_county'] == $county['id']&& $stock['stocking_year'] == $year) {
echo number_format((float)$stock['catfish_stocking_density'],3);
}
}      
?>
</td>

<?php endforeach; ?>


</tr>
<?php endforeach; ?>
                  
</tbody>

</table>
</div>

</div>

</div>

</div>
</div>

</div>
</div>
</div>
</div>