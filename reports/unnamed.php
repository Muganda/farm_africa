<?php
if (!defined('WEB_ROOT')) {
  exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

//stocking density= fish Stocked/Size of pond
$sql = "SELECT p.farmer_id,f.farmer_id,cy.year,cy.fingerlings_source,cy.main_feeds,cy.feeds_source,c.id,c.county_name from  farms_ponds p join farms f on p.farmer_id=f.farmer_id left join counties c on f.farm_county=c.id left join farms_production_cycles cy on p.farmer_id=cy.farmer_id group by p.farmer_id,cy.year";
$result = dbQuery($dbConn,$sql);
?>
    <div class="row" >
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title" style="margin-top: -10px;">
          <div><h5><font color="">Unnamed Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4><?php echo $errorMessage; ?></h4></div>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link hidden">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered table-sm"  style="width: 100%;">
              <thead>
                  <tr>
                   <td><b>County</td>
                   <td><b>Farmer ID</td>
                   <td><b>Year</td>
                   <td><b>Fingerlings Supplier</td>
                   <td><b>Main Type of Feed</td>
                   <td><b>Feeds Supplier</td>
                  </tr>
                  
                </thead>
                <tfoot>
                  <tr>
                   <th>County</th>
                   <th>Farmer ID</th>
                   <th>Year</th>
                   <th></th>
                   <th></th>
                   <th></th>
                  </tr>
                  
                </tfoot>
            <tbody >
              <?php
              while($row = dbFetchAssoc($result)) {
                extract($row);

                
                if ($i%2) {
                  $class = 'row1';
                } else {
                  $class = 'row2';
                }
               ?>
              <tr class="<?php echo $class; ?>">
                   <td><?php echo $county_name; ?></td>
                   <td><?php echo $farmer_id; ?></td> 
                   <td><?php echo $year; ?></td>
                   <td><?php echo $fingerlings_source; ?></td>
                   <td><?php echo $main_feeds; ?></td>
                   <td><?php echo $feeds_source; ?></td>
              </tr>
              
          
            <?php
          } // end while

          ?>
          </tbody>
                  
                  </table>
                  </div>
                  
                </div>

              </div>

            </div>
          </div>

        </div>
        </div>
    </div>
</div>