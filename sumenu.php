<nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu" >
          <li class="nav-header">
            <div class="dropdown profile-element"> <span>
                <img alt="image" class="img-circle" src="assets/img/avatar.jpg" />
                  </span>
              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo user($dbConn);?>
                  
                </strong>
                </span> <span class="text-muted text-xs block"><?php echo userrole($dbConn);?> <b class="caret"></b></span> </span>
              </a>
              <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="view.php?v=Userprofile&id=<?php echo userid($dbConn);?>">Profile</a></li>
                <!--
                <li><a href="contacts.html">Contacts</a></li>
                <li><a href="mailbox.html">Mailbox</a></li> -->
                <li class="divider"></li>
                <li><a href="library/logout.php">Logout</a></li>
              </ul>
            </div>
            <div class="logo-element">
              FA+
            </div>
          </li>
          <li class="active">
            <a href="index.php"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
          </li>
          <li>
            <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Farm Profiles</span>  <span class="pull-right label label-primary hidden">SPECIAL</span> <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="view.php?v=Farmer">Farmer Profiles</a></li>
              <li><a href="view.php?v=Farm">Farm Summary</a></li>
              <li><a href="view.php?v=Operations">Farm Operations</a></li>
              <li><a href="view.php?v=test3">Test</a></li>
              <li>
            <a href="#">Farm Costs<span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
              <li><a href="view.php?v=Costs">Operating costs</a></li>
                <li><a href="view.php?v=Recurentcosts">Recurrent Cost</a></li>
                <li><a href="view.php?v=assets">Assets</a></li>
                <li><a href="view.php?v=financialcosts">Financial Costs</a></li>
            </ul>
          </li>
              
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">SMS Messages </span><span class="label label-warning pull-right">0/0</span> </a>
            <ul class="nav nav-second-level">
              <li><a href="#">Inbox</a></li>
              <li><a href="#">Message view</a></li>
              <li><a href="#">Compose Message</a></li>
              <li><a href="#">Surveys & Polls</a></li>
              <li><a href="#">Group Management</a></li>
              <li><a href="#">Contact Management</a></li>
              <li><a href="#">SMS templates</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Training</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="view.php?v=Modules">Training Modules</a></li>
              <li><a href="view.php?v=Training">Training Calendar</a></li>
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Reports</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="view.php?v=StockingReport">Stocking</a></li>
              <!-- <li><a href="view.php?v=SamplingReport">Sampling</a></li>
              <li><a href="view.php?v=HarvestingReport">Harvesting</a></li>
              <li><a href="view.php?v=Incomplete">Pending</a></li> -->
              <li><a href="view.php?v=Costreport">Cost</a></li>
              <li><a href="view.php?v=sales">Sales</a></li>
              <li><a href="view.php?v=profitmargins">Profit Margins</a></li>
              <!-- <li><a href="view.php?v=PondsInformation">Pond Information</a></li>
              <li><a href="view.php?v=HarvestReport">Farm Harvests</a></li>
              <li><a href="#">Farm Feeds</a></li>
              <li><a href="#">Farmer Training</a></li>
              <li><a href="#">Product Cycle Report</a></li> -->
            </ul>
          </li>
          <li>
            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="#">User Levels</a></li>
              <li><a href="view.php?v=USER">Users</a></li>
              <li><a href="view.php?v=Userprofile&id=<?php echo userid($dbConn);?>">Profile</a></li>
              <li><a href="library/logout.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>