<?php

if (!defined('WEB_ROOT')) {
  exit;
}
$self = WEB_ROOT . 'index.php';
//checkTimeout();
checkUser();

?>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $pageTitle; ?></title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" />
  <script src="<?php echo WEB_ROOT;?>assets/fusioncharts/js/fusioncharts.js"></script> 
  <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/fusioncharts/js/elegant.js"></script>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>assets/css/bootstrap-multiselect.css" type="text/css">
  <link href="<?php echo WEB_ROOT;?>assets/css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/bootstrap-multiselect.js"></script>
  <script src="<?php echo WEB_ROOT;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
  <script src="<?php echo WEB_ROOT;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?php echo WEB_ROOT;?>assets/js/inspinia.js"></script>
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>assets/css/style.css" />
  <script src="//datatables.net/download/build/nightly/jquery.dataTables.js"></script>
  <link href="//datatables.net/download/build/nightly/jquery.dataTables.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.4/css/fixedColumns.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/modules/exporting.js"></script>
  <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon" />
  <script language="JavaScript" type="text/javascript" src="<?php echo WEB_ROOT;?>library/common.js"></script>

    <?php
$n = count($script);
for ($i = 0; $i < $n; $i++) {
  if ($script[$i] != '') {
    echo '<script language="JavaScript" type="text/javascript" src="' . WEB_ROOT. 'library/' . $script[$i]. '"></script>';
  }
}
?>

  <script>
    $(function () {
                $('#datetimepicker1').datetimepicker();
            });

    function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    }
    else document.getElementById('ifYes').style.visibility = 'hidden';

    };
    function businessplan() {
    if (document.getElementById('yes').checked) {
        document.getElementById('iftrue').style.visibility = 'visible';
    }
    else document.getElementById('iftrue').style.visibility = 'hidden';

    };
    

  </script>

</head>
<body  class="pace-done">
<div id="wrapper" style="">
  <?php
  if (userroleid($dbConn)=='1') {
    include 'sumenu.php';
  }
  elseif (userroleid($dbConn)=='2') {
    include 'admin_menu.php';
  }
  elseif (userroleid($dbConn)=='3') {
    include 'fa_menu.php';
  }
  elseif (userroleid($dbConn)=='4') {
    include 'com_menu.php';
  }
  else
  {
    include 'farmer_menu.php';
  }
  ?>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <nav class="navbar white-bg" role="navigation" style="margin-bottom: 0; height: 5px">
          <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom hidden" method="post" action="search_results.html">
              <div class="form-group">
                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
              </div>
            </form>
          </div>
          <ul class="nav navbar-top-links navbar-right">
            <li>
              <span class="m-r-sm text-muted welcome-message">Welcome to Farm Africa </span>
            </li>
            <li>
              <a href="library/logout.php">
                <i class="fa fa-sign-out"></i> Log out
              </a>
            </li>
          </ul>

        </nav>
        <div class="container-fluid content">
         <nav> </nav>
         <?php require_once $content; ?>
        </div>
      </div>

      <nav class="navbar navbar-default navbar-fixed-bottom">
      <div class="footer">
        <div align="center">
          <strong>Copyright</strong> Farm Africa &copy; <?php echo date('Y');?>
        </div>
      </div>
      </nav>
    </div>
  </div>   
</body>
</html>
