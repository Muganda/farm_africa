<?php
if (!defined('WEB_ROOT')) {
  exit;
}


$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT id, type FROM farmer_reg_type";
$result = dbQuery($dbConn,$sql);
?>
<h2 class="catHead">Add Training Module</h2>
<div class="prepend-1 span-12">
<p class="errorMessage"><?php echo $errorMessage; ?></p>
<div class="col-md-9">
<table class="table table-striped table-bordered ">
         <thead>
                
                <th width="15%">Module Name</th>
                <th width="15%">Trainer Name</th>
                <th width="15%">Number of people invited</th>
                <th width="15%">Training Day</th>
                <th width="15%">Training Venue</th>
                <th width="15%"></th>
          </thead>
        </table>
      </div>
      <div class="col-sm-9">
        <form action="<?php echo WEB_ROOT; ?>training/processTraining.php?action=add" method="post" enctype="multipart/form-data" name="frmAddModule" id="frmAddModule">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
          <td width="15%"><input type="text" class="form-control" name="training_module[]"  id="training_module" value="" required="" /></td>
          <td width="15%"><input type="text" class="form-control" name="trainer[]" id="trainer" value="" required="" /></td>
          <td width="15%"><input type="text" class="form-control" name="no_of_ppl_invited[]"  id="no_of_ppl_invited" value="" /></td>
          <td width="15%"><input type="text" class="date form-control" name="training_date[]"  id="training_date" value="" required="" /></td>
          <td width="15%"><input type="text" class="form-control" name="training_venue[]"  id="training_venue" value="" required="" /></td>
          <td width="7.5%"><button class="btn btn-success" id="AddRow" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
          <td width="7.5%"><button class="btn btn-danger remove" id="deleteRow" type="button"><i class="glyphicon glyphicon-remove"></i></button></td>
        </div>
      </tr>
      </tbody>
      </table>
     
    </div>
    <p align="left">
      <button class="btn btn-success" name="submit" id="submit" type="submit" ><i class="glyphicon glyphicon-save"></i></button> 
      <button class="btn " name="btnCancel" id="btnCancel" type="button" onClick="window.location.href='view.php?v=Modules';"><i class="glyphicon glyphicon-arrow-left"></i></button>
    </p>
  </form>

</div>
    
    
   
    
</div>
