<?php
if (!defined('WEB_ROOT')) {
  exit;
}


 $sql = "SELECT training_modules.*, users.name from training_modules left join users on training_modules.submitted_by = users.id";

 $result = dbQuery($dbConn,$sql);

// var_dump($result);

?> 

<div class="prepend-1 span-17">
  <h2 class="catHead col-md-12">Training Modules</h2>

<form action="processFarmerCosts.php?action=add" method="post"  name="frmListUser" id="frmListUser">
<div class="col-md-12">
<div class="table-responsive">
<table class="table table-striped table-bordered">
<thead>
  <tr>
   <td><b>ID</td>
   <td><b>Training Module</td>
    <td><b>Training Day</td>
    <td><b>Trainer</td>
    <td><b>Venue</td>
    <td><b>Region</td>
    <td><b>Invites Sent</td>
   <!-- <td><b>Date Created</td> -->
   <td><b>Created By</td>
  <!-- <td><b>Date Modified</td> -->
   <td><b>Delete</td>
  </tr>
</thead>
<tbody>
<?php

while($row = dbFetchAssoc($result)) {
  extract($row);
  
  if ($i%2) {
    $class = 'row1';
  } else {
    $class = 'row2';
  }
  // echo '<pre>';
  // var_dump($row);
  // echo '</pre>';
?>
  <tr class="<?php echo $class; ?>"> 
   <td><a href="javascript:editfarmsampling(<?php echo $id; ?>);"><?php echo $id; ?></a></td>
   <td><?php echo $training_module; ?></td>
   <td><?php echo $training_date; ?></td>
   <td><?php echo $trainer_name; ?></td>
   <td><?php echo $training_venue; ?></td>
   <td><?php echo $region; ?></td>
   <td><?php echo $no_of_ppl_invited; ?></td>
   <!-- <td><?php $date=date_create($date_created);
    echo date_format($date,"Y/m/d");?></td> -->
   <td><?php echo $name; ?></td>
   <!-- <td><?php $date=date_create($date_modified);
    echo date_format($date,"Y/m/d H:i");?></td> -->
   <td align="center"><a href="javascript:delete(<?php echo $id; ?>);">Delete</a></td>
  </tr>
<?php

} // end while

?>
  <tr> 
   <td colspan="14">&nbsp;#</td>
  </tr>
   <td colspan="14" align="right"><input name="btnAddUser" type="button" id="popup" value="Add Training Modules (+)" class="button" onClick="div_show()"></td>
 </tbody>
</table>
</div>
</form>
</div>

<body id="body" style="overflow:hidden;">
<div id="abc">
<!-- Popup Div Starts Here -->
<div class="col-sm-11">
  <div id="popupContact">
<div class="col-sm-12">
<table class="table table-striped table-sm" style="margin-top:40px" >
         <thead>
                
                <th width="20%">Module Name</th>
                <th width="10%">Name of trainer</th>
                <th width="15%">Number of people invited</th>
                <th width="20%">Training Day</th>
                <th width="15%">Training Venue</th>

                <th width="30%"><button class="btn btn-default btn-circle" id="close" type="button" onclick ="div_hide()"><i class="glyphicon glyphicon-remove"></i></button></th>
          </thead>

</table>
      
        <form action="<?php echo WEB_ROOT; ?>training/processTraining.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
    <div class="rowdata row1">
      <table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
        <tbody>
        <tr>
         <div class="form-group row">
          
          <td width="15%"><input type="text" class="form-control" name="training_module[]"  id="training_module" value="" required="" /></td>
          <td width="10%"><textarea type="text" class="form-control" name="trainer[]" id="trainer" value="" required="" /></textarea> </td>
          <td width="15%"><input type="text" class="form-control" name="no_of_ppl_invited[]"  id="no_of_ppl_invited" value="" /></td>
          <td width="15%"><input type="text" class="date form-control" name="training_date[]"  id="training_date" value="" required="" /></td>
          <td width="15%"><input type="text" class="form-control" name="training_venue[]"  id="training_venue" value="" required="" /></td>
          <td width="7.5%"><button class="btn btn-success" id="AddRow" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
          <td width="7.5%"><button class="btn btn-danger remove" id="deleteRow" type="button"><i class="glyphicon glyphicon-remove"></i></button></td>
        </div>
      </tr>
      </tbody>
      </table>

    </div>
    <span >
      <button class="btn btn-success" name="submit" id="submit" type="submit" ><i class="glyphicon glyphicon-save"></i></button> 
    </span>
  </form>

</div>
    
    
   
    
</div>
</div>
<!-- Popup Div Ends Here -->
</div>
<!-- Display Popup Button -->

</body>