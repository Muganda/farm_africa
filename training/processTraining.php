<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
	case 'add' :
		addModule();
		break;

	case 'train' :
		addTraining();
		break;
		
	case 'modify' :
		editprofile();
		break;
		
	case 'delete' :
		deleteHardware();
		break;
    

	default :
	    // if action is not defined or unknown
		// move to main user page
		header('Location: ../index.php');
}

function addModule()
{   
   $training_module=$_POST['training_module'];
   $trainer_name=$_POST['trainer']; 
   $no_of_ppl_invited=$_POST['no_of_ppl_invited']; 
   $training_date=$_POST['training_date'];
   $training_venue=$_POST['training_venue'];
   $modified_by=$_SESSION['user_id'];
	 //   echo "<pre>";
	 // print_r($_POST);
	 // echo"</pre>";
	 // exit();
   for($i=0;$i<count($training_module);$i++)
 {
  if($training_module[$i]!="" && $trainer_name[$i]!="" && $training_date[$i]!="")
  {

	$sql = "SELECT training_module, training_date, training_venue
	        FROM training_modules
			WHERE training_module = '$training_module[$i]' and training_date = '$training_date[$i]' and training_venue='$training_venue[$i]' ";
	$result = dbQuery($dbConn,$sql);
      
	if (dbNumRows($result) == 1) {
	header('Location: ../view.php?v=Modules&error=' . urlencode('&nbsp&nbsp&nbspModule already exists&nbsp&nbsp(' .$training_module[$i]. ')&nbsp&nbsp'));	
	} else {			
		$sql= "INSERT INTO training_modules (training_module, trainer_name, no_of_ppl_invited, training_date,submitted_by,date_submitted,training_venue )
	          VALUES ('$training_module[$i]', '$trainer_name[$i]', '$no_of_ppl_invited[$i]', '$training_date[$i]','$modified_by',NOW(),'$training_venue[$i]')";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=Modules');	
	}
}
	else
  {
  	header('Location: ../view.php?v=Modules&error=' . urlencode('Error uploading records'));
  }
}

}

function addTraining()
{   
   $farmer_id=$_POST['farmer_id'];
   $training_module=$_POST['training_module'];
   $created_by=$_SESSION['user_id'];
	 // echo "<pre>";
	 // print_r($_POST);
	 // echo"</pre>";
	 // exit();
   for($i=0;$i<count($farmer_id);$i++)
 {
  if($farmer_id[$i]!="" && $training_module[$i]!="" )
  {

	$sql = "SELECT farmer_id, training_module
	        FROM farmer_training
			WHERE farmer_id = '$farmer_id[$i]' and training_module = '$training_module[$i]' ";
	$result = dbQuery($dbConn,$sql);
      
	if (dbNumRows($result) == 1) {
	header('Location: ../view.php?v=Training&error=' . urlencode('&nbsp&nbsp&nbspTraining information exists&nbsp&nbsp(' .$training_module[$i]. ')&nbsp&nbsp'));	
	} else {			
		$sql= "INSERT INTO farmer_training (farmer_id, training_module, date_created, submitted_by)
	          VALUES ('$farmer_id[$i]', '$training_module[$i]', NOW(), '$created_by')";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=Training');	
	}
}
	else
  {
  	header('Location: ../view.php?v=Training&error=' . urlencode('Error uploading records'));
  }
}

}

//Edit farmer profile

function editprofile()
{
	$farmer_id = $_POST["farmer_id"];
	$owner_name = $_POST["owner_name"];
    $registration = $_POST['type'];
	$date_registered = $_POST['date_registered'];
	$date_enrolled = $_POST['date_enrolled'];
	$age = $_POST['age'];
	$owner_email = $_POST['owner_email'];
	$owner_telephone = $_POST['owner_telephone'];
	$owner_gender = $_POST["owner_gender"];
    $occupation = $_POST['occupation'];
	$finance_source = $_POST['finance_source'];
	$finance=implode( ", ", $finance_source );
	$type_of_enterprise = $_POST['type_of_enterprise'];
	$modified_by=$_SESSION['user_id']; 
	//echo $id;
    //exit();
	//$utype = 'USER';
	//$did = (int)$_POST['did'];
	
		$sql   = "UPDATE farms_owners
		   SET  owner_name = '$owner_name',
			    registration='$registration',
			    date_registered='$date_registered',
			    age='$age',
			    date_enrolled='$date_enrolled',
			    owner_email='$owner_email',
			    owner_telephone='$owner_telephone',
			    owner_gender='$owner_gender',
			    occupation='$occupation',
			    finance_source='$finance',
			    type_of_enterprise='$type_of_enterprise',
			    modified_by='$modified_by',
			    date_modified=NOW()


				WHERE farmer_id = '$farmer_id'";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=Farmer');	
	
}

/*
	Remove a Hardware
*/
function deleteHardware()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$id = (int)$_GET['id'];
	} else {
		header('Location: index.php');
	}
	
	
	$sql = "DELETE FROM tbl_hardwares
	        WHERE id = $id";
	dbQuery($dbConn,$sql);
	
	header('Location: ../menu.php?v=HRWR');
}
?>