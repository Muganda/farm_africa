<?php
if (!defined('WEB_ROOT')) {
exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

$sql = "SELECT COUNT(DISTINCT t.farmer_id) AS  farmer,t.training_date,t.venue,t.training_module,m.id,m.training_module as module from farmer_training t join training_modules m on t.training_module = m.id where t.attendance= 'Yes' Group by t.training_module";
$result = dbQuery($dbConn,$sql);
?>
<div class="row" >
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title" style="margin-top: -10px;">
<div><h5><font color="">Farmer Profiles&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4><?php echo $errorMessage; ?></h4></div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">
<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
<thead>
<tr>
<td><b>#</td>
<td><b>Training Module</td>
<td><b>Training Day</td>
<td><b>Attendance Number</td>
<td><b>Venue</td>
<td><b>Trainer</td>
<td><b>Date Created</td>
</tr>

</thead>
<tbody >
<?php
while($row = dbFetchAssoc($result)) {
extract($row);


if ($i%2) {
$class = 'row1';
} else {
$class = 'row2';
}
?>
<tr class="<?php echo $class; ?>"> 
<td><?php echo $id; ?></td>
<td><?php echo $module; ?></td>
<td><?php echo $training_date; ?></td>
<td><?php echo $farmer; ?></td>
<td><?php echo $training_venue; ?></td>
<td><?php echo $trainer_name; ?></td>
<td><?php $date=date_create($date_created);
echo date_format($date,"Y/m/d"); ?></td>
</tr>


<?php
} // end while

?>
</tbody>

</table>
<td colspan="14" align="right"><input name="btnAddUser" type="button" id="popup" value="Add Training Information (+)" class="btn btn-default" onClick="div_show()"></td>
</div>

</div>

</div>

</div>
</div>
<body id="body" style="overflow:hidden;">
<div id="abc">
<!-- Popup Div Starts Here -->
<div class="col-sm-11">
<div id="popupContact">
<div class="col-sm-9">
<table class="table table-striped table-sm" style="margin-top:40px" >
<thead>
<th width="30%"><b>Name of Farmer</th>
<th width="30%"><b>Training Module</th>
<th width="30%"><button class="btn btn-default btn-circle" id="close" type="button" onclick ="div_hide()"><i class="glyphicon glyphicon-remove"></i></button></th>

</thead>

</table>

<form action="<?php echo WEB_ROOT; ?>training/processTraining.php?action=train" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
<div class="rowdata row1">
<table id="dataTable" class="table table-striped table-bordered table-sm" style="margin-top:-30px">
<tbody>
<tr>
<div class="form-group row">

<td width="30%"><select class="form-control" name="farmer_id[]" type="text" id="farmer_id"  required="" ><?php getfarmer_id();?></select></td>
<td width="30%"><select class="form-control" name="training_module[]" type="text" id="training_module"  required="" ><?php getmodule();?></select></td>
<td width="15%"><button class="btn btn-success" id="AddRow" type="button"><i class="glyphicon glyphicon-plus"></i></button></td>
<td width="15%"><button class="btn btn-danger remove" id="deleteRow" type="button"><i class="glyphicon glyphicon-remove"></i></button></td>
</div>
</tr>
</tbody>
</table>

</div>
<span >
<button class="btn btn-success" name="submit" id="submit" type="submit" ><i class="glyphicon glyphicon-save"></i></button> 
</span>
</form>

</div>
</div>
</div>
<!-- Popup Div Ends Here -->
</div>
<!-- Display Popup Button -->

</body>