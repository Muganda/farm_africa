<?php
if (!defined('WEB_ROOT')) {
exit;
}
$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';
?>
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<div><h5><font color="">Add user&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5><h4><?php echo $errorMessage; ?></h4></div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<form action="<?php echo WEB_ROOT; ?>user/processUser.php?action=add" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
<div class="form-group row">
<label for="usr_role_id" class="col-sm-3 col-form-label">User Role :<select class="selectpicker" data-show-subtext="true" name="usr_role_id" type="text" id="usr_role_id"  required=""><?php getUserRole($dbConn);?></select></label>
</div>
<div class="form-group row">
<label for="txtFname" class="col-sm-3 col-form-label">First Name:<input class="form-control" name="txtFname" type="text" id="txtFname" required="" ></label>
</div>
<div class="form-group row">
<label for="txtLname" class="col-sm-3 col-form-label">Last Name:<input class="form-control" name="txtLname" type="text" id="txtLname" required="" ></label>
</div>
<div class="form-group row">
<label for="txtEmail" class="col-sm-3 col-form-label">E-Mail:<input class="form-control" name="txtEmail" type="text" id="txtEmail" required="" ></label>
</div>
<p align="left"> 
<input name="btnAddUser" type="button"   class="btn btn-primary" id="btnAddUser" value="Save" onClick="checkAddUserForm()" class="box">
&nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" class="btn btn-warning"  value="Cancel" onClick="window.location.href='view.php?v=USER';" class="box">
</p>
</form>

</div>

</div>
</div>

</div>
</div>
</div>
</div>