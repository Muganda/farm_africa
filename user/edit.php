<?php
if (!defined('WEB_ROOT')) {
  exit;

}
$errorMessage = "";

$sql = "SELECT usr_role_id, usr_role_title FROM user_roles";
$result = dbQuery($dbConn,$sql);
$id =(int)$_GET["id"];
$sql_u = "SELECT u.id,u.name,u.last_name,u.user_role,u.email,u.profile_photo,u.last_date_login,u.phone_no,r.usr_role_id,r.usr_role_title from users u join user_roles r on u.user_role=r.usr_role_id where u.id = '$id'";
$result_u = dbQuery($dbConn,$sql_u);

//echo $sql_u;
?> 
<h5><font color="">Edit User</font></h5>
<div class="prepend-1 span-12">
<p align="center"><strong><font color="#660000"><?php echo $errorMessage; ?></font></strong></p>
<?php
if(dbAffectedRows() == 1){
while($d = dbFetchAssoc($result_u)){
extract($d);
?>
<form action="<?php echo WEB_ROOT; ?>user/processUser.php?action=edit" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  <div class="form-group row">
<label for="usr_role_id" class="col-sm-3 col-form-label">User Role :<select class="form-control input-sm" data-show-subtext="true" name="usr_role_id" type="text" id="usr_role_id"  required=""><option value="<?php echo $usr_role_id; ?>"><?php echo $usr_role_title; ?></option><?php getUserRole($dbConn);?></select></label>
</div>
<div class="form-group row">
<label for="txtFname" class="col-sm-3 col-form-label">First Name:<input class="form-control" name="txtFname" type="text" id="txtFname" value="<?php echo $name; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="txtLname" class="col-sm-3 col-form-label">Last Name:<input class="form-control" name="txtLname" type="text" id="txtLname" value="<?php echo $last_name; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="txtEmail" class="col-sm-3 col-form-label">E-Mail:<input class="form-control" name="txtEmail" type="text" id="txtEmail" value="<?php echo $email; ?>" required="" ></label>
</div>
<p align="left"> 
<input name="btnAddUser" type="button"   class="btn btn-primary" id="btnAddUser" value="Save" onClick="checkAddUserForm()" class="box">
<input name="btnCancel" type="button" id="btnCancel" class="btn btn-default"  value="Cancel" onClick="window.location.href='view.php?v=USER';" class="box">
    
  </p>
</form>
<?php 
}//while
}else {
?>
<p> No user found.</p>
<?php 
} 
?>
</div>