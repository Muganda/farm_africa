<?php
if (!defined('WEB_ROOT')) {
exit;
}

$farmid=$_SESSION['farmer_id'];
$editProfileerrorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';
$passerrorMessage = (isset($_GET['m']) && $_GET['m'] != '') ? $_GET['m'] : '&nbsp;';


$sql="SELECT * from farms_owners  where f_id='$farmid' ";

$result = dbQuery($dbConn,$sql);
while($row = dbFetchAssoc($result)){
extract($row);
?>
<div class="container">
<div class="row">
<div class="col-md-6 ">
<div class="panel panel-default">
<div class="panel-heading">  <h4 >My Profile</h4></div>
<div class="panel-body">

<div class="box box-info">
<div class="box-body">
<div class="col-sm-6">
<div  align="center"> <img alt="User Pic" src="assets/img/avatar.jpg" width="150px" height="150px" id="profile-image1" class="img-circle img-responsive"> 

<!-- <input id="profile-image-upload" class="hidden" type="file">
<div style="color:#999;" >click here to change profile image</div> -->
</div>

<br>

<!-- /input-group -->              
</div>
<div class="col-sm-6">
            
</div>
<div class="clearfix"></div>
<hr style="margin:5px 0 5px 0;">
<div class="clearfix"></div>
<hr style="margin:5px 0 5px 0;">


<div class="col-sm-5 col-xs-6 tital " >First Name:</div><div class="col-sm-7 col-xs-6 "><?php echo $firstname; ?></div>
<div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Last Name:</div><div class="col-sm-7"> <?php echo $lastname; ?></div>
<div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >E-Mail:</div><div class="col-sm-7"> <?php echo $owner_email; ?></div>
<div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Phone Number:</div><div class="col-sm-7"><?php echo $owner_telephone; ?></div>

<div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Last Time Login:</div><div class="col-sm-7"><?php echo $last_date_login; ?></div>

<div class="clearfix"></div>
<div class="bot-border"></div>

<!-- /.box-body -->
</div>
<!-- /.box -->

</div>


</div> 
</div>
</div>
<div class="col-sm-6 " style="margin-top: -10px;">

<div class="panel panel-default">
<div class="panel-heading">  <h4 >Edit Profile</h4><?php echo $editProfileerrorMessage; ?></div>
<div class="panel-body">

<div class="box box-info">
<div class="box-body">


<form action="<?php echo WEB_ROOT; ?>user/processUser.php?action=editfmprofile" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">
  
<div class="form-group row">
<input class="form-control" name="id" type="hidden" id="id" value="<?php echo $f_id; ?>">
<label for="txtFname" class="col-sm-5 col-form-label">First Name:<input class="form-control" name="txtFname" type="text" id="txtFname" value="<?php echo $firstname; ?>" required="" ></label>
<label for="txtLname" class="col-sm-5 col-form-label">Last Name:<input class="form-control" name="txtLname" type="text" id="txtLname" value="<?php echo $lastname; ?>" required="" ></label>
</div>
<div class="form-group row">
<label for="txtEmail" class="col-sm-5 col-form-label">E-Mail:<input class="form-control" name="txtEmail" type="text" id="txtEmail" value="<?php echo $owner_email; ?>" required="" ></label>
<label for="phone_no" class="col-sm-5 col-form-label">Phone Number:<input class="form-control" name="phone_no" type="text" id="phone_no" value="<?php echo $owner_telephone; ?>" ></label>

</div>
<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<!-- <input name="btnCancel" type="button" id="btnCancel" class="btn btn-default"  value="Cancel" onClick="window.location.href='view.php?v=USER';" class="box"> -->
    
  </p>
</form>

<!-- /.box-body -->
</div>
<!-- /.box -->

</div>


</div> 
</div>
</div>
</div>
</div>
<div class="col-md-6 ">

<div class="panel panel-default">
<div class="panel-heading">  <h4 >Change password</h4><?php echo $passerrorMessage; ?><span id="confirmMessage" class="confirmMessage"></span></div>
<div class="panel-body">

<div class="box box-info">
<div class="box-body">


<form action="<?php echo WEB_ROOT; ?>user/processUser.php?action=changefmpass" method="post" enctype="multipart/form-data" name="frmAddUser" id="frmAddUser">

<div class="form-group row">
<input class="form-control" name="id" type="hidden" id="id" value="<?php echo $id; ?>">
<label for="opassword" class="col-sm-4 col-form-label">Current Password:<input class="form-control" name="opassword" type="password" id="opassword"  required="" ></label>
<label for="npassword" class="col-sm-4 col-form-label">New Password:<input class="form-control" name="npassword" type="password" id="npassword"  required="" ></label>

<label for="cpassword" class="col-sm-4 col-form-label">Confirm Password:<input class="form-control" name="cpassword" type="password" id="cpassword"  required="" onkeyup="checkPass(); return false;"></label>

</div>
<p align="left"> 
<input name="submit" id="submit" type="submit" value="Submit" class="btn btn-primary" />
<!-- <input name="btnCancel" type="button" id="btnCancel" class="btn btn-default"  value="Cancel" onClick="window.location.href='view.php?v=USER';" class="box"> -->
    
  </p>
</form>

<!-- /.box-body -->
</div>
<!-- /.box -->

</div>


</div> 
</div>
</div>

<?php

}//while


?>

