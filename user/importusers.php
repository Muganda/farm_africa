<?php
error_reporting(-1);
require_once '../library/config.php';
require_once '../library/functions.php';
checkUser();
if(isset($_POST['import_data'])){
$modified_by=$_SESSION['user_id'];
// validate to check uploaded file is a valid csv file
$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
if(is_uploaded_file($_FILES['file']['tmp_name'])){
$csv_file = fopen($_FILES['file']['tmp_name'], 'r');

$requiredHeaders = array('FirstName','LastName','EMail','Role'); //headers we expect
$firstLine = fgets($csv_file); //get first line of csv file
//fclose($csv_file);
$foundHeaders = str_getcsv(trim($firstLine), ',', '"'); //parse to array
if ($foundHeaders !== $requiredHeaders) {
   header('Location: ../view.php?v=USER&msg=<p class="text-danger">Headers do not match</p>');
   //echo 'Headers do not match: '.implode(', ', $foundHeaders);
   die();

}
else{
$skip = 0;
while(($data = fgetcsv($csv_file)) !== FALSE){
if($skip != 0){
$data = str_replace("'", "", $data);
if ($data[3]) {
$role = getRoleid($dbConn,$data[3]);
}
$year = date("Y");
$password = md5($data[1].'#'.$year);
// Check if record exists
$sql_query = "SELECT email

FROM users 
WHERE email = '".$data[2]."' ";
$resultset = dbQuery($dbConn, $sql_query);
// if record exists update otherwise insert new record
if(mysqli_num_rows($resultset)) {
$sql_update = "UPDATE users 
set 
name='".$data[0]."',
last_name='".$data[1]."',
email='".$data[2]."',
user_role='".$role."',
modified_by='".$modified_by."',
date_modified=NOW()
WHERE email = '".$data[2]."'";
dbQuery($dbConn, $sql_update);
} else{
$mysql_insert = "INSERT INTO users 
(
name, 
last_name,
email,
user_role,
password,
date_registered,
last_date_login,
modified_by,
date_modified 
)
VALUES
(
'".$data[0]."',
'".$data[1]."',
'".$data[2]."',
'".$role."', 
'".$password."',
NOW(), 
'Never',
'".$modified_by."',
NOW()
)";
dbQuery($dbConn, $mysql_insert);
}
}
$skip ++;
}

}


fclose($csv_file);
$import_status = '<p class="text-success">Records uploaded succesfully</p>';
} else {
$import_status = '<p class="text-danger">Error  uploading records</p>';
}
} else {
$import_status = '<p class="text-danger">Invalid file used</p>';
}
}
header("Location: ../view.php?v=USER&msg=".$import_status);
?>