<?php
if (!defined('WEB_ROOT')) {
exit;
}
$errorMessage = (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : '&nbsp;';

$sql = "SELECT u.id, u.user_role, u.email, u.name,u.last_name,u.date_registered, d.usr_role_title, d.usr_role_id
FROM users u,user_roles d WHERE u.user_role = d.usr_role_id ORDER BY name";
$result = dbQuery($dbConn,$sql);
?>
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">

<div><h5><font color="">User List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></h5>
<h4><?php echo $errorMessage; ?></h4>
Download User&ensp;<a href="<?php echo WEB_ROOT; ?>csv/user.csv" >Registration</a>&ensp;CSV Document
</div>
<div class="ibox-tools">
<a class="collapse-link">
<i class="fa fa-chevron-up"></i>
</a>
<a class="close-link hidden">
<i class="fa fa-times"></i>
</a>
</div>
</div>
<div class="ibox-content">
<div class="table-responsive">
<table id="paging" class="table table-striped table-bordered table-sm"  style="width: 100%;">
<thead>
<tr>
<td><b>#</td>
<td>First Name</td>
<td>Second Name</td>
<td>E-mail</td>
<td>Role</td>
<td>Date registered</td>
<td>Edit</td>
<td>Delete</td>
</tr>

</thead>
<tbody >
<?php
while($row = dbFetchAssoc($result)) {
extract($row);
static $userno=0;
$userno++;

if ($i%2) {
$class = 'row1';
} else {
$class = 'row2';
}
?>
<tr class="<?php echo $class; ?>"> 
<td><?php echo $userno; ?></td>
<td ><?php echo ucfirst($name); ?></td>
<td ><?php echo ucfirst($last_name); ?></td>
<td ><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
<td ><?php echo $usr_role_title; ?></td>
<td ><?php echo $date_registered; ?></td>
<td ><a href="javascript:editUser(<?php echo $id; ?>);">Edit</a></td>
<td ><a  style="font-weight:normal;" href="javascript:deleteUser(<?php echo $id; ?>);">Delete</a></td>
</tr>


<?php
} // end while

?>
</tbody>
</div>
</table>
<div class="form-group row">
<label class="col-sm-6 col-form-label"><b>Bulk user Registration:</b>
<form action="<?php echo WEB_ROOT; ?>user/importusers.php" method="post" enctype="multipart/form-data" id="import_form">
<input type="file" class="btn btn-primary" name="file" style="float: left;"/>&ensp;
<input type="submit" class="btn btn-primary" name="import_data" value="Import" style="float: center;">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="btnAddUser" type="button" id="btnAddUser" value="Add New User"  class="btn btn-default" onClick="addUser()">
</form>
</label>
</div>





</div>

</div>

</div>
</div>

</div>
</div>
</div>
</div>