<?php

require_once '../library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
	case 'add' :
		addUser($dbConn);
		break;

	case 'editprofile' :
		editprofile($dbConn);
		break;

	case 'editfmprofile' :
		editfmprofile($dbConn);
		break;

	case 'changepass' :
		changepass($dbConn);
		break;
	case 'changefmpass' :
		changefmpass($dbConn);
		break;
		
	case 'edit' :
		modifyUser($dbConn);
		break;
		
	case 'delete' :
		deleteUser($dbConn);
		break;
    

	default :
	    // if action is not defined or unknown
		// move to main user page
		header('Location: ../index.php');
}

/*
function used to create single user in table tbl_users
*/
function addUser($dbConn)
{
    $usr_role_id = $_POST['usr_role_id'];
	$email = $_POST['txtEmail'];
	$fname = $_POST['txtFname'];
	$lname = $_POST['txtLname'];
	$year = date("Y");
	$password = md5($lname.'#'.$year);
	// check if the email is taken
	$sql = "SELECT email
	        FROM users
			WHERE email = '$email'";
	$result = dbQuery($dbConn,$sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: ../view.php?v=adduser&error=' . urlencode('Email already exist. Please enter another'));	
	} else {			
		$sql   = "INSERT INTO users (user_role, password, email, name,last_name,date_registered)
		          VALUES ('$usr_role_id', '$password', '$email', '$fname', '$lname', NOW() )";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=USER');	
	}
}
function editprofile($dbConn)
{   
$target_dir="profile_pics/";
$target_file=$target_dir .basename($_FILES["file"]["name"]);
$uploadOk=1;
$imageFileType =pathinfo($target_file,PATHINFO_EXTENSION);
$uid = $_POST["id"];
$email = $_POST['txtEmail'];
$fname = $_POST['txtFname'];
$lname = $_POST['txtLname'];
$phone_no = $_POST['phone_no'];
$session=$_SESSION['user_id'];
if ($uid !=$session) {
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">Permission denied</p>');
}
else{
if(!empty($_FILES['file']['name'])){


$check =getimagesize($_FILES["files"]["tmp_name"]);
if($check !== false){
$uploadOk=1;
}else{
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">File uploaded is not image</p>');
$uploadOk=0;
}
if(file_exists($target_file)){
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">Image already exists!</p>');
$uploadOk= 0;
}
if($_FILES["files"]["size"]>500000){
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">File is too Large!</p>');
$uploadOk= 0;
}
if($imageFileType !="jpg" && $imageFileType !="png" && $imageFileType !="jpeg" && $imageFileType !="gif"){
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">Invalid File!</p>');
$uploadOk= 0;
}
if ($uploadOk == 0){
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">Upload not succesful!</p>');
}


if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)){

$sql   = "UPDATE users  
SET 
name = '$fname', 
last_name = '$lname', 
email = '$email', 
phone_no = '$phone_no', 
profile_photo = '$target_file', 
date_modified = NOW()
WHERE 
id = '$uid'";

dbQuery($dbConn,$sql);

header('Location: ../view.php?v=Userprofile&id='.$session);

}
else{
header('Location: ../view.php?v=Userprofile&id='.$session.'&msg=<p class="text-danger">Upload not succesful!</p>');
}
}
else{
$sql   = "UPDATE users  
SET 
name = '$fname', 
last_name = '$lname', 
email = '$email', 
phone_no = '$phone_no',
date_modified = NOW()
WHERE 
id = '$uid'";

dbQuery($dbConn,$sql);

header('Location: ../view.php?v=Userprofile&id='.$session);

}

}


}
function editfmprofile($dbConn)
{   
$uid = $_POST["id"];
$email = $_POST['txtEmail'];
$fname = $_POST['txtFname'];
$lname = $_POST['txtLname'];
$phone_no = $_POST['phone_no'];
$session=$_SESSION['farmer_id'];
if ($uid !=$session) {
header('Location: ../view.php?v=sessionprofile&id='.$session.'&msg=<p class="text-danger">Permission denied</p>');
}
else{

$sql   = "UPDATE farms_owners  
SET 
firstname = '$fname', 
lastname = '$lname', 
owner_email = '$email', 
owner_telephone = '$phone_no',
date_modified = NOW()
WHERE 
f_id = '$uid'";

dbQuery($dbConn,$sql);

header('Location: ../view.php?v=sessionprofile&id='.$session);






}
//change password
function changepass($dbConn)
{
 	$uid = $_POST["id"];
	$opassword = md5($_POST['opassword']);
	$npassword = md5($_POST['npassword']);
	$cpassword = md5($_POST['cpassword']);
	$session=$_SESSION['user_id'];
	if ($uid !=$session) {
	header('Location: ../view.php?v=Userprofile&id='.$session.'&m=<p class="text-danger">Permission denied</p>');
	}
	else{
	$sql="select password from users where id='$session'";
	$result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)){
    extract($row);
    if($password==$opassword){
		if($npassword==$cpassword){
		 $sql   = "UPDATE users  
			SET password = '$cpassword'
				WHERE id = '$session'";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=Userprofile&id='.$session.'&m=<p class="text-success">Password updated</p>');
		}
		else{
		header('Location: ../view.php?v=Userprofile&id='.$session.'&m=<p class="text-danger">Your new and confirmation password do not match!</p>');
		}
		}
		else
		{
		header('Location: ../view.php?v=Userprofile&id='.$session.'&m=<p class="text-danger">Your current password is wrong</p>');
		}
	}
	
	
}
}
function changefmpass($dbConn)
{
 	$uid = $_POST["id"];
	$opassword = md5($_POST['opassword']);
	$npassword = md5($_POST['npassword']);
	$cpassword = md5($_POST['cpassword']);
	$session=$_SESSION['farmer_id'];
	if ($uid !=$session) {
	header('Location: ../view.php?v=sessionprofile&id='.$session.'&m=<p class="text-danger">Permission denied</p>');
	}
	else{
	$sql="select password from users where id='$session'";
	$result = dbQuery($dbConn,$sql);
    while($row = dbFetchAssoc($result)){
    extract($row);
    if($password==$opassword){
		if($npassword==$cpassword){
		 $sql   = "UPDATE farms_owners  
			SET pass = '$cpassword'
				WHERE f_id = '$session'";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=sessionprofile&id='.$session.'&m=<p class="text-success">Password updated</p>');
		}
		else{
		header('Location: ../view.php?v=sessionprofile&id='.$session.'&m=<p class="text-danger">Your new and confirmation password do not match!</p>');
		}
		}
		else
		{
		header('Location: ../view.php?v=sessionprofile&id='.$session.'&m=<p class="text-danger">Your current password is wrong</p>');
		}
	}
	
	
}
}

/*
	Modify a user, it will mdify, edit user and able to update user details
*/
function modifyUser($dbConn)
{
 	$uid = $_POST["id"];
    $userName = $_POST['txtUserName'];
	$password = $_POST['txtPassword'];
	$email = $_POST['txtEmail'];
	$fname = $_POST['txtFname'];
	$lname = $_POST['txtLname'];
	$utype = 'USER';
	$did = (int)$_POST['did'];
	
	/*
	// the password must be at least 6 characters long and is 
	// a mix of alphabet & numbers
	if(strlen($password) < 6 || !preg_match('/[a-z]/i', $password) ||
	!preg_match('/[0-9]/', $password)) {
	  //bad password
	}
	*/	
	// check if the username is taken
		$sql   = "UPDATE tbl_users  
			SET uname = '$userName', 
				pwd = '$password', 
				email = '$email', 
				fname = '$fname', 
				lname = '$lname', 
				did = $did
				WHERE uid = $uid";
	
		dbQuery($dbConn,$sql);
		header('Location: ../view.php?v=USER');	
	
}

/*
	Remove a user
*/
function deleteUser($dbConn)
{
	if (isset($_GET['userId']) && (int)$_GET['userId'] > 0) {
		$userId = (int)$_GET['userId'];
	} else {
		header('Location: index.php');
	}
	
	
	$sql = "DELETE FROM users 
	        WHERE id = $userId";
	dbQuery($dbConn,$sql);
	
	header('Location: ../menu.php?v=USER');
}
?>