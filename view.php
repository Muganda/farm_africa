<?php
require_once 'library/config.php';
require_once 'library/functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '';

switch ($view) {
	case 'USER' :
		$content 	= 'user/list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
		break;

	case 'Userprofile' :
		$content 	= 'user/profile.php';		
		$pageTitle 	= 'Farm Africa - View Users';
		break;

	case 'sessionprofile' :
		$content 	= 'user/farmer.php';		
		$pageTitle 	= 'Farm Africa - View Users';
		break;


	case 'Farm' :
		$content 	= 'farm/list.php';		
		$pageTitle 	= 'Farm Africa - View Farms';
		break;

	case 'FarmView' :
		$content 	= 'farm/farmlist_agentview.php';		
		$pageTitle 	= 'Farm Africa - View Farm';
		break;

	case 'Farmer' :
		$content 	= 'farmer/list.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'FarmerView' :
		$content 	= 'farmer/farmerlist_agentview.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'Profile' :
		$content 	= 'farmer/profile.php';		
		$pageTitle 	= 'Farm Africa - View Farmers';
		break;

	case 'farmerProfile' :
		$content 	= 'farmer/farmer_profile.php';		
		$pageTitle 	= 'Farm Africa - Farmer';
		break;
		
	case 'adduser' :
		$content 	= 'user/add.php';		
		$pageTitle 	= 'Farm Africa - Add Users';
		break;

	case 'Costs' :
		$content 	= 'farm/costs.php';		
		$pageTitle 	= 'Farm Africa - View Farm Costs';
		break;

	case 'Recurentcosts' :
		$content 	= 'farm/recurrentcost.php';		
		$pageTitle 	= 'Farm Africa - View Recurrent Costs';
		break;
		
	case 'assets' :
		$content 	= 'farm/assets.php';		
		$pageTitle 	= 'Farm Africa - View Assets';
		break;
	case 'financialcosts' :
		$content 	= 'farm/financialcosts.php';		
		$pageTitle 	= 'Farm Africa - View Financial Costs';
		break;

	case 'AgentCostsView' :
		$content 	= 'farm/agentcostview.php';		
		$pageTitle 	= 'Farm Africa - View Farm Costs';
		break;

	case 'Attendancegraph' :
		$content 	= 'farm/attendancegraph.php';		
		$pageTitle 	= 'Farm Africa - View Attendance';
		break;

	case 'Operations' :
		$content 	= 'farm/farm_operations.php';		
		$pageTitle 	= 'Farm Africa - View Farm Operations';
		break;

	case 'AgentCountyOperations' :
		$content 	= 'farm/agentview_farmoperations.php';		
		$pageTitle 	= 'Farm Africa - View Farm Operations';
		break;

    case 'Farmoperationstable' :
		$content 	= 'farm/operation.php';		
		$pageTitle 	= 'Farm Africa - View Farm Operations';
		break;

	case 'PondsInformation' :
		$content 	= 'reports/pond_info.php';		
		$pageTitle 	= 'Farm Africa - View Pond Report';
		break;

	case 'HarvestReport' :
		$content 	= 'reports/harvest_info_report.php';		
		$pageTitle 	= 'Farm Africa - View Harvest Report';
		break;

	case 'stocking' :
		$content 	= 'farm/stocking.php';		
		$pageTitle 	= 'Farm Africa - View Stockings';
		break;

	case 'Sales' :
		$content 	= 'farm/sales.php';		
		$pageTitle 	= 'Farm Africa - View Farm Sales';
		break;

	case 'sampling' :
		$content 	= 'farm/sampling.php';		
		$pageTitle 	= 'Farm Africa - View Farm Sampling';
		break;

	case 'Feed' :
		$content 	= 'farm/feeds.php';		
		$pageTitle 	= 'Farm Africa - View Farm Feed';
		break;

	case 'Harvest' :
		$content 	= 'farm/harvest.php';		
		$pageTitle 	= 'Farm Africa - View Farm Harvest';
	break;

	case 'Market' :
		$content 	= 'farm/market.php';		
		$pageTitle 	= 'Farm Africa - View Farm Market';
		break;

	case 'Modules' :
		$content 	= 'training/modules.php';		
		$pageTitle 	= 'Farm Africa - View Training Modules';
		break;

	case 'Training' :
		$content 	= 'training/training.php';		
		$pageTitle 	= 'Farm Africa - View Training Information';
		break;

	case 'profile' :
		$content 	= 'farmer/profile.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'Test' :
		$content 	= 'test/pondtest.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'Another' :
		$content 	= 'test/another.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'filter' :
		$content 	= 'test/fetch.php';		
		$pageTitle 	= 'Farm Africa - Filter';
		break;

	case 'tprofile' :
		$content 	= 'farmer/testprofile.php';		
		$pageTitle 	= 'Farm Africa - View Farmer Profile';
		break;

	case 'dashboard' :
		$content 	= 'dashboard.php';		
		$pageTitle 	= 'Farm Africa - Dashboard';
		break;

	case 'highchart' :
		$content 	= 'highcharts.php';		
		$pageTitle 	= 'Farm Africa - Chart Test';
		break;

	case 'StockingReport' :
		$content 	= 'reports/stocking.php';		
		$pageTitle 	= 'Farm Africa - Stocking Report';
		break;
	case 'Costreport' :
		$content 	= 'reports/cost.php';		
		$pageTitle 	= 'Farm Africa - Cost Report';
		break;

	case 'sales' :
		$content 	= 'reports/salesreport.php';		
		$pageTitle 	= 'Farm Africa - Sales Report';
		break;

	case 'profitmargins' :
		$content 	= 'reports/profitmargins.php';		
		$pageTitle 	= 'Farm Africa - Profit Margins Report';
		break;
    
    case 'SalesReport' :
		$content 	= 'reports/sales.php';		
		$pageTitle 	= 'Farm Africa - Sales Report';
		break;

	case 'SamplingReport' :
		$content 	= 'reports/sampling.php';		
		$pageTitle 	= 'Farm Africa - Sampling Report';
		break;

	case 'HarvestingReport' :
		$content 	= 'reports/harvesting.php';		
		$pageTitle 	= 'Farm Africa - Harvesting Report';
		break;

	case 'Incomplete' :
		$content 	= 'reports/unnamed.php';		
		$pageTitle 	= 'Farm Africa - Report';
		break;

	case 'test3' :
		$content 	= 'reports/test.php';		
		$pageTitle 	= 'Farm Africa - Report';
		break;

	default :
		$content 	= 'list.php';		
		$pageTitle 	= 'Farm Africa - View Users';
}

$script    = array('user.js','farm.js','farmer.js');

require_once 'template.php';

?>